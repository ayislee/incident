CREATE TABLE `map_group_revenue_stream` (
`id`  bigint NOT NULL AUTO_INCREMENT ,
`group_id`  bigint NULL ,
`revenue_stream_id`  bigint NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`group_id`) REFERENCES `cms_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`revenue_stream_id`) REFERENCES `cms_revenue_stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
UNIQUE INDEX (`id`) USING BTREE ,
INDEX (`group_id`) USING BTREE ,
INDEX (`revenue_stream_id`) USING BTREE 
)
;