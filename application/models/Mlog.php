<?php 
class Mlog extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

    public function userlog($id,$activity,$id_module,$decription){
    	//echo $id.'<br>'.$activity.'<br>'.$id_module.'<br>'.$decription;
    	$sql = 'INSERT INTO cms_user_log (id_user,activity,id_module,description,log_time) VALUES (?,?,?,?,NOW())';
    	$this->db->query($sql,array($id,$activity,$id_module,$decription));
    }

    public function find_email($email){
    	$sql = 'SELECT * FROM cms_user WHERE email = ?';
    	$dt = $this->db->query($sql,array($email));
    	// echo $this->db->last_query();
    	// die;
    	if($dt->num_rows()>0){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function reset_pass($email,$flag){
    	require_once(APPPATH.'libraries/config.php');
    	$sql = 'SELECT
					cms_user.id,
					cms_user.email,
					reset_password.`code`,
					reset_password.reset_time,
					reset_password.expire_time
				FROM
					cms_user
				INNER JOIN reset_password ON cms_user.id = reset_password.user_id
				WHERE cms_user.email = ?
				ORDER BY reset_password.reset_time DESC LIMIT 1';

		$chk = $this->db->query($sql,array($email));
		if($chk->num_rows()>0){
			// is valid ?
			$row = $chk->row();
			//print_r(strtotime($row->reset_time));
			//echo '<br>';
			//print_r(strtotime('now'));
			if(strtotime($row->reset_time) < strtotime('now') && strtotime($row->expire_time) > strtotime('now')){
				$out['status'] = -2;
				if($flag==1){
					$out['desc'] = 'Already Sent';	
				}else{
					$out['desc'] = 'pwd_expire_already_sent';
				}
				
				$out['code'] = $row->code;
				$out['email'] = $row->email;
			}else{
				// Create
				$out = $this->create_code($email,$flag);
			}

		}else{
			$out = $this->create_code($email,$flag);	
		}

		return $out;

    }

    function create_code($email,$flag){
    	$time_reset = date('Y-m-d H:i:s');
		$date = date_create($time_reset);
		$expire_reset = date_add($date, date_interval_create_from_date_string(TIMES_EXPIRED));
		$code = md5(date('YmdHis').$email); 
		//echo md5($code).'<br>';
		//echo $time_reset.'<br>';
		//echo 'Ditambahkan 30 menit: '.date_format($date, 'Y-m-d H:i:s').'';
		// get user id
		$sql = 'SELECT * FROM cms_user WHERE email = ?';
		$dt = $this->db->query($sql,array($email));
		$row = $dt->row();
		$id = $row->id;
		$username = $row->user_name;

		$sql = 'INSERT INTO reset_password(user_id,code,reset_time,expire_time) VALUES (?,?,?,?)';

		$status = $this->db->query($sql,array($id,$code,$time_reset,date_format($date, 'Y-m-d H:i:s')));
		if($status){
			$desc = 'Request Reset Password username '. $username;
	        $activity = 'Request Reset Password';
	        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
	        $this->db->query($sql_log,array(null,$activity,$desc));

	        $out['status'] = 0;
	        if($flag==1){
	        	$out['desc'] = 'sent_link_success';	
	        }else{
	        	$out['desc'] = 'pwd_expire_sent_link_success';
	        }
	        
	        $out['code'] = $code;
	        $out['email'] = $email;
		}else {
			$out['status'] = -1;
	        $out['desc'] = 'fail_create_link';
	        $out['code'] = '';
	        $out['email'] = $email;
		}

		return $out;
    }

    public function check_expire_pwd($id){
    	$sql = 'SELECT * FROM cms_user WHERE pwd_expire < NOW() AND id=?';
    	$dt = $this->db->query($sql,array($id));
    	if($dt->num_rows()>0){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function check_code($code){
    	$sql = 'SELECT * FROM reset_password WHERE code=?';
    	$dt = $this->db->query($sql,array($code));
    	if($dt->num_rows()>0){
    		$row=$dt->row();
    		if(strtotime($row->reset_time) < strtotime('now') && strtotime($row->expire_time) > strtotime('now')){
    			$out['status'] = 0;
    			$out['desc'] = 'link_valid';
    		}else{
    			$out['status'] = -1;
    			$out['desc'] = 'link_invalid';
    		}
    	}else{
    		$out['status'] = -2;
    		$out['desc'] = 'error_code';
    	}

    	return $out;
    }

    public function get_email_from_code($code){
    	$sql = 'SELECT
					cms_user.id,
					cms_user.email,
					reset_password.`code`
				FROM
					cms_user
				INNER JOIN reset_password ON cms_user.id = reset_password.user_id
				WHERE reset_password.`code`=?';
		$dt = $this->db->query($sql,array($code));
		$row = $dt->row();
		return $row->email;
    }

    public function change_password($email,$Password,$code){
    	$sql = 'SELECT * FROM cms_user WHERE email=?';
    	$st = $this->db->query($sql,array($email));
    	if($st){
    		if($st->num_rows() > 0){
	    		$row = $st->row();
	    		$uid = $row->id;
	    		$username = $row->user_name;
	    		$pwd = KEY.$Password;
	    		$sql2 = 'UPDATE cms_authen_password SET password=md5(?) WHERE id= ?';
	    		$status = $this->db->query($sql2,array($pwd,$uid));
	    		if($status){
	    			// delete prev link
	    			$sql = 'DELETE FROM reset_password WHERE code=?';
	    			$this->db->query($sql,array($code));

	    			// echo $this->db->last_query();
	    			// create hlast resert
	    			$sql='UPDATE cms_user SET last_cpwd=NOW(), pwd_expire=TIMESTAMPADD(DAY,?,NOW()) WHERE email=?';
	    			$this->db->query($sql,array(EXPIRE_PWD,$email));
	    			
	    			$desc = 'Reset Password username '. $username;
			        $activity = 'Reset Password';
			        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
			        $this->db->query($sql_log,array($uid,$activity,$desc));

			        $out['status'] = 0;
			        $out['desc'] = 'success_change_password';
	    		}else{
	    			$out['status'] = -1;
			        $out['desc'] = 'change_password_failed';
	    		}
	    	}else{
	    		$out['status'] = -2;
			    $out['desc'] = 'user_not_found';
	    	}
    	}else{
    		$out['status'] = -3;
			$out['desc'] = 'back_end_error';
    	}
    	
    	return $out;
    }

}