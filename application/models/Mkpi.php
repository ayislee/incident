<?php 
class Mkpi extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

    public function kpi_array($year){
    	$sql = "SELECT * FROM cms_opco_list";
    	$dtOpco = $this->db->query($sql);

    	$RA = "Revenue Assurance";
    	$FM = "Fraud Management";
    	$kpi = array();
    	foreach ($dtOpco->result() as $opco) {
    		$kpi_opco = array();
    		$kpi_opco['opcoid'] = $opco->opcoid;
    		$kpi_opco['opconame'] = $opco->opconame;
    		$kpi_opco['year'] = $year;


    		$sql = "SELECT * FROM cms_risk_rut WHERE control=1";
    		$dtRUT = $this->db->query($sql);
    		$kpiRUT = array();
    		$order = 1;
    		foreach ($dtRUT->result() as $rut) {
    			$arut = array();
    			if($order==3){
    				$arut['order'] = $order;
	    			$arut['rutid'] = '';
	    			$arut['rutname'] = 'RAFM';
	    			$arut['reactive'] = 0;
	    			$arut['proactive'] = 0;
	    			$order++;
	    			array_push($kpiRUT,$arut);
    			}
    			
    			$arut['order'] = $order;
    			$arut['rutid'] = $rut->rutid;
    			$arut['rutname'] = $rut->rutname;
    			$arut['reactive'] = 0;
    			$arut['proactive'] = 0;

    			
    			array_push($kpiRUT,$arut);
    			$order++;

    		}

    		array_push($kpi,$kpi_opco);
    		array_push($kpi,array('rut'=>$kpiRUT));
    	}

    	return $kpi;
    }
}