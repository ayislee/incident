<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controlkpi extends CI_Controller {

    public function __construct() {
        parent::__construct();   
        require_once(APPPATH.'libraries/config.php');
        $this->session->set_userdata('loaded_module',USERADMIN_MODULE);
        if ($this->session->has_userdata('language')){

            
            $this->lang->load("message",$this->session->userdata('language'));
        }else{
            $this->session->set_userdata('language','english');
            $this->lang->load("message",$this->session->userdata('language'));
        }; 

        
        
        $this->load->model('mglobal');
        $this->load->model('mlog');

        $module_granted = $this->mglobal->group_to_module($this->session->userdata('group_id'),USERADMIN_MODULE);
        //echo $module_granted;
        //die;
        $module_status = $this->mglobal->module_status();
        if($module_status==0){

            $language['language'] =  $this->session->userdata('language');
            $data['language_switcher'] = $this->load->view('header\language',$language,true);
            $this->load->view('system/disablemodule',$data);
   
        }else{
            //header('Location: '.base_url().'index.php/admin/useradmin_start');
            // check login or not
            if(!($this->session->userdata('islogin') /*&& $this->session->userdata('module')==$this->session->userdata('loaded_module') */)){
                header('Location: '.base_url().'index.php/login/login');
            }else if($module_granted==0){
                header('Location: '.base_url().'index.php/login/login/user_not_allowed');
            }else if($this->mlog->check_expire_pwd($this->session->userdata('id_user'))){
                header('Location: '.base_url().'index.php/login/pwd_expire');
            }


        }
    }

    public function cckpi(){
        $this->load->model('madmin');
        $this->load->model('mkpi');

        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('control_coverage_kpi');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('control_coverage_kpi');
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $content['kpi'] = $this->mkpi->kpi_array(2017);
        echo json_encode($content['kpi']);
        //$data['content'] = $this->load->view('template/kpi_content',$content,TRUE);
        
        //$this->load->view('template/kpi',$data);
    }

}