<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incident extends CI_Controller {

    public function __construct() {
        parent::__construct();   
        require_once(APPPATH.'libraries/config.php');
         require_once(APPPATH.'libraries/bin/SSRSReport.php');
        ini_set('memory_limit', '-1');
        $this->session->set_userdata('loaded_module',INCIDENT_MODULE);

        $this->encryption->initialize(
                array(
                        'cipher' => 'aes-256',
                        'mode' => 'ctr'
                )
        );

        if ($this->session->has_userdata('language')){

            
            $this->lang->load("message",$this->session->userdata('language'));
        }else{
            $this->session->set_userdata('language','english');
            $this->lang->load("message",$this->session->userdata('language'));
        }; 

        $this->load->model('mglobal');
        $this->load->model('mlog');
        $module_granted = $this->mglobal->group_to_module($this->session->userdata('group_id'),INCIDENT_MODULE);
        //echo $module_granted;
        //die;
        $module_status = $this->mglobal->module_status();
        if($module_status==0){

            $language['language'] =  $this->session->userdata('language');
            $data['language_switcher'] = $this->load->view('header\language',$language,true);
            $this->load->view('system/disablemodule',$data);
   
        }else{
            //header('Location: '.base_url().'index.php/admin/useradmin_start');
            // check login or not
            if(!($this->session->userdata('islogin') /*&& $this->session->userdata('module')==$this->session->userdata('loaded_module')*/)){
                header('Location: '.base_url().'index.php/login/login');
                //echo 'tidak login';
            }else if($module_granted==0){
                header('Location: '.base_url().'index.php/login/login/user_not_allowed');
            }else if($this->mlog->check_expire_pwd($this->session->userdata('id_user'))){
                header('Location: '.base_url().'index.php/login/pwd_expire');
            }
        }
    }
 
    public function index()
    {
        //$this->session->sess_destroy();
        //print_r($this->session->all_userdata());
        //die;
        header('Location: '.base_url().'index.php/incident/incident_management');
    }

    public function incident_management($page=1){
        $this->load->model('mincident');
        
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Incident Management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Incident Management'),
                                                    'link'=> '#',
                                                    'active'=>''));
        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        
        $data['content'] = $this->mincident->get_by_opco($page,ROW_PER_PAGE);
        $this->load->view('incident/incident_management',$data);    
        

    }

    public function Page_not_found(){
        echo '404 Page not found';
    }

    public function incident_table($sorting=2,$status,$opcoid,$s='%25'){
        $this->load->model('mincident');
        $page = $this->input->get('page');
        $sort = $this->input->get('sort');
        $detection_tool = $this->input->get('dt');
        $rut = $this->input->get('rut');
        $st = $this->input->get('st');
        $rc = $this->input->get('rc');

        if($page=='') $page=1;
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Incident Management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        //back to original input
        $s=str_replace("tandasamadengan","=",$s);
        $s=str_replace("tandadan","&",$s);
        $s=str_replace("tandatanya","?",$s);
        $s=str_replace("tandaasterik","*",$s);
        $s=str_replace("tandaat","@",$s);
        $s=str_replace("tandaseru","!",$s);
        $s=str_replace("tandapagar","#",$s);
        $s=str_replace("tandakurungbuka","(",$s);
        $s=str_replace("tandakurungtutup",")",$s);
        $s=str_replace("tandaplus","+",$s);
        $s=str_replace("tandaminus","-",$s);
        $s=str_replace("tandapetikterbalik","`",$s);
        $s=str_replace("tandapetiksatu","'",$s);
        $s=str_replace("tandapetikdua",'"',$s);

        $s=str_replace("tandakurawalbuka","{",$s);
        $s=str_replace("tandakurawaltutup","}",$s);
        $s=str_replace("tandakurungbukakotak","[",$s);
        $s=str_replace("tandakurungtutupkotak","]",$s);
        $s=str_replace("tandatitikkoma",";",$s);
        $s=str_replace("tandatitikdua",":",$s);
        $s=str_replace("tandalebihkecil","<",$s);
        $s=str_replace("tandalebihbesar",">",$s);
        $s=str_replace("tandagarismiringkanan","/",$s);
        $s=str_replace("tandagarislurus","|",$s);
        

        
        
        /*
        st = st.split('=').join("tandasamadengan");
        st = st.split('&').join("tandadan");
        st = st.split('?').join("tandatanya");
        st = st.split('*').join("tandaasterik");
        st = st.split('@').join("tandaat");
        st = st.split('!').join("tandaseru");
        st = st.split('#').join("tandapagar");
        st = st.split('^').join("tandapangkat");
        st = st.split('(').join("tandakurungbuka");
        st = st.split(')').join("tandakurungtutup");
        st = st.split('+').join("tandaplus");
        st = st.split('-').join("tandaminus");
        st = st.split('`').join("tandapetikterbalik");
        st = st.split("'").join("tandapetiksatu");
        st = st.split('"').join("tandapetikdua");
        st = st.split('{').join("tandakurawalbuka");
        st = st.split('}').join("tandakurawaltutup");
        st = st.split('[').join("tandakurungbukakotak");
        st = st.split('[').join("tandakurungtutupkotak");
        st = st.split(';').join("tandatitikkoma");
        st = st.split(':').join("tandatitikdua");
        st = st.split('<').join("tandalebihkecil");
        st = st.split('>').join("tandalebihbesar");
        st = st.split('/').join("tandagarismiringkanan");
        st = st.split('|').join("tandagarislurus");
        
        */



        $opco = $this->mincident->get_opco($opcoid);
        if($opco['found']){
           $opconame = $opco['opconame']; 
        }else{
            $this->Page_not_found();
            die;
        }
        
        $s = urldecode($s);
        $status = urldecode($status);
        switch($status){
            case '0':
                $br_title = $this->lang->line('Open Incident');
                break;
            case '1':
                $br_title = $this->lang->line('Close Incident');
                break;

            case '%':
                break;
            default:
                $this->Page_not_found();
                die;
                break;
        }

        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Incident Management'),
                                                    'link'=> base_url().'index.php/incident/incident_management',
                                                    'active'=>'active'));
        array_push($breadcrumb['breadcrumb'], array('name'=>$opconame,
                                                    'link'=> '#',
                                                    'active'=>''));
        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        $data['content'] = $this->mincident->get_incident($opcoid,$status,$page,ROW_PER_PAGE,$s,$sorting,$detection_tool,$rut,$st,$rc);


        $this->load->view('incident/incident_table',$data);

    }

    public function new_incident($opcoid){
        $sql = 'SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=? AND create_incident=1';
        $dt=$this->db->query($sql,array($this->session->userdata('group_id'),$opcoid));
        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){

        }else{
            $this->Page_not_found();
            die;
        }
        $this->load->model('mincident');
        
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Incident Management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $opco = $this->mincident->get_opco($opcoid);
        if($opco['found']){
           $opconame = $opco['opconame']; 
        }else{
            $this->Page_not_found();
            die;
        }

        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Incident Management'),
                                                    'link'=> base_url().'index.php/incident/incident_management',
                                                    'active'=>'active'));
        array_push($breadcrumb['breadcrumb'], array('name'=>$opconame,
                                                    'link'=> base_url().'index.php/incident/incident_table/2/%25/'.$opcoid,
                                                    'active'=>''));
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('New Incident'),
                                                    'link'=> '#',
                                                    'active'=>''));

        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);

        $data['content'] = $this->mincident->form_incident($opcoid,'NEW','',base_url().'index.php/incident/incident_table/2/%25/'.$opcoid);
        
        $this->load->view('incident/incident_detail',$data);
    }

    public function get_st(){
        $rutid = $this->input->post('rutid');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            $out = $this->mincident->get_st($rutid);
            $rut = $this->mincident->get_rut($rutid);
            if($rut != null){
                echo json_encode(array('option'=>$out['option'],'type'=>$rut->type));    
            }else{
                echo json_encode(array('option'=>'<option value="-1">'.$this->lang->line('Please select').'</option>','type'=>0));  
            }
            
        }else{

        }
    }

    public function get_oc(){
        $stid = $this->input->post('stid');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            $out = $this->mincident->get_oc($stid);
            echo $out['option'];
        }else{

        }
    }    

    public function get_rc(){
        $ocid = $this->input->post('ocid');
        $stid = $this->input->post('stid');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            $out = $this->mincident->get_rc($ocid,$stid);
            echo $out['option'];
        }else{

        }
    }

    public function add_rc(){
        $incident_id = $this->input->post('incident_id');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            $out = $this->mincident->get_rc($ocid);
            echo $out['option'];
        }else{

        }
    }


    public function upload_attachment() {
        $status = "";
        $msg = "";
        $file_element_name = 'file-attachment';
        ini_set('display_errors', 0);
        error_reporting(0);
        if ($status != "error") {
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 1024;
            $config['encrypt_name'] = FALSE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                $filename = "";
                $filetype = "";
                $filepath = "";
                $fullpath = "";
            }else {
               $data = $this->upload->data();
               $image_path = $data['full_path'];
                if(file_exists($image_path)) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $filename = $data['file_name'];
                    $filetype = $data['file_type'];
                    $filepath = $data['file_path'];
                    $fullpath = $data['full_path'];

                }else {
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                    $filename = "";
                    $filetype = "";
                    $filepath = "";
                    $fullpath = "";

                }
            }
            @unlink($_FILES[$file_element_name]);
         }
         echo json_encode(array('status' => $status, 'msg' => $msg, 'filename'=> $filename,"filetype"=>$filetype,"filepath"=>$filepath,"fullpath"=>$fullpath));
         //print_r($data);
     }


    public function upload_bulk() {
        $status = "";
        $msg = "";
        $file_element_name = 'file-attachment';

        if ($status != "error") {
            $config['upload_path'] = 'csv/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = 1024 * 1024;
            $config['encrypt_name'] = FALSE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                $status = "error";
                //$msg = $this->lang->line('upload_error');
                $filename = "";
                $filetype = "";
                $filepath = "";
                $fullpath = "";
            }else {
               $data = $this->upload->data();
               $image_path = $data['full_path'];
                if(file_exists($image_path)) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $filename = $data['file_name'];
                    $filetype = $data['file_type'];
                    $filepath = $data['file_path'];
                    $fullpath = $data['full_path'];

                }else {
                    $status = "error";
                    $msg = $this->lang->line('upload_error');
                    $filename = "";
                    $filetype = "";
                    $filepath = "";
                    $fullpath = "";

                }
            }
            @unlink($_FILES[$file_element_name]);
         }else {

         }
         echo json_encode(array('status' => $status, 'msg' => $msg, 'filename'=> $filename,"filetype"=>$filetype,"filepath"=>$filepath,"fullpath"=>$fullpath));
         //print_r($data);
     }

    public function insert_incident(){

        $json = $this->input->post('json');
        $mode = $this->input->post('mode');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');

        if($sess == session_id()){
            
            switch ($mode) {
                case 'NEW':

                    $out =  $this->mincident->insert_incident($json,$mode,$this->session->userdata('id_user'));
                    echo json_encode(array('status' => $out['status'], 'msg' => $out['msg']));
                    break;
                
                case 'UPDATE':
                    $out =  $this->mincident->insert_incident($json,$mode,$this->session->userdata('id_user'));
                    echo json_encode(array('status' => $out['status'], 'msg' => $out['msg']));
                    break;
                    
            }
            
        }else{

        }
    }

    public function test_insert_incident(){
        //$json=$this->input->post('json');
        //$mode=$this->input->post('mode');
        $this->load->model('mincident');
         $out =  $this->mincident->insert_incident('{"incident_id":"200","external_id":"ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt","opcoid":"13","opconame":"Germany","updated_by":"32","incident_status":"0","incident_status_detail":"0","incident_detail_id":"","description":"tttdor","note":"tttdor","detection_tool":"9","rutid":"7","stid":"49","ocid":"39","revenue_stream":"7","impact_severity":"2","detection_date":"2016-10-26","case_start_date":"2016-10-26","case_end_date":"0000-00-00","c_final_leakage":"0.000000","c_final_leakage_lc":"0","recovery":"0.000000","recovery_lc":"0","leakage_freq":"2","leakage_timing":"2","leakage_id_type":"1","recovery_type":"2","list_rc":[{"incident_detail_id":"459","rcid":"52","rcname":"Invalid customer allowed to make call"}],"attachment":[],"prevented":"0.000000","prevented_lc":"0","over_charging":"0.000000","over_charging_lc":"0","on_net_terminated_minutes":"0","on_net_suspended_sims":"0","average_retail_on_net":"0.000000","average_retail_on_net_lc":"0","detection_cost":"0.000000","detection_cost_lc":"0","regulated_international_ic_price":"0.000000","regulated_international_ic_price_lc":"0","on_net_price_arbitrage":"0.000000","on_net_price_arbitrage_lc":"0","off_net_terminated_minutes":"0","off_net_suspensed_sims":"0","average_retail_off_net":"0.000000","average_retail_off_net_lc":"0","average_disconecting_duration":"0","off_net_price_arbitrage":"0.000000","off_net_price_arbitrage_lc":"0","recursive_frequency":"-1","incident_leakage":"0.000000","incident_leakage_lc":"0"}',
            'UPDATE',$this->session->userdata('id_user'));
         print_r($out);
    }

    public function test_date(){
        $this->load->model('mincident');
        echo $this->mincident->test_date('2015-11-01','2016-02-01');
    }


    public function incident_view($incident_id,$detail_id=-1){
        $this->load->model('mincident');
        
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Incident Management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        
        $opco = $this->mincident->get_opco_from_incident($incident_id);
        if($opco['found']){
           $opconame = $opco['opconame']; 
           $opcoid = $opco['opcoid'];
        }else{
            $this->Page_not_found();
            die;
        }
        


        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Incident Management'),
                                                    'link'=> base_url().'index.php/incident/incident_management',
                                                    'active'=>'active'));
        array_push($breadcrumb['breadcrumb'], array('name'=>$opconame,
                                                    'link'=> base_url().'index.php/incident/incident_table/2/%25/'.$opcoid,
                                                    'active'=>''));
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('View Incident'),
                                                    'link'=> '#',
                                                    'active'=>''));

        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        // public function form_incident($opcoid,$mode,$id,$cancel_operation_link)
        $data['content'] = $this->mincident->form_incident($opcoid,'VIEW',$incident_id,base_url().'index.php/incident/incident_table/2/%25/'.$opcoid,$detail_id);

        
        $this->load->view('incident/incident_detail',$data);
    }


    public function incident_update($incident_id,$detail_id,$opcoid){
        $sql = 'SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=? AND update_incident=1';
        $dt = $this->db->query($sql,array($this->session->userdata('group_id'),$opcoid));

        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
            
        }else{
            $this->Page_not_found();
            die;
        }

        if(!$this->session->userdata('get_privilege')[6]['map_group']){
            
        }
        $this->load->model('mincident');
        
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Incident Management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        
        $opco = $this->mincident->get_opco_from_incident($incident_id);
        if($opco['found']){
           $opconame = $opco['opconame']; 
           $opcoid = $opco['opcoid'];
        }else{
            $this->Page_not_found();
            die;
        }
        


        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Incident Management'),
                                                    'link'=> base_url().'index.php/incident/incident_management',
                                                    'active'=>'active'));
        array_push($breadcrumb['breadcrumb'], array('name'=>$opconame,
                                                    'link'=> base_url().'index.php/incident/incident_table/2/%25/'.$opcoid,
                                                    'active'=>''));
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Update Incident'),
                                                    'link'=> '#',
                                                    'active'=>''));

        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        // public function form_incident($opcoid,$mode,$id,$cancel_operation_link)
        $data['content'] = $this->mincident->form_incident($opcoid,'UPDATE',$incident_id,base_url().'index.php/incident/incident_table/2/%25/'.$opcoid,$detail_id);
        $this->load->view('incident/incident_detail',$data);
    }


    public function hapus(){
        $this->load->view('incident/hapus');
    }

    public function download($file_id,$session){
        $this->load->model('mincident');
        if($session == session_id()){
            $x="halo";
            $data = $this->mincident->get_file($file_id);
            $row = $data->row();
            //echo $session.$row->name;
            //$full_path = FCPATH.'exportto\/'.$session.$row->name;
            //$size = $this->save_temp($row->data,$full_path);
            

                header('Content-Description: File Transfer');
                header('Content-Type: '.$row->type);
                header('Content-Disposition: attachment; filename='.$row->name);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($row->data));
                ob_clean();
                echo $row->data;

            
        }
    }

    public function save_temp($data, $names) {
        return file_put_contents($names, $data);
    }    



    public function analyst_csv(){

        $filename = $this->input->post('filename');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');

        if($sess == session_id()){
            $csv_file = $filename;
            //$csv_file = 'D:\xampp\htdocs\incident\csv\test_bulk20fields.csv';
            $adata = array();
            $analyst_status = array();
            $lines = 1 ;
            $general_status = true;
            $general_msg = '';

            $file = fopen($csv_file,"r");
            while(! feof($file)) {
                $line_array = fgetcsv($file);
                if(count($line_array) != 20){
                    $status = false;
                    $general_status = false;
                    $msg = str_replace('%l', $lines, $this->lang->line('line_error'));
                    $msg = str_replace('%f', count($line_array), $msg);
                    $general_msg .=$msg.PHP_EOL;
                    array_push($adata,array('status lines'=>$status,'msg'=>$msg,'data'=>array()));
                }else{
                    $status = true;
                    $msg = '';
                    $la = $this->convert_line_array($line_array,$this->session->userdata('user_id'));
                    if(!$la['status']){
                        $general_status = false;
                        $general_msg .= $this->lang->line('Invalid field value').' '.$lines.' ('.$la['msg'].')'.PHP_EOL;
                    }

                    array_push($adata,array('status lines'=>$status,'msg'=>$msg,'data'=>array('status record'=>$la['status'],'msg'=>$la['msg'],'data'=>$la['data'])));
                }
                

                
                $lines++;
            }
            

            fclose($file);
           
            $out = array('general status'=>$general_status,'data'=>$adata);
            //echo json_encode($out);
            
            
            if($general_status){
                
                foreach ($out['data'] as $row) {
                    # code...
                    //print_r($row['data']['data']);
                    //echo '<br>';
                    $push_data = json_encode($row['data']['data']);
                   // print_r($push_data);
                    $dt = $this->mincident->insert_incident($push_data,'NEW',$this->session->userdata('user_id'));
                }
                 
                $msg_out = array('status'=>true,'msg'=>$this->lang->line('insert bulk file complete'));
            }else {
                //$msg_out = array('status'=>false,'msg'=>$this->lang->line('Invalid Format csv file'));
                $msg_out = array('status'=>false,'msg'=>$general_msg);
            }
            
        }else{
             $msg_out = array('status'=>false,'msg'=>$this->lang->line('Invalid command'));
        }

        echo json_encode($msg_out);

    }

    public function convert_line_array($fields,$user_id){
        $this->load->model('mincident');
        $status = true;
        $external_id = $fields[0];
        $error_field = '';
        $opco = $this->mincident->find_opco($fields[1]);
        if(!$opco['status']) { $status = false; $error_field .= 'opco'; }
        $detection_tool = $this->mincident->find_detection_tool($fields[2]);
        $rut = $this->mincident->find_rut($fields[3]);
        if($rut['status']){
            $st = $this->mincident->find_st($fields[4],$rut['rutid']);
            if($st['status']){
                $oc = $this->mincident->find_oc($fields[5],$st['stid']);
                if($oc['status']){
                    $rc = $this->mincident->find_rc($fields[6],$oc['ocid'],$st['stid']);
                }else { $status = false; if(trim($error_field)!='') $error_field .= ','.'oc'; else $error_field .= 'oc';}
            } else { $status = false; if(trim($error_field)!='') $error_field .= ','. 'st'; else  $error_field .= 'st';}
        }else { $status = false; if(trim($error_field)!='') $error_field .= ','. 'rut'; else  $error_field .= 'rut';}
        $impact_severity = $fields[7];
        $detection_date = $this->number__to_date($fields[9]);
        if(!$detection_date['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'detection_date'; else $error_field .= 'detection_date';}  
        $case_start_date = $this->number__to_date($fields[10]);
        if(!$case_start_date['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'case_start_date'; else  $error_field .= 'case_start_date';}
        $case_end_date = $this->number__to_date($fields[11]);
        if(!$case_end_date['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'case_end_date'; else  $error_field .= 'case_end_date';}
        $leakage_one_day = $fields[12];
        $recovery = $fields[13];
        $leakage_freq = $this->mincident->find_leakage_freq($fields[14]);
        if(!$leakage_freq['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'leakage_freq'; else  $error_field .= 'leakage_freq';}
        $leakage_timing = $this->mincident->find_leakage_timing($fields[15]);
        if(!$leakage_timing['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'leakage_timing';  else $error_field .= 'leakage_timing';}
        $leakage_id_type = $this->mincident->find_leakage_id_type($fields[16]);
        if(!$leakage_id_type['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'leakage_id_type'; else  $error_field .= 'leakage_id_type';}
        $recovery_type = $this->mincident->find_recovery_type($fields[17]);
        if(!$recovery_type['status']) { $status = false; if(trim($error_field)!='') $error_field .= ','. 'recovery_type'; else  $error_field .= 'recovery_type';}
        $description = $fields[18];
        $note = $fields[19];

        
        if($status){
            $rc_push = array('incident_detail_id'=>null,'rcid'=>$rc['rcid'],'rcname'=>$fields[6]);
            $fa = array(
                'incident_id'=>null,
                'external_id'=>$external_id,
                'opcoid'=>$opco['opcoid'],
                'opconame'=>$fields[1],
                'updated_by'=>$user_id,
                'incident_status'=>'0',
                'incident_status_detail'=>'0',
                'incident_detail_id'=>null,
                'description'=>$description,
                'note'=>$note,
                'detection_tool'=>$detection_tool['dtid'],
                'rutid'=>$rut['rutid'],
                'stid'=>$st['stid'],
                'ocid'=>$oc['ocid'],
                'impact_severity'=>(int)$impact_severity,
                'detection_date'=>$detection_date['date'],
                'case_start_date'=>$case_start_date['date'],
                'case_end_date'=>$case_end_date['date'],
                'leakage_one_day'=>(int)$leakage_one_day,
                'recovery'=>(int)$recovery,
                'leakage_freq'=>$leakage_freq['id'],
                'leakage_timing'=>$leakage_timing['id'],
                'leakage_id_type'=>$leakage_id_type['id'],
                'recovery_type'=>$recovery_type['id'],
                'list_rc'=>array($rc_push),
                'attachment'=>array());
            $out = array('status'=>$status,'msg'=>'','data'=>$fa);
        }else $out = array('status'=>$status,'msg'=>$error_field,'data'=>array());
        return $out;
    }

    function number__to_date($number){
        $num = strlen($number);
        switch ($num) {
            case 7:
                # code...
                $year = substr($number, 3);
                $month = substr($number, 1,2);
                $day = substr($number, 0,1);
                $date_str = $month.'/'.$day.'/'.$year;
                $status = true;
                break;
            case 8:
                $year = substr($number, 4);
                $month = substr($number, 2,2);
                $day = substr($number, 0,2);
                $date_str = $month.'/'.$day.'/'.$year;
                $status = true;
                break;
            
            default:
                $status = false;
                break;
        }
        if($status){
            $date = strtotime($date_str);
            $newformat = date('Y-m-d',$date);
            $out = array('status'=>$status,'date'=>$newformat);
        }else{
            $out = array('status'=>$status,'date'=>null);
        }
        return $out;

    }

    public function tes(){
        $time = strtotime('10/16/2003');
        $newformat = date('Y-m-d',$time);
        echo $newformat;
    }


    public function test(){
        $this->load->model('mincident');
        echo $this->mincident->insert_incident('','NEW',$this->session->userdata('username'));


        
    }

    public function calculation(){
        $format = 'Y-m-d';
        $date1 = '2016-12-30';
        $date2 = '2017-01-1';

        $selisih = ((abs(strtotime ($date1) - strtotime ($date2)))/(60*60*24));
        echo $selisih;
    }

    public function test_calculation(){
        $this->load->model('mincident');
        $hasil = $this->mincident->calculation(0,5,'2016-10-6','2016-10-20',1,1,1,'2016-11-27',0,1,10);
        print_r($hasil);


    }

    


    // Test section

    public function test_get_param(){
        //$param = $this->ssrs_get_params(UID,PWD,SERVICE_URL,'/VIP_Dashboard/VimpelCom Incident Cases');
        //print_r($this->ssrs_valid_params($param));
        $ssrs_report = new SSRSReport(new Credentials(UID, PWD), SERVICE_URL);
        $reportParameters = $ssrs_report ->GetReportParameters('/VIP_Dashboard/VimpelCom Incident Cases', null, true, null,null);
        //print_r(json_encode($reportParameters));


    }

    public function test_file(){
        echo file_get_contents("uploads\adm_provinsi.xls");
    }

    public function test_insert(){
        $this->load->model('mincident');
        ini_set('memory_limit', '-1');
        $dt = $this->mincident->insert_incident('{"incident_id":"147","external_id":"222","opcoid":"1","opconame":"Indonesia","updated_by":"","incident_status":"0","incident_status_detail":"0","incident_detail_id":"","description":"222","note":"222","detection_tool":"1","rutid":"1","stid":"1","ocid":"1","impact_severity":"1","risk_date":"2016-07-14","detection_date":"2016-07-14","case_start_date":"2016-07-14","case_end_date":"0000-00-00","leakage_one_day":"222.00","recovery":"222.00","leakage_freq":"1","leakage_timing":"1","leakage_id_type":"1","recovery_type":"1","list_rc":[],"attachment":[]}','UPDATE',1);

        echo json_encode(array('status' => $dt['status'], 'msg' => $dt['msg']));
    }

    public function test_seq(){
        $this->load->model('mincident');
        echo $this->mincident->app_incidentid_sequence('Indonesia');
    }

    public function priv(){
        print_r($this->session->userdata('get_privilege'));
    }

    public function ssrs_get_params($uid,$paswd,$service_url,$report,$params_url){
        try {
            $ssrs_report = new SSRSReport(new Credentials($uid, $paswd), $service_url);

            //print_r(json_encode($params_url));
            
            //die;
            
    
            //$params_url = $p;
            // print_r($params_url);
            // die;
            if(sizeof($params_url)>0){
                 $i = 0;

                 //var_dump($params_url->parameters);
                 //die;
                 foreach ($params_url['parameters'] as $par) {
                    # code...
                    
                    //echo $par['name'];
                    foreach ($par['value'] as $value) {
                        $parameters[$i] = new ParameterValue();
                        $parameters[$i]->Name = $par['name'];
                        $parameters[$i]->Value = $value;
                        # code...
                        // echo 'parameters['.$i.']= new ParameterValue()<br>';
                        // echo 'parameters['.$i.']->Name = '.$par['name'].'<br>';
                        // echo 'parameters['.$i.']->Value = '.$value.'<br>';
                        // //echo $value;
                        // echo '-----<br>';
                        $i++;
                    }
                    
                }    
            }else{
                $parameters = null;
            }
            
            //die;
            // print_r($parameters);
            // die;
            $reportParameters = $ssrs_report ->GetReportParameters($report, null, true, $parameters,     null);
            // echo json_encode($reportParameters);
            // die;

            $parameters = array();
            foreach($reportParameters as $reportParameter) {
                $parameters[] = array(
                                        "Name" => $reportParameter->Name,
                                        "ValidValues" => $reportParameter->ValidValues,
                                        "PromptUser"=>$reportParameter->PromptUser,
                                        "MultiValue"=>$reportParameter->MultiValue,
                                        "DefaultValues"=>$reportParameter->DefaultValues,
                                        "Type"=>$reportParameter->Type
                                      );     
                                      // echo $reportParameter->Type;  
                                      // echo "<br>";
            }
            // die;

        }

        catch(SSRSReportException $serviceException)
        {
            $out = array('status'=>-1,'msg' =>$serviceException->GetErrorMessage(),'parameters'=>array());
            return $out;
            exit();
        }
        $out = array('status'=>0,'msg' =>'','parameters'=>$parameters);
        // $out = array('status'=>0,'msg' =>'','parameters'=>$reportParameters);
        return $out;
    }

    public function ssrs_valid_params($params){
        if($params['status']==0){
            $num = 0;
            $new_params = array();
            foreach ($params['parameters'] as $param) {
                # code...
                //echo $param['Name'].'<br>';
                //print_r($param['ValidValues']); echo '<br>-------<br>';
                if($param['PromptUser']==1) array_push($new_params, $param);
            }
            //echo '<br>'.$num;
            $out=array('status'=>$params['status'],'msg'=>$params['msg'],'params'=>$new_params);
            return $out;
        }else{
            $out=array('status'=>$params['status'],'msg'=>$params['msg'],'params'=>$params['parameters']);
        }
    }




    public function ssrs_change_param(){

    }

    public function no_report(){
        $data['sidemenu'] = $this->load->view('incident/sidemenu_report','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Report');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Report'),
                                                    'link'=> '#',
                                                    'active'=>''));
        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        $this->load->view('incident/no_report',$data); 
    }

    public function report2($id=-1){
        $this->load->model('mincident');


        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Report');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Report'),
                                                    'link'=> '#',
                                                    'active'=>''));
        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        
        if($id==-1){
            $id = $this->mincident->get_first_report($id);
        }

        $privilege_report = $this->mincident->privilege_report($id);
        if($this->session->userdata('group_id')!=ADMIN_GROUP_ID && $privilege_report->num_rows()==0){
            header('Location: '.base_url().'index.php/incident/access_denied');
            die;
        }

        $select_report['select_report'] = $this->mincident->select_report();
        if($select_report['select_report']->num_rows()==0){
            header('Location: '.base_url().'index.php/incident/no_report');
        }

        $select_report['id'] = $id;
        $report_selected = $this->mincident->get_report_profile($id);
        $select_report['report'] = $report_selected['report'];



        $data['select_report'] = $this->load->view('incident/select_report.php',$select_report,TRUE);
        $content['id'] = $id;
        $data['content'] = $this->load->view('incident/report_content',$content,TRUE);

        
        

        
        $this->load->view('incident/report',$data);


    }

    public function report_credential(){
        $sid = $this->input->post('sid');
        $report = $this->input->post('report');

        if($sid  == session_id()){
           $pUrl = parse_url(SERVICE_URL);
           //$url = $pUrl['scheme'].'://'.UID.':'.PWD.'@'.$pUrl['host'].$pUrl['path']."Pages/ReportViewer.aspx?".urlencode($report)."&rs:Command=Render";
           $url = SERVICE_URL."Pages/ReportViewer.aspx?".urlencode($report)."&rs:Command=Render";

           echo $url;
        }
    }

    public function parseUrl(){
        var_dump(parse_url(SERVICE_URL));
    }

    public function report($id=-1){
        $this->load->model('mincident');


        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Report');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $breadcrumb['breadcrumb'] = array();
        array_push($breadcrumb['breadcrumb'], array('name'=>$this->lang->line('Report'),
                                                    'link'=> '#',
                                                    'active'=>''));
        $data['breadcrumb'] = $this->load->view('incident/breadcrumb',$breadcrumb,TRUE);
        
        if($id==-1){
            $id = $this->mincident->get_first_report($id);
        }

        
        // case for request url
        $reportpath_found = false;
        

        $params_url = $this->get_params_url();
        //echo json_encode($params_url);
        //die;

        //print_r($params_url);
        //die;

        if(count($params_url)>0){
            //echo "ada";
            foreach ($params_url as $key => $value) {
                //echo $key.'<br>';
                if($key=='reportpath') $reportpath_found=true;
            }            
            
            if($reportpath_found){
                $params_url = $this->convert_params_url($params_url);
                //print_r($params_url);
                $id = $this->mincident->get_report_id($params_url['reportpath'],$id);
                //echo "g".$id;
            }


            //die;
        }
        if(isset($_REQUEST['cmd:Norender'])){
            $content['Norender'] = $_REQUEST['cmd:Norender'];

        }else{
            $content['Norender'] = 0;
        }
        //echo $id;
        //die;

        $select_report['select_report'] = $this->mincident->select_report();

        $privilege_report = $this->mincident->privilege_report($id);
        if($this->session->userdata('group_id')!=ADMIN_GROUP_ID && $privilege_report->num_rows()==0){
            header('Location: '.base_url().'index.php/incident/access_denied');
            die;
        }

        if($select_report['select_report']->num_rows()==0){
            header('Location: '.base_url().'index.php/incident/no_report');
        }

        $select_report['id'] = $id;

        $data['select_report'] = $this->load->view('incident/select_report.php',$select_report,TRUE);

        $report_selected = $this->mincident->get_report_profile($id);
        // --- np here

        require_once(APPPATH.'libraries/report.php');
        $params = $this->ssrs_get_params(UID,PWD,SERVICE_URL,$report_selected['report'],$params_url);
        //print_r($report_selected);
        //exit;
        //print_r(json_encode($params));
        //exit;
        $content['id'] = $id;
        $content['parameters'] = $this->ssrs_valid_params($params);
        //$content['parameters'] = $params;
        $content['report'] = $report_selected;
        //print_r($report_selected);
        //print_r(json_encode($content['parameters']));
        //die;
        $content['report_url'] = $reportpath_found;
        $content['params_url'] = $params_url;
        $content['json_params_url'] = json_encode($params_url);
        $content['noreload'] = '1';
        $data['content'] = $this->load->view('incident/report_content',$content,TRUE);

        $this->load->view('incident/report',$data);        
    }

    public function get_sysdate(){
        echo date("Y-m-d");
    }

    function get_params_url(){
        //print_r($_SERVER["QUERY_STRING"]);
        //die();
        $json2 = $this->input->post('json2',true);
        //echo $json2;

        
        //die();

        if($json2 !== null){
            $qs = preg_replace("/(?<=^|&)(\w+)(?==)/", "$1[]", $json2);
            parse_str($qs, $new_GET);
            // print_r($json2);
            // die();
            //print_r(json_encode($new_GET));
            //die;
        }else{
            $qs = preg_replace("/(?<=^|&)(\w+)(?==)/", "$1[]", $_SERVER["QUERY_STRING"]);
            parse_str($qs, $new_GET);
            // print_r($new_GET);
            // die;
               
        }
        return $new_GET; 
        
    }

    function convert_params_url($p){
        $a = array();
        $path = '';
        // var_dump($p);
        // die;
        foreach ($p as $key => $value) {
            # code...

            if($key != 'reportpath' && $key != 'rs:Command' && $key != 'cmd:Norender'){
                if($value=='') $value=array();

                
                $i = 0;
                foreach ($value as $v) {
                    # code...
                   $value[$i] = $a_url =str_replace("#####","&",$value[$i]); 
                   // echo $value[$i].'<br>';
                   $i++;
                }
                array_push($a, array('name'=>$key,'value' => $value));    
            }
            
            if($key == 'reportpath'){
                $path = $value;
            }
        }
        
        $a_url = array('reportpath'=>$path[0],'parameters'=>$a);
        // $a_url =str_replace("#####","&",$a_url,$i);
        // var_dump($a_url);
        // die;
        return $a_url;
    }

    // encrypt_params -> json_params encrypt md5

    public function get_report($id){
        $parameters = $this->input->post('parameters');
        $report = $this->input->post('report');
        $page = $this->input->post('page');
        // $id = $this->input->post('id');
        $sess = $this->input->post('sess');

        $this->load->model('mincident');
        
        $rpt = $this->mincident->get_report_profile($id);
        if($report=='') $report=$rpt['report'];
        
        if($parameters != ''){
            $this->session->set_userdata(array('last_params'=>$parameters));
        }else{
            $parameters = $this->session->userdata('last_params');
        }

        // echo $parameters;
        // die; 

        //$qs = preg_replace("/(?<=^|&)(\w+)(?==)/", "$1[]", $_SERVER["QUERY_STRING"]);
        //parse_str($qs, $new_GET);

        //print_r($new_GET);

        //die;
        //if($sess == session_id()){
            //echo $report;
            // echo $parameters;
            if($page==null)$page=1;
            $out = $this->mincident->get_report($report,$parameters,$page);
            $j = json_encode($out);
            if($out['sort']==1){
                // print_r($out['params']);
                // die;
                $this->session->set_userdata(array('html_result'=>$out['html_result']));
                // die;
                $ps='?reportpath='.$report.'&cmd:Norender=1';
                $i = 1;
                foreach ($out['params'] as $params) {
                    # code...
                    if($i==1){
                        $ps .= '&'.$params->Name.'='.$params->Value;
                    }else{
                        $ps .= '&'.$params->Name.'='.$params->Value;
                    }
                    $i++;

                }
                header("Location: ".base_url().'index.php/incident/report/'.$id.$ps);
                //echo $ps;
            }else{
                echo $j;    
            }
            
        //}else{

        //}        
    }

    public function test_report(){
        $this->load->model('mincident');
        echo '<html><body><br/><br/>';
        echo '<div align="center">';
        echo '<div style="overflow:auto; width:700px; height:600px">';
        $a = $this->mincident->get_report('/VIP_Dashboard/2.1 VimpelCom GRAFM General Dashboard','[{"Name":"Year","Value":"2016"},{"Name":"Month","Value":"10"}]');
        echo $a['html_result'];
        echo '</div>';
        echo '</div>';
        echo '</body></html>';

        
    }

    public function export_report(){
        $parameters = $this->input->post('parameters');
        $export_to = $this->input->post('export_to');
        $filename = $this->input->post('filename');
        $report = $this->input->post('report');

        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            //echo $report;
            //echo $parameters;
            $out = $this->mincident->export_report($filename,$report,$parameters,$export_to);
            $j = json_encode($out);
            echo $j;
        }else{

        }                
    }

    public function download_delete(){
        $file = $this->input->get('file');
        if ($file && file_exists($file)) 
        {
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename='.basename($file));
          header('Content-Transfer-Encoding: binary');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file));
          ob_clean();
          flush();
          if (readfile($file)) 
          {
            unlink($file);
          }
        }

    }

    


    public function test_export(){
        $this->load->model('mincident');
        $out = $this->mincident->export_report('abc','/VIP_Dashboard/2.3 VimpelCom GRAFM OpCo Summary Dashboard','[{"Name":"Year","Value":"2016"},{"Name":"OpCo","Value":"24"},{"Name":"OpCo","Value":"17"},{"Name":"OpCo","Value":"27"},{"Name":"OpCo","Value":"14"},{"Name":"OpCo","Value":"19"},{"Name":"OpCo","Value":"13"},{"Name":"OpCo","Value":"31"},{"Name":"OpCo","Value":"23"},{"Name":"OpCo","Value":"18"},{"Name":"OpCo","Value":"20"},{"Name":"OpCo","Value":"26"},{"Name":"OpCo","Value":"16"},{"Name":"OpCo","Value":"21"},{"Name":"OpCo","Value":"15"},{"Name":"OpCo","Value":"25"},{"Name":"OpCo","Value":"22"},{"Name":"OpCo","Value":"28"}]','PDF');
        print_r($out);
    }

    public function faq($page=1){
        $this->load->model('madmin');
        $dt = $this->madmin->getuser($page,ROW_PER_PAGE);
        //print_r($dt);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_faq');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $data['faqcontent'] = $this->load->view('incident/faqcontent',$header,TRUE);

        $this->load->view('incident/faq',$data);
    }   

    public function get_rate(){
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $currency_id = $this->input->post('currency_id');

        $sql = 'SELECT * FROM cms_currency_exchange
                    WHERE cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? AND currency_id=?';
        $dt_rate = $this->db->query($sql,array($year,$month,$currency_id));
        //echo $this->db->last_query();
        //die;
        if($dt_rate->num_rows()>0){
            $row = $dt_rate->row();
            $out = $row->rate;
        }else{
            $out = 0;
        }

        //echo $this->db->last_query();
        echo $out;
    }

    public function access_denied($page=1){
        $this->load->model('madmin');
        $dt = $this->madmin->getuser($page,ROW_PER_PAGE);
        //print_r($dt);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('access_denied');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $this->load->view('incident/access_denied',$data);
    }   

    public function opco_config_table($opco=-1){

        if($this->session->userdata('get_privilege')[12]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/incident/access_denied');
        }

        $this->load->model('mincident');

        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $content_title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['config_list'] = $this->mincident->get_opco_config_table($opco);
        
        //$data['ui_content'] = $this->load->view('template/opco_config_table',$menu,TRUE);

        


        $this->load->view('incident/opco_config_table',$data);        
    }


    public function opco_config($opco_id=1,$month='00',$year='0000'){

        if($this->session->userdata('get_privilege')[12]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/incident/access_denied');
        }

        if($month=='00'){
            $month==$this->get_current_month();
        }
        if($year=='0000'){
            $year=$this->get_current_year();
        }

        $mode = $this->input->get('mode');

        $this->load->model('mincident');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('admin_menu_opco_configuration');
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['content'] = $this->mincident->opco_config($opco_id,$month,$year,$mode);
        $this->load->view('incident/opco_config',$data);           
    }


    public function update_all_month(){
        $group_id = $this->input->post('group_id');
        $opco_id   = $this->input->post('opco_id');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        $sql='  SELECT
                    map_group_opco.group_id,
                    map_group_opco.opco_id,
                    map_group_opco.update_all_month
                FROM
                    map_group_opco
                WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_all_month=1';
        $dt = $this->db->query($sql,array($group_id,$opco_id));
        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
            echo '1';
        }else{
            $c_month = (int) date('m');
            $c_year = (int) date('Y');

            if(($c_month==$month && $c_year==$year)){
                echo '1';
            }else{
                echo '0';
                //echo $c_year.' '.$c_month.' --- '.$month.' '.$year;
            }

        }
    }

    public function get_add_all_month(){
        $group_id = $this->input->post('group_id');
        $opco_id   = $this->input->post('opco_id');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        $sql='  SELECT
                    map_group_opco.group_id,
                    map_group_opco.opco_id,
                    map_group_opco.add_all_month
                FROM
                    map_group_opco
                WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_all_month=1';
        $dt = $this->db->query($sql,array($group_id,$opco_id));
        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
            echo '1';
        }else{
            echo '0';
        }
    }    

    public function get_rate2(){

        $this->load->model('mincident');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $opcoid = $this->input->post('opcoid');
        $sess = $this->input->post('sess');
        
        $out = $this->mincident->get_rate($month,$year,$opcoid);
        echo json_encode($out);
        //echo $year;
    }    



    public function check_opco_config(){
        $opcoid =$this->input->post('opcoid');       
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');
        if($sess == session_id()){
            echo $this->mincident->check_opco_config($opcoid,$month,$year);
        }else{
            echo '2';
        }
    }  

    public function add_opco_config(){
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        $this->load->model('mincident');    
        if($sess == session_id()){
            echo $this->mincident->add_opco_config($json,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
    }

    public function test_add_opco_config(){
        $this->load->model('mincident');    
            echo $this->mincident->add_opco_config('',$this->session->userdata('id_user'));
    }


    public function update_opco_config(){
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        //echo $json;
        
        $this->load->model('mincident');    
        if($sess == session_id()){
            echo $this->mincident->update_opco_config($json,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
        
    }

    public function personal_setting(){
        $this->load->model('mincident'); 
        $data['sidemenu'] = $this->load->view('incident/sidemenu','',TRUE);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('incident/header',$header,TRUE);
        $title['title'] = $this->lang->line('Personal Setting');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        
        $data['content'] = $this->mincident->personal_setting();
        $this->load->view('incident/personal_setting',$data);
    }

    public function test_update_opco_config(){
        $this->load->model('mincident');
        echo $this->mincident->update_opco_config('[{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_number_of_subscribers_prepaid","value":"22"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_number_of_subscribers_postpaid","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_gross_adds_prepaid","value":"22"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_gross_adds_postpaid","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_churn_prepaid","value":"22"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_churn_postpaid","value":"2"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_no_of_mo_voice_minutes_prepaid","value":"22"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_no_of_mo_voice_minutes_postpaid","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_total_mt_international_minutes","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_total_mo_international_minutes","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_deferred_revenue_balance_in_system","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_deferred_revenue_balance_gl_system","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_third_parties","value":"1221"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_third_parties_lc","value":"1110"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_postpaid_defaulters","value":"12"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_postpaid_defaulters_lc","value":"11"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_prepaid_customers","value":"1221"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_prepaid_customers_lc","value":"1110"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_local_interconnect_partners","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_local_interconnect_partners_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_international_interconnect_partners","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_international_interconnect_partners_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_roaming_partners","value":"121"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_roaming_partners_lc","value":"110"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_mfs","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"bad_debts_mfs_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"total_operating_revenue_sales_of_equipment_and_accessories","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"total_operating_revenue_sales_of_equipment_and_accessories_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"total_operating_revenue_other_revenue_and_operating_income","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"total_operating_revenue_other_revenue_and_operating_income_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_standard_voice_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_standard_voice_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_national_interconnection_revenue_mobile","value":"11"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_national_interconnection_revenue_mobile_lc","value":"10"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_international_interconnection_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_international_interconnection_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_data_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_data_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_mfs_revenue_mobile","value":"11"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_mfs_revenue_mobile_lc","value":"10"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_guest_roaming_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_guest_roaming_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_messaging_revenue_mobile","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_messaging_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_value_added_services_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_value_added_services_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_connection_fees_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_connection_fees_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_other_service_revenue_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_other_service_revenue_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_fixed_line_revenue","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"service_revenue_fixed_line_revenue_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_roaming_wholesale_cost_mobile","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_roaming_wholesale_cost_mobile_lc","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_national_interconnection_wholesale_cost_mobile","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_national_interconnection_wholesale_cost_mobile_lc","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"control_coverage_fm","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"control_coverage_ra","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"control_coverage_mfs","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"control_coverage_bypass","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"currency_currency_symbol","value":"0"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"currency_rate","value":"1.100000"},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_deferred_revenue_balance_in_system_lc","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"general_deferred_revenue_balance_gl_system_lc","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_international_interconnection_wholesale_cost_mobile","value":""},{"opcoid":"24","config_date":"2016-02-01 00:00:00","variable":"roaming_and_interconnect_cost_international_interconnection_wholesale_cost_mobile_lc","value":""}]',$this->session->userdata('id_user'));
    }

    public function check_ldap(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $s         = $this->input->post('s');
        if($s==session_id()){
            if($this->madmin->check_ldap($id)){
                // ldp user
                echo 1;
            }else{
                // db user
                echo 0;
            }
        }

    }

    
    public function change_pass2(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $p0 = $this->input->post('p0');
        $p1 = $this->input->post('p1');
        $p2 = $this->input->post('p2');
        $s  = $this->input->post('s');
        if($s == session_id()){
            if($p1 == $p2){
                $regex_text = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/m';
                $a = preg_match($regex_text, $p1);
                if($a){
                    echo $this->madmin->ch_pass2($id,$p1,$p0,$this->session->userdata('id_user'));
                }else{
                    echo $this->lang->line('pass_rule');
                }
                
            }else{
                echo $this->lang->line('Password you entered is not the same');
            }
        }
    }

    public function delete_incident(){
        $usr = $this->input->post('usr');
        $pwd = $this->input->post('pwd');
        $iid = $this->input->post('iid');
        $sid = $this->input->post('sid');
        
        $this->load->model('mincident');

        if($sid == session_id()){
            $out = $this->mincident->delete_incident($usr,$pwd,$iid);
            echo json_encode($out);
        }else{
            $out['status']=3;
            $out['desc'] = $this->lang->line('Invalid command');
            echo json_encode($out);
        }

    }

    public function test_request(){
            if(!key_exists("params", $_REQUEST))
    {
        return;
    }
    
    //Check for Drill down, means user navigate to a new report so the params
    //are not valid. We can assume user moved to new page if ps:OrginalUri
    //is set and no sort or toggle flags are on.
    if(key_exists("ps:OrginalUri", $_REQUEST) &&
       !key_exists("rs:Command", $_REQUEST) &&
       !key_exists("rs:ShowHideToggle", $_REQUEST))  
    {
        unset($_REQUEST['params']);
        $settings = parse_ini_file("app.config", 1);
        $length = strlen($settings["SERVICE_URL"]);         
        $query = substr($_REQUEST['ps:OrginalUri'], $length + 2); //adding for ?
        $_REQUEST['reportName'] = $query;       
    }

    $parameters = array();
    $params = explode('$$', $_REQUEST['params']);
    foreach($params as $param)
    {
        $keyval = explode('=', $param);
        if(count($keyval) == 2)
        {
                $_REQUEST[$keyval[0]] = $keyval[1];                 
        }
    }
    unset($_REQUEST['params']);

    }

    public  function safe_b64encode($string) {
    
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public  function encode($value){ 
        
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, KEY, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }

    public  function encode_public(){ 
        $value = $this->input->post('parameters');
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, KEY, $text, MCRYPT_MODE_ECB, $iv);
        echo trim($this->safe_b64encode($crypttext)); 
    }

    public function decode($value){
        
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, KEY, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function test_encrypt(){

        $plain_text = 'This is a plain-text message!';
        $ciphertext = urldecode($this->encode($plain_text));
        echo $ciphertext.'<br>';
        // Outputs: This is a plain-text message!
        echo $this->decode($ciphertext);

    }

    public function rootpath(){
        echo FCPATH;   
    }

    public function test_curl(){
        $process = curl_init($host);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', $additionalHeaders));
        curl_setopt($process, CURLOPT_HEADER, 1);
        curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $payloadName);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
        curl_close($process);
    }

    public function opensession(){
        $username=UID;
        $password=PWD;
        $URL=SERVICE_URL;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $result=curl_exec ($ch);
        echo $result;
        //$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        $redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );
        //echo $redirectURL;
        //header('Location: '.$redirectURL);
    }

    public function multiselect(){
        $this->load->view('multiselect');
    }

    public function ch_st(){
        $rut = $this->input->post("rut");

        $this->load->model('madmin');
        $dt_st = $this->madmin->get_st($rut);
        echo '<option value="" >'.$this->lang->line('no_selected').'</option>';
        foreach ($dt_st->result() as $row) {
            echo '<option value="'.$row->stid.'" >'.$row->stname.'</option>';
        }

    }

    public function ch_rc(){
        $st = $this->input->post("st");

        $this->load->model('madmin');
        $ocid = $this->madmin->get_first_oc_id($st);
        $dt_rc = $this->madmin->get_rc($st,$ocid);

        echo '<option value="" >'.$this->lang->line('no_selected').'</option>';
        foreach ($dt_rc->result() as $row) {
            echo '<option value="'.$row->rcid.'" >'.$row->rcname.'</option>';
        }

    }

    // CR002
    public function get_rct(){
        $rccid = $this->input->post('rccid');
        $sess = $this->input->post('sess');
        // $rccid = 10;
        // $sess= session_id();
        $this->load->model('mincident');
        if($sess == session_id()){
            $rct = $this->mincident->get_rct($rccid);
            echo $rct['option'];
        }
    }

    public function get_rcn(){
        $rctid = $this->input->post('rctid');
        $sess = $this->input->post('sess');
        // $rccid = 10;
        // $sess= session_id();
        $this->load->model('mincident');
        if($sess == session_id()){
            $rcn = $this->mincident->get_rcn($rctid);
            echo $rcn['option'];
        }
    }

    public function get_rca(){
        $rcnid = $this->input->post('rcnid');
        $sess = $this->input->post('sess');
        // $rccid = 10;
        // $sess= session_id();
        $this->load->model('mincident');
        if($sess == session_id()){
            $rca = $this->mincident->get_rca($rcnid);
            echo $rca['option'];
        }
    }
    //END CR002
}