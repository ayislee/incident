<style type="text/css">
	.personal-setting-content {
		margin-top: 10px;
		margin-left: 10px;
		margin-right: 10px;
		display: block;

	}

	.personal-background {
		display: block;
		background-color: #FFC711;
		border-radius: 3px;

		padding: 10px;

	}

	.personal-label {
		display: inline-block;
		width: 150px;
		font-weight: bold;
		vertical-align: middle;
	}

	.personal-value {
		display: inline-block;
		vertical-align: middle;
	}
</style>
<div class="personal-setting-content">
	<div class="personal-background">
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('id')?></div>
			<div class="personal-value">: #<?php echo $id?></div>
		</div>
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('username')?></div>
			<div class="personal-value">: <?php echo $username?></div>
		</div>
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('first_name')?></div>
			<div class="personal-value">: <?php echo $firstname?></div>
		</div>
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('last_name')?></div>
			<div class="personal-value">: <?php echo $lastname?></div>
		</div>
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('email')?></div>
			<div class="personal-value">: <?php echo $email?></div>
		</div>
		<div class="rows">
			<div class="personal-label"><?php echo $this->lang->line('group')?></div>
			<div class="personal-value">: <?php echo $group?></div>
		</div>

		<?php 
			if($ch_pass){
				$disabled = '';
				$desc = '';
			}else{
				$disabled = 'disabled';
				$desc = '';
			}
		?>
		<div class="rows">
			<button class="std-btn bkgr-blue" <?php echo $disabled?> onclick="pass()"><?php echo $this->lang->line('Change password')?></button>
		</div>
		<?php 
			
		?>
	</div>
</div>


<div class="ontop" id="disable-background2">

</div>

<div class="ontop" id="disable-background3">

</div>

<div class="pop-ch-pass" id="ch-pass">
	<div class="modify-title">
		<?php echo $this->lang->line("Change password"); ?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('Old password')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="old-password" name="old-password" class="input-items" type="password">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('New password')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="new-password" name="new-password" class="input-items" type="password">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('Retype password')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="retype-password" name="retype-password" class="input-items" type="password">
			</div>
		</div>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="change_pass();"><?php echo $this->lang->line('ok'); ?></button>
		<button class="std-btn bkgr-red" onClick="hide('disable-background2','ch-pass');"><?php echo $this->lang->line('cancel'); ?></button>
	</div>

</div>

<div class="pop-msg-pass" id="msg-pass">
	<div class="modify-title">
		<?php echo $this->lang->line("Notify"); ?>
	</div>
	<div class="confirm-message" id="notify-pass">
		
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background2','msg-pass');"><?php echo $this->lang->line('ok'); ?></button>
	</div>

</div>

<script type="text/javascript">
	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}
	function cancel(){
		hide('disable-background','win-del-confirm');
	}

	function pass(){
		// check ldp user
		var ldap_user;
		$.post("<?php echo base_url()?>index.php/incident/check_ldap",
	    {
	        id: "<?php echo $id ?>",
	        s: "<?php echo session_id()?>"
	    },
	    function(data, status){
	    	//location.reload();
	        //alert("Data: " + data + "\nStatus: " + status);
	        
	        if(data==0){
	        	// change password
	        	$("#new-password").val('');
	        	$("#retype-password").val('');

	        	pop('disable-background2','ch-pass');
	        }else{
	        	// cant change password
	        	pop('disable-background2','ldap-user');


	        }
	    });
	}

	function change_pass(){
		if(($("#new-password").val().trim() != '') && ($("#retype-password").val().trim() != '')){
			$.post("<?php echo base_url()?>index.php/incident/change_pass2",
		    {
		        id: "<?php echo $id ?>",
		        p0: $('#old-password').val(),
		        p1: $("#new-password").val(),
		        p2: $("#retype-password").val(),
		        s: "<?php echo session_id()?>"
		    },
		    function(data, status){
		    	//location.reload();
		        //alert("Data: " + data + "\nStatus: " + status);
		        hide('disable-background2','ch-pass');
		        document.getElementById("notify-pass").innerHTML = data;
		        pop('disable-background2','msg-pass');

		        
		    });
		}else{
			 hide('disable-background2','ch-pass');
		     document.getElementById("notify-pass").innerHTML = "<?php echo $this->lang->line('Password you entered is not the same')?>";
		     pop('disable-background2','msg-pass');
		}
	}
</script>