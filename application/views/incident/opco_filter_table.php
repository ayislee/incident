<style type="text/css">
	.fo-c2 {
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}
</style>
<div class="opco-filter-table">
	<div class="table-head">
		<div class="table-head-item fo-c1"><?php echo $this->lang->line('No')?></div>
		<div class="table-head-item fo-c2"><?php echo $this->lang->line('OpCo')?></div>
		<div class="table-head-item fo-c3"><?php echo $this->lang->line('Open Incident')?></div>
		<div class="table-head-item fo-c4"><?php echo $this->lang->line('Close Incident')?></div>
	</div>

	<?php
	$count = 0; 
	foreach($by_opco->result() AS $row) {
		$count++;
	?>
	<div class="table-body">
		<div class="table-body-item fo-c1"><?php echo $count?></div>
		<div class="table-body-item fo-c2"><a href="<?php echo base_url()?>index.php/incident/incident_table/2/<?php echo urlencode('%')?>/<?php echo $row->opcoid?>"><?php echo $row->opconame?></a></div>
		<div class="table-body-item fo-c3"><a href="<?php echo base_url()?>index.php/incident/incident_table/2/0/<?php echo $row->opcoid?>"><?php echo $this->lang->line('Open Incident').'('.$row->open.')';?></a></div>
		<div class="table-body-item fo-c4"><a href="<?php echo base_url()?>index.php/incident/incident_table/2/1/<?php echo $row->opcoid?>"><?php echo $this->lang->line('Close Incident').'('.$row->close.')';?></a></div>
		
	</div>
	<?php } ?>

</div>