<style type="text/css">
.sidemenu {
	position: absolute;
	display: inline-block;
	
	width: 235px;
	height: 100%;
	min-height: 665px;
	padding-top: 56px;
	padding-left: 11px;
	color: white;
	overflow: hidden; 

}
.sidemenu-2 {
	position: fixed;
	top: 0px;
	left: 0px;
	display: inline-block;
	background-color: #4a4c4e;
	/* width: 300px; */
	width: 235px;
	height: 100%;
	min-height: 665px;
	padding-top: 56px;
	padding-left: 11px;
	color: white;
	overflow: hidden; 
	z-index: 3000;

}

.menu-item {
	width: 206px;
}


.main-content2 {
	left: 235px;
	width: calc(100% - 235px);
	min-width: 1000px;
	height: 100%;
	display: inline-block;

}

.main-content {
	left: 235px;
	width: calc(100% - 235px);
	min-width: 1000px;
	height: 100%;
	display: inline-block;

}

</style>

<div class="sidemenu">
</div>
<div class="sidemenu-2">
	<div class="menu-item">
		<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/incident_management" class="menu-link">
			<?php echo $this->lang->line('Incident Management'); ?>
		</a>
	</div>

	<?php
	if($this->session->userdata('get_privilege')[12]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
	<div class="menu-item">
		<i class="fa fa-cogs" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/opco_config_table" class="menu-link" class="menu-link">
			<?php echo $this->lang->line('admin_menu_opco_configuration'); ?>
		</a>
	</div>

	<?php } ?>
	<?php 
	if($this->session->userdata('get_privilege')[11]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
	<div class="menu-item">
		<i class="fa fa-bar-chart" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link" class="menu-link">
			<?php echo $this->lang->line('Report'); ?>
		</a>
	</div>

	<?php } ?>


	<div class="menu-item">
		<i class="fa fa-question-circle" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/faq" class="menu-link">
			<?php echo $this->lang->line('admin_menu_faq'); ?>
		</a>
	</div>
	<div class="menu-item item-last">
		<i class="fa fa-sign-out" aria-hidden="true"></i>
		<a href="#" onclick="logout()" class="menu-link">
			<?php echo $this->lang->line('admin_menu_logout'); ?>
		</a>
	</div>
</div>

<script type="text/javascript">
	function deleteAllCookiesGlobal() {
	    var cookies = document.cookie.split(";");

	    for (var i = 0; i < cookies.length; i++) {
	        var cookie = cookies[i];
	        var eqPos = cookie.indexOf("=");
	        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
	        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	    }
	}

	function logout(){
		deleteAllCookiesGlobal();
		location.href="<?php echo base_url()?>index.php/login/logout";
	}
</script>

