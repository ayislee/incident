<style type="text/css">
	.setting {
		display: inline-block;
		cursor: pointer;
	}

	.welcome-user{
		margin-right: 10px;
	}
</style>
<div class="header">
	<img src="<?php echo base_url()?>assets/images/Logo-Veon-2.svg" height="37px">
	<div class="user-message">
		<div class="switch-lang">
			
			<select onchange="select_language()" name="select_lang" id="select_lang">
			    <option value="english" <?php if($language == 'english') echo 'selected="selected"'; ?>>English</option>
			    <option value="french" <?php if($language == 'french') echo 'selected="selected"'; ?>>French</option>
			    <!--<option value="german" <?php if($language == 'german') echo 'selected="selected"'; ?>>German</option> -->   
			    <option value="russian" <?php if($language == 'russian') echo 'selected="selected"'; ?>>Russian</option> 
			</select>
			<script type="text/javascript">
				function select_language(){
					var language = document.getElementById("select_lang"); 
					//alert('<?php echo base_url()?>');
					this.document.location.href = "<?php echo base_url()?>index.php/LanguageSwitcher/index/"+language.value;
				}
			</script>
		</div>
		<div class="welcome-user"><?php echo $this->lang->line('hello')?>, <?php echo $firstname?></div>

		<div class="setting">
			<a href="<?php echo base_url()?>index.php/incident/personal_setting">
				<i class="fa fa-cog" aria-hidden="true" id="personal-setting" ></i>
			</a>
		</div>
		
	</div>
</div>

