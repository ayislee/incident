<div class="sidemenu">
	<div class="menu-item">
		<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/incident_management" class="menu-link">
			<?php echo $this->lang->line('Incident Management'); ?>
		</a>
	</div>
	<?php 
	if($this->session->userdata('get_privilege')[7]['map_group']){ ?>
	<div class="menu-item">
		<i class="fa fa-bar-chart" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link" class="menu-link">
			<?php echo $this->lang->line('Report'); ?>
		</a>
	</div>
<!--
	<div class="menu-item" id="privilege-1">
		<i class="fa fa-bar-chart" aria-hidden="true"></i>
		<button class="menu-link btn-sidemenu bkgr-black" data-toggle="collapse" data-target="#report-list">
			<?php echo $this->lang->line('Report'); ?>
		</button>
	</div>

	<div class="sidemenu-report collapse in" id="report-list">
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('MFS Report'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA Leakage per Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA % Leakage'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM % Leakage'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Combined RAFM Leakage'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Combined RAFM % Leakage by Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Comparison RA Leakage, Prevented & Recovery'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Comparison FM Leakage, Prevented & Recovery'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Combined Leakage per OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Combined Comparison Leakage, Prevented & Recovery'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Leakage, Prevented & Recovery by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Leakage, Saving By OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Incidents by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA % Leakage by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM % Leakage by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Combined % Leakage by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Bad Debt by OpCo'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Resource by % Saving'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Bad Debt by Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Leakage Calculation per Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Leakage V Saving'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('Incidents Reported By Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA, FM and Combined Dashboard by Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA Leakage Calculation by Month'); ?></a>
		</div>
		
		<div class="menu-report-item">
			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage Calculation by Month'); ?></a>
		</div>
		
	</div>
	-->
	<?php } ?>
	
	<div class="menu-item">
		<i class="fa fa-question-circle" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/admin/faq" class="menu-link">
			<?php echo $this->lang->line('admin_menu_faq'); ?>
		</a>
	</div>
	<div class="menu-item item-last">
		<i class="fa fa-sign-out" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/login/logout" class="menu-link">
			<?php echo $this->lang->line('admin_menu_logout'); ?>
		</a>
	</div>
</div>


