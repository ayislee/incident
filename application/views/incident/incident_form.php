<style type="text/css">
.tab-content > .risk-pane {
    max-height: 500px;
    height: 500px;
}  

.tab-content {
    /* background-color: #ffc711; */
    /* padding: 5px; */
    height: 500px;
    border-top-right-radius: 10px;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    width: 1100px;
}


.w100{
    width:107px;
}
.w110{
    width:120px;
}
.w127 {
    width: 127px;
}

.w177 {
    width: 177px;
}


.w311 {
    width: 311px;
}

.opco-symbol {
    display: inline-block;
    width: 43px;
}

.helper {
    vertical-align: middle;
}

.helper-hide{
    display: inline-block;
    width: 10px;
}

.after-close {
    width:495px;
}

.rc1 {
    display: inline-block;
    width: 125px;
    vertical-align: middle;
    text-align: left;
    padding-left: 5px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
}

/* CR002 */


.a1 {
    width: 170px;
}

.rc1a {
    display: inline-block;
    width: 170px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    vertical-align: middle;
}


.tc1 {
    display: inline-block;
    width: 275px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    vertical-align: middle;
}

.risk-pane > .column655 {
    display: inline-block;
    width: 675px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
    vertical-align: top;
   
}

.risk-pane > .column348 {
    display: inline-block;
    width: 348px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
    vertical-align: top;
}
.w108{
    width:108px;
}

.sc01 {
    display: inline-block;
    width: 335px;
    
    vertical-align: top;
}

.sc02 {
    display: inline-block;
    width: 290px;
    
    vertical-align: top;
}

.half-line{
    padding-top: 7px;
}

.on-net {
    display : block;
    border-color: white;
    border-width:  1px;
    border-style: double;
    border-radius: 5px;
    padding: 5px;
    margin-bottom: 3px;


}

.amlsox{
    display: inline-block;
}

</style>

<div  class="incident-content">
    <ul id="rowTab" class="nav nav-tabs">
    	  <li class="active"><a data-toggle="tab" href="#general" class="width161 center21"><?php echo $this->lang->line('risk_tab')?></a></li>
    	  <li><a data-toggle="tab" href="#attachment" class="width161 center21"><?php echo $this->lang->line('Attachment')?></a></li>
    	  <li><a data-toggle="tab" href="#history" class="width161 center21"><?php echo $this->lang->line('History')?></a></li>
	  </ul>

  	<div class="tab-content risk-setting">
  	  	<div id="general" class="tab-pane risk-pane fade in active">
    	  		<div class="column348">
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('Incident ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="incident_num" name="incident_num" value="<?php echo $detail['incident_num']?>" class="std-input  dis w177" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('External ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="external_id" name="external_id" value="<?php echo $detail['external_id']?>" class="std-input  dis w177" disabled>
        	  				</div>
      	  			</div>

                    <script type="text/javascript">
                        $("#external_id").change(function(){
                            document.cookie = "external_id="+$("#external_id").val();
                        });
                    </script>


      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('OpCo')?></div>
        	  				<div class="rc2">
                        <input type="hidden" id="opcoid" name="opcoid" value="<?php echo $detail['opcoid']?>">
        	  					  <input id="opconame" name="opconame" value="<?php echo $detail['opconame']?>" class="std-input  dis w177" disabled>
        	  				</div>
      	  			</div>

                    <!-- here -->

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('status')?>
                        </div>
                        <div class="rc2">
                            <select class="std-select  dis w177" id="incident_status" name="incident_status" onchange="status_onchange()">
                                <?php switch ($detail['incident_status']) {
                                  case '0':
                                    $select_status = '';
                                    $select_status .= '<option value="0" selected>'.$this->lang->line('Open').'</option>';
                                    $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                    //$select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                    break;
                                  
                                  case '1':
                                    $select_status = '';
                                    $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                    $select_status .= '<option value="1" selected>'.$this->lang->line('Close').'</option>';
                                    //$select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                    break;
                                  case '2':
                                    $select_status = '';
                                    $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                    $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                    //$select_status .= '<option value="2" selected>'.$this->lang->line('Under Investigation').'</option>';
                                    break;
                                }
                                echo $select_status?>
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                      function status_onchange(){
                          $("#incident_status_detail").val($("#incident_status").val());
                          //alert($("#incident_status").val());
                          document.cookie = "incident_status="+$("#incident_status").val();
                          document.cookie="incident_status_detail="+$("#incident_status_detail").val();
                      }
                    </script>

                    
                    <div class="half-line"></div>

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('Detection Tool')?>
                        </div>
                        <div class="rc2">
                            <select id="detection_tool" name="detection_tool" class="std-select  dis w177">
                            <?php 
                            $option_dt = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                            foreach ($select_detection_tool->result() as $row) {
                                # code...
                                if($detail['detection_tool']==$row->dtid) $dt_selected = 'selected'; else $dt_selected ='';
                                $option_dt .= '<option value="'.$row->dtid.'" '.$dt_selected.'>'.$row->dtname.'</option>';
                            }
                            echo $option_dt;
                            ?>
                            </select>
                            
                        </div>
                    </div>
                    <script type="text/javascript">
                        $("#detection_tool").change(function(){
                            document.cookie = "detection_tool="+$("#detection_tool").val();
                            //alert(document.cookie);
                        });
                    </script>


                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('Risk Universe Type')?>
                        </div>
                        <div class="rc2">
                            <select id="rutid" name="rutid" class="std-select  dis w177" onchange="onchange_rut()">
                            <?php 
                          
                          $option_rut = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                          foreach ($select_rut->result() as $row) {
                            # code...
                            if($detail['rutid']==$row->rutid) $rut_selected = 'selected'; else $rut_selected ='';
                            $option_rut .= '<option value="'.$row->rutid.'" '.$rut_selected.'>'.$row->rutname.'</option>';
                          }
                          echo $option_rut;
                        
                            ?>
                            </select>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function onchange_rut(){
                          if(list_rc.length>0){
                              pop('disable-background','rut-confirm');
                          }else{
                            change_rut();
                            document.cookie="rutid="+$("#rutid").val();
                            //document.getElementById('cookie2').innerHTML=document.cookie;
                          }
                        }

                        function change_rut(){
                        //alert(JSON.stringify(list_rc));
                        //alert(list_rc);
                        list_rc = [];
                        document.cookie='list_rc='+JSON.stringify(list_rc);
                        document.getElementById("rc-data").innerHTML = '';
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_st",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    rutid: $("#rutid").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    //alert(data);
                                    var obj = JSON.parse(data);
                                    rut_type = obj.type;
                                    //alert(rut_type);
                                    onoff_selection();
                                    document.getElementById('stid').innerHTML = obj.option;
                                    if(obj.type==1){
                                        // disini kuncinya
                                        document.getElementById('on-off1').style.display="none";
                                        document.getElementById('on-off2').style.display="block"; 
                                        reset_value();

                                    }else{
                                        document.getElementById('on-off1').style.display="block";
                                        document.getElementById('on-off2').style.display="none"; 
                                        reset_value();       

                                    }

                                    change_st();
                                    change_oc();
                              //alert(document.cookie);
                              //alert($("#stid").val());
                              document.cookie="stid="+$("#stid").val();
                              document.cookie="ocid="+$("#ocid").val();
                              
                                }
                            });
                        }
                    </script>

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('Section Title')?>
                        </div>
                        <div class="rc2">
                            <select id="stid" name="stid" class="std-select  dis w177" onchange="change_st()">
                            <?php  
                            $option_st = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                            foreach ($select_st->result() as $row) {
                                # code...
                                if($detail['stid']==$row->stid) $st_selected = 'selected'; else $st_selected ='';
                                $option_st .= '<option value="'.$row->stid.'" '.$st_selected.'>'.$row->stname.'</option>';
                            }
                            echo $option_st;
                            ?>
                               </select>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function change_st(){
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_oc",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    stid: $("#stid").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    //alert(data);
                                    document.getElementById('ocid').innerHTML = data;
                                    change_oc();
                              
                                }
                            });
                     document.cookie="stid="+$("#stid").val();
                     //alert($("#stid").val());
                     //document.getElementById('cookie2').innerHTML=document.cookie; 

                        }

                    </script>


                    <div class="rows" style="display:none">
                        <div class="rc1">
                            <?php echo $this->lang->line('Operator Category')?>
                        </div>
                        <div class="rc2">
                            <select id="ocid" name="ocid" class="std-select  dis w177" onchange="change_oc()">
                                <?php 
                                $option_oc ="";
                                foreach ($select_oc->result() as $row) {
                                    # code...
                                    if($detail['ocid']==$row->ocid) $oc_selected = 'selected'; else $oc_selected ='';
                                    $option_oc .= '<option value="'.$row->ocid.'" '.$oc_selected.'>'.$row->ocname.'</option>';
                                }
                                echo $option_oc;
                                ?>
                             </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function change_oc(){
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_rc",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    ocid: $("#ocid").val(),
                                    stid: $("#stid").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    
                                    document.getElementById('rcid').innerHTML = data;
                              
                              //alert(document.cookie);
                                }
                            });

                            document.cookie="ocid="+$("#ocid").val();
                            //alert($("#stid").val());
                            //document.getElementById('cookie2').innerHTML=document.cookie;
                    
                        }
                    </script>

                

                    <div class="rows">
                            <div class="rc1">
                                <?php echo $this->lang->line('Risk')?>
                            </div>
                            <div class="rc2">
                                <select id="rcid" name="rcid" class="std-select  dis w177">
                                    <?php 
                                    $option_rc = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                                    foreach ($select_rc->result() as $row) {
                                        # code...
                                        $option_rc .= '<option value="'.$row->rcid.'" >'.$row->rcname.'</option>';
                                    }
                                    echo $option_rc;
                                    ?>
                                </select>
                                <button id="btn-add-rc" onclick="add_rc()">+</button>
                            </div>
                    </div>


                    <div class="rows">
                          <div class="rc-data-area" id="rc-data"></div>
                    </div>


                    <div class="rows">
                        <div class="rc1"><?php echo $this->lang->line('aml_sox')?></div>
                        <div class="rc2">
                            <!--
                            <input type="number" id="revenue_stream" name="revenue_stream" class="std-input  dis w177" value="<?php echo $detail['revenue_stream']?>">
                            -->

                            <div class="amlsox">
                                <input type="checkbox" id='aml_checked' <?php echo $aml_checked; ?> > <?php echo $this->lang->line('aml')?>
                                <input type="hidden" id="aml_status" value="<?php echo $detail['aml_status']?>">
                             </div>
                            <div class="amlsox">
                                <input type="checkbox" id='sox_checked' <?php echo $sox_checked; ?>> <?php echo $this->lang->line('sox')?>
                                <input type="hidden" id="sox_status" value="<?php echo $detail['sox_status']?>">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $("#aml_checked").change(function(){
                            if($("#aml_checked").is(':checked'))
                                $("#aml_status").val('1');
                            else
                                $("#aml_status").val('0');
                        });

                        $("#sox_checked").change(function(){
                            if($("#sox_checked").is(':checked'))
                                $("#sox_status").val('1');
                            else
                                $("#sox_status").val('0');
                        });

                    </script>

                    <div class="rows">
                        <div class="rc1"><?php echo $this->lang->line('Revenue Stream')?></div>
                        <div class="rc2">
                            <!--
                            <input type="number" id="revenue_stream" name="revenue_stream" class="std-input  dis w177" value="<?php echo $detail['revenue_stream']?>">
                            -->
                            <select id="revenue_stream" class="std-select  dis w177">
                                <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                            <?php
                                foreach ($select_revenue_stream->result() as $row) {
                                    # code...
                                    if($detail['revenue_stream']==$row->id){
                                        $selected="selected";
                                    }else{
                                        $selected='';
                                    }
                                    echo '<option value="'.$row->id.'" '.$selected.'>'.$row->revenue_stream_name.'</option>';
                                }
                            ?>
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $('#revenue_stream').change(function(){
                            document.cookie="revenue_stream="+$("#revenue_stream").val();
                            // document.getElementById('cookie2').innerHTML=document.cookie;
                        });
                        
                    </script>



                    <!-- dummy-->

                    <!-- CR002 -->
                    <div class="rows"></div>
                    <div class="rows">
                        <div class="rc1-title fc-1">
                            <?php echo $this->lang->line('root_cause_category')?>
                        </div>
                        
                    </div>

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('category')?>
                        </div>
                        <div class="rc2">
                            <select id="id_root_cause_category" name="id_root_cause_category" class="std-select  dis w177" onchange="onchange_rcc()">
                            <?php 
                          
                          $option_rcc = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                          foreach ($select_rcc->result() as $row) {
                            # code...
                            if($detail['id_root_cause_category']==$row->id_root_cause_category) $rcc_selected = 'selected'; else $rcc_selected ='';
                            $option_rcc .= '<option value="'.$row->id_root_cause_category.'" '.$rcc_selected.'>'.$row->root_cause_category.'</option>';
                          }
                          echo $option_rcc;
                        
                            ?>
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function onchange_rcc(){
                            change_rcc();
                            document.cookie="id_root_cause_category="+$("#id_root_cause_category").val();
                        }

                        function change_rcc(){
                            //alert(JSON.stringify(list_rc));
                            //alert(list_rc);
                            //alert($("#id_root_cause_category").val());
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_rct",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    rccid: $("#id_root_cause_category").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    //alert(data);
                                    console.log(data);
                                    // var obj = JSON.parse(data);
                                    //alert(obj.option);
                                    document.getElementById('id_root_cause_type').innerHTML = data;
                                    change_rct();
                                    change_rcn();
                                    //change_rca();
                                    document.cookie="id_root_cause_type="+$("#id_root_cause_type").val();
                                    document.cookie="id_root_cause_name="+$("#id_root_cause_name").val();
                                    //document.cookie="id_root_cause_action="+$("#id_root_cause_action").val();
                              
                                }
                            });
                        }
                    </script>

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('type')?>
                        </div>
                        <div class="rc2">
                            <select id="id_root_cause_type" name="id_root_cause_type" class="std-select  dis w177" onchange="onchange_rct()">
                            <?php 
                          
                          $option_rct = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                          foreach ($select_rct->result() as $row) {
                            # code...
                            if($detail['id_root_cause_type']==$row->id_root_cause_type) $rct_selected = 'selected'; else $rct_selected ='';
                            $option_rct .= '<option value="'.$row->id_root_cause_type.'" '.$rct_selected.'>'.$row->root_cause_type.'</option>';
                          }
                          echo $option_rct;
                        
                            ?>
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function onchange_rct(){
                            change_rct();
                            document.cookie="id_root_cause_type="+$("#id_root_cause_type").val();
                        }

                        function change_rct(){
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_rcn",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    rctid: $("#id_root_cause_type").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    //alert(data);
                                    document.getElementById('id_root_cause_name').innerHTML = data;
                                    change_rcn();
                                    //change_rca();
                                    document.cookie="id_root_cause_name="+$("#id_root_cause_name").val();
                                    //document.cookie="id_root_cause_action="+$("#id_root_cause_action").val();
                              
                                }
                            });
                        }
                    </script>
                    
                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('name')?>
                        </div>
                        <div class="rc2">
                            <select id="id_root_cause_name" name="id_root_cause_name" class="std-select  dis w177" onchange="onchange_rcn()">
                            <?php 
                          
                          $option_rcn = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                          foreach ($select_rcn->result() as $row) {
                            # code...
                            if($detail['id_root_cause_name']==$row->id_root_cause_name) $rcn_selected = 'selected'; else $rcn_selected ='';
                            $option_rcn .= '<option value="'.$row->id_root_cause_name.'" '.$rcn_selected.'>'.$row->root_cause_name.'</option>';
                          }
                          echo $option_rcn;
                        
                            ?>
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function onchange_rcn(){
                            change_rcn();
                            document.cookie="id_root_cause_name="+$("#id_root_cause_name").val();
                        }

                        function change_rcn(){
                            $.ajax({
                                type: "POST",  
                                url: "<?php echo base_url()?>index.php/incident/get_rca",  
                                contentType: 'application/x-www-form-urlencoded',
                                data: { 
                                    rcnid: $("#id_root_cause_name").val(),
                                    sess: "<?php echo session_id()?>"
                                },
                                dataType: "text",
                                beforeSend: function(){

                                },
                                complete: function(){
                                    
                                },
                                success: function(data){
                                    //alert(data);
                                    document.getElementById('id_root_cause_action').innerHTML = data;
                                    //change_rca();
                                    document.cookie="id_root_cause_name="+$("#id_root_cause_name").val();
                                    document.cookie="id_root_cause_action="+$("#id_root_cause_action").val();
                              
                                }
                            });
                        }
                    </script>

                    <div class="rows">
                        <div class="rc1">
                            <?php echo $this->lang->line('action')?>
                        </div>
                        <div class="rc2">
                            <select id="id_root_cause_action" name="id_root_cause_action" class="std-select  dis w177" onchange="onchange_rca()">
                            <?php 
                          $option_rca = '<option value="-1">'.$this->lang->line('Please select').'</option>';
                          foreach ($select_rca->result() as $row) {
                            # code...
                            if($detail['id_root_cause_action']==$row->id_root_cause_action) $rca_selected = 'selected'; else $rca_selected ='';
                            $option_rca .= '<option value="'.$row->id_root_cause_action.'" '.$rca_selected.'>'.$row->root_cause_action.'</option>';
                          }
                          echo $option_rca;
                        
                            ?>
                            </select>
                        </div>
                    </div>
                    
                    <script>
                    function onchange_rca(){
                            document.cookie="id_root_cause_action="+$("#id_root_cause_action").val();
                        }
                    </script>
                    <div class="rows"></div>
                    <div class="rows">
                        <div class="rc3">
                            <?php echo $this->lang->line('Liquidate damages claim')?>
                        </div>

                        <div class="rc3-input">
                            <input id="liquidated_damages_claims" name="number" value="<?php echo $detail['liquidated_damages_claims']?>" class="std-input  dis w100" disabled>
                        </div>
                        
                    </div>

                    <script type="text/javascript">
                        $("#liquidated_damages_claims").change(function(){
                            document.cookie = "liquidated_damages_claims="+$("#liquidated_damages_claims").val();
                        });
                    </script>
                    
                    <!-- END CR002 -->
                    <style type="text/css">
                        .column100 {
                          width: 390px;
                    }
                    </style>

                    <div class="column348">
                      <?php echo $this->lang->line('Description')?>
                    </div>

                    <div class="w311">

                        <textarea id="description" name="description" class="std-textarea" rows="3"><?php echo $detail['description']?></textarea>
                    </div>

                    <script type="text/javascript">
                        $("#description").change(function(){
                            document.cookie = "description="+$("#description").val();
                        });
                    </script>
                    

                    <div class="column348" style="padding-top:5px;">
                      <?php echo $this->lang->line('root_cause_dec')?>
                    </div>

                    <div class="w311">

                        <textarea id="root_cause_dec" name="root_cause_dec" class="std-textarea" rows="3"><?php echo $detail['root_cause_dec']?></textarea>
                    </div>

                    <script type="text/javascript">
                        $("#root_cause_dec").change(function(){
                            document.cookie = "root_cause_dec="+$("#root_cause_dec").val();
                        });
                    </script>


                    <div class="column348" style="padding-top:5px;">
                      <?php echo $this->lang->line('error_correction_dec')?>
                    </div>

                    <div class="w311">

                        <textarea id="error_correction_dec" name="error_correction_dec" class="std-textarea" rows="3"><?php echo $detail['error_correction_dec']?></textarea>
                    </div>

                    <script type="text/javascript">
                        $("#error_correction_dec").change(function(){
                            document.cookie = "error_correction_dec="+$("#error_correction_dec").val();
                        });
                    </script>


                    <div class="w311" style="display: none;">
                        <?php echo $this->lang->line('Note')?>
                    </div>

                    <div style="display: none;">
                        <div class="w311" id="note-area" style="display: none;">
                            <textarea id="note" name="note" class="std-textarea" rows="3"><?php echo $detail['note']?></textarea>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $("#note").change(function(){
                            document.cookie = "note="+$("#note").val();
                        });
                    </script>      	  			


                

    	  		</div>
  	  		    <!-- base -->
    	  		<div class="column655">
                    <div class="sc01">
                        <div class="rows">
                            <div class="rc1 a1">
                                <?php echo $this->lang->line('Impact Severity')?>
                            </div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <select id="impact_severity" name="impact_severity" value="<?php echo $detail['impact_severity']?>" class="std-select w108 dis ">
                                    <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                    <?php for ($i=1; $i<= 10 ; $i++) {
                                      if($i== $detail['impact_severity']) $is = 'selected'; else $is = '';?>
                                    <option value="<?php echo $i?>" <?php echo $is?>><?php echo $i?></option>
                                    <?php }?>

                                </select>
                            </div>

                        </div>
                            
                        <script type="text/javascript">
                            $('#impact_severity').change(function(){
                                document.cookie="impact_severity="+$("#impact_severity").val();
                                //document.getElementById('cookie2').innerHTML=document.cookie;
                            });
                            
                        </script>

                        <div class="rows">

                            <div class="rc1 a1"><?php echo $this->lang->line('Detection Date')?></div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <input type="text" id="case_start_date" name="case_start_date" class="std-input  dis w108" value="<?php echo $detail['case_start_date']?>" readonly='true'>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#case_start_date').change(function(){
                                document.cookie="case_start_date="+$("#case_start_date").val();
                                // document.getElementById('cookie2').innerHTML=document.cookie;
                            });
                            
                        </script>

                    </div>

                    


                    <div class="sc01">
                        <div class="rows">
                            <div class="rc1 a1"><?php echo $this->lang->line('Case Start Date')?></div>
                            <div class="helper-hide"></div>

                            <div class="rc2">
                                <input type="text" id="detection_date" name="detection_date" class="std-input  dis w108" value="<?php echo $detail['detection_date']?>" readonly='true'>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#detection_date').change(function(){
                                document.cookie="detection_date="+$("#detection_date").val();
                                // document.getElementById('cookie2').innerHTML=document.cookie;
                                var ddate = new Date($("#detection_date").val());
                                var selected_year = ddate.getFullYear();
                                var selected_month = ddate.getMonth()+1;

                                Get_Rate(selected_year,selected_month,<?php echo $opcoid?>);

                            });
                            
                        </script>
                        

                        <div class="rows">
                            <div class="rc1 a1"><?php echo $this->lang->line('Case End Date')?></div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <input type="text" id="case_end_date" name="case_end_date" class="std-input  dis w108" value="<?php echo $detail['case_end_date']?>" readonly='true'>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#case_end_date').change(function(){
                                document.cookie="case_end_date="+$("#case_end_date").val();
                                // document.getElementById('cookie2').innerHTML=document.cookie;
                            });
                            
                        </script>
                    </div>

                    <div id="on-off1" style="padding-top:10px;">

                        <div class="sc01">
                            <!-- opsi-->
                            <div class="rows">
                                <div class="rc1 a1">
                                    <?php echo $this->lang->line('Leakage Frequency')?>
                                </div>
                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Leakage Frequency')?>"></i>
                                <div class="rc2">
                                    <select id="leakage_freq" name="leakage_freq" value="<?php echo $detail['leakage_freq']?>" class="std-select dis w108" onchange="onoff_selection();reset_value()">
                                        <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                        <?php foreach ($leakage_freq->result() as $row) {
                                        if(intval($detail['leakage_freq'])==$row->id) $is = 'selected'; else $is = '';
                                        echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                                        }?>
                                    </select>

                                </div>
                            </div>


                            
                            <div id="onoff_rec_freq" style="display:block">
                                <div id="flag-recursive" class="rows" style="display:none">
                                    <div class="rc1 a1">
                                        <?php echo $this->lang->line('Recursive Frequency')?>
                                    </div>
                                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Recursive Frequency')?>"></i>
                                    <div class="rc2">
                                        <select id="recursive_frequency" name="recursive_frequency" value="<?php echo $detail['recursive_frequency']?>" class="std-select  dis w108">
                                            <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                            <?php foreach ($recursive_frequency->result() as $row) {

                                              if(intval($detail['recursive_frequency'])==$row->id) $is = 'selected'; else $is = '';
                                              echo '<option value ="'.$row->id.'" '.$is.'>'.$this->lang->line($row->name).'</option>';
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="rows">
                                <div class="rc1 a1">
                                    <?php echo $this->lang->line('Leakage Timing')?>
                                </div>
                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Leakage Timing')?>"></i>
                                <div class="rc2">
                                    <select id="leakage_timing" name="leakage_timing" value="<?php echo $detail['leakage_timing']?>" class="std-select  dis w108" onchange="onoff_selection();reset_value()">
                                        <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                        <?php foreach ($leakage_timing->result() as $row) {
                                          if(intval($detail['leakage_timing'])==$row->id) $is = 'selected'; else $is = '';
                                          echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                                        }?>
                                    </select>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#leakage_timing').change(function(){
                                    document.cookie="leakage_timing="+$("#leakage_timing").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                });
                                
                            </script>

                            <div class="rows">
                                <div class="rc1 a1">
                                    <?php echo $this->lang->line('Leakage ID Type')?>
                                </div>
                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Leakage ID Type')?>"></i>
                                <div class="rc2">
                                    <select id="leakage_id_type" name="leakage_id_type" value="<?php echo $detail['leakage_id_type']?>" class="std-select dis  w108" onchange="onoff_selection();reset_value()">
                                        <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                        <?php foreach ($leakage_id_type->result() as $row) {
                                          if(intval($detail['leakage_id_type'])==$row->id) $is = 'selected'; else $is = '';
                                          echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                                        }?>
                                    </select>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#leakage_id_type').change(function(){
                                    document.cookie="leakage_id_type="+$("#leakage_id_type").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                });
                                
                            </script>

                        </div>

                        <div class="sc01">
                            <!-- opsi-->
                            

                            <div class="rows" style="display:none">
                              <div class="rc1 a1">
                                  <?php echo $this->lang->line('Recovery Type')?>
                              </div>
                              <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Recovery Type')?>"></i>
                              <div class="rc2">
                                  <select id="recovery_type" name="recovery_type" value="<?php echo $detail['recovery_type']?>" class="std-select w108 dis " onchange="onoff_selection();reset_value()">
                                      <option value="-1"><?php echo $this->lang->line('Please select') ?></option>
                                      <?php foreach ($recovery_type->result() as $row) {
                                        if(intval($detail['recovery_type'])==$row->id) $is = 'selected'; else $is = '';
                                        echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                                      }?>
                                  </select>
                              </div>
                            </div>

                            <script type="text/javascript">
                              $('#recovery_type').change(function(){
                                  document.cookie="recovery_type="+$("#recovery_type").val();
                                  // document.getElementById('cookie2').innerHTML=document.cookie;
                              });
                              
                            </script>
                        </div>
                        <div class="half-line"></div>

                        <div class="rows" id="actual-leakage-block">
                            <div class="rc1 a1"><?php echo $this->lang->line('actual_leakage')?></div>
                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('actual_leakage')?>"></i>
                            <div class="rc2">

                                <input type="text" id="c_final_leakage_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['c_final_leakage_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="c_final_leakage_display" name="text" class="std-input  dis w100" value="<?php echo $detail['c_final_leakage']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="c_final_leakage" name="number"  value="<?php echo $detail['c_final_leakage']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#c_final_leakage_lc').focusout(function(){

                                if($('#c_final_leakage_lc').val()<0){
                                    pop('disable-background','negative-msg');

                                    $('#c_final_leakage_lc').val('');
                                    $('#c_final_leakage').val('');
                                    $('#c_final_leakage_display').val('');
                                    $('#c_final_leakage_lc').focus();
                                }else{
                                    $("#c_final_leakage").val($("#c_final_leakage_lc").val()*global_rate);
                                    $("#c_final_leakage_display").val($("#c_final_leakage_lc").val()*global_rate);
                                    document.cookie="c_final_leakage_lc="+$("#c_final_leakage_lc").val();
                                    document.cookie="c_final_leakage="+$("#c_final_leakage").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                                

                            });
                            
                        </script>

                        <div class="rows" id="incident-leakage-block">
                            <div class="rc1 a1"><?php echo $this->lang->line('incident_leakage')?></div>
                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('incident_leakage')?>"></i>
                            <div class="rc2">

                                <input type="text" id="incident_leakage_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['incident_leakage_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="incident_leakage_display" name="text" class="std-input  dis w100" value="<?php echo $detail['incident_leakage']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="incident_leakage" name="number"  value="<?php echo $detail['incident_leakage']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                         <script type="text/javascript">
                            $('#incident_leakage_lc').focusout(function(){

                                if($('#incident_leakage_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#incident_leakage_lc').val('');
                                    $('#incident_leakage').val('');
                                    $('#incident_leakage_display').val('');
                                    $('#incident_leakage_lc').focus();
                                }else{

                                
                                    $("#incident_leakage").val($("#incident_leakage_lc").val()*global_rate);
                                    $("#incident_leakage_display").val($("#incident_leakage_lc").val()*global_rate);
                                    document.cookie="incident_leakage_lc="+$("#incident_leakage_lc").val();
                                    document.cookie="incident_leakage="+$("#incident_leakage").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }

                            });
                            
                        </script>


                        <div class="rows" id="recovered-block">
                            <div class="rc1 a1"><?php echo $this->lang->line('Recovery')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Recovery')?>"></i>
                            <div class="rc2">
                                <input type="text" id="recovery_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['recovery_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="recovery_display" name="text" class="std-input  dis w100" value="<?php echo $detail['recovery']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="recovery" name="number"  value="<?php echo $detail['recovery']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#recovery_lc').focusout(function(){

                                if($('#recovery_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#recovery_lc').val('');
                                    $('#recovery').val('');
                                    $('#recovery_display').val('');

                                    $('#recovery2_lc').val('');
                                    $('#recovery2').val('');
                                    $('#recovery2_display').val('');
                                    $('#recovery_lc').focus()
                                }else{
                                    $("#recovery").val($("#recovery_lc").val()*global_rate);
                                    $("#recovery_display").val($("#recovery_lc").val()*global_rate);
                                    
                                    $("#recovery2").val($("#recovery").val());
                                    $("#recovery2_lc").val($("#recovery_lc").val());
                                    $("#recovery2_display").val($("#recovery_display").val());

                                    document.cookie="recovery="+$("#recovery").val();
                                    document.cookie="recovery_lc="+$("#recovery_lc").val();
                                    document.cookie="recovery2="+$("#recovery2").val();
                                    document.cookie="recovery2_lc="+$("#recovery2_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }


                                
                            });
                            
                        </script>

                        <div class="rows" id="prevented-block">
                            <div class="rc1 a1"><?php echo $this->lang->line('Prevented')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Prevented')?>"></i>
                            <div class="rc2">
                                <input type="text" id="prevented_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['prevented_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="prevented_display" name="text" class="std-input  dis w100" value="<?php echo $detail['prevented']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="prevented" name="number"  value="<?php echo $detail['prevented']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#prevented_lc').focusout(function(){

                                if($('#prevented_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#prevented_lc').val('');
                                    $('#prevented').val('')
                                    $('#prevented_display').val('');
                                    $('#prevented_lc').val('');
                                    $('#prevented').val('')
                                    $('#prevented_display').val('');
                                    $('#prevented_lc').focus();
                                }else{
                                    $('#prevented2_lc').val($('#prevented_lc').val());
                                    $("#prevented").val($("#prevented_lc").val()*global_rate);
                                    $("#prevented2").val($("#prevented").val());
                                    $("#prevented_display").val($("#prevented_lc").val()*global_rate);
                                    $("#prevented2_display").val($("#prevented_display").val());

                                    document.cookie="prevented="+$("#prevented").val();
                                    document.cookie="prevented_lc="+$("#prevented_lc").val();
                                    document.cookie="prevented2="+$("#prevented2").val();
                                    document.cookie="prevented2_lc="+$("#prevented2_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                            });
                            
                        </script>

                        <div class="rows">
                            <div class="rc1 a1"><?php echo $this->lang->line('over_charging')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('over_charging')?>"></i>
                            <div class="rc2">
                                <input type="text" id="over_charging_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['over_charging_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="over_charging_display" name="text" class="std-input  dis w100" value="<?php echo $detail['over_charging']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="over_charging" name="number"  value="<?php echo $detail['over_charging']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#over_charging_lc').focusout(function(){
                                if($('#over_charging_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#over_charging_lc').val('');
                                    $('#over_charging').val('');
                                    $('#over_charging_display').val('');
                                    $('#over_charging_lc').focus();
                                }else{
                                    $("#over_charging").val($("#over_charging_lc").val()*global_rate);
                                    $("#over_charging_display").val($("#over_charging_lc").val()*global_rate);
                                    document.cookie="over_charging="+$("#over_charging").val();
                                    document.cookie="over_charging_lc="+$("#over_charging_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                                
                            });
                            
                        </script>

                        <input id="incident_status_detail" name="incident_status_detail" value="<?php echo $detail['incident_status_detail']?>" type="hidden">
                        <?php
                          if($detail['c_final_leakage']==null) $c_final_leakage = ''; else $c_final_leakage = number_format($detail['c_final_leakage'],0);
                          if($detail['c_recovered']==null) $c_recovered = ''; else $c_recovered = number_format($detail['c_recovered'],0);
                          if($detail['c_prevented']==null) $c_prevented = ''; else $c_prevented = number_format($detail['c_prevented'],0);
                          if($detail['c_potential']==null) $c_potential = ''; else $c_potential = number_format($detail['c_potential'],0);

                          if($detail['c_final_leakage_lc']==null) $c_final_leakage_lc = ''; else $c_final_leakage_lc = number_format($detail['c_final_leakage_lc'],0);
                          if($detail['c_recovered_lc']==null) $c_recovered_lc = ''; else $c_recovered_lc = number_format($detail['c_recovered_lc'],0);
                          if($detail['c_prevented_lc']==null) $c_prevented_lc = ''; else $c_prevented_lc = number_format($detail['c_prevented_lc'],0);
                          if($detail['c_potential_lc']==null) $c_potential_lc = ''; else $c_potential_lc = number_format($detail['c_potential_lc'],0);
                        ?>

                        <div class="row">
                            <div class="after-close">


                              <?php
                                //echo $detail['incident_status'];
                                if( (int) $detail['incident_status']==1){ 

                                    $d = $c_final_leakage;
                                    $d_lc = $c_final_leakage_lc;

                                    $incident_leakage = number_format($detail['incident_leakage'],0);
                                    $incident_leakage_lc = number_format($detail['incident_leakage_lc'],0);



                                }else{
                                    $d = '';
                                    $d_lc = '';

                                    $incident_leakage = '';
                                    $incident_leakage_lc = '';
                                    
                                }
                              ?>

                              <div class="rows2" id="summary-incident-leakage">
                                    <div class="fc-1"><?php echo $this->lang->line('incident_leakage_summary')?></div>
                                    <div class="fc-2 w110" ><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$incident_leakage?></div>
                                    <div class="fc-2 w110" ><?php echo $symbol.' '.$incident_leakage_lc?></div>
                              </div>
                              <div class="rows2" id="summary-actual-loss">
                                    <div class="fc-1"><?php echo $this->lang->line('actual_loss')?></div>
                                    <div class="fc-2 w110" ><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$d?></div>
                                    <div class="fc-2 w110" ><?php echo $symbol.' '.$d_lc?></div>
                              </div>
                              <div class="rows2" id="summary-actual-leakage">
                                    <div class="fc-1"><?php echo $this->lang->line('actual_leakage')?></div>
                                    <div class="fc-2 w110" ><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$d?></div>
                                    <div class="fc-2 w110" ><?php echo $symbol.' '.$d_lc?></div>
                              </div>
                              <div class="rows2" id="summary-recovered">
                                    <div class="fc-1"><?php echo $this->lang->line('Recovered Value')?></div>
                                    <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_recovered?></div>
                                    <div class="fc-2 w110"><?php echo  $symbol.' '.$c_recovered_lc?></div>
                              </div>
                              <div class="rows2" id="summary-prevented">
                                    <div class="fc-1"><?php echo $this->lang->line('Prevented Value')?></div>
                                    <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_prevented?></div>
                                    <div class="fc-2 w110"><?php echo  $symbol.' '.$c_prevented_lc?></div>
                              </div>
                              <div class="rows2">
                                    <div class="fc-1"><?php echo $this->lang->line('Potential Leakage')?></div>
                                    <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_potential?></div>
                                    <div class="fc-2 w110"><?php echo  $symbol.' '.$c_potential_lc?></div>
                              </div>
                            </div>
                        </div>

                    </div>



                    <!-- begin -->
                    <div id="on-off2" style="padding-top:10px;" >


                        <div class="rows">
                            <div class="rc1a"><?php echo $this->lang->line('regulated_international_ic_price')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('regulated_international_ic_price')?>"></i>
                            <div class="rc2">
                                <input type="text" id="regulated_international_ic_price_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['regulated_international_ic_price_lc']?>" >
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="regulated_international_ic_price_display" name="text" class="std-input  dis w100" value="<?php echo $detail['regulated_international_ic_price']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="regulated_international_ic_price" name="number"  value="<?php echo $detail['regulated_international_ic_price']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#regulated_international_ic_price_lc').focusout(function(){


                                if($('#regulated_international_ic_price_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#regulated_international_ic_price_lc').val('');
                                    $('#regulated_international_ic_price').val('');
                                    $('#regulated_international_ic_price_display').val('');
                                    $('#regulated_international_ic_price_lc').focus();
                                }else{
                                    $("#regulated_international_ic_price").val($("#regulated_international_ic_price_lc").val()*global_rate);
                                    $("#regulated_international_ic_price_display").val($("#regulated_international_ic_price_lc").val()*global_rate);
                                    document.cookie="regulated_international_ic_price="+$("#regulated_international_ic_price").val();
                                    document.cookie="regulated_international_ic_price_lc="+$("#regulated_international_ic_price_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;    
                                }

                                var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
                                var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
                                var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
                                var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

                                $("#on_net_price_arbitrage_lc").val(a);
                                $("#on_net_price_arbitrage").val(b);
                                $("#on_net_price_arbitrage_display").val(b);

                                $("#off_net_price_arbitrage_lc").val(c);
                                $("#off_net_price_arbitrage").val(d);
                                $("#off_net_price_arbitrage_display").val(d);
                                
                            });
                            
                        </script>

                        <div class="rows">
                            <div class="rc1a"><?php echo $this->lang->line('average_disconecting_duration')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('average_disconecting_duration')?>"></i>
                            <div class="rc2">
                                <input type="text" id="average_disconecting_duration" name="number" class="std-input  dis w100" value="<?php echo $detail['average_disconecting_duration']?>">
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#average_disconecting_duration').focusout(function(){
                                if($('#average_disconecting_duration').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#average_disconecting_duration').val('');
                                    $('#average_disconecting_duration').focus();
                                }else {
                                    document.cookie="average_disconecting_duration="+$("#average_disconecting_duration").val();
                                
                                }
                                // document.getElementById('cookie2').innerHTML=document.cookie;
                            });
                            
                        </script>

                        <div class="on-net">
                            <div class="sc01">
                                <div class="rows">
                                    <div class="rc1a"><?php echo $this->lang->line('On-net Terminated minutes')?></div>
                                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('On-net Terminated minutes')?>"></i>
                                    <div class="rc2">

                                        <input type="text" id="on_net_terminated_minutes" name="number" class="std-input  dis w100" value="<?php echo $detail['on_net_terminated_minutes']?>">
                                        

                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $('#on_net_terminated_minutes').focusout(function(){
                                        if($('#on_net_terminated_minutes').val()<0){
                                            pop('disable-background','negative-msg'); 
                                            $('#on_net_terminated_minutes').val('');
                                            $('#on_net_terminated_minutes').focus();   
                                        }else{
                                            document.cookie="on_net_terminated_minutes="+$("#on_net_terminated_minutes").val();
                                            // document.getElementById('cookie2').innerHTML=document.cookie;    
                                        }
                                        

                                        
                                    });
                                    
                                </script>
                            </div>

                            <div class="sc01">
                                <div class="rows">
                                    <div class="rc1a"><?php echo $this->lang->line('on_net_suspended_sims')?></div>
                                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('on_net_suspended_sims')?>"></i>
                                    <div class="rc2">

                                        <input type="text" id="on_net_suspended_sims" name="number" class="std-input  dis w100" value="<?php echo $detail['on_net_suspended_sims']?>">
                                        

                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $('#on_net_suspended_sims').focusout(function(){
                                        if($('#on_net_suspended_sims').val()<0){
                                            pop('disable-background','negative-msg');
                                            $('#on_net_suspended_sims').val('');
                                            $('#on_net_suspended_sims').focus();
                                        }else{
                                            document.cookie="on_net_suspended_sims="+$("#on_net_suspended_sims").val();
                                            // document.getElementById('cookie2').innerHTML=document.cookie;
                                        }
                                        
                                    });
                                    
                                </script>
                            </div>

                            <div class="rows">
                                <div class="rc1a"><?php echo $this->lang->line('average_retail_on_net')?></div>

                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('average_retail_on_net')?>"></i>
                                <div class="rc2">
                                    <input type="text" id="average_retail_on_net_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['average_retail_on_net_lc']?>">
                                    <div class="opco-symbol"><?php echo $symbol?></div>
                                    <input type="text" id="average_retail_on_net_display" name="text" class="std-input  dis w100" value="<?php echo $detail['average_retail_on_net']?>" disabled readonly>
                                    <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                    <input type="hidden" id="average_retail_on_net" name="number"  value="<?php echo $detail['average_retail_on_net']?>" readonly>
                                    <div class="opco-symbol"></div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#average_retail_on_net_lc').focusout(function(){
                                    if($('#average_retail_on_net_lc').val()<0){
                                        pop('disable-background','negative-msg');
                                        $('#average_retail_on_net_lc').val('');
                                        $('#average_retail_on_net').val('');
                                        $('#average_retail_on_net_diaplay').val('');
                                        $('#average_retail_on_net_lc').focus();
                                    }else{
                                        $("#average_retail_on_net").val($("#average_retail_on_net_lc").val()*global_rate);
                                        $("#average_retail_on_net_display").val($("#average_retail_on_net_lc").val()*global_rate);
                                        document.cookie="average_retail_on_net="+$("#average_retail_on_net").val();
                                        document.cookie="average_retail_on_net_lc="+$("#average_retail_on_net_lc").val();
                                        // document.getElementById('cookie2').innerHTML=document.cookie;    
                                    }

                                    var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
                                    var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
                                    var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
                                    var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

                                    $("#on_net_price_arbitrage_lc").val(a);
                                    $("#on_net_price_arbitrage").val(b);
                                    $("#on_net_price_arbitrage_display").val(b);

                                    $("#off_net_price_arbitrage_lc").val(c);
                                    $("#off_net_price_arbitrage").val(d);
                                    $("#off_net_price_arbitrage_display").val(d);
                                    
                                });
                                
                            </script>

                            

                            

                            <div class="rows">
                                <div class="rc1a"><?php echo $this->lang->line('on_net_price_arbitrage')?></div>

                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('on_net_price_arbitrage')?>"></i>
                                <div class="rc2">
                                    <input type="text" id="on_net_price_arbitrage_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['on_net_price_arbitrage_lc']?>" disabled>
                                    <div class="opco-symbol"><?php echo $symbol?></div>
                                    <input type="text" id="on_net_price_arbitrage_display" name="text" class="std-input  dis w100" value="<?php echo $detail['on_net_price_arbitrage']?>" disabled readonly>
                                    <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                    <input type="hidden" id="on_net_price_arbitrage" name="number"  value="<?php echo $detail['on_net_price_arbitrage']?>" readonly>
                                    <div class="opco-symbol"></div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#on_net_price_arbitrage_lc').focusout(function(){
                                    if($('#on_net_price_arbitrage_lc').val()<0){
                                        pop('disable-background','negative-msg');
                                        $('#on_net_price_arbitrage_lc').val('');
                                        $('#on_net_price_arbitrage').val('');
                                        $('#on_net_price_arbitrage_display').val('');
                                        $('#on_net_price_arbitrage_lc').focus();
                                    }else{
                                        $("#on_net_price_arbitrage").val($("#on_net_price_arbitrage_lc").val()*global_rate);
                                        $("#on_net_price_arbitrage_display").val($("#on_net_price_arbitrage_lc").val()*global_rate);
                                        document.cookie="on_net_price_arbitrage="+$("#on_net_price_arbitrage").val();
                                        document.cookie="on_net_price_arbitrage_lc="+$("#on_net_price_arbitrage_lc").val();
                                        // document.getElementById('cookie2').innerHTML=document.cookie;
                                    }
                                });
                                
                            </script>
                        </div>
                        

                        <div class="on-net">
                            <div class="sc01">

                                <div class="rows">
                                    <div class="rc1a"><?php echo $this->lang->line('off_net_terminated_minutes')?></div>

                                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('off_net_terminated_minutes')?>"></i>
                                    <div class="rc2">
                                        <input type="text" id="off_net_terminated_minutes" name="number" class="std-input  dis w100" value="<?php echo $detail['off_net_terminated_minutes']?>">
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $('#off_net_terminated_minutes').focusout(function(){
                                        if($('#off_net_terminated_minutes').val()<0){
                                            pop('disable-background','negative-msg');
                                            $('#off_net_terminated_minutes').val('');
                                            $('#off_net_terminated_minutes').focus();
                                        }else{
                                            document.cookie="off_net_terminated_minutes="+$("#off_net_terminated_minutes").val();
                                            // document.getElementById('cookie2').innerHTML=document.cookie;
                                        }
                                    });
                                    
                                </script>
                            </div>

                            <div class="sc01">

                                <div class="rows">
                                    <div class="rc1a"><?php echo $this->lang->line('off_net_suspensed_sims')?></div>

                                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('off_net_suspensed_sims')?>"></i>
                                    <div class="rc2">
                                        <input type="text" id="off_net_suspensed_sims" name="number" class="std-input  dis w100" value="<?php echo $detail['off_net_suspensed_sims']?>">
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $('#off_net_suspensed_sims').focusout(function(){
                                        if($('#off_net_suspensed_sims').val()<0){
                                            pop('disable-background','negative-msg');
                                            $('#off_net_suspensed_sims').val('');
                                            $('#off_net_suspensed_sims').focus();
                                        }else{
                                            document.cookie="off_net_suspensed_sims="+$("#off_net_suspensed_sims").val();
                                            // document.getElementById('cookie2').innerHTML=document.cookie;
                                        }
                                    });
                                    
                                </script>
                            </div>

                            <div class="rows">
                                <div class="rc1a"><?php echo $this->lang->line('average_retail_off_net')?></div>

                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('average_retail_off_net')?>"></i>
                                <div class="rc2">
                                    <input type="text" id="average_retail_off_net_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['average_retail_off_net_lc']?>">
                                    <div class="opco-symbol"><?php echo $symbol?></div>
                                    <input type="text" id="average_retail_off_net_display" name="text" class="std-input  dis w100" value="<?php echo $detail['average_retail_off_net']?>" disabled readonly>
                                    <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                    <input type="hidden" id="average_retail_off_net" name="number"  value="<?php echo $detail['average_retail_off_net']?>" readonly>
                                    <div class="opco-symbol"></div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#average_retail_off_net_lc').focusout(function(){
                                    if($('#average_retail_off_net_lc').val()<0){
                                        pop('disable-background','negative-msg');
                                        $('#average_retail_off_net_lc').val('');
                                        $('#average_retail_off_net').val('');
                                        $('#average_retail_off_display').val('');
                                        $('#average_retail_off_net_lc').focus();
                                    }else{
                                        $("#average_retail_off_net").val($("#average_retail_off_net_lc").val()*global_rate);
                                        $("#average_retail_off_net_display").val($("#average_retail_off_net_lc").val()*global_rate);
                                        document.cookie="average_retail_off_net="+$("#average_retail_off_net").val();
                                        document.cookie="average_retail_off_net_lc="+$("#average_retail_off_net_lc").val();
                                        // document.getElementById('cookie2').innerHTML=document.cookie;
                                    }

                                    var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
                                    var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
                                    var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
                                    var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

                                    $("#on_net_price_arbitrage_lc").val(a);
                                    $("#on_net_price_arbitrage").val(b);
                                    $("#on_net_price_arbitrage_display").val(b);

                                    $("#off_net_price_arbitrage_lc").val(c);
                                    $("#off_net_price_arbitrage").val(d);
                                    $("#off_net_price_arbitrage_display").val(d);
                                });
                                
                            </script>

                            

                            <div class="rows">
                                <div class="rc1a"><?php echo $this->lang->line('off_net_price_arbitrage')?></div>

                                <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('off_net_price_arbitrage')?>"></i>
                                <div class="rc2">
                                    <input type="text" id="off_net_price_arbitrage_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['off_net_price_arbitrage_lc']?>" disabled>
                                    <div class="opco-symbol"><?php echo $symbol?></div>
                                    <input type="text" id="off_net_price_arbitrage_display" name="text" class="std-input  dis w100" value="<?php echo $detail['off_net_price_arbitrage']?>" disabled readonly>
                                    <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                    <input type="hidden" id="off_net_price_arbitrage" name="number"  value="<?php echo $detail['off_net_price_arbitrage']?>" readonly>
                                    <div class="opco-symbol"></div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $('#off_net_price_arbitrage_lc').focusout(function(){
                                    if($('#off_net_price_arbitrage_lc').val() <0){
                                        pop('disable-background','negative-msg');
                                        $('#off_net_price_arbitrage_lc').val('');
                                        $('#off_net_price_arbitrage').val('');
                                        $('#off_net_price_arbitrage_display').val('');
                                    }else{
                                        $("#off_net_price_arbitrage").val($("#off_net_price_arbitrage_lc").val()*global_rate);
                                        $("#off_net_price_arbitrage_display").val($("#off_net_price_arbitrage_lc").val()*global_rate);
                                        document.cookie="off_net_price_arbitrage="+$("#off_net_price_arbitrage").val();
                                        document.cookie="off_net_price_arbitrage_lc="+$("#off_net_price_arbitrage_lc").val();
                                        // document.getElementById('cookie2').innerHTML=document.cookie;
                                    }
                                    
                                });
                                
                            </script>
                        </div>

                        <div class="rows">
                            <div class="rc1a"><?php echo $this->lang->line('detection_cost')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('detection_cost')?>"></i>
                            <div class="rc2">
                                <input type="text" id="detection_cost_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['detection_cost_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="detection_cost_display" name="text" class="std-input  dis w100" value="<?php echo $detail['detection_cost']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="detection_cost" name="number"  value="<?php echo $detail['detection_cost']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#detection_cost_lc').focusout(function(){
                                if($('#detection_cost_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $('#detection_cost_lc').val('');
                                    $('#detection_cost').val('');
                                    $('#detection_cost_display').val('');
                                    $('#detection_cost_lc').focus();
                                }else{
                                    $("#detection_cost").val($("#detection_cost_lc").val()*global_rate);
                                    $("#detection_cost_display").val($("#detection_cost_lc").val()*global_rate);
                                    document.cookie="detection_cost_net="+$("#detection_cost").val();
                                    document.cookie="detection_cost_lc="+$("#detection_cost_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                                
                            });
                            
                        </script>

                        <div class="rows">
                            <div class="rc1a"><?php echo $this->lang->line('Recovery')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Recovery')?>"></i>
                            <div class="rc2">
                                <input type="text" id="recovery2_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['recovery2_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="recovery2_display" name="text" class="std-input  dis w100" value="<?php echo $detail['recovery2']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="recovery2" name="number"  value="<?php echo $detail['recovery2']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#recovery2_lc').focusout(function(){
                                if($('#recovery2_lc').val() <0){
                                    pop('disable-background','negative-msg');
                                    $('#recovery2_lc').val('');
                                    $('#recovery2').val('');
                                    $('#recovery_lc').val('');
                                    $('#recovery_lc').val('');
                                    $('#recovery').val('');
                                    $('#recovery_lc').val('');
                                    $('#recovery2_display').val('');
                                    $('#recovery2_lc').focus();
                                }else{
                                    $("#recovery2").val($("#recovery2_lc").val()*global_rate);
                                    $("#recovery2_display").val($("#recovery2_lc").val()*global_rate);

                                    $("#recovery").val($("#recovery2").val());
                                    $("#recovery_lc").val($("#recovery2_lc").val());
                                    $("#recovery_display").val($("#recovery2_display").val());


                                    document.cookie="recovery="+$("#recovery").val();
                                    document.cookie="recovery_lc="+$("#recovery_lc").val();
                                    document.cookie="recovery2="+$("#recovery2").val();
                                    document.cookie="recovery2_lc="+$("#recovery2_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                                
                            });
                            
                        </script>

                        <div class="rows" style="display: none">
                            <div class="rc1a"><?php echo $this->lang->line('Prevented')?></div>

                            <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Prevented')?>"></i>
                            <div class="rc2">
                                <input type="text" id="prevented2_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['prevented2_lc']?>">
                                <div class="opco-symbol"><?php echo $symbol?></div>
                                <input type="text" id="prevented2_display" name="text" class="std-input  dis w100" value="<?php echo $detail['prevented2']?>" disabled readonly>
                                <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                                <input type="hidden" id="prevented2" name="number"  value="<?php echo $detail['prevented2']?>" readonly>
                                <div class="opco-symbol"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#prevented2_lc').focusout(function(){
                                if(('#prevented2_lc').val()<0){
                                    pop('disable-background','negative-msg');
                                    $("#prevented2").val('');
                                    $("#prevented2_lc").val('');
                                    $("#prevented2_display").val('');
                                    $("#prevented_lc").val('');
                                    $("#prevented2").val();
                                    $("#prevented2_lc")
                                }else{
                                    $("#prevented2").val($("#prevented2_lc").val()*global_rate);
                                    $("#prevented2_display").val($("#prevented2_lc").val()*global_rate);

                                    $("#prevented").val($("#prevented2").val());
                                    $("#prevented_lc").val($("#prevented2_lc").val());
                                    $("#prevented_display").val($("#prevented2_display").val());


                                    document.cookie="prevented="+$("#prevented").val();
                                    document.cookie="prevented_lc="+$("#prevented_lc").val();
                                    document.cookie="prevented2="+$("#prevented2").val();
                                    document.cookie="prevented2_lc="+$("#prevented2_lc").val();
                                    // document.getElementById('cookie2').innerHTML=document.cookie;
                                }
                                
                            });
                            
                        </script>


                        <?php
                          if($detail['revenue_loss']==null) $revenue_loss = ''; else $revenue_loss = number_format($detail['revenue_loss'],0);

                          if($detail['revenue_loss_lc']==null) $revenue_loss_lc = ''; else $revenue_loss_lc = number_format($detail['revenue_loss_lc'],0);

                          if($detail['recovery3']==null) $recovered = ''; else $recovered = number_format($detail['recovery3'],0);
                          if($detail['recovery3_lc']==null) $recovered_lc = ''; else $recovered_lc = number_format($detail['recovery3_lc'],0);

                          if($detail['prevented']==null) $prevented = ''; else $prevented = number_format($detail['prevented'],0);

                          if($detail['prevented_lc']==null) $prevented_lc = ''; else $prevented_lc = number_format($detail['prevented_lc'],0);

                        ?>

                        <div class="row">
                            <div class="after-close">
                              <?php
                                //echo $detail['incident_status'];
                                if( (int) $detail['incident_status']==1){ 

                                    $r = $revenue_loss;
                                    $r_lc = $revenue_loss_lc;
                                    $s = $recovered;
                                    $s_lc = $recovered_lc;
                                    $t = $prevented;
                                    $t_lc = $prevented_lc;
                                }else{
                                    $r = '';
                                    $r_lc = '';
                                    $s = '';
                                    $s_lc = '';
                                    $t = '';
                                    $t_lc = '';
                                    
                                }
                              ?>
                              <div class="rows2">
                                    <div class="fc-1"><?php echo $this->lang->line('revenue_loss')?></div>
                                    <div class="fc-2 w110" ><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$r?></div>
                                    <div class="fc-2 w110" ><?php echo $symbol.' '.$r_lc?></div>
                              </div>
                              <div class="rows2">
                                    <div class="fc-1"><?php echo $this->lang->line('recovered')?></div>
                                    <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$s?></div>
                                    <div class="fc-2 w110"><?php echo  $symbol.' '.$s_lc?></div>
                              </div>
                              <div class="rows2">
                                    <div class="fc-1"><?php echo $this->lang->line('prevented_loss')?></div>
                                    <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$t?></div>
                                    <div class="fc-2 w110"><?php echo  $symbol.' '.$t_lc?></div>
                              </div>

                            </div>
                        </div>
                    </div>                    
                    
                    <div class="half-line"></div>
                    <!-- date created -->
                    <div class="sc01">
                        <div class="rows">
                            <div class="rc1"><?php echo $this->lang->line('date_created')?></div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                  <input id="create_date" name="create_date" value="<?php echo $detail['create_date']?>" class="std-input  dis w127" disabled>
                            </div>
                        </div>

                        <div class="rows">
                            <div class="rc1"><?php echo $this->lang->line('created_by')?></div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <input id="create_by" name="create_by" value="<?php echo $detail['create_by']?>" class="std-input w127" type="hidden">
                                <input id="user_create" name="user_create" value="<?php echo $detail['user_create']?>" class="std-input  dis w127" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="sc01">
                        <div class="rows">
                            <div class="rc1">
                                <?php echo $this->lang->line('date_update')?>
                            </div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <input id="update_date" name="update_date" value="<?php echo $detail['update_date']?>" class="std-input dis w127" disabled>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="rc1">
                                <?php echo $this->lang->line('update_by')?>
                            </div>
                            <div class="helper-hide"></div>
                            <div class="rc2">
                                <input id="updated_by" name="updated_by" value="<?php echo $detail['updated_by']?>" class="std-input w127" type="hidden">
                                <input id="user_update" name="user_update" value="<?php echo $detail['user_update']?>" class="std-input  dis w127" disabled>
                            </div>
                        </div>
                    </div>
    	  		</div>
  	  	</div>



            <div id="attachment" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <div class="table-head">
                        <div class="table-head-item col-no"><?php echo $this->lang->line('No')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Date Time')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('User')?></div>
                        <div class="table-head-item col-filename"><?php echo $this->lang->line('Filename')?></div>
                        <div class="table-head-item col-desc"><?php echo $this->lang->line('File Description')?></div>
                    </div>
                    <div id="fa">
                    <?php
                    $no = 1;
                    if(sizeof($attachment)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Files').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($attachment as $row) { ?>
                        <div class="table-body">
                            <div class="table-body-item col-no"><?php echo $no ?></div>
                            <div class="table-body-item col-username"><?php echo $row['correct_date']?></div>
                            <div class="table-body-item col-username"><a href="#" onclick="show_user('<?php echo $row['corerct_by']?>','<?php echo $row['first_name']?>','<?php echo $row['last_name']?>','<?php echo $row['email']?>')"><?php echo $row['user_name']?></a></div>
                            <div class="table-body-item col-filename"><a href="<?php echo base_url()?>index.php/incident/download/<?php echo $row['file_id']?>/<?php echo session_id()?>" target="_blank"><?php echo $row['name']?></a></div>
                            <div class="table-body-item col-desc"><?php echo $row['file_description']?></div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    </div>
                    <script type="text/javascript">
                        function show_user(id,first_name,last_name,email){
                            document.getElementById('contact-full-name').innerHTML='Full name: '+first_name+' '+last_name;
                            document.getElementById('contact-email').innerHTML='Email: '+email;
                            pop('disable-background','show-contact');
                        }
                    </script> 
                    <div class="navigator-2" >
                        <div class="command">
                            <button class="std-btn bkgr-blue" onclick="pop('disable-background','upload-form')" id="btn-upload"><?php echo $this->lang->line('UPLOAD')?></button>
                        </div>
                    </div>
                </div>
                
            </div>

          	<div id="history" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <div class="table-head">
                        
                        <div class="table-head-item col-no"><?php echo $this->lang->line('No')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Date Time')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Action')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('User')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Comments')?></div>
                    </div>
                    
                    <?php

                    $no = 1;
                    if(sizeof($history)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Histories').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($history as $row) { ?>
                        <div class="table-body">
                            <div class="table-body-item col-no"><?php echo $no?></div>
                            <div class="table-body-item col-username"><?php echo $row['correct_date']?></div>
                            <div class="table-body-item col-username"><?php echo $row['action']?></div>
                            <div class="table-body-item col-username"><?php echo $row['user_name']?></div>
                            <div class="table-body-item col-username"><?php echo $row['note']?></div>
                            <div class="table-body-item col-username">
                                <?php if($row['attactment'] > 0) {?>
                                <a href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row['incident_id']?>/<?php echo $row['id']?>?open=attactment">
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                </a>
                                <?php } ?>

                            </div>
                            <div class="table-body-item col-username"><a class= "btn btn-info std-btn alink" style="height:24px" role="button" href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row['incident_id']?>/<?php echo $row['id']?>"><?php echo $this->lang->line('view')?></a></div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    
                </div>
                <div class="navigator-2">
                    
                </div>
          	</div>
        </div>	
        <div class="navigator-2" style="width: 1100px">
            <div class="command">
                <?php
                if($this->session->userdata('group_id')==ADMIN_GROUP_ID && $mode=='VIEW'){
                ?>
                <button class="std-btn bkgr-red" onclick="pop('disable-background','confirm-delete')" id="btn-delete"><?php echo $this->lang->line('delete')?></button>
                <?php } ?>
                <button class="std-btn bkgr-green" onclick="btn_save_click()" id="btn-save"><?php echo $this->lang->line('save')?></button>
                <?php if($update_incident) {?>
                <button class="std-btn bkgr-blue" onclick="btn_update_click()" id="btn-modify"><?php echo $this->lang->line('Update')?></button>
                <?php } ?>
                <button class="std-btn bkgr-red" onclick="btn_cancel_click()" id="btn-cancel"><?php echo $this->lang->line('cancel')?></button>
            
            </div>
        </div>
</div>


<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div id="confirm-save" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('Confirm')?></div>
        <div class="confirm-message"><?php echo $this->lang->line('Are you sure to update this incident ?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-green" onclick="save_incident()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-green" onclick="hide('disable-background','confirm-save')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>

<div id="error-message" class="add-access-module" >
    <div class="modify-title"><?php echo $this->lang->line('Error Message')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('error_external_id')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','error-message')"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="cancel-message" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('Are you sure to cancel the operation?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="prev_page()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','cancel-message')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>


<div id="upload-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload attacment')?>
    </div>
    <form name="form-upload" id="form-upload" >
        <div class="content-message">
            <input type="file" name="file-attachment" id="file-attachment" class="margin-left30" />
            <div class="label-text"><?php echo $this->lang->line('File Description')?></div>
            <textarea id="file-description" name="file-description"  class="std-textarea" rows="4"></textarea>
        </div>
        <div class="confirm-btn">
            <button type="submit" onClick="return validate()" class="std-btn bkgr-green"><?php echo $this->lang->line('Upload')?></button>
            <button type="button" class="std-btn bkgr-red" onclick="cancel_upload()"><?php echo $this->lang->line('cancel')?></button>
        </div>
    </form>

</div>

<script type="text/javascript">
function validate(){
    var size=1024 * 1024 * 2;
    var file_size=document.getElementById('file-attachment').files[0].size;
    if(file_size>size){
        hide('disable-background','upload-form');
        pop('disable-background','error-size');
        return false;
    }else{
        return true;
    }
}
</script>

<div id="error-size" class="error-message">
    <div class="modify-title" id="title-msg"><?php echo $this->lang->line('Error Message')?></div>
    <div class="confirm-message" id="content-msg"><?php echo $this->lang->line('error_filesize')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-ok-upload" onclick="hide('disable-background','error-size');pop('disable-background','upload-form');"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>

<div id="delete-msg" class="error-message">
    <div class="modify-title" id="delete-title"></div>
    <div class="confirm-message" id="delete-desc"></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-ok-delete" ><?php echo $this->lang->line('ok')?></button>

    </div>
</div>

<div id="success-message" class="error-message">
    <div class="modify-title" id="title-msg-save"></div>
    <div class="confirm-message" id="content-msg-save"></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-success-message" onclick="success_save()"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="negative-msg" class="error-message">
    <div class="modify-title"><?php echo $this->lang->line('Error Message')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('negative_value')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-success-message" onclick="hide('disable-background','negative-msg')"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="rut-confirm" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('rut reset')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="change_rut();hide('disable-background','rut-confirm');"><?php echo $this->lang->line('yes')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','rut-confirm')"><?php echo $this->lang->line('No')?></button>
    </div>
</div>

<div id="show-contact" class="show-contact">
    <div class="modify-title"><?php echo $this->lang->line('Contact')?></div>
    <div class="contact-message" id="contact-full-name"></div>
    <div class="contact-message" id="contact-email"></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','show-contact');"><?php echo $this->lang->line('ok')?></button>
        
    </div>
</div>

<?php
if($this->session->userdata('group_id')==ADMIN_GROUP_ID && $mode=='VIEW'){
?>
<div id="confirm-delete" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm_delete')?></div>

        <div class="confirm-message" style="padding-bottom: 7px">
            <?php echo $this->lang->line('confirm_delete_message')?>
            <div>
                <input id="delete-pass" name="delete-pass"  class="std-input" type="password" autocomplete="new-password" value="">
            </div>
        </div>
            
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onclick="btn_delete()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-green" onclick="hide('disable-background','confirm-delete');$('#delete-pass').val('');"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>
<?php } ?>

<div id="json"></div>
<div id="cookie"></div>
<div id="cookie2"></div>
<script type="text/javascript">
	var list_rc = [];
	var obj_rc;
    var mode = "<?php echo $mode?>";
    var attachment = [];
    var incident_status = <?php echo $detail['incident_status']?>;
    var group = <?php echo $this->session->userdata('group_id')?>;
    var global_rate = <?php echo $rate?>;
    var rut_type=<?php echo $detail['type'] ?>;
    //alert(rut_type);

    //onchange_rut();

    $("#recovery_display").number(true,0);
    $("#recovery_lc").number(true,0);
    
    $("#c_final_leakage_display").number(true,0);
    $("#c_final_leakage_lc").number(true,2);

    $("#prevented_lc").number(true,0);
    $("#prevented_display").number(true,0);
    
    $("#over_charging_lc").number(true,0);
    $("#over_charging_display").number(true,0);

    $("#on_net_terminated_minutes").number(true,0);
    $("#on_net_suspended_sims").number(true,0);
  
    $("#average_retail_on_net_lc").number(true,2);
    $("#average_retail_on_net_display").number(true,2);

    $("#detection_cost_lc").number(true,0);
    $("#detection_cost_display").number(true,0);

    $("#regulated_international_ic_price_lc").number(true,2);
    $("#regulated_international_ic_price_display").number(true,2);

    $("#on_net_price_arbitrage_lc").number(true,2);
    $("#on_net_price_arbitrage_display").number(true,2);


    $("#off_net_terminated_minutes").number(true,0);
    $("#off_net_suspensed_sims").number(true,0);

    $("#average_retail_off_net_lc").number(true,2);
    $("#average_retail_off_net_display").number(true,2);

    $("#average_disconecting_duration").number(true,2);

    $("#off_net_price_arbitrage_lc").number(true,2);
    $("#off_net_price_arbitrage_display").number(true,2);
    
    $("#recovery2_lc").number(true,0);
    $("#recovery2_display").number(true,0);

    $("#prevented2_lc").number(true,0);
    $("#prevented2_display").number(true,0);

    // new fields
    $("#incident_leakage_lc").number(true,0);
    $("#incident_leakage_display").number(true,0);

    // CR002
    $("#liquidated_damages_claims").number(true,0);
    // END CR002



    if("<?php echo $detail['type'] ?>"=="1"){ 
        // // for bypass
        document.getElementById('on-off1').style.display="none";
        document.getElementById('on-off2').style.display="block";        
    }else if("<?php echo $detail['type'] ?>"=="2"){
        // for fraud management
        
        document.getElementById('on-off1').style.display="block";
        document.getElementById('on-off2').style.display="none";  

        document.getElementById('onoff_rec_freq').style.display="none";      
    }else{
        document.getElementById('on-off1').style.display="block";
        document.getElementById('on-off2').style.display="none";  

        document.getElementById('onoff_rec_freq').style.display="block";
    }

    if("<?php echo $detail['leakage_freq']?>"=="2"){
        document.getElementById('prevented-block').style.display="block";

    }else{
        document.getElementById('prevented-block').style.display="none";
    }

    if("<?php echo $detail['leakage_freq']?>"=="1"){
        document.getElementById('flag-recursive').style.display="block";
        
    }else{
        document.getElementById('flag-recursive').style.display="none";
    }

  function Get_Rate(year,month,opcoid){
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/get_rate",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                year: year,
                month: month,
                currency_id:<?php echo $currency_id?>,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //alert(data);
                global_rate = data;

                refresh_rate();
                //alert(data);
                
            }
        });
  }

  function get_st(rutid){
      document.getElementById("rc-data").innerHTML = '';
      $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/get_st",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                rutid: rutid,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //alert(data);

                document.getElementById('stid').innerHTML = data;
                
            }
        });      
  }

  function get_oc(stid){
    //alert(stid);

    $.ajax({
        type: "POST",  
        url: "<?php echo base_url()?>index.php/incident/get_oc",  
        contentType: 'application/x-www-form-urlencoded',
        data: { 
            stid: 13,
            sess: "<?php echo session_id()?>"
        },
        dataType: "text",
        beforeSend: function(){

        },
        complete: function(){
            
        },
        success: function(data){
            //alert(data);
            document.getElementById('ocid').innerHTML = data;
            
            
        }
    });


  }

	$(document).ready(function(){
      // read cookie -----

      //alert('<?php echo $detail["opcoid"] ?>');     



  		<?php foreach ($categories as $cats) {

  			# code...
  			$jvs = 'obj_rc = { incident_detail_id: "'.$cats['incident_detail_id'].'", rcid: "'.$cats['rcid'].'",rcname: "'.$cats['rcname'].'"};';
  			$jvs .= 'list_rc.push(obj_rc);';
  			echo $jvs;
  		}?>
  		//json_rc = JSON.stringify(list_rc);
  		//alert(list_rc[1].incident_detail_id);
  		
  		for(i=0;i<list_rc.length;i++){
    			//document.getElementById("rc-data").appendChild("haha");
    			insert_div = '<div class="rc-items">';
    			insert_div += list_rc[i].rcname;
    			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
    			insert_div += '</div>';
    			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
  		}

      switch(mode){
          case "NEW":
              document.getElementById('external_id').disabled=false;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              // CR002
              document.getElementById('liquidated_damages_claims').disabled=false;
              // END CR002

              document.getElementById('btn-save').style.display="inline-block";
              <?php if($update_incident) {?>              
              document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";
              
              var opco_cookie = getCookie('opco_id');
              if(opco_cookie != '<?php echo $detail["opcoid"] ?>'){
                deleteAllCookies();
                location.reload();

              }
              document.cookie = "opco_id=<?php echo $detail['opcoid']?>";
              //alert(get_sysdate());
              //$("#detection_date").val(get_sysdate());
              //$("#case_start_date").val(get_sysdate());

              onchange_rut();
              $("#recovery_type").val("2");
               onoff_selection();

              break;
          case "UPDATE":
              document.getElementById('external_id').disabled=true;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('incident_status_detail').disabled=false;              
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              // CR002
              document.getElementById('liquidated_damages_claims').disabled=false;
              // END CR002


              document.getElementById('btn-save').style.display="inline-block"; 
              <?php if($update_incident) {?>             
              document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";
               $("#note").val('');
               $("#recovery_type").val("2");
               onoff_selection();

              break;
          case "VIEW":

              document.getElementById('external_id').disabled=true;
              
              document.getElementsByName("incident_status").disabled=true;
              document.getElementById('incident_status_detail').disabled=true;
              $('[name=incident_status]').attr('disabled','disabled');
              document.getElementById('description').disabled=true;
              document.getElementById('note').disabled=true;

              document.getElementById('aml_checked').disabled=true;
              document.getElementById('sox_checked').disabled=true;


              document.getElementById('btn-save').style.display="none";
              
              <?php if($update_incident) { ?>
                if(incident_status==0 || incident_status==2 || group==<?php echo ADMIN_GROUP_ID?>){              
                    document.getElementById('btn-modify').style.display="inline-block";
                }else{
                    document.getElementById('btn-modify').style.display="none";
                }
              <?php } else {?>
                // document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              
              document.getElementById('btn-cancel').style.display="none";
              document.getElementById('btn-upload').style.display="none";

              document.getElementById('detection_tool').disabled=true;
              document.getElementById('rutid').disabled=true;
              document.getElementById('stid').disabled=true;
              document.getElementById('ocid').disabled=true;
              document.getElementById('rcid').disabled=true;
              document.getElementById('btn-add-rc').disabled=true;



              document.getElementsByClassName("btn-rc").disabled=true;
              $(".btn-rc").hide().attr('disabled','disabled');
              document.getElementById('btn-add-rc').style.display="none";

              
              document.getElementById('revenue_stream').disabled=true;
              document.getElementById('impact_severity').disabled=true;
              document.getElementById('detection_date').disabled=true;
              document.getElementById('case_start_date').disabled=true;
              document.getElementById('case_end_date').disabled=true;
              document.getElementById('c_final_leakage').disabled=true;
              document.getElementById('c_final_leakage_lc').disabled=true;
              document.getElementById('recovery').disabled=true;
              document.getElementById('recovery_lc').disabled=true;
              document.getElementById('leakage_freq').disabled=true;
              document.getElementById('leakage_timing').disabled=true;
              document.getElementById('leakage_id_type').disabled=true;
              document.getElementById('recovery_type').disabled=true;
              document.getElementById('prevented_lc').disabled=true;
              document.getElementById('over_charging_lc').disabled=true;

              document.getElementById('on_net_terminated_minutes').disabled=true;
              document.getElementById('on_net_suspended_sims').disabled=true;
              document.getElementById('average_retail_on_net_lc').disabled=true;
              document.getElementById('detection_cost_lc').disabled=true;
              document.getElementById('regulated_international_ic_price_lc').disabled=true;
              document.getElementById('on_net_price_arbitrage_lc').disabled=true;

              document.getElementById('off_net_terminated_minutes').disabled=true;
              document.getElementById('off_net_suspensed_sims').disabled=true;
              document.getElementById('average_retail_off_net_lc').disabled=true;
              document.getElementById('average_disconecting_duration').disabled=true;
              document.getElementById('off_net_price_arbitrage_lc').disabled=true;
              document.getElementById('recovery2_lc').disabled=true;
              document.getElementById('prevented2_lc').disabled=true;

              // new fields
              document.getElementById('recursive_frequency').disabled=true;
              document.getElementById('incident_leakage_lc').disabled=true;

              // CR002
              document.getElementById('root_cause_dec').disabled=true;
              document.getElementById('error_correction_dec').disabled=true;  

              document.getElementById('id_root_cause_category').disabled=true;
              document.getElementById('id_root_cause_type').disabled=true;
              document.getElementById('id_root_cause_name').disabled=true;
              document.getElementById('id_root_cause_action').disabled=true;
              document.getElementById('liquidated_damages_claims').disabled=true;             
              // END CR002

              onoff_selection();

              break;
      }
		
	});


  function pop(div,div2) {
    document.getElementById(div).style.display = 'block';
    document.getElementById(div2).style.display = 'block';

  }
  function hide(div,div2) {
    document.getElementById(div).style.display = 'none';
    document.getElementById(div2).style.display = 'none';
  }
  
  function search(nameKey, myArray){
      found = false;
      for (var i=0; i < myArray.length; i++) {
          if (myArray[i].rcid == nameKey) {
              found = true;
          }
      }
      return found;
  }

	function add_rc(){
		// find same rcid
        if($("#rcid").val() != "-1"){
           a = search($("#rcid").val(),list_rc);

            if(!a){
                insert_rc = { incident_detail_id: "<?php $id?>",rcid: $("#rcid").val(), rcname: document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text};
                list_rc.push(insert_rc);
                last_num = list_rc.length -1 ;
                insert_div = '<div class="rc-items">';
                insert_div += document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text;
                insert_div += '<button class="btn-rc" onclick="del_rc('+last_num+')">x</button>';
                insert_div += '</div>';
                document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
                document.cookie="list_rc="+JSON.stringify(list_rc);
                //document.getElementById('cookie2').innerHTML=document.cookie;

            }else{
              alert("<?php echo $this->lang->line('Risk Category already selected')?>");
            } 
        }else{
            alert("<?php echo $this->lang->line('Please select')?>");
        }
        


	}

	function del_rc(num){
		list_rc.splice(num,1);
		// redraw;
		document.getElementById("rc-data").innerHTML = '';
		for(i=0;i<list_rc.length;i++){
			//document.getElementById("rc-data").appendChild("haha");
			insert_div = '<div class="rc-items">';
			insert_div += list_rc[i].rcname;
			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
			insert_div += '</div>';
			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
      document.cookie="list_rc="+JSON.stringify(list_rc);
      //document.getElementById('cookie2').innerHTML=document.cookie;
		}
	}

<?php if($mode != 'VIEW') {?>
	$(function() {
	    $( "#detection_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_start_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_end_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });



<?php } ?>
  function btn_save_click(){

      var detection_date = $("#detection_date").val();
      var case_start_date = $("#case_start_date").val();

      if(detection_date > case_start_date){
        //alert('lebih besar');
      }else{
        //alert('lebih kecil');
      }

      if($("#external_id").val().trim()=='' ) {
         // $("#c_final_leakage").val().trim()=='' ||
         // $("#leakage_one_day_lc").val().trim()=='' ||
         // $("#recovery").val().trim()=='' || 
         // $("#recovery_lc").val().trim()=='' ) {
          pop('disable-background','error-message');
      }else{
        // code here 

        //var car = {type:"Fiat", model:"500", color:"white"};        

        <?php if($mode=='NEW'){?>
        var incident = { 
        incident_id: "", 
        external_id: $("#external_id").val(),
        opcoid: $("#opcoid").val(),
        opconame: $('#opconame').val(),
        date_created: $("#date_created").val(),
        create_by: $("create_by").val(),
        date_update: $("#date_update").val(),
        updated_by: $("#updated_by").val(),
        incident_status: $("#incident_status").val(),
        incident_status_detail: $("#incident_status_detail").val(),
        aml_status: $("#aml_status").val(),
        sox_status: $("#sox_status").val(),
        aml_default: <?php echo $aml_default?>,
        sox_default: <?php echo $sox_default?>,

        incident_detail_id: "",
        description: $("#description").val(),
        note: $("#description").val(),
        detection_tool : $("#detection_tool").val(),
        rutid: $("#rutid").val(),
        stid: $("#stid").val(),
        ocid: $("#ocid").val(),
        revenue_stream: $("#revenue_stream").val(),
        impact_severity: $("#impact_severity").val(),
        detection_date: $("#detection_date").val(),
        case_start_date: $("#case_start_date").val(),
        case_end_date: $("#case_end_date").val(),
        c_final_leakage: $("#c_final_leakage").val(),
        c_final_leakage_lc: $("#c_final_leakage_lc").val(),
        recovery: $("#recovery").val(),
        recovery_lc: $("#recovery_lc").val(),
        leakage_freq: $("#leakage_freq").val(),
        leakage_timing: $("#leakage_timing").val(),
        leakage_id_type: $("#leakage_id_type").val(),
        recovery_type: $("#recovery_type").val(),
        list_rc: list_rc,
        attachment: attachment,
        prevented: $("#prevented").val(),
        prevented_lc: $("#prevented_lc").val(),
        over_charging: $("#over_charging").val(),
        over_charging_lc: $("#over_charging_lc").val(),
        
        on_net_terminated_minutes: $("#on_net_terminated_minutes").val(),
        on_net_suspended_sims: $("#on_net_suspended_sims").val(),
        average_retail_on_net: $("#average_retail_on_net").val(),
        average_retail_on_net_lc: $("#average_retail_on_net_lc").val(),
        detection_cost: $("#detection_cost").val(),
        detection_cost_lc: $("#detection_cost_lc").val(),
        regulated_international_ic_price: $("#regulated_international_ic_price").val(),
        regulated_international_ic_price_lc: $("#regulated_international_ic_price_lc").val(),
        on_net_price_arbitrage: $("#on_net_price_arbitrage").val(),
        on_net_price_arbitrage_lc: $("#on_net_price_arbitrage_lc").val(),

        off_net_terminated_minutes: $("#off_net_terminated_minutes").val(),
        off_net_suspensed_sims: $("#off_net_suspensed_sims").val(),
        average_retail_off_net: $("#average_retail_off_net").val(),
        average_retail_off_net_lc: $("#average_retail_off_net_lc").val(),
        average_disconecting_duration: $("#average_disconecting_duration").val(),
        off_net_price_arbitrage: $("#off_net_price_arbitrage").val(),
        off_net_price_arbitrage_lc: $("#off_net_price_arbitrage_lc").val(),

        recursive_frequency: $("#recursive_frequency").val(),
        incident_leakage: $("#incident_leakage").val(),
        incident_leakage_lc: $("#incident_leakage_lc").val(),
        
        root_cause_dec: $("#root_cause_dec").val(),
        error_correction_dec: $("#error_correction_dec").val(),
        id_root_cause_category: $('#id_root_cause_category').val(),
        id_root_cause_type: $('#id_root_cause_type').val(),
        id_root_cause_name: $('#id_root_cause_name').val(),
        id_root_cause_action: $('#id_root_cause_action').val(),
        id_root_cause_category: $('#id_root_cause_category').val(),
        liquidated_damages_claims: $('#liquidated_damages_claims').val()

        };

        json_incident = JSON.stringify(incident);
        // alert(json_incident);
        console.log(json_incident);
        //document.getElementById('json').innerHTML = json_incident;
        document.getElementById('disable-background2').style.display = 'block';
        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  document.getElementById('disable-background2').style.display = 'none';
                  //alert(data);
                  console.log(data);
                  var obj = jQuery.parseJSON(data);
                  
                   
                  switch(obj.status){
                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";
                          //pop('disable-background','success-message');
                          //document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          success_save(0);
                          break;
                      default:
                          //alert("error new");
                          document.getElementById('title-msg-save').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg-save').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });  
      <?php }else {?>
        var incident = { 
        incident_id: "<?php echo $detail['id']?>", 
        external_id: $("#external_id").val(),
        opcoid: $("#opcoid").val(),
        opconame: $('#opconame').val(),
        date_created: $("#date_created").val(),
        create_by: $("create_by").val(),
        date_update: $("#date_update").val(),
        updated_by: $("#updated_by").val(),
        incident_status: $("#incident_status").val(),
        incident_status_detail: $("#incident_status_detail").val(),
        aml_status: $("#aml_status").val(),
        sox_status: $("#sox_status").val(),
        aml_default: <?php echo $aml_default?>,
        sox_default: <?php echo $sox_default?>,

        incident_detail_id: "",
        description: $("#description").val(),
        note: $("#description").val(),
        detection_tool : $("#detection_tool").val(),
        rutid: $("#rutid").val(),
        stid: $("#stid").val(),
        ocid: $("#ocid").val(),
        revenue_stream: $("#revenue_stream").val(),
        impact_severity: $("#impact_severity").val(),
        detection_date: $("#detection_date").val(),
        case_start_date: $("#case_start_date").val(),
        case_end_date: $("#case_end_date").val(),
        c_final_leakage: $("#c_final_leakage").val(),
        c_final_leakage_lc: $("#c_final_leakage_lc").val(),            
        recovery: $("#recovery").val(),
        recovery_lc: $("#recovery_lc").val(),          
        leakage_freq: $("#leakage_freq").val(),
        leakage_timing: $("#leakage_timing").val(),
        leakage_id_type: $("#leakage_id_type").val(),
        recovery_type: $("#recovery_type").val(),
        list_rc: list_rc,
        attachment: attachment,
        prevented: $("#prevented").val(),
        prevented_lc: $("#prevented_lc").val(),
        over_charging: $("#over_charging").val(),
        over_charging_lc: $("#over_charging_lc").val(),
        on_net_terminated_minutes: $("#on_net_terminated_minutes").val(),

        on_net_suspended_sims: $("#on_net_suspended_sims").val(),
        average_retail_on_net: $("#average_retail_on_net").val(),
        average_retail_on_net_lc: $("#average_retail_on_net_lc").val(),
        detection_cost: $("#detection_cost").val(),
        detection_cost_lc: $("#detection_cost_lc").val(),
        regulated_international_ic_price: $("#regulated_international_ic_price").val(),
        regulated_international_ic_price_lc: $("#regulated_international_ic_price_lc").val(),
        on_net_price_arbitrage: $("#on_net_price_arbitrage").val(),
        on_net_price_arbitrage_lc: $("#on_net_price_arbitrage_lc").val(),

        off_net_terminated_minutes: $("#off_net_terminated_minutes").val(),
        off_net_suspensed_sims: $("#off_net_suspensed_sims").val(),
        average_retail_off_net: $("#average_retail_off_net").val(),
        average_retail_off_net_lc: $("#average_retail_off_net_lc").val(),
        average_disconecting_duration: $("#average_disconecting_duration").val(),
        off_net_price_arbitrage: $("#off_net_price_arbitrage").val(),
        off_net_price_arbitrage_lc: $("#off_net_price_arbitrage_lc").val(),

        recursive_frequency: $("#recursive_frequency").val(),
        incident_leakage: $("#incident_leakage").val(),
        incident_leakage_lc: $("#incident_leakage_lc").val(),

        root_cause_dec: $("#root_cause_dec").val(),
        error_correction_dec: $("#error_correction_dec").val(),
        id_root_cause_category: $('#id_root_cause_category').val(),
        id_root_cause_type: $('#id_root_cause_type').val(),
        id_root_cause_name: $('#id_root_cause_name').val(),
        id_root_cause_action: $('#id_root_cause_action').val(),
        id_root_cause_category: $('#id_root_cause_category').val(),
        liquidated_damages_claims: $('#liquidated_damages_claims').val()
        };
        document.getElementById('disable-background2').style.display = 'block';

        json_incident = JSON.stringify(incident);
        // alert(json_incident);
        //document.getElementById('json').innerHTML = json_incident;
        console.log(json_incident);
        
        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  document.getElementById('disable-background2').style.display = 'none';
                  //alert(data);
                  console.log(data);
                  var obj = jQuery.parseJSON(data);
                  
                                    
                  switch(obj.status){

                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";

                          //pop('disable-background','success-message');
                          //document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          success_save(0);
                          break;
                      default:
                          //alert(obj.msg);
                          document.getElementById('title-msg-save').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg-save').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });

      <?php } ?>      
        
      }
  }

  function btn_cancel_click(){

      pop('disable-background','cancel-message');

  }

  function prev_page(){
      deleteAllCookies();
      location.href="<?php echo $cancel_operation_link?>";
  }

  function do_upload() {
      //alert('jsv');
      var data = new FormData($('#form-upload')[0]);

      if($("#file-description").val()==''){
          alert('Please type file description');
      }else {
          $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/upload_attachment",  
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              data: data,
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                    
              },
              success: function(data){
                    //location.reload();
                    alert(data);
                    
              }
          });          
      }
      
  }
  function cancel_upload(){
      hide('disable-background','upload-form');
      return false;
  }

  $(function() {
      $('#form-upload').submit(function(e) {
          hide('disable-background','upload-form');
          document.getElementById('disable-background2').style.display = 'block';

          e.preventDefault();
          $.ajaxFileUpload({
              url             :"<?php echo base_url()?>index.php/incident/upload_attachment", 
              secureuri       :false,
              fileElementId   :'file-attachment',
              dataType: 'JSON',
              beforeSend: function(){
                  
              },
              complete: function(){

              },
              success : function (data){
                  document.getElementById('disable-background2').style.display = 'none';
                  fn = data.indexOf('{');
                  rem = data.substring(0,fn-1);
                  data = data.replace(rem,'');
                  //alert(data);
                  var obj = jQuery.parseJSON(data);
                  if(obj.status=='success'){
                      fa = {
                          incident_detail_id: "",
                          file_id: "",
                          file_description: $("#file-description").val(),
                          type: obj.filetype,
                          name: obj.filename,
                          fullpath: obj.fullpath
                          }

                      attachment.push(fa);
                      num_file = attachment.length;
                      tbl_attachment = '';
                      for(i=0;i<attachment.length;i++){
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth()+1; //January is 0!
                          var yyyy = today.getFullYear();
                          var hh = today.getHours();
                          var mm = today.getMinutes();
                          var ss = today.getSeconds();

                          if(dd<10){
                              dd='0'+dd
                          } 
                          if(mm<10){
                              mm='0'+mm
                          } 
                          var today = yyyy+'-'+mm+'-'+dd+' '+hh+':'+mm+':'+ss;
                          var user = "<?php echo $this->session->userdata('username')?>";
                          //var desc = 

                          tbl_attachment += '<div class="table-body">';
                          tbl_attachment += '   <div class="table-body-item col-no">'+(i+1)+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-username">'+today+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-username">'+user+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-filename">'+(attachment[i].name)+'</div>';    
                          tbl_attachment += '   <div class="table-body-item col-desc">'+attachment[i].file_description+'</div>';
                          tbl_attachment += '</div>'; 

                      }
                      //alert(tbl_attachment);
                      document.getElementById('fa').innerHTML = tbl_attachment;

                  }else{
                      //alert(obj.msg);
                  }
              }
          });
          return false;  
      });
  });

  function success_save(c){
    if(c==0){
        hide('disable-background','success-message');
        deleteAllCookies();
        location.href ="<?php echo $cancel_operation_link?>";
    }else{
        hide('disable-background','success-message');
    }
  }

  function btn_update_click(){
    location.href = "<?php echo base_url()?>index.php/incident/incident_update/<?php echo $detail['id']?>/<?php echo $detail['incident_detail_id']?>/"+$("#opcoid").val();
  }


  function get_sysdate(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = yyyy+'-'+mm+'-'+dd;
     

    $.ajax({
          type: "POST",  
          url: "<?php echo base_url()?>index.php/incident/get_sysdate",  
          contentType: 'application/x-www-form-urlencoded',
          dataType: "text",
          beforeSend: function(){

          },
          complete: function(){
            
          },
          success: function(data){
              today = data;                
          }
    });

    return today;
  }

  function getCookie(c_name) {
      var i,x,y,ARRcookies=document.cookie.split(";");

      for (i=0;i<ARRcookies.length;i++)
      {
          x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
          y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
          x=x.replace(/^\s+|\s+$/g,"");
          if (x==c_name)
          {
              return unescape(y);
          }
       }
  }

  function deleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }
  // working 

  function nevative(){
  }

  function reset_value(){
    $("#c_final_leakage").val(0);
    $("#c_final_leakage_lc").val(0);
    $("#c_final_leakage_display").val(0);
    $("#recovered").val(0);
    $("#recovered_lc").val(0);
    $("#recovered_display").val(0);
    $("#recovery").val(0);
    $("#recovery_lc").val(0);
    $("#recovery_display").val(0);
    $("#incident_leakage").val(0);
    $("#incident_leakage_lc").val(0);
    $("#incident_leakage_display").val(0);
    $("#over_charging").val(0);
    $("#over_charging_lc").val(0);
    $("#over_charging_display").val(0);
    $("#prevented").val(0);
    $("#prevented_lc").val(0);
    $("#prevented_display").val(0);
  }

  function onoff_selection(){

    var leakage_freq = $("#leakage_freq").val();
    var leakage_timing = $("#leakage_timing").val();
    var leakage_id_type = $("#leakage_id_type").val();
    //alert(leakage_freq +' '+leakage_timing+ ' '+leakage_id_type);
    //alert(rut_type);




    switch (leakage_freq){
        case "-1": // please select
                document.getElementById('actual-leakage-block').style.display="none";
                document.getElementById('recovered-block').style.display="block";
                document.getElementById('prevented-block').style.display="block";
                document.getElementById('summary-actual-leakage').style.display="none";
                document.getElementById('summary-recovered').style.display="block";
                document.getElementById('summary-prevented').style.display="block";

                document.getElementById('flag-recursive').style.display="none";
                document.getElementById('incident-leakage-block').style.display="block";

                document.getElementById('summary-incident-leakage').style.display="block";
                document.getElementById('summary-actual-loss').style.display="none";


                break;
        case "1" : // recrusive

                if(rut_type==2){
                    document.getElementById('onoff_rec_freq').style.display="none";
                    //document.getElementById('flag-recursive').style.display="none";
                    
                    //alert("no2");
                }else{
                    document.getElementById('onoff_rec_freq').style.display="block";
                    //document.getElementById('flag-recursive').style.display="block";
                    //alert("selain2");
                }
                switch (leakage_timing){
                    case "-1": // please select
                        document.getElementById('actual-leakage-block').style.display="none";
                        document.getElementById('recovered-block').style.display="block";
                        document.getElementById('prevented-block').style.display="block";
                        document.getElementById('summary-actual-leakage').style.display="none";
                        document.getElementById('summary-recovered').style.display="block";
                        document.getElementById('summary-prevented').style.display="block";

                        document.getElementById('flag-recursive').style.display="block";
                        document.getElementById('incident-leakage-block').style.display="block";

                        document.getElementById('summary-incident-leakage').style.display="block";
                        document.getElementById('summary-actual-loss').style.display="none";
                            break;
                    case "1" : // real-time
                            switch (leakage_id_type){
                                case "-1": // please select
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('flag-recursive').style.display="block";
                                        document.getElementById('incident-leakage-block').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="none";
                                      
                                        

                                        break;
                                case "1" : // proactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="block";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        

                                        break;
                                case "2" : // reactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="block";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        break;
                            }
                            break;
                    case "2" : // post-event
                            switch (leakage_id_type){
                                case "-1": // please select
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="none";
                                        break;
                                case "1" : // proactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="block";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        break;
                                case "2" : // reactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="block";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        break;
                            }
                            break;
                }
                break;
        // focus here ....
        case "2" : // One-off
                switch (leakage_timing){
                    case "-1": // please select 
                            document.getElementById('actual-leakage-block').style.display="none";
                            document.getElementById('recovered-block').style.display="block";
                            document.getElementById('prevented-block').style.display="block";
                            document.getElementById('summary-actual-leakage').style.display="none";
                            document.getElementById('summary-recovered').style.display="block";
                            document.getElementById('summary-prevented').style.display="block";

                            document.getElementById('flag-recursive').style.display="none";
                            document.getElementById('incident-leakage-block').style.display="block";

                            document.getElementById('summary-incident-leakage').style.display="block";
                            document.getElementById('summary-actual-loss').style.display="none";
                            
                            break;
                    case "1" : // real-time
                            switch (leakage_id_type){
                                case "-1": // please select
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="none";
                                        break;
                                case "1" : // proactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="none";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="none";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="none";
                                        document.getElementById('summary-actual-loss').style.display="none";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="none";
                                        break;
                                case "2" : // reactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="none";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="none";
                                        document.getElementById('summary-prevented').style.display="none";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        break;
                            }
                            break;
                    case "2" : // post-event
                            switch (leakage_id_type){
                                case "-1": // please select
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="none";
                                        break;
                                case "1" : // proactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="none";
                                        document.getElementById('prevented-block').style.display="block";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="none";
                                        document.getElementById('summary-prevented').style.display="block";

                                        document.getElementById('summary-incident-leakage').style.display="none";
                                        document.getElementById('summary-actual-loss').style.display="none";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="none";
                                        break;
                                case "2" : // reactive
                                        document.getElementById('actual-leakage-block').style.display="none";
                                        document.getElementById('recovered-block').style.display="block";
                                        document.getElementById('prevented-block').style.display="none";
                                        document.getElementById('summary-actual-leakage').style.display="none";
                                        document.getElementById('summary-recovered').style.display="block";
                                        document.getElementById('summary-prevented').style.display="none";

                                        document.getElementById('summary-incident-leakage').style.display="block";
                                        document.getElementById('summary-actual-loss').style.display="block";

                                        document.getElementById('flag-recursive').style.display="none";
                                        document.getElementById('incident-leakage-block').style.display="block";
                                        break;
                            }
                            break;
                }
                break;
    }
  }    

  <?php
    if($this->session->userdata('group_id')==ADMIN_GROUP_ID && $mode=='VIEW'){
    ?>

    function success_delete(c){
        if(c==0){
            hide('disable-background','delete-msg');
            deleteAllCookies();
            location.href ="<?php echo $cancel_operation_link?>";
        }else{
            hide('disable-background','delete-msg');
        }
    }

  function btn_delete(){
    $.ajax({
        type: "POST",  
        url: "<?php echo base_url()?>index.php/incident/delete_incident",  
        contentType: 'application/x-www-form-urlencoded',
        data: { 
            usr: "<?php echo $this->session->userdata('username')?>",
            pwd: $("#delete-pass").val(),
            sid: "<?php echo session_id()?>",
            iid: "<?php echo $detail['incident_id']?>"
            
        },
        dataType: "text",
        beforeSend: function(){

        },
        complete: function(){
            
        },
        success: function(data){
            $('#delete-pass').val('');
            hide('disable-background','confirm-delete');
            console.log(data);
            var obj = jQuery.parseJSON(data);
            switch(obj.status){
                case 0:
                    document.getElementById('delete-title').innerHTML = obj.desc;
                    document.getElementById('delete-desc').innerHTML = obj.desc;
                    pop('disable-background','delete-msg');
                    document.getElementById("btn-ok-delete").onclick = function() { success_delete(0); }
                    //success_save(0);
                    break;
                case 1:
                    document.getElementById('delete-title').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                    document.getElementById('delete-desc').innerHTML = obj.desc;
                    pop('disable-background','delete-msg');
                    document.getElementById("btn-ok-delete").onclick = function() { success_delete(1); }
                    //success_save(1);
                    break;
                case 2:
                    document.getElementById('delete-title').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                    document.getElementById('delete-desc').innerHTML = obj.desc;
                    pop('disable-background','delete-msg');
                    document.getElementById("btn-ok-delete").onclick = function() { success_delete(2); }
                    //success_save(2);
                    break;
                case 3:
                    document.getElementById('delete-title').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                    document.getElementById('delete-desc').innerHTML = obj.desc;
                    pop('disable-background','delete-msg');
                    document.getElementById("btn-ok-delete").onclick = function() { success_delete(3); }
                    //success_save(2);
                    break;
            }
            
        }
    });
  }

  <?php } ?>

  $(window).load(function(){
      
  });


    $(document).ready(function(){

        <?php
            if($mode=='VIEW' && $open=='attactment') {
        ?>
                
                $('.nav-tabs a[href="#attachment"]').tab('show');
        <?php
            }
        ?>
    });


    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })


    function refresh_rate(){
        $("#c_final_leakage").val($("#c_final_leakage_lc").val()*global_rate);
        $("#c_final_leakage_display").val($("#c_final_leakage_lc").val()*global_rate);
        document.cookie="c_final_leakage_lc="+$("#c_final_leakage_lc").val();
        document.cookie="c_final_leakage="+$("#c_final_leakage").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                            
        $("#incident_leakage").val($("#incident_leakage_lc").val()*global_rate);
        $("#incident_leakage_display").val($("#incident_leakage_lc").val()*global_rate);
        document.cookie="incident_leakage_lc="+$("#incident_leakage_lc").val();
        document.cookie="incident_leakage="+$("#incident_leakage").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                            
        $("#recovery").val($("#recovery_lc").val()*global_rate);
        $("#recovery_display").val($("#recovery_lc").val()*global_rate);

        $("#recovery2").val($("#recovery").val());
        $("#recovery2_lc").val($("#recovery_lc").val());
        $("#recovery2_display").val($("#recovery_display").val());

        document.cookie="recovery="+$("#recovery").val();
        document.cookie="recovery_lc="+$("#recovery_lc").val();
        document.cookie="recovery2="+$("#recovery2").val();
        document.cookie="recovery2_lc="+$("#recovery2_lc").val();
                                            
                                            
        $('#prevented2_lc').val($('#prevented_lc').val());
        $("#prevented").val($("#prevented_lc").val()*global_rate);
        $("#prevented2").val($("#prevented").val());
        $("#prevented_display").val($("#prevented_lc").val()*global_rate);
        $("#prevented2_display").val($("#prevented_display").val());

        document.cookie="prevented="+$("#prevented").val();
        document.cookie="prevented_lc="+$("#prevented_lc").val();
        document.cookie="prevented2="+$("#prevented2").val();
        document.cookie="prevented2_lc="+$("#prevented2_lc").val();
                                            // document.getElementById('cookie2').innerHTML=document.cookie;

                                            
        $("#over_charging").val($("#over_charging_lc").val()*global_rate);
        $("#over_charging_display").val($("#over_charging_lc").val()*global_rate);
        document.cookie="over_charging="+$("#over_charging").val();
        document.cookie="over_charging_lc="+$("#over_charging_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;

        $("#regulated_international_ic_price").val($("#regulated_international_ic_price_lc").val()*global_rate);
        $("#regulated_international_ic_price_display").val($("#regulated_international_ic_price_lc").val()*global_rate);
        document.cookie="regulated_international_ic_price="+$("#regulated_international_ic_price").val();
        document.cookie="regulated_international_ic_price_lc="+$("#regulated_international_ic_price_lc").val();
                                            
        var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
        var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
        var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
        var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

        $("#on_net_price_arbitrage_lc").val(a);
        $("#on_net_price_arbitrage").val(b);
        $("#on_net_price_arbitrage_display").val(b);

        $("#off_net_price_arbitrage_lc").val(c);
        $("#off_net_price_arbitrage").val(d);
        $("#off_net_price_arbitrage_display").val(d);

        $("#average_retail_on_net").val($("#average_retail_on_net_lc").val()*global_rate);
        $("#average_retail_on_net_display").val($("#average_retail_on_net_lc").val()*global_rate);
        document.cookie="average_retail_on_net="+$("#average_retail_on_net").val();
        document.cookie="average_retail_on_net_lc="+$("#average_retail_on_net_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;    
                                                
        var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
        var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
        var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
        var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

        $("#on_net_price_arbitrage_lc").val(a);
        $("#on_net_price_arbitrage").val(b);
        $("#on_net_price_arbitrage_display").val(b);

        $("#off_net_price_arbitrage_lc").val(c);
        $("#off_net_price_arbitrage").val(d);
        $("#off_net_price_arbitrage_display").val(d);
                                            
        $("#on_net_price_arbitrage").val($("#on_net_price_arbitrage_lc").val()*global_rate);
        $("#on_net_price_arbitrage_display").val($("#on_net_price_arbitrage_lc").val()*global_rate);
        document.cookie="on_net_price_arbitrage="+$("#on_net_price_arbitrage").val();
        document.cookie="on_net_price_arbitrage_lc="+$("#on_net_price_arbitrage_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                                
        $("#average_retail_off_net").val($("#average_retail_off_net_lc").val()*global_rate);
        $("#average_retail_off_net_display").val($("#average_retail_off_net_lc").val()*global_rate);
        document.cookie="average_retail_off_net="+$("#average_retail_off_net").val();
        document.cookie="average_retail_off_net_lc="+$("#average_retail_off_net_lc").val();
                                                
        var a = $("#regulated_international_ic_price_lc").val() - $("#average_retail_on_net_lc").val();
        var b = $("#regulated_international_ic_price").val() - $("#average_retail_on_net").val();
        var c = $("#regulated_international_ic_price_lc").val() - $("#average_retail_off_net_lc").val();
        var d = $("#regulated_international_ic_price").val() - $("#average_retail_off_net").val();

        $("#on_net_price_arbitrage_lc").val(a);
        $("#on_net_price_arbitrage").val(b);
        $("#on_net_price_arbitrage_display").val(b);

        $("#off_net_price_arbitrage_lc").val(c);
        $("#off_net_price_arbitrage").val(d);
        $("#off_net_price_arbitrage_display").val(d);
                                            
        $("#off_net_price_arbitrage").val($("#off_net_price_arbitrage_lc").val()*global_rate);
        $("#off_net_price_arbitrage_display").val($("#off_net_price_arbitrage_lc").val()*global_rate);
        document.cookie="off_net_price_arbitrage="+$("#off_net_price_arbitrage").val();
        document.cookie="off_net_price_arbitrage_lc="+$("#off_net_price_arbitrage_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                                
        $("#detection_cost").val($("#detection_cost_lc").val()*global_rate);
        $("#detection_cost_display").val($("#detection_cost_lc").val()*global_rate);
        document.cookie="detection_cost_net="+$("#detection_cost").val();
        document.cookie="detection_cost_lc="+$("#detection_cost_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                            
        ("#recovery2").val($("#recovery2_lc").val()*global_rate);
        $("#recovery2_display").val($("#recovery2_lc").val()*global_rate);

        $("#recovery").val($("#recovery2").val());
        $("#recovery_lc").val($("#recovery2_lc").val());
        $("#recovery_display").val($("#recovery2_display").val());


        document.cookie="recovery="+$("#recovery").val();
        document.cookie="recovery_lc="+$("#recovery_lc").val();
        document.cookie="recovery2="+$("#recovery2").val();
        document.cookie="recovery2_lc="+$("#recovery2_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;
                                            

        $("#prevented2").val($("#prevented2_lc").val()*global_rate);
        $("#prevented2_display").val($("#prevented2_lc").val()*global_rate);

        $("#prevented").val($("#prevented2").val());
        $("#prevented_lc").val($("#prevented2_lc").val());
        $("#prevented_display").val($("#prevented2_display").val());


        document.cookie="prevented="+$("#prevented").val();
        document.cookie="prevented_lc="+$("#prevented_lc").val();
        document.cookie="prevented2="+$("#prevented2").val();
        document.cookie="prevented2_lc="+$("#prevented2_lc").val();
        // document.getElementById('cookie2').innerHTML=document.cookie;    
    }

</script>
