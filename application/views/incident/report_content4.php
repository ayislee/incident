<?php
	$username=UID;
    $password=PWD;
    $URL=SERVICE_URL.'Pages/ReportViewer.aspx?rs:Command=Render';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    $result=curl_exec ($ch);
    //echo $result;
    //$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    $redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );
    echo $redirectURL;
?>        
<style type="text/css">

.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: 1px solid #cccccc;
    background: none;
    font-weight: normal; 
    color: #333; 
}

.ui-widget-content {
    border: 1px solid #dddddd;
    background: white;
    color: #333333;
}


b {
    font-weight: 700;
    float: right;
    margin-top: 7px;
}

.btn {
	min-width: 200px;
	width: auto;

}

.rowcol {
	display: inline-block;
	margin-bottom: 5px;
}

.params-label {
	display: inline-block;
	padding-left: 5px;
	width: 150px;

}

.params {
	padding-right: 5px;
	width: 100px;
}

.params-label {
	padding-right: 5px;
	width: 100px;

}

.btn-preview {
    height: 25px;
    border-radius: 5px;
    border-style: none;
    outline: none;
    vertical-align: top;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 5px;
    background-color: #4a55dc;
    color: white;
}

.ui-widget-header {
    border: 1px solid #4A4C4E;
    background-color: #4A4C4E;
    background: #4A4C4E;
    color: #ffffff;
    font-weight: bold;
}

.ui-state-default .ui-icon {

}

.breadcrumb{
	height: 32px;
	padding-top: 2px;
}

.print-report {
	overflow: auto;
	width: calc(100% - 40px);
	position: relative;
	height: 90%;
	/* zoom: 75%; */
	/* -moz-transform: scale(1); */
	margin-top: 10px;
	margin-left: 20px;
	margin-right: 20px;
	margin-bottom: 0px;
	padding: 5px;
	border-style: solid;
	border-radius: 5px;
	border-width: 1px;
	border-color: #F5F5F5;
	/* resize: both; */
}

.std-select {
	display: inline-block;
}

.msg-export {
	z-index: 3000;
	width: 450px;
	display: none;
	position: fixed;
	top: 30%;
	left: 50%;
	margin-top: -76px;
	margin-left: -163px;
	display: none;
	background-color: white;
	padding-top: 7px;
	padding-bottom: 10px;
}

.w150 {
	width: 150px;
}

.w50 {
	width: 50px !important;
}

</style>

<iframe class="print-report" src="" id="print-report"></iframe>


<script type="text/javascript">

	function handleIFRAME(thestring){ 
     
	    var iframe = document.getElementById('print-report'); 

	    iframe.src = thestring; 
	    //iframe.reload();     

	}

	function preview(){
		$.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/incident/report_credential",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	          		report: "<?php echo $report?>",
	                sid: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
				purl = data;
				console.log(purl);
				handleIFRAME(purl);            
	                
	          }
	    });
	}

	$(document).ready(function(){
		
		preview();
	});	

	

</script>