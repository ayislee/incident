<style type="text/css">
.tab-content > .risk-pane {
    max-height: 475px;
    height: 475px;
}  

.tab-content {
    /* background-color: #ffc711; */
    /* padding: 5px; */
    height: 475px;
    border-top-right-radius: 10px;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    width: 1000px;
}
.opco-symbol {
    display: inline-block;
    width: 30px;
}

.w100{
    width:107px;
}
.w110{
    width:120px;
}
.opco-symbol {
        display: inline-block;
        width: 30px;
    }

.helper {
    vertical-align: middle;
}

.helper-hide{
    display: inline-block;
    width: 10px;
}

.after-close {
    width:405px;
}


.rc1 {
    vertical-align: middle;
}
</style>

<div class="incident-content">
    <ul class="nav nav-tabs">
    	  <li class="active"><a data-toggle="tab" href="#general" class="width161 center21"><?php echo $this->lang->line('leakage')?></a></li>
    	  <li><a data-toggle="tab" href="#risk" class="width161 center21"><?php echo $this->lang->line('risk_tab')?></a></li>
    	  <li><a data-toggle="tab" href="#attachment" class="width161 center21"><?php echo $this->lang->line('Attachment')?></a></li>
    	  <li><a data-toggle="tab" href="#history" class="width161 center21"><?php echo $this->lang->line('History')?></a></li>
	  </ul>

  	<div class="tab-content risk-setting">
  	  	<div id="general" class="tab-pane risk-pane fade in active">
    	  		<div class="column50">
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('Incident ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="incident_num" name="incident_num" value="<?php echo $detail['incident_num']?>" class="std-input  dis width250" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('External ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="external_id" name="external_id" value="<?php echo $detail['external_id']?>" class="std-input  dis width250" disabled>
        	  				</div>
      	  			</div>

                <script type="text/javascript">
                    $("#external_id").change(function(){
                        document.cookie = "external_id="+$("#external_id").val();
                    });
                </script>


      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('OpCo')?></div>
        	  				<div class="rc2">
                        <input type="hidden" id="opcoid" name="opcoid" value="<?php echo $detail['opcoid']?>">
        	  					  <input id="opconame" name="opconame" value="<?php echo $detail['opconame']?>" class="std-input  dis width250" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
      	  				  <div class="rc1"><?php echo $this->lang->line('date_created')?></div>
        	  				<div class="rc2">
        	  					  <input id="create_date" name="create_date" value="<?php echo $detail['create_date']?>" class="std-input  dis width250" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
      	  			    <div class="rc1"><?php echo $this->lang->line('created_by')?></div>
      	  				  <div class="rc2">
      	  					    <input id="create_by" name="create_by" value="<?php echo $detail['create_by']?>" class="std-input width250" type="hidden">
      	  					    <input id="user_create" name="user_create" value="<?php echo $detail['user_create']?>" class="std-input  dis width250" disabled>
      	  				  </div>
      	  			</div>
                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('date_update')?>
                    </div>
                    <div class="rc2">
                        <input id="update_date" name="update_date" value="<?php echo $detail['update_date']?>" class="std-input dis width250" disabled>
                    </div>
                </div>
                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('update_by')?>
                    </div>
                    <div class="rc2">
                        <input id="updated_by" name="updated_by" value="<?php echo $detail['updated_by']?>" class="std-input width250" type="hidden">
                        <input id="user_update" name="user_update" value="<?php echo $detail['user_update']?>" class="std-input  dis width250" disabled>
                    </div>
                </div>
                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('status')?>
                    </div>
                    <div class="rc2">
                        <select class="std-select  dis width250" id="incident_status" name="incident_status" onchange="status_onchange()">
                            <?php switch ($detail['incident_status']) {
                              case '0':
                                $select_status = '';
                                $select_status .= '<option value="0" selected>'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                break;
                              
                              case '1':
                                $select_status = '';
                                $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1" selected>'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                break;
                              case '2':
                                $select_status = '';
                                $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2" selected>'.$this->lang->line('Under Investigation').'</option>';
                                break;
                            }
                            echo $select_status?>
                        </select>
                    </div>
                </div>

                <script type="text/javascript">
                  function status_onchange(){
                      $("#incident_status_detail").val($("#incident_status").val());
                      //alert($("#incident_status").val());
                      document.cookie = "incident_status="+$("#incident_status").val();
                      document.cookie="incident_status_detail="+$("#incident_status_detail").val();
                  }
                </script>
                <style type="text/css">
                .column100 {
                  width: 390px;
                }
                </style>
                <div class="column100">
                  <?php echo $this->lang->line('Description')?>
                </div>

                <div class="column100">

                    <textarea id="description" name="description" class="std-textarea" rows="4"><?php echo $detail['description']?></textarea>
                </div>

                <script type="text/javascript">
                    $("#description").change(function(){
                        document.cookie = "description="+$("#description").val();
                    });
                </script>

                <div class="column100">
                    <?php echo $this->lang->line('Note')?>
                </div>

                <div class="column100" id="note-area">
                    <textarea id="note" name="note" class="std-textarea" rows="4"><?php echo $detail['note']?></textarea>
                </div>

                <script type="text/javascript">
                    $("#note").change(function(){
                        document.cookie = "note="+$("#note").val();
                    });
                </script>

    	  		</div>
  	  		
    	  		<div class="column50">
                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('Impact Severity')?>
                    </div>
                    <div class="helper-hide"></div>
                    <div class="rc2">
                        <select id="impact_severity" name="impact_severity" value="<?php echo $detail['impact_severity']?>" class="std-select width250 dis ">
                            <?php for ($i=1; $i<= 10 ; $i++) {
                              if($i== $detail['impact_severity']) $is = 'selected'; else $is = '';?>
                            <option value="<?php echo $i?>" <?php echo $is?>><?php echo $i?></option>
                            <?php }?>

                        </select>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#impact_severity').change(function(){
                        document.cookie="impact_severity="+$("#impact_severity").val();
                        //document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1"><?php echo $this->lang->line('Detection Date')?></div>
                    <div class="helper-hide"></div>
                    <div class="rc2">
                        <input type="text" id="detection_date" name="detection_date" class="std-input  dis width250" value="<?php echo $detail['detection_date']?>" readonly='true'>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#detection_date').change(function(){
                        document.cookie="detection_date="+$("#detection_date").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                        var ddate = new Date($("#detection_date").val());
                        var selected_year = ddate.getFullYear();
                        var selected_month = ddate.getMonth()+1;

                        Get_Rate(selected_year,selected_month,<?php echo $opcoid?>);

                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1"><?php echo $this->lang->line('Case Start Date')?></div>
                    <div class="helper-hide"></div>
                    <div class="rc2">
                        <input type="text" id="case_start_date" name="case_start_date" class="std-input  dis width250" value="<?php echo $detail['case_start_date']?>" readonly='true'>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#case_start_date').change(function(){
                        document.cookie="case_start_date="+$("#case_start_date").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1"><?php echo $this->lang->line('Case End Date')?></div>
                    <div class="helper-hide"></div>
                    <div class="rc2">
                        <input type="text" id="case_end_date" name="case_end_date" class="std-input  dis width250" value="<?php echo $detail['case_end_date']?>" readonly='true'>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#case_end_date').change(function(){
                        document.cookie="case_end_date="+$("#case_end_date").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1"><?php echo $this->lang->line('actual_leakage')?></div>
                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                    <div class="rc2">

                        <input type="text" id="c_final_leakage_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['c_final_leakage_lc']?>">
                        <div class="opco-symbol"><?php echo $symbol?></div>
                        <input type="text" id="c_final_leakage_display" name="text" class="std-input  dis w100" value="<?php echo $detail['c_final_leakage']?>" disabled readonly>
                        <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                        <input type="hidden" id="c_final_leakage" name="number"  value="<?php echo $detail['c_final_leakage']?>" readonly>
                        <div class="opco-symbol"></div>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#c_final_leakage_lc').focusout(function(){
                        $("#c_final_leakage").val($("#c_final_leakage_lc").val()*global_rate);
                        $("#c_final_leakage_display").val($("#c_final_leakage_lc").val()*global_rate);
                        document.cookie="c_final_leakage_lc="+$("#c_final_leakage_lc").val();
                        document.cookie="c_final_leakage="+$("#c_final_leakage").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;

                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1"><?php echo $this->lang->line('Recovery')?></div>

                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                    <div class="rc2">
                        <input type="text" id="recovery_lc" name="number" class="std-input  dis w100" value="<?php echo $detail['recovery_lc']?>">
                        <div class="opco-symbol"><?php echo $symbol?></div>
                        <input type="text" id="recovery_display" name="text" class="std-input  dis w100" value="<?php echo $detail['recovery']?>" disabled readonly>
                        <div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
                        <input type="hidden" id="recovery" name="number"  value="<?php echo $detail['recovery']?>" readonly>
                        <div class="opco-symbol"></div>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#recovery_lc').focusout(function(){
                        $("#recovery").val($("#recovery_lc").val()*global_rate);
                        $("#recovery_display").val($("#recovery_lc").val()*global_rate);
                        document.cookie="recovery="+$("#recovery").val();
                        document.cookie="recovery_lc="+$("#recovery_lc").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('Leakage Frequency')?>
                    </div>
                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                    <div class="rc2">
                        <select id="leakage_freq" name="leakage_freq" value="<?php echo $detail['leakage_freq']?>" class="std-select dis width250">
                            <?php foreach ($leakage_freq->result() as $row) {
                            if(intval($detail['leakage_freq'])==$row->id) $is = 'selected'; else $is = '';
                            echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                            }?>
                        </select>

                    </div>
                </div>

                <script type="text/javascript">
                    $('#leakage_freq').change(function(){
                        document.cookie="leakage_freq="+$("#leakage_freq").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('Leakage Timing')?>
                    </div>
                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                    <div class="rc2">
                        <select id="leakage_timing" name="leakage_timing" value="<?php echo $detail['leakage_timing']?>" class="std-select  dis width250">
                            <?php foreach ($leakage_timing->result() as $row) {
                              if(intval($detail['leakage_timing'])==$row->id) $is = 'selected'; else $is = '';
                              echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                            }?>
                        </select>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#leakage_timing').change(function(){
                        document.cookie="leakage_timing="+$("#leakage_timing").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

                <div class="rows">
                    <div class="rc1">
                        <?php echo $this->lang->line('Leakage ID Type')?>
                    </div>
                    <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                    <div class="rc2">
                        <select id="leakage_id_type" name="leakage_id_type" value="<?php echo $detail['leakage_id_type']?>" class="std-select dis  width250">
                            <?php foreach ($leakage_id_type->result() as $row) {
                              if(intval($detail['leakage_id_type'])==$row->id) $is = 'selected'; else $is = '';
                              echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                            }?>
                        </select>
                    </div>
                </div>

                <script type="text/javascript">
                    $('#leakage_id_type').change(function(){
                        document.cookie="leakage_id_type="+$("#leakage_id_type").val();
                        // document.getElementById('cookie2').innerHTML=document.cookie;
                    });
                    
                </script>

              <div class="rows">
                  <div class="rc1">
                      <?php echo $this->lang->line('Recovery Type')?>
                  </div>
                  <i class="fa fa-question-circle helper" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('tooltip')?>"></i>
                  <div class="rc2">
                      <select id="recovery_type" name="recovery_type" value="<?php echo $detail['recovery_type']?>" class="std-select width250 dis ">
                          <?php foreach ($recovery_type->result() as $row) {
                            if(intval($detail['recovery_type'])==$row->id) $is = 'selected'; else $is = '';
                            echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                          }?>
                      </select>
                  </div>
              </div>

              <script type="text/javascript">
                  $('#recovery_type').change(function(){
                      document.cookie="recovery_type="+$("#recovery_type").val();
                      // document.getElementById('cookie2').innerHTML=document.cookie;
                  });
                  
              </script>

              
              <input id="incident_status_detail" name="incident_status_detail" value="<?php echo $detail['incident_status_detail']?>" type="hidden">





              <?php
                  if($detail['c_final_leakage']==null) $c_final_leakage = ''; else $c_final_leakage = number_format($detail['c_final_leakage'],0);
                  if($detail['c_recovered']==null) $c_recovered = ''; else $c_recovered = number_format($detail['c_recovered'],0);
                  if($detail['c_prevented']==null) $c_prevented = ''; else $c_prevented = number_format($detail['c_prevented'],0);
                  if($detail['c_potential']==null) $c_potential = ''; else $c_potential = number_format($detail['c_potential'],0);

                  if($detail['c_final_leakage_lc']==null) $c_final_leakage_lc = ''; else $c_final_leakage_lc = number_format($detail['c_final_leakage_lc'],0);
                  if($detail['c_recovered_lc']==null) $c_recovered_lc = ''; else $c_recovered_lc = number_format($detail['c_recovered_lc'],0);
                  if($detail['c_prevented_lc']==null) $c_prevented_lc = ''; else $c_prevented_lc = number_format($detail['c_prevented_lc'],0);
                  if($detail['c_potential_lc']==null) $c_potential_lc = ''; else $c_potential_lc = number_format($detail['c_potential_lc'],0);
              ?>
              <div class="row">
                  <div class="after-close">
  <?php
    //echo $detail['incident_status'];
    if( (int) $detail['incident_status']==1){ 

        $d = $c_final_leakage;
        $d_lc = $c_final_leakage_lc;
    }else{
        $d = '';
        $d_lc = '';
        
    }
  ?>
                      <div class="rows2">
                            <div class="fc-1"><?php echo $this->lang->line('actual_leakage')?></div>
                            <div class="fc-2 w110" ><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$d?></div>
                            <div class="fc-2 w110" ><?php echo $symbol.' '.$d_lc?></div>
                      </div>
                      <div class="rows2">
                            <div class="fc-1"><?php echo $this->lang->line('Recovered Value')?></div>
                            <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_recovered?></div>
                            <div class="fc-2 w110"><?php echo  $symbol.' '.$c_recovered_lc?></div>
                      </div>
                      <div class="rows2">
                            <div class="fc-1"><?php echo $this->lang->line('Prevented Value')?></div>
                            <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_prevented?></div>
                            <div class="fc-2 w110"><?php echo  $symbol.' '.$c_prevented_lc?></div>
                      </div>
                      <div class="rows2">
                            <div class="fc-1"><?php echo $this->lang->line('Potential Leakage')?></div>
                            <div class="fc-2 w110"><?php echo DEFAULT_CURRENCY_SYMBOL.' '.$c_potential?></div>
                            <div class="fc-2 w110"><?php echo  $symbol.' '.$c_potential_lc?></div>
                      </div>
                  </div>
              </div>



    	  		</div>
  	  	</div>

  	<div id="risk" class="tab-pane fade risk-pane">
    		<div class="column50">
      			<div class="rows">
        				<div class="rc1">
        					<?php echo $this->lang->line('Detection Tool')?>
        				</div>
        				<div class="rc2">
        					<select id="detection_tool" name="detection_tool" class="std-select  dis width250">
        					<?php 
        					$option_dt = '';
        					foreach ($select_detection_tool->result() as $row) {
        						# code...
        						if($detail['detection_tool']==$row->dtid) $dt_selected = 'selected'; else $dt_selected ='';
        						$option_dt .= '<option value="'.$row->dtid.'" '.$dt_selected.'>'.$row->dtname.'</option>';
        					}
        					echo $option_dt;
        					?>
        					</select>
        					
        				</div>
        		</div>
            <script type="text/javascript">
                $("#detection_tool").change(function(){
                    document.cookie = "detection_tool="+$("#detection_tool").val();
                    //alert(document.cookie);
                });
            </script>

      			<div class="rows">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Risk Universe Type')?>
        				</div>
        				<div class="rc2">
          					<select id="rutid" name="rutid" class="std-select  dis width250" onchange="onchange_rut()">
          					<?php 
                          
                          $option_rut = '';
                          foreach ($select_rut->result() as $row) {
                            # code...
                            if($detail['rutid']==$row->rutid) $rut_selected = 'selected'; else $rut_selected ='';
                            $option_rut .= '<option value="'.$row->rutid.'" '.$rut_selected.'>'.$row->rutname.'</option>';
                          }
                          echo $option_rut;
                        
          					?>
          					</select>
        				</div>
      			</div>
      			<script type="text/javascript">
              function onchange_rut(){
                  if(list_rc.length>0){
                      pop('disable-background','rut-confirm');
                  }else{
                    change_rut();
                    document.cookie="rutid="+$("#rutid").val();
                    //document.getElementById('cookie2').innerHTML=document.cookie;
                  }
              }

      				function change_rut(){
                //alert(JSON.stringify(list_rc));
                //alert(list_rc);
                list_rc = [];
                document.cookie='list_rc='+JSON.stringify(list_rc);
                document.getElementById("rc-data").innerHTML = '';
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_st",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                rutid: $("#rutid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                //alert(data);
    			                document.getElementById('stid').innerHTML = data;
    			                change_st();
    			                change_oc();
                          //alert(document.cookie);
                          //alert($("#stid").val());
                          document.cookie="stid="+$("#stid").val();
                          document.cookie="ocid="+$("#ocid").val();
                          
    			            }
    			        });
      				}
      			</script>

      			<div class="rows">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Section Title')?>
        				</div>
        				<div class="rc2">
        				    <select id="stid" name="stid" class="std-select  dis width250" onchange="change_st()">
          					<?php  
          					$option_st = '';
          					foreach ($select_st->result() as $row) {
          						# code...
          						if($detail['stid']==$row->stid) $st_selected = 'selected'; else $st_selected ='';
          						$option_st .= '<option value="'.$row->stid.'" '.$st_selected.'>'.$row->stname.'</option>';
          					}
          					echo $option_st;
          					?>
        					   </select>
        				</div>
      			</div>
      			<script type="text/javascript">
      				function change_st(){
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_oc",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                stid: $("#stid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                //alert(data);
    			                document.getElementById('ocid').innerHTML = data;
    			                change_oc();
                          
    			            }
    			        });
                 document.cookie="stid="+$("#stid").val();
                 //alert($("#stid").val());
                 //document.getElementById('cookie2').innerHTML=document.cookie; 

      				}

      			</script>

      			<div class="rows" style="display:none">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Operator Category')?>
        				</div>
        				<div class="rc2">
          					<select id="ocid" name="ocid" class="std-select  dis width250" onchange="change_oc()">
              					<?php 
              					$option_oc = '';
              					foreach ($select_oc->result() as $row) {
              						# code...
              						if($detail['ocid']==$row->ocid) $oc_selected = 'selected'; else $oc_selected ='';
              						$option_oc .= '<option value="'.$row->ocid.'" '.$oc_selected.'>'.$row->ocname.'</option>';
              					}
              					echo $option_oc;
              					?>
        					 </select>
        				</div>
      			</div>

      			<script type="text/javascript">
      				function change_oc(){
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_rc",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                ocid: $("#ocid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                
    			                document.getElementById('rcid').innerHTML = data;
                          
                          //alert(document.cookie);
    			            }
    			        });

                document.cookie="ocid="+$("#ocid").val();
                //alert($("#stid").val());
                //document.getElementById('cookie2').innerHTML=document.cookie;
                
      				}
      			</script>

      			<div class="rows">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Risk')?>
        				</div>
        				<div class="rc2">
          					<select id="rcid" name="rcid" class="std-select  dis width250">
              					<?php 
              					$option_rc = '';
              					foreach ($select_rc->result() as $row) {
              						# code...
              						$option_rc .= '<option value="'.$row->rcid.'" >'.$row->rcname.'</option>';
              					}
              					echo $option_rc;
              					?>
          					</select>
          					<button id="btn-add-rc" onclick="add_rc()">+</button>
        				</div>
      			</div>


      			<div class="rows">
    	  			  <div class="rc-data-area" id="rc-data"></div>
      			</div>

            <div class="rows">
                <div class="rc1"><?php echo $this->lang->line('Revenue Stream')?></div>
                <div class="rc2">
                    <!--
                    <input type="number" id="revenue_stream" name="revenue_stream" class="std-input  dis width250" value="<?php echo $detail['revenue_stream']?>">
                    -->
                    <select id="revenue_stream" class="std-select  dis width250">
                    <?php
                        foreach ($select_revenue_stream->result() as $row) {
                            # code...
                            if($detail['revenue_stream']==$row->id){
                                $selected="selected";
                            }else{
                                $selected='';
                            }
                            echo '<option value="'.$row->id.'" '.$selected.'>'.$row->revenue_stream_name.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <script type="text/javascript">
                $('#revenue_stream').change(function(){
                    document.cookie="revenue_stream="+$("#revenue_stream").val();
                    // document.getElementById('cookie2').innerHTML=document.cookie;
                });
                
            </script>

    		</div>

  		<!-- Column 2 -->
  		<div class="column50">
  			


    			

    			

    			


          

            
          

            


  		</div>
  	</div>

            <div id="attachment" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <div class="table-head">
                        <div class="table-head-item col-no"><?php echo $this->lang->line('No')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Date Time')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('User')?></div>
                        <div class="table-head-item col-filename"><?php echo $this->lang->line('Filename')?></div>
                        <div class="table-head-item col-desc"><?php echo $this->lang->line('File Description')?></div>
                    </div>
                    <div id="fa">
                    <?php
                    $no = 1;
                    if(sizeof($attachment)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Files').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($attachment as $row) { ?>
                        <div class="table-body">
                            <div class="table-body-item col-no"><?php echo $no ?></div>
                            <div class="table-body-item col-username"><?php echo $row['correct_date']?></div>
                            <div class="table-body-item col-username"><a href="#" onclick="show_user('<?php echo $row['corerct_by']?>','<?php echo $row['first_name']?>','<?php echo $row['last_name']?>','<?php echo $row['email']?>')"><?php echo $row['user_name']?></a></div>
                            <div class="table-body-item col-filename"><a href="<?php echo base_url()?>index.php/incident/download/<?php echo $row['file_id']?>/<?php echo session_id()?>" target="_blank"><?php echo $row['name']?></a></div>
                            <div class="table-body-item col-desc"><?php echo $row['file_description']?></div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    </div>
                    <script type="text/javascript">
                        function show_user(id,first_name,last_name,email){
                            document.getElementById('contact-full-name').innerHTML='Full name: '+first_name+' '+last_name;
                            document.getElementById('contact-email').innerHTML='Email: '+email;
                            pop('disable-background','show-contact');
                        }
                    </script> 
                    <div class="navigator-2">
                        <div class="command">
                            <button class="std-btn bkgr-blue" onclick="pop('disable-background','upload-form')" id="btn-upload"><?php echo $this->lang->line('UPLOAD')?></button>
                        </div>
                    </div>
                </div>
                
            </div>

          	<div id="history" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <div class="table-head">
                        <div class="table-head-item col-no"><?php echo $this->lang->line('No')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Date Time')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Action')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('User')?></div>
                        <div class="table-head-item col-username"><?php echo $this->lang->line('Comments')?></div>
                    </div>
                    
                    <?php

                    $no = 1;
                    if(sizeof($history)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Histories').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($history as $row) { ?>
                        <div class="table-body">
                            <div class="table-body-item col-no"><?php echo $no?></div>
                            <div class="table-body-item col-username"><?php echo $row['correct_date']?></div>
                            <div class="table-body-item col-username"><?php echo $row['action']?></div>
                            <div class="table-body-item col-username"><?php echo $row['user_name']?></div>
                            <div class="table-body-item col-username"><?php echo $row['note']?></div>
                            <div class="table-body-item col-username"><a class= "btn btn-info std-btn alink" style="height:24px" role="button" href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row['incident_id']?>/<?php echo $row['id']?>"><?php echo $this->lang->line('view')?></a></div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    
                </div>
                <div class="navigator-2">
                    
                </div>
          	</div>
        </div>	
        <div class="navigator-2">
            <div class="command">

                <button class="std-btn bkgr-green" onclick="btn_save_click()" id="btn-save"><?php echo $this->lang->line('save')?></button>
                <?php if($update_incident) {?>
                <button class="std-btn bkgr-blue" onclick="btn_update_click()" id="btn-modify"><?php echo $this->lang->line('Update')?></button>
                <?php } ?>
                <button class="std-btn bkgr-red" onclick="btn_cancel_click()" id="btn-cancel"><?php echo $this->lang->line('cancel')?></button>
            
            </div>
        </div>
</div>


<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div id="confirm-save" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('Confirm')?></div>
        <div class="confirm-message"><?php echo $this->lang->line('Are you sure to update this incident ?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-green" onclick="save_incident()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-green" onclick="hide('disable-background','confirm-save')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>

<div id="error-message" class="add-access-module" >
    <div class="modify-title"><?php echo $this->lang->line('Error Message')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('Please fill in all fields')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','error-message')"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="cancel-message" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('Are you sure to cancel the operation?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="prev_page()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','cancel-message')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>


<div id="upload-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload attacment')?>
    </div>
    <form name="form-upload" id="form-upload" >
        <div class="content-message">
            <input type="file" name="file-attachment" id="file-attachment" class="margin-left30" />
            <div class="label-text"><?php echo $this->lang->line('File Description')?></div>
            <textarea id="file-description" name="file-description"  class="std-textarea" rows="4"></textarea>
        </div>
        <div class="confirm-btn">
            <button type="submit" class="std-btn bkgr-green"><?php echo $this->lang->line('Upload')?></button>
            <button type="button" class="std-btn bkgr-red" onclick="cancel_upload()"><?php echo $this->lang->line('cancel')?></button>
        </div>
    </form>

</div>

<div id="success-message" class="error-message">
    <div class="modify-title" id="title-msg"><?php echo $this->lang->line('Create Incident Succes')?></div>
    <div class="confirm-message" id="content-msg"><?php echo $this->lang->line('Incident already saved')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-success-message" onclick="success_save()"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="rut-confirm" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('rut reset')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="change_rut();hide('disable-background','rut-confirm');"><?php echo $this->lang->line('yes')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','rut-confirm')"><?php echo $this->lang->line('No')?></button>
    </div>
</div>

<div id="show-contact" class="show-contact">
    <div class="modify-title"><?php echo $this->lang->line('Contact')?></div>
    <div class="contact-message" id="contact-full-name"></div>
    <div class="contact-message" id="contact-email"></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','show-contact');"><?php echo $this->lang->line('ok')?></button>
        
    </div>
</div>


<div id="json"></div>
<div id="cookie"></div>
<div id="cookie2"></div>
<script type="text/javascript">
	var list_rc = [];
	var obj_rc;
  var mode = "<?php echo $mode?>";
  var attachment = [];
  var incident_status = <?php echo $detail['incident_status']?>;
  var group = <?php echo $this->session->userdata('group_id')?>;
  var global_rate = <?php echo $rate?>;

  $("#recovery_display").number(true,0);
  $("#recovery_lc").number(true,0);
  $("#c_final_leakage_display").number(true,0);
  $("#c_final_leakage_lc").number(true,0);




  function Get_Rate(year,month,opcoid){
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/get_rate",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                year: year,
                month: month,
                currency_id:<?php echo $currency_id?>,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //alert(data);
                global_rate = data;
                //alert(data);
                
            }
        });
  }

  function get_st(rutid){
      document.getElementById("rc-data").innerHTML = '';
      $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/get_st",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                rutid: rutid,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //alert(data);
                document.getElementById('stid').innerHTML = data;
                
            }
        });      
  }

  function get_oc(stid){
    //alert(stid);

    $.ajax({
        type: "POST",  
        url: "<?php echo base_url()?>index.php/incident/get_oc",  
        contentType: 'application/x-www-form-urlencoded',
        data: { 
            stid: 13,
            sess: "<?php echo session_id()?>"
        },
        dataType: "text",
        beforeSend: function(){

        },
        complete: function(){
            
        },
        success: function(data){
            //alert(data);
            document.getElementById('ocid').innerHTML = data;
            
            
        }
    });


  }

	$(document).ready(function(){
      // read cookie -----

      //alert('<?php echo $detail["opcoid"] ?>');     



  		<?php foreach ($categories as $cats) {

  			# code...
  			$jvs = 'obj_rc = { incident_detail_id: "'.$cats['incident_detail_id'].'", rcid: "'.$cats['rcid'].'",rcname: "'.$cats['rcname'].'"};';
  			$jvs .= 'list_rc.push(obj_rc);';
  			echo $jvs;
  		}?>
  		//json_rc = JSON.stringify(list_rc);
  		//alert(list_rc[1].incident_detail_id);
  		
  		for(i=0;i<list_rc.length;i++){
    			//document.getElementById("rc-data").appendChild("haha");
    			insert_div = '<div class="rc-items">';
    			insert_div += list_rc[i].rcname;
    			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
    			insert_div += '</div>';
    			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
  		}

      switch(mode){
          case "NEW":
              document.getElementById('external_id').disabled=false;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              document.getElementById('btn-save').style.display="inline-block";
              <?php if($update_incident) {?>              
              document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";
              
              var opco_cookie = getCookie('opco_id');
              if(opco_cookie != '<?php echo $detail["opcoid"] ?>'){
                deleteAllCookies();
                location.reload();

              }
              document.cookie = "opco_id=<?php echo $detail['opcoid']?>";
              //alert(get_sysdate());
              //$("#detection_date").val(get_sysdate());
              //$("#case_start_date").val(get_sysdate());




              break;
          case "UPDATE":
              document.getElementById('external_id').disabled=true;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('incident_status_detail').disabled=false;              
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              document.getElementById('btn-save').style.display="inline-block"; 
              <?php if($update_incident) {?>             
              document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";
               $("#note").val('');

              break;
          case "VIEW":

              document.getElementById('external_id').disabled=true;
              
              document.getElementsByName("incident_status").disabled=true;
              document.getElementById('incident_status_detail').disabled=true;
              $('[name=incident_status]').attr('disabled','disabled');
              document.getElementById('description').disabled=true;
              document.getElementById('note').disabled=true;

              document.getElementById('btn-save').style.display="none";
              
              <?php if($update_incident) { ?>
                if(incident_status==0 || incident_status==2 || group==<?php echo ADMIN_GROUP_ID?>){              
                    document.getElementById('btn-modify').style.display="inline-block";
                }else{
                    document.getElementById('btn-modify').style.display="none";
                }
              <?php } else {?>
                // document.getElementById('btn-modify').style.display="none";
              <?php } ?>
              
              document.getElementById('btn-cancel').style.display="none";
              document.getElementById('btn-upload').style.display="none";

              document.getElementById('detection_tool').disabled=true;
              document.getElementById('rutid').disabled=true;
              document.getElementById('stid').disabled=true;
              document.getElementById('ocid').disabled=true;
              document.getElementById('rcid').disabled=true;
              document.getElementById('btn-add-rc').disabled=true;

              document.getElementsByClassName("btn-rc").disabled=true;
              $(".btn-rc").hide().attr('disabled','disabled');
              document.getElementById('btn-add-rc').style.display="none";

              
              document.getElementById('revenue_stream').disabled=true;
              document.getElementById('impact_severity').disabled=true;
              document.getElementById('detection_date').disabled=true;
              document.getElementById('case_start_date').disabled=true;
              document.getElementById('case_end_date').disabled=true;
              document.getElementById('c_final_leakage').disabled=true;
              document.getElementById('c_final_leakage_lc').disabled=true;
              document.getElementById('recovery').disabled=true;
              document.getElementById('recovery_lc').disabled=true;
              document.getElementById('leakage_freq').disabled=true;
              document.getElementById('leakage_timing').disabled=true;
              document.getElementById('leakage_id_type').disabled=true;
              document.getElementById('recovery_type').disabled=true;

              break;
      }
		
	});


  function pop(div,div2) {
    document.getElementById(div).style.display = 'block';
    document.getElementById(div2).style.display = 'block';

  }
  function hide(div,div2) {
    document.getElementById(div).style.display = 'none';
    document.getElementById(div2).style.display = 'none';
  }
  
  function search(nameKey, myArray){
      found = false;
      for (var i=0; i < myArray.length; i++) {
          if (myArray[i].rcid == nameKey) {
              found = true;
          }
      }
      return found;
  }

	function add_rc(){
		// find same rcid
        a = search($("#rcid").val(),list_rc);

        if(!a){
            insert_rc = { incident_detail_id: "<?php $id?>",rcid: $("#rcid").val(), rcname: document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text};
            list_rc.push(insert_rc);
            last_num = list_rc.length -1 ;
            insert_div = '<div class="rc-items">';
            insert_div += document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text;
            insert_div += '<button class="btn-rc" onclick="del_rc('+last_num+')">x</button>';
            insert_div += '</div>';
            document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
            document.cookie="list_rc="+JSON.stringify(list_rc);
            //document.getElementById('cookie2').innerHTML=document.cookie;

        }else{
          alert("<?php echo $this->lang->line('Risk Category already selected')?>");
        }


	}

	function del_rc(num){
		list_rc.splice(num,1);
		// redraw;
		document.getElementById("rc-data").innerHTML = '';
		for(i=0;i<list_rc.length;i++){
			//document.getElementById("rc-data").appendChild("haha");
			insert_div = '<div class="rc-items">';
			insert_div += list_rc[i].rcname;
			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
			insert_div += '</div>';
			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
      document.cookie="list_rc="+JSON.stringify(list_rc);
      //document.getElementById('cookie2').innerHTML=document.cookie;
		}
	}

<?php if($mode != 'VIEW') {?>
	$(function() {
	    $( "#detection_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_start_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_end_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });



<?php } ?>
  function btn_save_click(){

      var detection_date = $("#detection_date").val();
      var case_start_date = $("#case_start_date").val();

      if(detection_date > case_start_date){
        //alert('lebih besar');
      }else{
        //alert('lebih kecil');
      }

      if($("#external_id").val().trim()=='' || 
         $("#description").val().trim()=='' ||
         $("#note").val().trim()=='' ) {
         // $("#c_final_leakage").val().trim()=='' ||
         // $("#leakage_one_day_lc").val().trim()=='' ||
         // $("#recovery").val().trim()=='' || 
         // $("#recovery_lc").val().trim()=='' ) {
          pop('disable-background','error-message');
      }else{
        // code here 

        //var car = {type:"Fiat", model:"500", color:"white"};        

        <?php if($mode=='NEW'){?>
        var incident = { 
          incident_id: "", 
          external_id: $("#external_id").val(),
          opcoid: $("#opcoid").val(),
          opconame: $('#opconame').val(),
          date_created: $("#date_created").val(),
          create_by: $("create_by").val(),
          date_update: $("#date_update").val(),
          updated_by: $("#updated_by").val(),
          incident_status: $("#incident_status").val(),
          incident_status_detail: $("#incident_status_detail").val(),

          incident_detail_id: "",
          description: $("#description").val(),
          note: $("#note").val(),
          detection_tool : $("#detection_tool").val(),
          rutid: $("#rutid").val(),
          stid: $("#stid").val(),
          ocid: $("#ocid").val(),
          revenue_stream: $("#revenue_stream").val(),
          impact_severity: $("#impact_severity").val(),
          detection_date: $("#detection_date").val(),
          case_start_date: $("#case_start_date").val(),
          case_end_date: $("#case_end_date").val(),
          c_final_leakage: $("#c_final_leakage").val(),
          c_final_leakage_lc: $("#c_final_leakage_lc").val(),
          recovery: $("#recovery").val(),
          recovery_lc: $("#recovery_lc").val(),
          leakage_freq: $("#leakage_freq").val(),
          leakage_timing: $("#leakage_timing").val(),
          leakage_id_type: $("#leakage_id_type").val(),
          recovery_type: $("#recovery_type").val(),
          list_rc: list_rc,
          attachment: attachment
        };

        json_incident = JSON.stringify(incident);
        //alert(json_incident);
        console.log(json_incident);
        //document.getElementById('json').innerHTML = json_incident;
        document.getElementById('disable-background2').style.display = 'block';
        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  document.getElementById('disable-background2').style.display = 'none';

                  var obj = jQuery.parseJSON(data);
                  //alert(data);
                  console.log(data); 
                  switch(obj.status){
                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";
                          pop('disable-background','success-message');
                          document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          break;
                      default:
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });  
      <?php }else {?>
        var incident = { 
          incident_id: "<?php echo $detail['id']?>", 
          external_id: $("#external_id").val(),
          opcoid: $("#opcoid").val(),
          opconame: $('#opconame').val(),
          date_created: $("#date_created").val(),
          create_by: $("create_by").val(),
          date_update: $("#date_update").val(),
          updated_by: $("#updated_by").val(),
          incident_status: $("#incident_status").val(),
          incident_status_detail: $("#incident_status_detail").val(),

          incident_detail_id: "",
          description: $("#description").val(),
          note: $("#note").val(),
          detection_tool : $("#detection_tool").val(),
          rutid: $("#rutid").val(),
          stid: $("#stid").val(),
          ocid: $("#ocid").val(),
          revenue_stream: $("#revenue_stream").val(),
          impact_severity: $("#impact_severity").val(),
          detection_date: $("#detection_date").val(),
          case_start_date: $("#case_start_date").val(),
          case_end_date: $("#case_end_date").val(),
          c_final_leakage: $("#c_final_leakage").val(),
          c_final_leakage_lc: $("#c_final_leakage_lc").val(),            
          recovery: $("#recovery").val(),
          recovery_lc: $("#recovery_lc").val(),          
          leakage_freq: $("#leakage_freq").val(),
          leakage_timing: $("#leakage_timing").val(),
          leakage_id_type: $("#leakage_id_type").val(),
          recovery_type: $("#recovery_type").val(),
          list_rc: list_rc,
          attachment: attachment
        };
        document.getElementById('disable-background2').style.display = 'block';

        json_incident = JSON.stringify(incident);
        //alert(json_incident);
        //document.getElementById('json').innerHTML = json_incident;
        console.log(json_incident);
        
        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  document.getElementById('disable-background2').style.display = 'none';

                  var obj = jQuery.parseJSON(data);
                  //alert(data);
                  //console.log(data);                  
                  switch(obj.status){

                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";

                          pop('disable-background','success-message');
                          document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          break;
                      default:
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });

      <?php } ?>      
        
      }
  }

  function btn_cancel_click(){

      pop('disable-background','cancel-message');

  }

  function prev_page(){
      deleteAllCookies();
      location.href="<?php echo $cancel_operation_link?>";
  }

  function do_upload() {
      //alert('jsv');
      var data = new FormData($('#form-upload')[0]);

      if($("#file-description").val()==''){
          alert('Please type file description');
      }else {
          $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/upload_attachment",  
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              data: data,
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                    
              },
              success: function(data){
                    //location.reload();
                    alert(data);
                    
              }
          });          
      }
      
  }
  function cancel_upload(){
      hide('disable-background','upload-form');
      return false;
  }

  $(function() {
      $('#form-upload').submit(function(e) {
          hide('disable-background','upload-form');
          document.getElementById('disable-background2').style.display = 'block';

          e.preventDefault();
          $.ajaxFileUpload({
              url             :"<?php echo base_url()?>index.php/incident/upload_attachment", 
              secureuri       :false,
              fileElementId   :'file-attachment',
              dataType: 'JSON',
              beforeSend: function(){
                  
              },
              complete: function(){

              },
              success : function (data){
                  document.getElementById('disable-background2').style.display = 'none';
                  //alert(data);
                  var obj = jQuery.parseJSON(data);
                  if(obj.status=='success'){
                      fa = {
                          incident_detail_id: "",
                          file_id: "",
                          file_description: $("#file-description").val(),
                          type: obj.filetype,
                          name: obj.filename,
                          fullpath: obj.fullpath
                          }

                      attachment.push(fa);
                      num_file = attachment.length;
                      tbl_attachment = '';
                      for(i=0;i<attachment.length;i++){
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth()+1; //January is 0!
                          var yyyy = today.getFullYear();
                          var hh = today.getHours();
                          var mm = today.getMinutes();
                          var ss = today.getSeconds();

                          if(dd<10){
                              dd='0'+dd
                          } 
                          if(mm<10){
                              mm='0'+mm
                          } 
                          var today = yyyy+'-'+mm+'-'+dd+' '+hh+':'+mm+':'+ss;
                          var user = "<?php echo $this->session->userdata('username')?>";
                          //var desc = 

                          tbl_attachment += '<div class="table-body">';
                          tbl_attachment += '   <div class="table-body-item col-no">'+(i+1)+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-username">'+today+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-username">'+user+'</div>';
                          tbl_attachment += '   <div class="table-body-item col-filename">'+(attachment[i].name)+'</div>';    
                          tbl_attachment += '   <div class="table-body-item col-desc">'+attachment[i].file_description+'</div>';
                          tbl_attachment += '</div>'; 

                      }
                      //alert(tbl_attachment);
                      document.getElementById('fa').innerHTML = tbl_attachment;
                  }else{
                      alert(obj.msg);
                  }
              }
          });
          return false;  
      });
  });

  function success_save(c){
    if(c==0){
        hide('disable-background','success-message');
        deleteAllCookies();
        location.href ="<?php echo $cancel_operation_link?>";
    }else{
        hide('disable-background','success-message');
    }
  }

  function btn_update_click(){
    location.href = "<?php echo base_url()?>index.php/incident/incident_update/<?php echo $detail['id']?>/<?php echo $detail['incident_detail_id']?>/"+$("#opcoid").val();
  }


  function get_sysdate(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = yyyy+'-'+mm+'-'+dd;
     

    $.ajax({
          type: "POST",  
          url: "<?php echo base_url()?>index.php/incident/get_sysdate",  
          contentType: 'application/x-www-form-urlencoded',
          dataType: "text",
          beforeSend: function(){

          },
          complete: function(){
            
          },
          success: function(data){
              today = data;                
          }
    });

    return today;
  }

  function getCookie(c_name) {
      var i,x,y,ARRcookies=document.cookie.split(";");

      for (i=0;i<ARRcookies.length;i++)
      {
          x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
          y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
          x=x.replace(/^\s+|\s+$/g,"");
          if (x==c_name)
          {
              return unescape(y);
          }
       }
  }

  function deleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

  $(window).load(function(){
      //alert('loaded');
  });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })


</script>
