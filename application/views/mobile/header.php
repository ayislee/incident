<div class="header fixed">
    <button id="showLeft" class="menu-bar">
      <img class="img-menu-bar"src="<?php echo base_url()?>assets/images/menu.svg" >
    </button>
    
    <img class ="logo-white" src="<?php echo base_url()?>assets/images/logo.png" >
    <div class="switch-lang">
      <select onchange="select_language()" name="select_lang" id="select_lang">
          <option value="english" <?php if($language == 'english') echo 'selected="selected"'; ?>>English</option>
          <option value="french" <?php if($language == 'french') echo 'selected="selected"'; ?>>French</option>
          <option value="russian" <?php if($language == 'russian') echo 'selected="selected"'; ?>>Russian</option> 
      </select>
      <script type="text/javascript">
        function select_language(){
          var language = document.getElementById("select_lang"); 
          //alert('<?php echo base_url()?>');
          this.document.location.href = "<?php echo base_url()?>index.php/LanguageSwitcher/index/"+language.value;
        }
      </script>
    </div> 
</div><!-- /o-wrapper -->

<script type="text/javascript">
$(window).on("orientationchange",function(){
  if(screen.height > 360){
    
  }
});

</script>

