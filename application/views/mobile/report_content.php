<div class="user-search2">
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('OpCo')?>		
		</div>
		<div class="user-search-item" >
			<select class="std-input" id="filter-opco" name="filter-opco">
				<?php foreach ($opco_list->result() as $row) {
					# code...
					echo '<option value="'.$row->opcoid.'">'.$row->opconame.'</option>';
				}?>
			</select>
		</div>
	</div>
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('Year')?>		
		</div>
		<div class="user-search-item">
			<select class="std-input" id="filter-year" name="filter-year">
				<option>2010</option>
				<option>2011</option>
				<option>2013</option>
				<option>2014</option>
				<option>2015</option>
				<option>2016</option>
			</select>
		</div>
		

	</div>
</div>