<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Incdent Management')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/mobile.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.mmenu.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.mmenu.dragopen.css" />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/hammer.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.mmenu.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.mmenu.dragopen.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.mmenu.fixedelements.min.js"></script>


	</head>

	<body>
		<div id="page">
			<?php echo $header ?>
			<?php echo $sidemenu?>
			<div class="main-content">
				<?php echo $title ?>
				<?php echo $breadcrumb ?>
				<?php echo $content ?>
				<?php //echo $user_table ?>
			</div>
		
		
			<div class="content"></div>
		</div>
	</body>
</html>