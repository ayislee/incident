<div class="user-search2">
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('Search')?>		
		</div>
		<div class="std-input" >
			<input name="id-search" id="id-search">
		</div>
	</div>
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('status')?>		
		</div>
		<div class="user-search-item">
			<?php echo $select_status?>
		</div>
		
		<div class="user-search-item">
			<button class="std-btn bkgr-green" style="height: 28px" id="btn-filter"><?php echo $this->lang->line('Filter')?></button>
		</div>
	</div>
</div>

<div class="user-table">

	<?php

	if($data->num_rows()>0){	
		$number = (($page-1)*ROW_PER_PAGE) + 1; 
		foreach ($data->result() as $row) {?>
	<div class="table-body">
		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('No')?></div>
			<div class="table-body-item c2"><?php echo $number?></div>
		</div>
		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('Incident ID')?></div>
			<div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row->id?>"><?php echo $row->incident_num?></a></div>
		</div>

		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('External ID')?></div>
			<div class="table-body-item c2"><?php echo $row->external_id?></div>
		</div>

		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('date_created')?></div>
			<div class="table-body-item c2"><?php echo $row->create_date?></div>
		</div>

		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('Last Update')?></div>
			<div class="table-body-item c2"><?php echo $row->create_date?></div>
		</div>

		<?php
			switch ($row->incident_status) {
			 	case '0':
			 		$dsp = $this->lang->line('Open');
			 		break;
			 	
			 	case '1':
			 		$dsp = $this->lang->line('Close');
			 		break;

			 	case '2':
			 		$dsp = $this->lang->line('Under Investigation');
			 		break;

			 } 
		?>

		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('status')?></div>
			<div class="table-body-item c2"><?php echo $dsp?></div>
		</div>
	</div>
	<?php
		$number++; 
		}
	}else{?>
	<div class="table-body center21">
		<?php echo $this->lang->line('No Records')?>
	</div>
	<?php }?>

	<div class="navigator">
		<?php echo $pagination ?>
	</div>

</div>


<div class="footer">
	<div class="command">
		<?php if($this->session->userdata('get_privilege')[5]['map_group']){ ?>
		<button class="std-btn bkgr-green" id="btn-add"><?php echo $this->lang->line('add')?></button>
		<?php }?>
		<?php if($this->session->userdata('get_privilege')[4]['map_group']){ ?>
		<button class="std-btn bkgr-blue" id="btn-upload"><?php echo $this->lang->line('BULK UPLOAD')?></button>
		<button class="std-btn bkgr-blue" id="btn-upload" onclick="pop('disable-background','help-form')"><?php echo $this->lang->line('?')?></button>
		<?php }?>
	</div>
</div>

<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div id="upload-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload bulk file')?>
    </div>
    <form name="form-upload" id="form-upload" >
        <div class="content-message">
            <input type="file" name="file-attachment" id="file-attachment" class="margin-left30" />
        </div>
        <div class="confirm-btn">
            <button type="submit" class="std-btn bkgr-green"><?php echo $this->lang->line('Upload')?></button>
            <button type="button" class="std-btn bkgr-red" onclick="hide('disable-background','upload-form')"><?php echo $this->lang->line('cancel')?></button>
        </div>
    </form>

</div>

<div id="help-form" class="add-access-module3">
    <div class="modify-title">
        <?php echo $this->lang->line('Example Upload bulk CSV file')?>
    </div>
    
    <div class="content-message">
    	<div class="center21" style="font-weight:bold;"><?php echo $this->lang->line('Examples')?></div>
        <?php $example = '<ExternalID>,<OpCoID>,<Detection Tool>,<Risk Universe Type>,<Section Title>,<Operator Category>,<RiskCat>,<Impact Severity>,<Detection Date>,<Case Start Date>, <Case End Date>,<Leakage One Day>,<Recovery>,<Leakage Freq>,<Leakage Timing>,<Leakage ID Type>,<Recovery Type>, <Description>,<Investigation Note>

829882075,Algeria,FMS,RA (Revenue Assurance),Order Management and Provisioning,Mobile,"Failure to meet contract volume commitment,Ported-in number not billed,Incorrect capture of equipment sales",5,1012016,1012016,31012016,10000,100000,Recursive,Real-Time,Proactive,Recovered,"Incident reported by FMS, pending detailed investigation.",Confirmed and create incident
';?>
        <textarea class="std-textarea" rows="10" disabled><?php echo $example?></textarea>
    </div>
    <div class="center21">
        <button type="button" class="std-btn bkgr-blue" onclick="hide('disable-background','help-form')"><?php echo $this->lang->line('ok')?></button>
    </div>
   

</div>


<div id="uploading" class="add-access-module2">
    <div class="content-message2" id="upload-message">
    	<?php echo $this->lang->line('Uploading file ... please wait')?>
    </div>
    <div class="confirm-btn" style="display:none" id="btn-area">
    	<button class="std-btn bkgr-green" onclick="hide('disable-background2','uploading');"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>

<div id="success-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload Result')?>
    </div>
    
    <div class="content-message">
    	<div class="center21" style="font-weight:bold;" id="success-result"></div>
    </div>

    <div class="confirm-btn">
        <button type="button" class="std-btn bkgr-blue" onclick="location.reload()"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>

<div id="error-form" class="add-access-module3">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload Result')?>
    </div>

    <div class="content-message">
        <textarea class="std-textarea" rows="10" disabled id="error-result"></textarea>
    </div>

    <div class="confirm-btn">
        <button type="button" class="std-btn bkgr-blue" onclick="hide('disable-background','error-form')"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>


<script type="text/javascript">
	$("#btn-filter").click(function(){
		location.href = "<?php echo base_url()?>index.php/incident/incident_table/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val());
	});

	$("#btn-add").click(function(){
		location.href = "<?php echo base_url()?>index.php/incident/new_incident/<?php echo $opcoid?>";
	});

	$("#btn-upload").click(function(){
		pop('disable-background','upload-form');
	});

  function pop(div,div2) {
    document.getElementById(div).style.display = 'block';
    document.getElementById(div2).style.display = 'block';

  }
  function hide(div,div2) {
    document.getElementById(div).style.display = 'none';
    document.getElementById(div2).style.display = 'none';
  }

  $(function() {
      $('#form-upload').submit(function(e) {
          hide('disable-background','upload-form');
          document.getElementById('disable-background2').style.display = 'block';
          document.body.style.cursor='wait';
          document.getElementById('uploading').style.display = 'block';


          e.preventDefault();
          $.ajaxFileUpload({
              url             :"<?php echo base_url()?>index.php/incident/upload_bulk", 
              secureuri       :false,
              fileElementId   :'file-attachment',
              dataType: 'JSON',
              beforeSend: function(){
                  
              },
              complete: function(){

              },
              success : function (data){
              		

				var obj = jQuery.parseJSON(data);
				//alert(obj.fullpath);
				
				if(obj.status=="success"){
						document.getElementById('upload-message').innerHTML = "<?php echo $this->lang->line('process_bulk')?>";
						$.ajax({
				        type: "POST",  
				        url: "<?php echo base_url()?>index.php/incident/analyst_csv",  
				        contentType: 'application/x-www-form-urlencoded',
				        data: { 
				            filename: obj.fullpath,
				            sess: "<?php echo session_id()?>"
				        },
				        dataType: "text",
				        beforeSend: function(){

				        },
				        complete: function(){
				            
				        },
				        success: function(data){
				            //location.reload();
				            hide('disable-background2','uploading');
				            document.body.style.cursor='default';
				            var status = jQuery.parseJSON(data);
				            //alert(status.msg);
				            if(status.status){
				            	document.getElementById('success-result').innerHTML = status.msg; 
				            	pop('disable-background','success-form');
				            }else{
				            	document.getElementById('error-result').innerHTML = status.msg; 
				            	pop('disable-background','error-form');
				            }
				            //location.reload();
				            //stateChange(-1);

				        }
				    });


				}else{
						document.body.style.cursor='default';
						document.getElementById('upload-message').innerHTML = obj.msg;
						document.getElementById('btn-area').style.display = 'block';
				}
                 
              }
        });
        return false;  
      });
  });

	function stateChange(newState) {
	    setTimeout(function(){
	        if(newState == -1){alert('VIDEO HAS STOPPED');}
	    }, 3000);
	}
</script>