<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Incdent Management')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >

		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/component.css" />
		<script src="<?php echo base_url()?>assets/js/modernizr.custom.js"></script>
		<script src="<?php echo base_url()?>assets/js/classie.js"></script>
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/mobile.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>



	</head>
	<body class="cbp-spmenu-push">
<nav id="sidemenu" class="cbp-spmenu">
  <ul>
    <li >
    	<a href="<?php echo base_url()?>index.php/incident/incident_management" class="menu-link" style="">
    		<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
    		<?php echo $this->lang->line('Incident Management'); ?>
    	</a>
    </li>
    <li>
    	<a href="#report" class="menu-link" style="">
    		<i class="fa fa-bar-chart" aria-hidden="true"></i>
    		<?php echo $this->lang->line('Report'); ?>
    	</a>
    	<ul>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('MFS Report'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('RA % Leakage'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    		<li>
    			<a href="<?php echo base_url()?>index.php/incident/report" class="menu-link indent10"><?php echo $this->lang->line('FM Leakage per Month'); ?></a>
    		</li>
    	</ul>
    </li>

  </ul>
</nav><!-- /c-menu slide-left -->

	
		<div class="container">
			<?php echo $header?>
			<div class="content" id="content">
				<?php echo $title ?>
				<?php echo $breadcrumb ?>
				<?php echo $content ?>
			</div>
			<?php echo $sidemenu?>
		</div>
	</body>
</html>