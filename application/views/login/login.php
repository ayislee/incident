<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />

		<title>Login</title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	</head>
	<style type="text/css">

	</style>
	<body>
		
		<div class="login-content">
			<div class="login-logo" align="center">
				<img class="login-logo" width="100px" src="<?php echo base_url()?>assets/images/Logo-Veon.svg">
			</div>
			<div class="login-title" align="center">
				Group Revenue Assurance & Fraud <br>
				Management Reporting Portal
			</div>
			<div class="module-title">
				<?php 
				if($this->session->userdata('loaded_module')=='2'){
					echo $this->lang->line('Incident Module');
				}else{
					echo $this->lang->line('Administrator Module');
				}
				?>
				<form method="post" action="<?php echo $action ?>">
					<br>
					<?php echo $language_switcher?>
					<br>
					<br>
					<div class="login_title"><?php echo $this->lang->line('please_login')?></div>
					<input class="login-input" name="username" id="username" type="text" placeholder="<?php echo $this->lang->line('username')?>" autocomplete="off">
					<input class="login-input" name="password" id="password" type="password" placeholder="<?php echo $this->lang->line('password')?>">
					<button class="login-btn"><?php echo $this->lang->line('login')?></button>

				</form>
			</div>

			<div id="forgot-password">
				<a href="<?php echo base_url()?>index.php/login/reset_password"><?php echo $this->lang->line("forget_password")?></a>
			</div>
			
			<br>
				<?php echo $message ?>
			<br>

		</div>
	</body>
</html>

<script type="text/javascript">
	function deleteAllCookiesGlobal() {
	    var cookies = document.cookie.split(";");

	    for (var i = 0; i < cookies.length; i++) {
	        var cookie = cookies[i];
	        var eqPos = cookie.indexOf("=");
	        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
	        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	    }
	}

	$(document).ready(function(){
		deleteAllCookiesGlobal();
	});
		

</script>