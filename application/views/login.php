<?php echo $language_switcher?>
<br>
<?php echo $message ?>
<br>
<br>
<form method="post" action="<?php echo $action ?>">
	<div>
		<div><?php echo $this->lang->line('username'); ?></div>
		<div><input name="username" id="username" type="text"></div>
		<div><?php echo $this->lang->line('password'); ?></div>
		<div><input name="password" id="password" type="password"></div>
		<button><?php echo $this->lang->line('login'); ?></button>
	</div>
</form> 

