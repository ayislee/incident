<style type="text/css">
	.w-cc{
		width: 180px;
	}	

	.view-header {
		display: inline-block;
	}

	.opco-symbol {
		display: inline-block;
		width: 30px;
	}

	.special {
		display: inline-block;
		text-align: right;
	}

</style>
<div class="opco-content" >
	<div class="label-item-var" ><?php echo $this->lang->line('OpCo')?></div>
	<div class="view-header" id="opco-name-view"></div>
	<input id="var-opco-id" type="hidden" value="<?php echo $opco_id?>">
	
	<div class="label-item align-right"><?php echo $this->lang->line('Month')?></div>
	<div class="view-header" id="month-view"></div>
	<input id="var-month" type="hidden" value="<?php echo $month?>">

	<div class="label-item align-right"><?php echo $this->lang->line('Year')?></div>
	<div class="view-header" id="year-view"></div>
	<input id="var-year" type="hidden" value="<?php echo $year?>">
	<!--<button class="std-btn bkgr-green text-right" onclick="reload_page()" id="btn-get-config"><?php echo $this->lang->line('Get Opco Configuration')?></button> -->
</div>


<div class="opco-content">
	<ul class="nav nav-tabs">
	  <li  style="display: none"><a data-toggle="tab" href="#general" class="tab-constant"><?php echo $this->lang->line('General')?></a></li>
	  <li class="active"><a data-toggle="tab" href="#cc" class="tab-constant w-cc"><?php echo $this->lang->line('Control Coverage')?></a></li>
	  <li style="display: none"><a data-toggle="tab" href="#bypass" class="tab-constant"><?php echo $this->lang->line('ByPass')?></a></li>
	  <li style="display: none"><a data-toggle="tab" href="#mfs" class="tab-constant"><?php echo $this->lang->line('MFS')?></a></li>
	  <li><a data-toggle="tab" href="#currency" class="tab-constant"><?php echo $this->lang->line('Currency')?></a></li>
	</ul>

	<div class="tab-content">
	  <div class="deviden"></div>
	  <div id="general" class="tab-pane fade">

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[0]['id']?>" id="id-<?php echo $config[0]['id']?>" value="<?php echo $config[0]['id']?>">
	  			<input type="hidden" name="<?php echo $config[0]['variable']?>" id="<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[1]['id']?>" id="id-<?php echo $config[1]['id']?>" value="<?php echo $config[1]['id']?>">
	  			<input type="hidden" name="<?php echo $config[1]['variable']?>" id="<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['variable']?>">
	  			<?php echo $config[0]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[0]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[1]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[2]['id']?>" id="id-<?php echo $config[2]['id']?>" value="<?php echo $config[2]['id']?>">
	  			<input type="hidden" name="<?php echo $config[2]['variable']?>" id="<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[3]['id']?>" id="id-<?php echo $config[3]['id']?>" value="<?php echo $config[3]['id']?>">
	  			<input type="hidden" name="<?php echo $config[3]['variable']?>" id="<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['variable']?>">
	  			<?php echo $config[2]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[2]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[3]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['value']?>" disabled>

	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[4]['id']?>" id="id-<?php echo $config[4]['id']?>" value="<?php echo $config[4]['id']?>">
	  			<input type="hidden" name="<?php echo $config[4]['variable']?>" id="<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[5]['id']?>" id="id-<?php echo $config[5]['id']?>" value="<?php echo $config[5]['id']?>">
	  			<input type="hidden" name="<?php echo $config[5]['variable']?>" id="<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['variable']?>">
	  			<?php echo $config[4]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[4]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[5]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[64]['id']?>" id="id-<?php echo $config[64]['id']?>" value="<?php echo $config[64]['id']?>">
	  			<input type="hidden" name="<?php echo $config[64]['variable']?>" id="<?php echo $config[64]['variable']?>" value="<?php echo $config[64]['variable']?>">
	  			<?php echo $config[6]['desc']?>

	  			<input type="hidden" name="id-<?php echo $config[6]['id']?>" id="id-<?php echo $config[6]['id']?>" value="<?php echo $config[6]['id']?>">
	  			<input type="hidden" name="<?php echo $config[6]['variable']?>" id="<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['variable']?>">


	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[64]['variable']?>" value="<?php echo $config[64]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>



	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[65]['id']?>" id="id-<?php echo $config[65]['id']?>" value="<?php echo $config[65]['id']?>">
	  			<input type="hidden" name="<?php echo $config[65]['variable']?>" id="<?php echo $config[65]['variable']?>" value="<?php echo $config[65]['variable']?>">
	  			
	  			<input type="hidden" name="id-<?php echo $config[7]['id']?>" id="id-<?php echo $config[7]['id']?>" value="<?php echo $config[7]['id']?>">
	  			<input type="hidden" name="<?php echo $config[7]['variable']?>" id="<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['variable']?>">
	  			<?php echo $config[7]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[65]['variable']?>" value="<?php echo $config[65]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[8]['id']?>" id="id-<?php echo $config[8]['id']?>" value="<?php echo $config[8]['id']?>">
	  			<input type="hidden" name="<?php echo $config[8]['variable']?>" id="<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['variable']?>">
	  			<?php echo $config[8]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[66]['id']?>" id="id-<?php echo $config[66]['id']?>" value="<?php echo $config[66]['id']?>">
	  			<input type="hidden" name="<?php echo $config[66]['variable']?>" id="<?php echo $config[66]['variable']?>" value="<?php echo $config[66]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[9]['id']?>" id="id-<?php echo $config[9]['id']?>" value="<?php echo $config[9]['id']?>">
	  			<input type="hidden" name="<?php echo $config[9]['variable']?>" id="<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['variable']?>">
	  			
	  			<?php echo $config[9]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[66]['variable']?>" value="<?php echo $config[66]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[10]['id']?>" id="id-<?php echo $config[10]['id']?>" value="<?php echo $config[10]['id']?>">
	  			<input type="hidden" name="<?php echo $config[10]['variable']?>" id="<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['variable']?>">
	  			<?php echo $config[10]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[67]['id']?>" id="id-<?php echo $config[67]['id']?>" value="<?php echo $config[67]['id']?>">
	  			<input type="hidden" name="<?php echo $config[67]['variable']?>" id="<?php echo $config[67]['variable']?>" value="<?php echo $config[67]['variable']?>">
	  			<?php echo $config[11]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[11]['id']?>" id="id-<?php echo $config[11]['id']?>" value="<?php echo $config[11]['id']?>">
	  			<input type="hidden" name="<?php echo $config[11]['variable']?>" id="<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[67]['variable']?>" value="<?php echo $config[67]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[68]['id']?>" id="id-<?php echo $config[68]['id']?>" value="<?php echo $config[68]['id']?>">
	  			<input type="hidden" name="<?php echo $config[68]['variable']?>" id="<?php echo $config[68]['variable']?>" value="<?php echo $config[68]['variable']?>">
	  			<?php echo $config[12]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[12]['id']?>" id="id-<?php echo $config[12]['id']?>" value="<?php echo $config[12]['id']?>">
	  			<input type="hidden" name="<?php echo $config[12]['variable']?>" id="<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[68]['variable']?>" value="<?php echo $config[68]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[13]['id']?>" id="id-<?php echo $config[13]['id']?>" value="<?php echo $config[13]['id']?>">
	  			<input type="hidden" name="<?php echo $config[13]['variable']?>" id="<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['variable']?>">
	  			<?php echo $config[13]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[69]['id']?>" id="id-<?php echo $config[69]['id']?>" value="<?php echo $config[69]['id']?>">
	  			<input type="hidden" name="<?php echo $config[69]['variable']?>" id="<?php echo $config[69]['variable']?>" value="<?php echo $config[69]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[69]['variable']?>" value="<?php echo $config[69]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<!-- Extend -->
	    <div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[58]['id']?>" id="id-<?php echo $config[58]['id']?>" value="<?php echo $config[58]['id']?>">
	  			<input type="hidden" name="<?php echo $config[58]['variable']?>" id="<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['variable']?>">
	  			<?php echo $config[58]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[70]['id']?>" id="id-<?php echo $config[70]['id']?>" value="<?php echo $config[70]['id']?>">
	  			<input type="hidden" name="<?php echo $config[70]['variable']?>" id="<?php echo $config[70]['variable']?>" value="<?php echo $config[70]['variable']?>">
	  			
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[70]['variable']?>" value="<?php echo $config[70]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[59]['id']?>" id="id-<?php echo $config[59]['id']?>" value="<?php echo $config[59]['id']?>">
	  			<input type="hidden" name="<?php echo $config[59]['variable']?>" id="<?php echo $config[59]['variable']?>" value="<?php echo $config[71]['variable']?>">
	  			<?php echo $config[59]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[71]['id']?>" id="id-<?php echo $config[71]['id']?>" value="<?php echo $config[59]['id']?>">
	  			<input type="hidden" name="<?php echo $config[71]['variable']?>" id="<?php echo $config[71]['variable']?>" value="<?php echo $config[71]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[71]['variable']?>" value="<?php echo $config[71]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[59]['variable']?>" value="<?php echo $config[59]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>
	  	<!-- End Extend -->

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[14]['id']?>" id="id-<?php echo $config[14]['id']?>" value="<?php echo $config[14]['id']?>">
	  			<input type="hidden" name="<?php echo $config[14]['variable']?>" id="<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[15]['id']?>" id="id-<?php echo $config[15]['id']?>" value="<?php echo $config[15]['id']?>">
	  			<input type="hidden" name="<?php echo $config[15]['variable']?>" id="<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['variable']?>">
	  			<?php echo $config[14]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[72]['id']?>" id="id-<?php echo $config[72]['id']?>" value="<?php echo $config[72]['id']?>">
	  			<input type="hidden" name="<?php echo $config[72]['variable']?>" id="<?php echo $config[72]['variable']?>" value="<?php echo $config[72]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[73]['id']?>" id="id-<?php echo $config[73]['id']?>" value="<?php echo $config[73]['id']?>">
	  			<input type="hidden" name="<?php echo $config[73]['variable']?>" id="<?php echo $config[73]['variable']?>" value="<?php echo $config[73]['variable']?>">
	  			
	  		</div>
	  		<div class="value-item"><?php echo $config[14]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[72]['variable']?>" value="<?php echo $config[72]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[15]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[73]['variable']?>" value="<?php echo $config[73]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[16]['id']?>" id="id-<?php echo $config[16]['id']?>" value="<?php echo $config[16]['id']?>">
	  			<input type="hidden" name="<?php echo $config[16]['variable']?>" id="<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[17]['id']?>" id="id-<?php echo $config[17]['id']?>" value="<?php echo $config[17]['id']?>">
	  			<input type="hidden" name="<?php echo $config[17]['variable']?>" id="<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['variable']?>">
	  			<?php echo $config[16]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[74]['id']?>" id="id-<?php echo $config[74]['id']?>" value="<?php echo $config[74]['id']?>">
	  			<input type="hidden" name="<?php echo $config[74]['variable']?>" id="<?php echo $config[74]['variable']?>" value="<?php echo $config[74]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[75]['id']?>" id="id-<?php echo $config[75]['id']?>" value="<?php echo $config[75]['id']?>">
	  			<input type="hidden" name="<?php echo $config[75]['variable']?>" id="<?php echo $config[75]['variable']?>" value="<?php echo $config[75]['variable']?>">

	  		</div>
	  		<div class="value-item"><?php echo $config[16]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[74]['variable']?>" value="<?php echo $config[74]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[17]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[75]['variable']?>" value="<?php echo $config[75]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>
	  	<!-- sampai disini  -->
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[18]['id']?>" id="id-<?php echo $config[18]['id']?>" value="<?php echo $config[18]['id']?>">
	  			<input type="hidden" name="<?php echo $config[18]['variable']?>" id="<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[19]['id']?>" id="id-<?php echo $config[19]['id']?>" value="<?php echo $config[19]['id']?>">
	  			<input type="hidden" name="<?php echo $config[19]['variable']?>" id="<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['variable']?>">
	  			<?php echo $config[18]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[76]['id']?>" id="id-<?php echo $config[76]['id']?>" value="<?php echo $config[76]['id']?>">
	  			<input type="hidden" name="<?php echo $config[76]['variable']?>" id="<?php echo $config[76]['variable']?>" value="<?php echo $config[76]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[77]['id']?>" id="id-<?php echo $config[77]['id']?>" value="<?php echo $config[77]['id']?>">
	  			<input type="hidden" name="<?php echo $config[77]['variable']?>" id="<?php echo $config[77]['variable']?>" value="<?php echo $config[77]['variable']?>">
	  			
	  		</div>
	  		<div class="value-item"><?php echo $config[18]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[76]['variable']?>" value="<?php echo $config[76]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[19]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[77]['variable']?>" value="<?php echo $config[77]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[20]['id']?>" id="id-<?php echo $config[20]['id']?>" value="<?php echo $config[20]['id']?>">
	  			<input type="hidden" name="<?php echo $config[20]['variable']?>" id="<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[21]['id']?>" id="id-<?php echo $config[21]['id']?>" value="<?php echo $config[21]['id']?>">
	  			<input type="hidden" name="<?php echo $config[21]['variable']?>" id="<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['variable']?>">
	  			<?php echo $config[20]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[78]['id']?>" id="id-<?php echo $config[78]['id']?>" value="<?php echo $config[78]['id']?>">
	  			<input type="hidden" name="<?php echo $config[78]['variable']?>" id="<?php echo $config[78]['variable']?>" value="<?php echo $config[78]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[79]['id']?>" id="id-<?php echo $config[79]['id']?>" value="<?php echo $config[79]['id']?>">
	  			<input type="hidden" name="<?php echo $config[79]['variable']?>" id="<?php echo $config[79]['variable']?>" value="<?php echo $config[79]['variable']?>">
	  		</div>
	  		<div class="value-item"><?php echo $config[20]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[78]['variable']?>" value="<?php echo $config[78]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[21]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[79]['variable']?>" value="<?php echo $config[79]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[22]['id']?>" id="id-<?php echo $config[22]['id']?>" value="<?php echo $config[22]['id']?>">
	  			<input type="hidden" name="<?php echo $config[22]['variable']?>" id="<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[23]['id']?>" id="id-<?php echo $config[23]['id']?>" value="<?php echo $config[23]['id']?>">
	  			<input type="hidden" name="<?php echo $config[23]['variable']?>" id="<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['variable']?>">
	  			<?php echo $config[22]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[80]['id']?>" id="id-<?php echo $config[80]['id']?>" value="<?php echo $config[80]['id']?>">
	  			<input type="hidden" name="<?php echo $config[80]['variable']?>" id="<?php echo $config[80]['variable']?>" value="<?php echo $config[80]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[81]['id']?>" id="id-<?php echo $config[81]['id']?>" value="<?php echo $config[81]['id']?>">
	  			<input type="hidden" name="<?php echo $config[81]['variable']?>" id="<?php echo $config[81]['variable']?>" value="<?php echo $config[81]['variable']?>">
	  		</div>
	  		<div class="value-item"><?php echo $config[22]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[80]['variable']?>" value="<?php echo $config[80]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[23]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[81]['variable']?>" value="<?php echo $config[81]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[24]['id']?>" id="id-<?php echo $config[24]['id']?>" value="<?php echo $config[24]['id']?>">
	  			<input type="hidden" name="<?php echo $config[24]['variable']?>" id="<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[25]['id']?>" id="id-<?php echo $config[25]['id']?>" value="<?php echo $config[25]['id']?>">
	  			<input type="hidden" name="<?php echo $config[25]['variable']?>" id="<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['variable']?>">
	  			<?php echo $config[24]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[82]['id']?>" id="id-<?php echo $config[82]['id']?>" value="<?php echo $config[82]['id']?>">
	  			<input type="hidden" name="<?php echo $config[82]['variable']?>" id="<?php echo $config[82]['variable']?>" value="<?php echo $config[82]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[83]['id']?>" id="id-<?php echo $config[83]['id']?>" value="<?php echo $config[83]['id']?>">
	  			<input type="hidden" name="<?php echo $config[83]['variable']?>" id="<?php echo $config[83]['variable']?>" value="<?php echo $config[83]['variable']?>">
	  		</div>
	  		<div class="value-item"><?php echo $config[24]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[82]['variable']?>" value="<?php echo $config[82]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[25]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[83]['variable']?>" value="<?php echo $config[83]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[26]['id']?>" id="id-<?php echo $config[26]['id']?>" value="<?php echo $config[26]['id']?>">
	  			<input type="hidden" name="<?php echo $config[26]['variable']?>" id="<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[27]['id']?>" id="id-<?php echo $config[27]['id']?>" value="<?php echo $config[27]['id']?>">
	  			<input type="hidden" name="<?php echo $config[27]['variable']?>" id="<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['variable']?>">
	  			<?php echo $config[26]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[84]['id']?>" id="id-<?php echo $config[84]['id']?>" value="<?php echo $config[84]['id']?>">
	  			<input type="hidden" name="<?php echo $config[84]['variable']?>" id="<?php echo $config[84]['variable']?>" value="<?php echo $config[84]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[85]['id']?>" id="id-<?php echo $config[85]['id']?>" value="<?php echo $config[85]['id']?>">
	  			<input type="hidden" name="<?php echo $config[85]['variable']?>" id="<?php echo $config[85]['variable']?>" value="<?php echo $config[85]['variable']?>">
	  		</div>
	  		<div class="value-item"><?php echo $config[26]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[84]['variable']?>" value="<?php echo $config[84]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<div class="width98"><?php echo $config[27]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[85]['variable']?>" value="<?php echo $config[85]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>


	  </div>

	  <div id="bypass" class="tab-pane fade">

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[28]['id']?>" id="id-<?php echo $config[28]['id']?>" value="<?php echo $config[28]['id']?>">
	  			<input type="hidden" name="<?php echo $config[28]['variable']?>" id="<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['variable']?>">
	  			<?php echo $config[28]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[29]['id']?>" id="id-<?php echo $config[29]['id']?>" value="<?php echo $config[29]['id']?>">
	  			<input type="hidden" name="<?php echo $config[29]['variable']?>" id="<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['variable']?>">
	  			<?php echo $config[29]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[30]['id']?>" id="id-<?php echo $config[30]['id']?>" value="<?php echo $config[30]['id']?>">
	  			<input type="hidden" name="<?php echo $config[30]['variable']?>" id="<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['variable']?>">
	  			<?php echo $config[30]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[31]['id']?>" id="id-<?php echo $config[31]['id']?>" value="<?php echo $config[31]['id']?>">
	  			<input type="hidden" name="<?php echo $config[31]['variable']?>" id="<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['variable']?>">
	  			<?php echo $config[31]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[32]['id']?>" id="id-<?php echo $config[32]['id']?>" value="<?php echo $config[32]['id']?>">
	  			<input type="hidden" name="<?php echo $config[32]['variable']?>" id="<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['variable']?>">
	  			<?php echo $config[32]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[33]['id']?>" id="id-<?php echo $config[33]['id']?>" value="<?php echo $config[33]['id']?>">
	  			<input type="hidden" name="<?php echo $config[33]['variable']?>" id="<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['variable']?>">
	  			<?php echo $config[33]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[86]['id']?>" id="id-<?php echo $config[86]['id']?>" value="<?php echo $config[86]['id']?>">
	  			<input type="hidden" name="<?php echo $config[86]['variable']?>" id="<?php echo $config[86]['variable']?>" value="<?php echo $config[86]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[86]['variable']?>" value="<?php echo $config[86]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[34]['id']?>" id="id-<?php echo $config[34]['id']?>" value="<?php echo $config[34]['id']?>">
	  			<input type="hidden" name="<?php echo $config[34]['variable']?>" id="<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['variable']?>">
	  			<?php echo $config[34]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[87]['id']?>" id="id-<?php echo $config[87]['id']?>" value="<?php echo $config[87]['id']?>">
	  			<input type="hidden" name="<?php echo $config[87]['variable']?>" id="<?php echo $config[87]['variable']?>" value="<?php echo $config[87]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[87]['variable']?>" value="<?php echo $config[87]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[35]['id']?>" id="id-<?php echo $config[35]['id']?>" value="<?php echo $config[35]['id']?>">
	  			<input type="hidden" name="<?php echo $config[35]['variable']?>" id="<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['variable']?>">
	  			<?php echo $config[35]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[88]['id']?>" id="id-<?php echo $config[88]['id']?>" value="<?php echo $config[88]['id']?>">
	  			<input type="hidden" name="<?php echo $config[88]['variable']?>" id="<?php echo $config[88]['variable']?>" value="<?php echo $config[88]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[88]['variable']?>" value="<?php echo $config[88]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[36]['id']?>" id="id-<?php echo $config[36]['id']?>" value="<?php echo $config[36]['id']?>">
	  			<input type="hidden" name="<?php echo $config[36]['variable']?>" id="<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['variable']?>">
	  			<?php echo $config[36]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[89]['id']?>" id="id-<?php echo $config[89]['id']?>" value="<?php echo $config[89]['id']?>">
	  			<input type="hidden" name="<?php echo $config[89]['variable']?>" id="<?php echo $config[89]['variable']?>" value="<?php echo $config[89]['variable']?>">
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[89]['variable']?>" value="<?php echo $config[89]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[37]['id']?>" id="id-<?php echo $config[37]['id']?>" value="<?php echo $config[37]['id']?>">
	  			<input type="hidden" name="<?php echo $config[37]['variable']?>" id="<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['variable']?>">
	  			<?php echo $config[37]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[90]['id']?>" id="id-<?php echo $config[90]['id']?>" value="<?php echo $config[90]['id']?>">
	  			<input type="hidden" name="<?php echo $config[37]['variable']?>" id="<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['variable']?>">
	  			
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[90]['variable']?>" value="<?php echo $config[90]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['value']?>" readonly disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  	</div>

	  	<div class="deviden2"></div>
	  </div>

	  <div id="mfs" class="tab-pane fade">
	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[38]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[38]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[38]['variable']?>" value="<?php echo $config[38]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[39]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[91]['variable']?>" value="<?php echo $config[91]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[39]['variable']?>" value="<?php echo $config[39]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[40]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[92]['variable']?>" value="<?php echo $config[92]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[40]['variable']?>" value="<?php echo $config[40]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[41]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[41]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[41]['variable']?>" value="<?php echo $config[41]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[42]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[93]['variable']?>" value="<?php echo $config[93]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[42]['variable']?>" value="<?php echo $config[42]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[43]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[94]['variable']?>" value="<?php echo $config[94]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[43]['variable']?>" value="<?php echo $config[43]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>


	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[44]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[44]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[44]['variable']?>" value="<?php echo $config[44]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[45]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[95]['variable']?>" value="<?php echo $config[95]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[45]['variable']?>" value="<?php echo $config[45]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[46]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[96]['variable']?>" value="<?php echo $config[96]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[46]['variable']?>" value="<?php echo $config[46]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[47]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[47]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[47]['variable']?>" value="<?php echo $config[47]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[48]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[97]['variable']?>" value="<?php echo $config[97]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[48]['variable']?>" value="<?php echo $config[48]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[49]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[98]['variable']?>" value="<?php echo $config[98]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[49]['variable']?>" value="<?php echo $config[49]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[50]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[50]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[50]['variable']?>" value="<?php echo $config[50]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[51]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[99]['variable']?>" value="<?php echo $config[99]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[51]['variable']?>" value="<?php echo $config[51]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[52]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[100]['variable']?>" value="<?php echo $config[100]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[52]['variable']?>" value="<?php echo $config[52]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>


	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[53]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[53]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[53]['variable']?>" value="<?php echo $config[53]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[54]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[101]['variable']?>" value="<?php echo $config[101]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[54]['variable']?>" value="<?php echo $config[54]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[55]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[102]['variable']?>" value="<?php echo $config[102]['value']?>" disabled>
	  				<div class="opco-symbol"><?php echo $symbol?></div>
	  				<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[55]['variable']?>" value="<?php echo $config[55]['value']?>" readonly disabled>
	  				<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  			</div>
	  		</div>
	  	</div>
	  	<!-- <div class="deviden3"></div> -->

	  </div>

	  <div id="currency" class="tab-pane fade">
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[56]['id']?>" id="id-<?php echo $config[56]['id']?>" value="<?php echo $config[56]['id']?>">
	  			<input type="hidden" name="<?php echo $config[56]['variable']?>" id="<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['variable']?>">
	  			<?php echo $config[56]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[56]['variable']?>" value="<?php echo $symbol?>" readonly disabled>
	  		<!--
	  		<select class="input-90 width161" id="value-<?php echo $config[56]['variable']?>" disabled>
	  		<?php foreach ($currency->result() as $row) {
	  			if(intval($config[56]['value']) == $row->id) $curr_selected = 'selected'; else $curr_selected='';
	  			echo '<option value="'.$row->id.'" '.$curr_selected.'>'.$row->currencysymbol.'</option>';
	  		}?>
	  		</select>
			-->
	  		<!--
	  		<input class="input-90 width161" type="number" name="number" id="value-<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['value']?>" disabled>
	  		-->
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[57]['id']?>" id="id-<?php echo $config[57]['id']?>" value="<?php echo $config[57]['id']?>">
	  			<input type="hidden" name="<?php echo $config[57]['variable']?>" id="<?php echo $config[57]['variable']?>" value="<?php echo $config[57]['variable']?>">
	  			<?php echo $config[57]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="hidden" name="number" id="value-<?php echo $config[57]['variable']?>" value="<?php echo $rate?>" readonly disabled>
	  		<div class="special width161" id="fix-rate"><?php echo $rate?></div>
	  	</div>
	  	

	  </div>

	  <div id="cc"  class="tab-pane fade in active">
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[60]['id']?>" id="id-<?php echo $config[60]['id']?>" value="<?php echo $config[60]['id']?>">
	  			<input type="hidden" name="<?php echo $config[60]['variable']?>" id="<?php echo $config[60]['variable']?>" value="<?php echo $config[60]['variable']?>">
	  			<?php echo $config[60]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[60]['variable']?>" value="<?php echo $config[60]['value']?>" disabled>
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[61]['id']?>" id="id-<?php echo $config[61]['id']?>" value="<?php echo $config[61]['id']?>">
	  			<input type="hidden" name="<?php echo $config[61]['variable']?>" id="<?php echo $config[61]['variable']?>" value="<?php echo $config[61]['variable']?>">
	  			<?php echo $config[61]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[61]['variable']?>" value="<?php echo $config[61]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[62]['id']?>" id="id-<?php echo $config[62]['id']?>" value="<?php echo $config[62]['id']?>">
	  			<input type="hidden" name="<?php echo $config[62]['variable']?>" id="<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['variable']?>">
	  			<?php echo $config[62]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['value']?>" disabled>
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[63]['id']?>" id="id-<?php echo $config[63]['id']?>" value="<?php echo $config[63]['id']?>">
	  			<input type="hidden" name="<?php echo $config[63]['variable']?>" id="<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['variable']?>">
	  			<?php echo $config[63]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['value']?>" disabled>
	  	</div>

	  </div>

	</div>
	<div class="cmd-command">
		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.add_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>
		<button class="std-btn bkgr-green" id="btn-add-opco" onclick="add_opco_config()"><?php echo $this->lang->line('add')?></button>
		<?php } ?>
		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.update_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>
		<button class="std-btn bkgr-blue" id="btn-modify-opco" onclick="modify_opco()"><?php echo $this->lang->line('modify')?></button>
		<?php } ?>
		<button class="std-btn bkgr-green" id="btn-save-opco" onclick="save_value()" disabled><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" id="btn-cancel-opco" onclick="pop('disable-background','cancel-process')" disabled><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div class="add-access-module" id="status-config">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('OpCo no configuration, configuration values set to default')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','status-config')"><?php echo $this->lang->line('ok')?></button>
	</div>

</div>

<div class="add-modify-ui-detail" id="add-opco-config">
	<div class="modify-title">
		<?php echo $this->lang->line('Add OpCo Configuration')?>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('OpCo')?></div>
		<div class="label-item"><?php echo $select_opco2 ?></div>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('Period')?></div>
		<div class="label-item"><?php echo $select_month2 ?></div>
		<div class="label-item"><?php echo $select_year2 ?></div>
	</div>
	<div class="rows center10">
		<button class="std-btn bkgr-green" onclick="new_open()"><?php echo $this->lang->line('add')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','add-opco-config')"><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="add-access-module" id="config-found">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration in selected already exists, the process is canceled')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background2','config-found')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="save-success">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration has been saved')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','save-success')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="result-group-error">
	<div class="modify-title">
		<?php echo $this->lang->line('Error Message'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','result-group-error');pop('disable-background','edit-group');"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div class="del-access-module" id="cancel-process">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('Are you sure to cancel the operation?'); ?>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-greed" onClick="cancel_op()"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-red" onClick="hide('disable-background','cancel-process')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="del-access-module" id="modify-restrict">
	<div class="modify-title">
		<?php echo $this->lang->line('Notify'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('cant_modify'); ?>
	</div>
	<div class="confirm-btn">
		
		<button class="std-btn bkgr-red" onClick="hide('disable-background','modify-restrict')"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div id="testout"></div>
<script type="text/javascript">
	var mode = 'view';
	var selected_opco='<?php echo $opco_id?>';
	var selected_month = '<?php echo $month ?>';
	var selected_year = '<?php echo $year?>';
	var new_value = [];

	var general_rate = <?php echo $rate?>;

	

	<?php for($i=0;$i<count($config);$i++){
		//echo $i; 
	if($i != 56){ ?>

	$("#value-<?php echo $config[$i]['variable']?>").number(true,0); // <?php echo $i ?>
	
	<?php }
	} ?>

	// ** tagonchange
	$("#value-<?php echo $config[64]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[64]['variable']?>").val());
		$("#value-<?php echo $config[6]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[64]['variable']?>").number(true,0);
	$("#value-<?php echo $config[6]['variable']?>").number(true,0);

	$("#value-<?php echo $config[65]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[65]['variable']?>").val());
		$("#value-<?php echo $config[7]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[65]['variable']?>").number(true,0);
	$("#value-<?php echo $config[7]['variable']?>").number(true,0);

	$("#value-<?php echo $config[66]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[66]['variable']?>").val());
		$("#value-<?php echo $config[9]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[66]['variable']?>").number(true,0);
	$("#value-<?php echo $config[9]['variable']?>").number(true,0);

	$("#value-<?php echo $config[67]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[67]['variable']?>").val());
		$("#value-<?php echo $config[11]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[67]['variable']?>").number(true,0);
	$("#value-<?php echo $config[12]['variable']?>").number(true,0);

	$("#value-<?php echo $config[68]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[68]['variable']?>").val());
		$("#value-<?php echo $config[12]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[68]['variable']?>").number(true,0);
	$("#value-<?php echo $config[11]['variable']?>").number(true,0);

	$("#value-<?php echo $config[69]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[69]['variable']?>").val());
		$("#value-<?php echo $config[13]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[69]['variable']?>").number(true,0);
	$("#value-<?php echo $config[13]['variable']?>").number(true,0);

	$("#value-<?php echo $config[70]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[70]['variable']?>").val());
		$("#value-<?php echo $config[58]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[70]['variable']?>").number(true,0);
	$("#value-<?php echo $config[58]['variable']?>").number(true,0);

	$("#value-<?php echo $config[71]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[71]['variable']?>").val());
		$("#value-<?php echo $config[59]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[71]['variable']?>").number(true,0);
	$("#value-<?php echo $config[59]['variable']?>").number(true,0);

	$("#value-<?php echo $config[72]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[72]['variable']?>").val());
		$("#value-<?php echo $config[14]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[72]['variable']?>").number(true,0);
	$("#value-<?php echo $config[14]['variable']?>").number(true,0);

	$("#value-<?php echo $config[73]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[73]['variable']?>").val());
		$("#value-<?php echo $config[15]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[73]['variable']?>").number(true,0);
	$("#value-<?php echo $config[15]['variable']?>").number(true,0);

	$("#value-<?php echo $config[74]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[74]['variable']?>").val());
		$("#value-<?php echo $config[16]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[74]['variable']?>").number(true,0);
	$("#value-<?php echo $config[16]['variable']?>").number(true,0);

	$("#value-<?php echo $config[75]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[75]['variable']?>").val());
		$("#value-<?php echo $config[17]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[75]['variable']?>").number(true,0);
	$("#value-<?php echo $config[17]['variable']?>").number(true,0);

	$("#value-<?php echo $config[76]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[76]['variable']?>").val());
		$("#value-<?php echo $config[18]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[76]['variable']?>").number(true,0);
	$("#value-<?php echo $config[18]['variable']?>").number(true,0);

	$("#value-<?php echo $config[77]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[77]['variable']?>").val());
		$("#value-<?php echo $config[19]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[77]['variable']?>").number(true,0);
	$("#value-<?php echo $config[19]['variable']?>").number(true,0);

	$("#value-<?php echo $config[78]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[78]['variable']?>").val());
		$("#value-<?php echo $config[20]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[78]['variable']?>").number(true,0);
	$("#value-<?php echo $config[20]['variable']?>").number(true,0);

	$("#value-<?php echo $config[79]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[79]['variable']?>").val());
		$("#value-<?php echo $config[21]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[79]['variable']?>").number(true,0);
	$("#value-<?php echo $config[21]['variable']?>").number(true,0);

	$("#value-<?php echo $config[80]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[80]['variable']?>").val());
		$("#value-<?php echo $config[22]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[80]['variable']?>").number(true,0);
	$("#value-<?php echo $config[22]['variable']?>").number(true,0);

	$("#value-<?php echo $config[81]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[80]['variable']?>").val());
		$("#value-<?php echo $config[23]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[81]['variable']?>").number(true,0);
	$("#value-<?php echo $config[23]['variable']?>").number(true,0);

	$("#value-<?php echo $config[82]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[82]['variable']?>").val());
		$("#value-<?php echo $config[24]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[82]['variable']?>").number(true,0);
	$("#value-<?php echo $config[24]['variable']?>").number(true,0);

	$("#value-<?php echo $config[83]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[83]['variable']?>").val());
		$("#value-<?php echo $config[25]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[83]['variable']?>").number(true,0);
	$("#value-<?php echo $config[25]['variable']?>").number(true,0);

	$("#value-<?php echo $config[84]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[84]['variable']?>").val());
		$("#value-<?php echo $config[26]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[84]['variable']?>").number(true,0);
	$("#value-<?php echo $config[26]['variable']?>").number(true,0);

	$("#value-<?php echo $config[85]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[85]['variable']?>").val());
		$("#value-<?php echo $config[27]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[85]['variable']?>").number(true,0);
	$("#value-<?php echo $config[27]['variable']?>").number(true,0);

	// By Pass
	$("#value-<?php echo $config[86]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[86]['variable']?>").val());
		$("#value-<?php echo $config[33]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[86]['variable']?>").number(true,0);
	$("#value-<?php echo $config[33]['variable']?>").number(true,0);

	$("#value-<?php echo $config[87]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[87]['variable']?>").val());
		$("#value-<?php echo $config[34]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[87]['variable']?>").number(true,0);
	$("#value-<?php echo $config[34]['variable']?>").number(true,0);

	$("#value-<?php echo $config[88]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[88]['variable']?>").val());
		$("#value-<?php echo $config[35]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[88]['variable']?>").number(true,0);
	$("#value-<?php echo $config[35]['variable']?>").number(true,0);

	$("#value-<?php echo $config[89]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[89]['variable']?>").val());
		$("#value-<?php echo $config[36]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[89]['variable']?>").number(true,0);
	$("#value-<?php echo $config[36]['variable']?>").number(true,0);

	$("#value-<?php echo $config[90]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[90]['variable']?>").val());
		$("#value-<?php echo $config[37]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[90]['variable']?>").number(true,0);
	$("#value-<?php echo $config[37]['variable']?>").number(true,0);

	// MFS
	$("#value-<?php echo $config[91]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[91]['variable']?>").val());
		$("#value-<?php echo $config[39]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[91]['variable']?>").number(true,0);
	$("#value-<?php echo $config[39]['variable']?>").number(true,0);

	$("#value-<?php echo $config[92]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[92]['variable']?>").val());
		$("#value-<?php echo $config[40]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[92]['variable']?>").number(true,0);
	$("#value-<?php echo $config[40]['variable']?>").number(true,0);

	$("#value-<?php echo $config[93]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[93]['variable']?>").val());
		$("#value-<?php echo $config[42]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[93]['variable']?>").number(true,0);
	$("#value-<?php echo $config[42]['variable']?>").number(true,0);

	$("#value-<?php echo $config[94]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[94]['variable']?>").val());
		$("#value-<?php echo $config[43]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[94]['variable']?>").number(true,0);
	$("#value-<?php echo $config[43]['variable']?>").number(true,0);

	$("#value-<?php echo $config[95]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[95]['variable']?>").val());
		$("#value-<?php echo $config[45]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[95]['variable']?>").number(true,0);
	$("#value-<?php echo $config[45]['variable']?>").number(true,0);

	$("#value-<?php echo $config[96]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[96]['variable']?>").val());
		$("#value-<?php echo $config[46]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[96]['variable']?>").number(true,0);
	$("#value-<?php echo $config[46]['variable']?>").number(true,0);

	$("#value-<?php echo $config[97]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[97]['variable']?>").val());
		$("#value-<?php echo $config[48]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[97]['variable']?>").number(true,0);
	$("#value-<?php echo $config[48]['variable']?>").number(true,0);

	$("#value-<?php echo $config[98]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[98]['variable']?>").val());
		$("#value-<?php echo $config[49]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[98]['variable']?>").number(true,0);
	$("#value-<?php echo $config[49]['variable']?>").number(true,0);

	$("#value-<?php echo $config[99]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[99]['variable']?>").val());
		$("#value-<?php echo $config[51]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[99]['variable']?>").number(true,0);
	$("#value-<?php echo $config[51]['variable']?>").number(true,0);

	$("#value-<?php echo $config[100]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[100]['variable']?>").val());
		$("#value-<?php echo $config[52]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[100]['variable']?>").number(true,0);
	$("#value-<?php echo $config[52]['variable']?>").number(true,0);

	$("#value-<?php echo $config[101]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[101]['variable']?>").val());
		$("#value-<?php echo $config[54]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[101]['variable']?>").number(true,0);
	$("#value-<?php echo $config[54]['variable']?>").number(true,0);

	$("#value-<?php echo $config[102]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[102]['variable']?>").val());
		$("#value-<?php echo $config[55]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[102]['variable']?>").number(true,0);
	$("#value-<?php echo $config[55]['variable']?>").number(true,0);


	$("#value-<?php echo $config[57]['variable']?>").number(true,0);











	  		


	$(document).ready(function(){
		
		$("#opco-list").collapse("show");
		data_config = <?php echo $message?>;
		if(data_config==1){
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.add_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>

			document.getElementById('btn-modify-opco').disabled=true;
			//pop('disable-background','status-config');
			<?php } ?>


		}else{
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.update_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>

			document.getElementById('btn-modify-opco').disabled=false;

			<?php } ?>
		}

		if("<?php echo $mode?>"=="add"){
			add_opco_config();
		}else{
			document.getElementById('opco-name-view').innerHTML='<?php echo $opco_name ?>';
			document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
			document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		}
	});

	function reload_page(){
		window.location.href = '<?php echo base_url()?>index.php/admin/opco_config/'+$("#id-opco").val()+'/'+$("#month").val()+'/'+$("#year").val();
	}

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}



	function add_opco_config(){
		
		//$("#new-id-opco").val($("#id-opco").val());
		//$("#new-month").val($("#month").val());
		//$("#new-year").val($("#year").val());

		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());

		pop('disable-background','add-opco-config');
	}

	function get_add_all_month(group_id,opco_id){
		$.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/get_add_all_month",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: group_id,
                opco_id: opco_id,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                if(data=='1'){
                	document.getElementById("new-month").disabled = false;
                	document.getElementById("new-year").disabled = false;
                }else{
                	$("#new-month").val('<?php echo $current_month?>');
                	$("#new-year").val('<?php echo $current_year?>');
                	document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
					document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
                	document.getElementById("new-month").disabled = true;
                	document.getElementById("new-year").disabled = true;

                }
            }
        });
	}

	function Get_Rate(year,month,opcoid){
	    $.ajax({
	        type: "POST",  
	        url: "<?php echo base_url()?>index.php/admin/get_rate",  
	        contentType: 'application/x-www-form-urlencoded',
	        data: { 
	            year: year,
	            month: month,
	            opcoid:opcoid,
	            sess: "<?php echo session_id()?>"
	        },
	        dataType: "text",
	        beforeSend: function(){

	        },
	        complete: function(){
	            
	        },
	        success: function(data){
	            //alert(data);
	            var obj = JSON.parse(data);
	            general_rate = obj[0];
	            document.getElementById('fix-rate').innerHTML=obj[0];
	            $("#value-<?php echo $config[56]['variable']?>").val(obj[1]);
	            //alert(obj[0]);


	            //alert(data);
	            
	        }
	    });
	}

	function new_open(){
		
		// Check existing data
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/check_opco_config",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	opcoid: $("#new-id-opco").val(),
            	month: $("#new-month").val(),
            	year: $("#new-year").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	switch(data){
            		case '0':
            			// disable select opco,mounth,year
            			hide('disable-background','add-opco-config');
            			//document.getElementById('btn-get-config').disabled=true;
            			//document.getElementById('id-opco').disabled=true
            			//document.getElementById('month').disabled=true
            			//document.getElementById('year').disabled=true;

            			$("#id-opco").val($("#new-id-opco").val());
            			$("#month").val($("#new-month").val());
            			$("#year").val($("#new-year").val());

            			Get_Rate($("#new-year").val(),$("#new-month").val(),$("#new-id-opco").val());

            			<?php
						$sql = 'SELECT
									map_group_opco.group_id,
									map_group_opco.opco_id,
									map_group_opco.add_opco_config
								FROM
									map_group_opco
								WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
						$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
						if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
						?>
            			document.getElementById('btn-add-opco').disabled=true;
            			<?php } ?>

            			<?php
						$sql = 'SELECT
									map_group_opco.group_id,
									map_group_opco.opco_id,
									map_group_opco.update_opco_config
								FROM
									map_group_opco
								WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
						$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
						if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
						?>

            			document.getElementById('btn-modify-opco').disabled=true;
            			<?php } ?>
            			document.getElementById('btn-save-opco').disabled=false;
            			document.getElementById('btn-cancel-opco').disabled=false;

            			<?php 
            				$i=0;
            				foreach ($config as $new_config) {
            					
            					if($i===57) $comment = '// '; else $comment='';
            			?>
		            			<?php echo $comment ?>document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;
		            			<?php echo $comment ?>$("#value-<?php echo $new_config['variable']?>").val("");
            			<?php
            					
            				$i++;
            				}
            			?>
            			mode = 'add';
            			break;
            		case '1':
            			hide('disable-background','add-opco-config');
            			pop('disable-background2','config-found');
            			break;
            		case '2':
            			
            			break;
            	}
            }
		});
	}


	function save_value(){
		//alert(mode);
		if(mode=='add'){
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			item = { 
				opcoid:$("#new-id-opco").val(),
				config_date: $("#new-year").val()+'-'+$("#new-month").val()+"-01 00:00:00",
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);

			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/add_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
	            	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";
	            },
	            success: function(data){
	            	
	            	switch(data){
	            		case '0':
		            		//document.getElementById('btn-get-config').disabled=false;
	            			//document.getElementById('id-opco').disabled=false
	            			//document.getElementById('month').disabled=false
	            			//document.getElementById('year').disabled=false;
	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.add_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>
	            			document.getElementById('btn-add-opco').disabled=false;
	            			<?php }?>
	            			
							<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.update_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>

	            			document.getElementById('btn-modify-opco').disabled=false;
	            			<?php } ?>
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');
	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

			
		}else{
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			var key = $("#var-year").val()+"-"+$("#var-month").val()+"-01 00:00:00";
			//alert(key);
			item = { 
				//opcoid:$("#id-opco").val(),
				opcoid: $("#var-opco-id").val(),
				config_date: key,
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);
			//document.getElementById("testout").innerHTML=json_newdata;
			//alert(json_newdata);
			
			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/update_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
			    	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";       	
	            },
	            success: function(data){
	            	//alert(data);
	            	switch(data){
	            		case '0':

		            		//document.getElementById('btn-get-config').disabled=false;
	            			//document.getElementById('id-opco').disabled=false
	            			//document.getElementById('month').disabled=false
	            			//document.getElementById('year').disabled=false;
	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.add_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>
	            			document.getElementById('btn-add-opco').disabled=false;
	            			<?php }?>

	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.update_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>

	            			document.getElementById('btn-modify-opco').disabled=false;
	            			<?php } ?>
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');

	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

		
		}
	}

	function go_modify(){

		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.add_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>
		document.getElementById('btn-add-opco').disabled=true;
		<?php }?>

		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.update_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>

		document.getElementById('btn-modify-opco').disabled=true;
		<?php } ?>
		document.getElementById('btn-save-opco').disabled=false;
		document.getElementById('btn-cancel-opco').disabled=false;

		<?php 
			foreach ($config as $new_config) {
		?>
		document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;

		<?php
			}
		?>
		mode = 'edit';	

	}

	function modify_opco(){
		//alert('modify');
		//document.getElementById('btn-get-config').disabled=true;
		//document.getElementById('id-opco').disabled=true
		//document.getElementById('month').disabled=true
		//document.getElementById('year').disabled=true;

		// check modify Current Month
		//alert($("#var-year").val()+' '+$("#var-month").val())
		$.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/update_all_month",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: <?php echo $this->session->userdata("group_id")?>,
                opco_id: $("#var-opco-id").val(),
                month: $("#var-month").val(),
                year: $("#var-year").val(),
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
            	//alert(data);
                if(data==1){
                	go_modify();
                } else{
                	pop('disable-background','modify-restrict');
                }
            }
        });

			
	}

	function cancel_op(){
		window.location.href="<?php echo base_url()?>index.php/admin/opco_config/"+$("#var-opco-id").val()+"/"+$("#var-month").val()+"/"+$("#var-year").val();
	}

	$("#new-id-opco").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-opco-id").val($("#new-id-opco").val());
		//alert($("#var-opco-id").val());

		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})

	$("#new-month").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-month").val($("#new-month").val());
		//alert($("#var-month").val());
		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})

	$("#new-year").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-year").val($("#new-year").val());
		//alert($("#var-year").val());
		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})

</script>