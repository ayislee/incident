<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Welcome to VIP')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	</head>
	<style type="text/css">
		.main-logo {
			text-align: center;
		}
	</style>
	<body>
		<?php echo $sidemenu ?>
		<div class="main-content">
			<?php echo $header ?>
			<?php echo $title ?>
			<div class="main-logo">
				<img class="login-logo" src="<?php echo base_url()?>assets/images/Logo-Veon.svg">
			</div>
		</div>
		
		
		
	</body>
</html>