<style type="text/css">
	.section-module {
		max-height: 220px;
		width: 642px;
		overflow: auto;

	}
</style>
<div class="">
	<script type="text/javascript">
		var priv_id=[];
	</script>
	<?php
		foreach ($data as $mod_section) { ?>
		<div class="content-title2"><?php echo $mod_section['module_name']?></div>
			<div class="section-module">
			<?php foreach ($mod_section['privilege'] as $key ) { ?>
				<div class="table-body width600">
					<div class="table-body-item width-priv">
						<?php echo $key['privilege_name']?>
					</div>
					<div class="width-checkbox">
						<input class="" type="checkbox" <?php if($key['privilege_access']) echo 'checked'?> name="privilege-id-<?php echo $key['privilege_id']?>" id="privilege-id-<?php echo $key['privilege_id']?>" >
					</div>
					<script type="text/javascript">
						priv_id.push(<?php echo $key['privilege_id']?>)
					</script>
				</div>
				
			<?php }?>
			</div>

	<?php  }?>
</div>
<div class="privilege-btn">
	<button class="std-btn bkgr-green" onclick="saving()"><?php echo $this->lang->line('save')?></button>
	<button class="std-btn bkgr-red" onclick="window.location.href='<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $group_id ?>'"><?php echo $this->lang->line('cancel')?></button>
</div>


<div class="save-privilege-complete" id="save-module">
    <div class="modify-title">
        <?php echo $this->lang->line('Notify'); ?>
    </div>
    <div class="confirm-message" ><?php echo $this->lang->line('Save privilege is complete') ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-green" onClick="window.location.href='<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $group_id ?>'" ><?php echo $this->lang->line('ok'); ?></button>
    </div>
</div>

<div class="ontop" id="disable-background"></div>

<script type="text/javascript">
	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}

	function saving(){
		size_priv = priv_id.length;
		for(i=0;i<size_priv;i++){
			change_privilege(priv_id[i]);
		}

		pop('disable-background','save-module');
	}


	function change_privilege(id){
		ids="privilege-id-"+id;
		if(document.getElementById(ids).checked){
			$.ajax({
	            type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/set_map_privilege",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	                privilege_id: id,
	                group_id: "<?php echo $group_id?>",
	                sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){

	            },
	            complete: function(){
	                
	            },
	            success: function(data){
	                //location.reload();
	                //alert(data);
	            }
	        });     

		}else{
			$.ajax({
	            type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/del_map_privilege",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	                privilege_id: id,
	                group_id: "<?php echo $group_id?>",
	                sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){

	            },
	            complete: function(){
	                
	            },
	            success: function(data){
	                //location.reload();
	                //alert(data);
	            }
	        });
		}
	}
</script>