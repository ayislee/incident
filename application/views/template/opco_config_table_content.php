<style type="text/css">
	.config-content {
		width: 100%;
		padding-left: 25px;
		padding-right: 25px;

	}	

	.select-opco-area{
		margin-top: 10px;
	}

	.select-opco-label {
		display: inline-block;
		min-width: 100px;
	}
	.select-opco{
		display: inline-block;
		min-width: 100px; 
	}
	.c1 {
		width: 100px;
		padding-left: 10px;
		vertical-align: middle;

	}
	.c2 {
		width: 200px;
		padding-left: 10px;
		vertical-align: middle;

	}
	.c3 {
		float: right;
		padding-right: 30px;
	}

	.table-area {
		padding-top: 10px;
	    display: block;
    	max-height: 400px;
    	overflow: auto;
    	width: 500px;
	}
</style>

<div class="config-content">
	<div class="select-opco-area">
		<div class="select-opco-label">
			<?php echo $this->lang->line('OpCo'); ?>
		</div>
		<div class="select-opco">
			<select id="select-opco" class="std-select">
			<?php foreach ($select_opco->result() as $row): 
				if($opco_id==$row->opcoid) $selected='selected'; else $selected='';
			?>
				<option value="<?php echo $row->opcoid?>" <?php echo $selected?>><?php echo $row->opconame?></option>
			<?php endforeach; ?>
			</select>
		</div>
	</div>
	<script type="text/javascript">
		$("#select-opco").change(function(){
			window.location.href="<?php echo base_url()?>index.php/admin/opco_config_table/"+$("#select-opco").val();
		});
	</script>

	<div class="table-area">
		<div class="table-head">
			<div class="table-head-item c1"><?php echo $this->lang->line('Year')?></div>
			<div class="table-head-item c2"><?php echo $this->lang->line('Month')?></div>
		</div>
		
		<?php foreach ($data->result() as $dt):?>
		<div class="table-body">
		<?php
		switch ($dt->Month) {
			case '01':
				$month = $this->lang->line('January');
				break;
			case '02':
				$month = $this->lang->line('February');
				break;
			case '03':
				$month = $this->lang->line('March');
				break;
			case '04':
				$month = $this->lang->line('April');
				break;
			case '05':
				$month = $this->lang->line('May');
				break;
			case '06':
				$month = $this->lang->line('June');
				break;
			case '07':
				$month = $this->lang->line('July');
				break;
			case '08':
				$month = $this->lang->line('August');
				break;
			case '09':
				$month = $this->lang->line('September');
				break;
			case '10':
				$month = $this->lang->line('October');
				break;
			case '11':
				$month = $this->lang->line('November');
				break;
			case '12':
				$month = $this->lang->line('December');
				break;
			

		}
		?>	
			<div class="table-body-item c1"><?php echo $dt->Year?></div>
			<div class="table-body-item c2"><?php echo $month?></div>
			<?php
				$sql=	'SELECT
							map_group_opco.group_id,
							map_group_opco.opco_id,
							map_group_opco.view_opco_config
						FROM
							map_group_opco
						WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.view_opco_config=1';
				$dti = $this->db->query($sql,array($this->session->userdata('group_id'),$dt->opcoid));
				//echo $this->db->last_query();
        		//die;
				if($dti->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>
			<div class="table-body-item c3"><button class="std-btn bkgr-blue" onclick="view_config('<?php echo $dt->Month?>','<?php echo $dt->Year?>','<?php echo $dt->opcoid?>')"><?php echo $this->lang->line('View')?></div>
			
			<?php } ?>
			</div>
		<?php endforeach; ?>
		
	</div>
	<?php
		$sql=	'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.add_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
		$dti = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dti->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
	?>
	<div class="btn-area">
		<button class="std-btn bkgr-green" onclick="add_config($('#select-opco').val())"><?php echo $this->lang->line('Add OpCo Configuration')?></button>
	</div>
	<?php } ?>



</div>

<script type="text/javascript">
	function view_config(m,y,id){
		//alert(m +' '+' '+y+' '+id);
		window.location.href = '<?php echo base_url()?>index.php/admin/opco_config/'+id+'/'+m+'/'+y;
	}

	function add_config(id){

		window.location.href = '<?php echo base_url()?>index.php/admin/opco_config/'+id+'/'+<?php echo $current_month?>+'/'+<?php echo $current_year?>+'?mode=add';
	}
</script>
