<style type="text/css">
      .inline {
            display: inline-block;
            padding-left: 30px;
            vertical-align: middle;
      }

      .rut-col-2 {
            width: 300px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;

      }

      .rut-col-3 {
            width: 300px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;

      }

      .center-text {
            text-align: center;
      }

      .rcc_content {
          margin-left: 25px;
          margin-top: 5px;
          display: block;
          background-color: #ffc711;
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 20px;
          border-radius: 5px;
          width: 850px;
      }

      .rut-btn-block {
          margin-left: 25px;
          margin-top: 10px;
          display: block;
          background-color: white;
          padding-top: 1px;
          padding-bottom: 1px;
          padding-left: 20px;
          border-radius: 5px;
          width: 850px;
          text-align: right;
      }

      .table-risk {
          margin-left: 25px;
          width: 850px;
          display: block;
      }



</style>

<div class="rcc_content">
    <div class="rows">
        <div class="label-item width10">
            <?php echo $this->lang->line('category')?>
        </div>
        <div class="detail-item width250">
            <select class="std-select width250" name="rcc_id" id="rcc_id">
                <?php foreach ($rcc->result() as $row) { 
                    if($row->id_root_cause_category == $rcc_id_default) $select = "selected"; else $select="";
                ?>
                <option value="<?php echo $row->id_root_cause_category?>" <?php echo $select ?>><?php echo $row->root_cause_category?></option>
                <?php }?>
            </select>
        </div>
    </div>
    <div class="rows">
        <div class="label-item width10">
            <?php echo $this->lang->line('type')?>
        </div>
        <div class="detail-item width250">
            <input class="std-input width250" name="rct" id="rct">
        </div>
    </div>
</div>


  

<div class="rut-btn-block">
      <button class="std-btn bkgr-green" onclick="save_rct()"><?php echo $this->lang->line('add')?>
</div>

<div class="rut-title">
      <?php echo $this->lang->line('type_list')?>
</div>

<div class="table-risk">
      <div class="table-head">
            <div class="table-head-item rut-col-2"><?php echo $this->lang->line('type')?></div>
      </div>
      <?php
            foreach ($rct->result() as $row) {
      ?>
      <div class="table-body">
            <div class="table-body-item rut-col-2"><?php echo $row->root_cause_type?></div>
            <div class="table-body-item right"><button class="std-btn bkgr-red" onclick="delete_id(<?php echo $row->id_root_cause_type?>)"><?php echo $this->lang->line('delete')?></button></div>
            <div class="table-body-item right"><button class="std-btn bkgr-blue" onclick="modify_id(<?php echo $row->id_root_cause_type?>,'<?php echo $row->root_cause_type?>')"><?php echo $this->lang->line('modify')?></button></div>
      </div>
      <?php
             } 
      ?>

</div>
<div class="ontop" id="disable-background"></div>

<div class="add-access-module" id="win-msg">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="detail-group-error"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-green" onClick="hide('disable-background','win-msg')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>

<div class="del-access-module" id="del-rct">
    <div class="modify-title">
        <?php echo $this->lang->line('delete'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Are you sure to delete'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onClick="remove_rct()"><?php echo $this->lang->line('delete'); ?></button>
        <button class="std-btn bkgr-grey" onClick="hide('disable-background','del-rct')"><?php echo $this->lang->line('cancel'); ?></button>
    </div>
</div>

<div class="add-modify-ui-detail" id="modify-rct">
      <div class="modify-title" id="title-modify-detail"></div>
      <div class="rows">
            <input type="hidden" name="modify-rctid" id="modify-rctid">
            <input class="input-ui-detail" id="modify-rct-name" name="modify-rct-name" type="text">
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-rut" onclick="update_rct()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-rct')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>

<script type="text/javascript">
      var delete_rct_id;
      $( "#rcc_id" ).change(function() {
          window.location.href="<?php echo base_url()?>index.php/admin/root_type/"+$("#rcc_id").val();
      });

      function pop(div,div2) {
            document.getElementById(div).style.display = 'block';
            document.getElementById(div2).style.display = 'block';

      }
      function hide(div,div2) {
            document.getElementById(div).style.display = 'none';
            document.getElementById(div2).style.display = 'none';
      }

	function save_rct(){
		$.ajax({
			type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/save_rct",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                    rcc_id: $("#rcc_id").val(), 
                  	rct: $("#rct").val(),
                  	sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                  	
                  },
                  success: function(data){
                    //alert(data);
                  	switch(data){
                  		case '0':
                  			location.reload();
                  			break;
                  		case '1':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to save data')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  		case '2':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Root Cause Category is blank')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  		case '3':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  	}
                  }
		});
	}

      function delete_id(id){
            delete_rct_id = id;
            pop('disable-background','del-rct');
      }

      function remove_rct(){
            //alert(delete_rcc_id);
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_rct",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        rct_id: delete_rct_id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        switch(data){
                              case '0':
                                    location.reload();
                                    break;
                              case '1':
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to delete data')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '2':
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                                    pop('disable-background','win-msg');
                                    break;
                        }
                  }
            });
      }

      function modify_id(id,rct){
            // alert(rct);
            $("#modify-rct-name").val(rct);
            $("#modify-rctid").val(id);
            pop('disable-background','modify-rct');
      }

      function update_rct(){
            //var bypass = ( $("#modify-bypass").is(':checked') ) ? 1 : 0;
            //alert("gga");

            //alert(type);
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/update_rct",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        rctid: $("#modify-rctid").val(),
                        rct: $("#modify-rct-name").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        switch(data){
                              case '0':
                                    location.reload();
                                    break;
                              case '1':
                                    hide('disable-background','modify-rct');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to update data')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '2':
                                    hide('disable-background','modify-rct');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Risk Universe Type Name is blank')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '3':
                                    hide('disable-background','modify-rct');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                                    pop('disable-background','win-msg');
                                    break;
                        }
                  }
            });
      }

</script>