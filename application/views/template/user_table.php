<style type="text/css">
	.col-all {
		width: 50px;
	}
</style>

<div class="user-table-content2">
	<div class="user-table" id="user-table">
		<div class="table-head">
			<div class="table-head-item col-no"><?php echo $this->lang->line('number')?></div>
			<div class="table-head-item col-name"><?php echo $this->lang->line('name')?></div>
			<div class="table-head-item col-username"><?php echo $this->lang->line('username')?></div>
			<div class="table-head-item col-group"><?php echo $this->lang->line('group')?></div>
			<div class="table-head-item col-status"><?php echo $this->lang->line('status')?></div>
			<div class="table-head-item col-last"><?php echo $this->lang->line('last_login')?></div>
			<div class="table-head-item col-all"><?php echo $this->lang->line('all')?> <input type="checkbox" class ="check-all" name="check-all" id="check-all" onchange="selectall()"></div>

		</div>

		<?php echo $user_table ?>
		
		<?php echo $js1 ?>

		<script>
			function selectall(){

				if(document.getElementById('check-all').checked){
					for(i=1;i<=idshow;i++){
						document.getElementById('check-id-'+ids[i-1]).checked=true;
					}
				}else {
					for(i=1;i<=idshow;i++){
						document.getElementById('check-id-'+ids[i-1]).checked=false;
					}
				}
			}
		</script>


	</div>

	<div class="navigator">
		<?php echo $pagination ?>
		<div class="command">
			<button class="std-btn bkgr-green" id="add-btn" onClick="adduser()"><?php echo $this->lang->line('add')?></button>
			<button class="std-btn bkgr-red"  id="del-btn" onClick="pop('disable-background','win-del-confirm')"><?php echo $this->lang->line('disable')?></button>
			<button class="std-btn bkgr-blue"  id="en-btn" onClick="pop('disable-background','win-en-confirm')"><?php echo $this->lang->line('enable')?></button>
		</div>
	</div>
</div>

<div class="ontop" id="disable-background"></div>

<div class="del-access-module" id="win-del-confirm">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('confirm_message'); ?>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-greed" onClick="confirm()"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-red" onClick="cancel()"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="del-access-module" id="win-en-confirm">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('confirm_message_en'); ?>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-greed" onClick="confirm2()"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-red" onClick="cancel2()"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="pop-user-add" id="add-user">
	<div class="modify-title">
		<?php echo $this->lang->line('Add User'); ?>
	</div>
	<div class="tips" id="msg-tips"><?php echo $this->lang->line('type_username')?></div>
	<div class="modify-content">

		<div class="rows">
			<div class="">
				<input class="input-username" id="new-user" name="new-user" type="text">
			</div>
		</div>
		
		<div class="rows add-user-msg-error" id="user-msg-error"></div>

		<div class="rows">
			<div class="tips" id="ldap-tips"></div>
		</div>
		
		<div class="list-user" id="list-user">

		</div>

		<div class="rows-btn-check" style="margin-bottom: 10px;" id="btn-check">
			<div class="add-user-cmd">
				<button class="std-btn bkgr-green fix-width-100" onClick="check_ldap_user()"><?php echo $this->lang->line('ok')?></button>
				<button class="std-btn bkgr-red fix-width-100" onClick="hide('disable-background','add-user')"><?php echo $this->lang->line('cancel')?></button>
			</div>
		</div>

		<div class="rows-btn-add-user" style="margin-bottom: 10px;" id="btn-add-user">
			<div class="add-user-cmd">
				<button class="std-btn bkgr-blue fix-width-100" onClick="show_add()"><?php echo $this->lang->line('ok')?></button>
				<button class="std-btn bkgr-red fix-width-100" onClick="hide('disable-background','add-user')"><?php echo $this->lang->line('cancel')?></button>
			</div>
		</div>

		<div class="ldap-status" id="ldapstatus">
			<div class="ldap-status-title" id="status-msg"></div>

			<div class="ldap-detail" id="ldap-user-detail">
					

					
			</div>
		</div>
		<div class="error-status" id="error-status">
			<div class="ldap-status-title" id="error-msg"></div>
			<div class="rows">
				<div class="error-detail" id="error-detail"></div>
			</div>
		</div>
	</div>

</div>

<div class="del-confirm" id="result-user-ok">
	<div class="modify-title">
		<?php echo $this->lang->line('admin_menu_user_management'); ?>
	</div>
	<div class="confirm-message" id="result-add-user"><?php echo $this->lang->line('User created') ?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="window.location.reload()"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div class="del-confirm" id="result-user-error">
	<div class="modify-title">
		<?php echo $this->lang->line('admin_menu_user_management'); ?>
	</div>
	<div class="confirm-message" id="detail-user-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-red" onClick="backtoaddlocal()"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>


<div class="pop-user-add" id="form-new-user">
	<div class="modify-title">
		<?php echo $this->lang->line('Add User'); ?>
	</div>


	<div class="add-user-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('username')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-username" name="ldap-username" class="std-input2" type="text">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('First name')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-firstname" name="ldap-firstname" class="std-input2" type="text">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('Last name')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-lastname" name="ldap-lastname" class="std-input2" type="text">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('email')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-email" name="ldap-email" class="std-input2" type="text">
			</div>
		</div>

		<div class="rows" id="row-password1">
			<div class="label-item width10">
				<?php echo $this->lang->line('Password')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-password" name="ldap-password" class="std-input2" type="password">
			</div>
		</div>

		<div class="rows" id="row-password2">
			<div class="label-item width10">
				<?php echo $this->lang->line('Retype password')?>
			</div>
			:
			<div class="detail-item">
				<input id="ldap-retype-password" name="ldap-retype-password" class="std-input2" type="password">
			</div>
		</div>


		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group')?>
			</div>
			:
			<div class="detail-item">
				<?php echo $group ?>
			</div>
		</div>
		
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('status')?>
			</div>
			:
			<div class="detail-item">
				<select class="std-input2" id="ldap-status" name="ldap-status" >
					<option value="1" selected><?php echo $this->lang->line('enable')?>
					<option value="0" ><?php echo $this->lang->line('disable')?>
				</select>
			</div>
		</div>
		<div class="" id="error-message"></div>

		<div class="center11">
			<div class="rows" id="btn-ldap-user">
				<div class="command-line">
					<button class="std-btn bkgr-green width10" onclick="add_ldap_user()"><?php echo $this->lang->line('add')?></button>
					<button class="std-btn bkgr-red width10" onclick="cancel_add_user()"><?php echo $this->lang->line('cancel')?></button>
				</div>
			</div>																
			<div class="rows" id="btn-local-user">
				<div class="command-line">
					<button class="std-btn bkgr-green width10" onclick="add_local_user()"><?php echo $this->lang->line('add')?></button>
					<button class="std-btn bkgr-red width10" onclick="cancel_add_user()"><?php echo $this->lang->line('cancel')?></button>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}
	function cancel(){
		hide('disable-background','win-del-confirm');
		hide('disable-background','add-user');
	}

	function cancel2(){
		hide('disable-background','win-en-confirm');
		hide('disable-background','add-user');
	}
	//To detect escape button


	function confirm(){
		var aselect = [];
		for(i=1;i<=idshow;i++){
			
			if(document.getElementById('check-id-'+ids[i-1]).checked){
				aselect.push(ids[i-1]);
			}
		} 
		var JsonString = JSON.stringify(aselect);

		$.post("<?php echo base_url()?>index.php/admin/disable_acc",
	    {
	        users: JsonString,
	        s: "<?php echo session_id()?>"
	    },
	    function(data, status){
	    	location.reload();
	        //alert("Data: " + data + "\nStatus: " + status);
	    });


	}

	function confirm2(){
		var aselect = [];
		for(i=1;i<=idshow;i++){
			
			if(document.getElementById('check-id-'+ids[i-1]).checked){
				aselect.push(ids[i-1]);
			}
		} 
		var JsonString = JSON.stringify(aselect);

		$.post("<?php echo base_url()?>index.php/admin/enable_acc",
	    {
	        users: JsonString,
	        s: "<?php echo session_id()?>"
	    },
	    function(data, status){
	    	location.reload();
	        //alert("Data: " + data + "\nStatus: " + status);
	    });


	}

	function adduser(){
		$("#new-user").val('');
		document.getElementById('msg-tips').style.display='block';
		document.getElementById('user-msg-error').style.display='none';
		document.getElementById('ldap-tips').style.display='none';
		document.getElementById('list-user').style.display='none';
		document.getElementById('btn-check').style.display='block';
		document.getElementById('btn-add-user').style.display='none';


		pop('disable-background','add-user');
	}

	function add_ldap_user(){
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_ldap_user",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	username: $("#ldap-username").val(),
            	firstname: $("#ldap-firstname").val(),
            	lastname: $("#ldap-lastname").val(),
            	email: $("#ldap-email").val(),
            	group: $("#ldap-group").val(),
            	status: $("#ldap-status").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	window.location.href = "<?php echo base_url()?>index.php/admin/user_management";

            	
            }
		});

	}

	function backtoaddlocal(){
		pop('disable-background','form-new-user');
		hide('result-user-error','')
	}

	function add_local_user(){

		
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_local_user",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	username: $("#ldap-username").val(),
            	firstname: $("#ldap-firstname").val(),
            	lastname: $("#ldap-lastname").val(),
            	email: $("#ldap-email").val(),
            	group: $("#ldap-group").val(),
            	status: $("#ldap-status").val(),
            	p1: $("#ldap-password").val(),
            	p2: $("#ldap-retype-password").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	switch(data){
            		case '1':
            			hide('disable-background','form-new-user');
            			pop('disable-background','result-user-ok');
            			break;
            		case '2':
            			//alert(data);
            			hide('disable-background','form-new-user');
            			document.getElementById("detail-user-error").innerHTML = "<?php echo $this->lang->line('Invalid email')?>";
            			pop('disable-background','result-user-error');
            			break;
            		case '3':
            			hide('disable-background','form-new-user');
            			document.getElementById("detail-user-error").innerHTML = "<?php echo $this->lang->line('Invalid password')?>";
            			pop('disable-background','result-user-error');
            			break;
            		case '4':
            			hide('disable-background','form-new-user');
            			document.getElementById("detail-user-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			pop('disable-background','result-user-error');
            			break;
            	}
            	

            	
            }
		});
		

	}	

	function cancel_add_user(){
		hide('disable-background','form-new-user');
	}

	function show_add(){
		hide('disable-background','add-user');
		pop('disable-background','form-new-user');

		new_user = $("input[name=username]:checked").val();
		//alert(new_user);
		
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/verify_ldap_user",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	user: new_user,
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	obj = JSON.parse(data);

            	switch(obj.code){
            		case '0':
            			// user ldap
            			//alert(data);
            			document.getElementById('btn-ldap-user').style.display = 'block';
            			document.getElementById('btn-local-user').style.display = 'none';

            			document.getElementById('row-password1').style.display = 'none';
            			document.getElementById('row-password2').style.display = 'none';

            			document.getElementById("ldap-username").disabled = true;
            			document.getElementById("ldap-firstname").disabled = true;
            			document.getElementById("ldap-lastname").disabled = true;
            			document.getElementById("ldap-email").disabled = true;
            			$("#ldap-username").val(obj.username);
            			$("#ldap-firstname").val(obj.firstname);
            			$("#ldap-lastname").val(obj.lastname);
            			$("#ldap-email").val(obj.email);

            			break;
            		case '1':
 						// user local
            			//alert(data);
            			document.getElementById('btn-ldap-user').style.display = 'none';
            			document.getElementById('btn-local-user').style.display = 'block';

            			document.getElementById('row-password1').style.display = 'block';
            			document.getElementById('row-password2').style.display = 'block';

            			document.getElementById('error-status').style.display = 'none';

            			document.getElementById("ldap-username").disabled = true;
            			document.getElementById("ldap-firstname").disabled = false;
            			document.getElementById("ldap-lastname").disabled = false;
            			document.getElementById("ldap-email").disabled = false;
            			$("#ldap-username").val($("#new-user").val());
            			$("#ldap-firstname").val('');
            			$("#ldap-lastname").val('');
            			$("#ldap-email").val('');
            			$("#ldap-password").val('');
            			$("#ldap-retype-password").val('');

            			break;
            		case '2':
            			
            			break;
            		case '3':
            			           			
            			break;
            		case '4':
            			
            			break;
            		case '5':
            			
            			//alert();             			
            			break;

            		case '6':
            			
            			break;
            	}
            }
		});
		
	}

	function check_ldap_user(){

		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/check_ldap_user",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	user: $("#new-user").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	obj = JSON.parse(data);

            	switch(obj.code){
            		case '0':
            			if(obj.ldap_users==''){
            				document.getElementById('list-user').style.display = 'none';
            				document.getElementById('ldap-tips').style.display = 'none';
            				document.getElementById('user-msg-error').style.display = 'block';
            				document.getElementById("user-msg-error").innerHTML = "<?php echo $this->lang->line('Username already registered')?>";
            				document.getElementById('btn-check').style.display = 'block';
            				document.getElementById('btn-add-user').style.display = 'none';
            			}else{
            				$.post("<?php echo base_url()?>index.php/admin/available/"+$("#new-user").val(), function(data, status){
            					document.getElementById('user-msg-error').style.display = 'none';
            					document.getElementById('ldap-tips').style.display = 'block';
            					document.getElementById('msg-tips').style.display = 'none';
            					document.getElementById("ldap-tips").innerHTML = "<?php echo $this->lang->line('similar_name')?>";
            					document.getElementById('msg-tips').style.display = 'none';
            					document.getElementById('list-user').style.display = 'block';
						        document.getElementById("list-user").innerHTML = data;
						        document.getElementById('btn-check').style.display = 'none';
            					document.getElementById('btn-add-user').style.display = 'block';
						    });
            			}

            			break;
            		case '1':
 						$.post("<?php echo base_url()?>index.php/admin/available/"+$("#new-user").val(), function(data, status){
        					document.getElementById('user-msg-error').style.display = 'none';
        					document.getElementById('ldap-tips').style.display = 'block';
        					document.getElementById('msg-tips').style.display = 'none';
        					document.getElementById("ldap-tips").innerHTML = "<?php echo $this->lang->line('similar_name')?>";
        					document.getElementById('msg-tips').style.display = 'none';
        					document.getElementById('list-user').style.display = 'block';
					        document.getElementById("list-user").innerHTML = data;
					        document.getElementById('btn-check').style.display = 'none';
            				document.getElementById('btn-add-user').style.display = 'block';
					    });
            			
            			break;
            		case '2':
            			document.getElementById('list-user').style.display = 'none';
        				document.getElementById('ldap-tips').style.display = 'none';
        				document.getElementById('user-msg-error').style.display = 'block';
        				document.getElementById("user-msg-error").innerHTML = "<?php echo $this->lang->line('Username already registered')?>";
        				document.getElementById('btn-check').style.display = 'block';
        				document.getElementById('btn-add-user').style.display = 'none';
            			
            			break;
            		case '3':
            			document.getElementById('list-user').style.display = 'none';
        				document.getElementById('ldap-tips').style.display = 'none';
        				document.getElementById('user-msg-error').style.display = 'block';
        				document.getElementById("user-msg-error").innerHTML = "<?php echo $this->lang->line('LDAP Connection Failed')?>";
        				document.getElementById('btn-check').style.display = 'block';
            			document.getElementById('btn-add-user').style.display = 'none';            			
            			break;
            		case '4':
            			document.getElementById('list-user').style.display = 'none';
        				document.getElementById('ldap-tips').style.display = 'none';
        				document.getElementById('user-msg-error').style.display = 'block';
            			document.getElementById("user-msg-error").innerHTML = "<?php echo $this->lang->line('LDAP binding Failed')?>";              			
            			document.getElementById('btn-check').style.display = 'block';
            			document.getElementById('btn-add-user').style.display = 'none';
            			break;
            		case '5':
            			document.getElementById('list-user').style.display = 'none';
        				document.getElementById('ldap-tips').style.display = 'none';
        				document.getElementById('user-msg-error').style.display = 'block';
            			document.getElementById("user-msg-error").innerHTML = "<?php echo $this->lang->line('Username is blank')?>"; 
            			document.getElementById('btn-check').style.display = 'block';
            			document.getElementById('btn-add-user').style.display = 'none';
            			//alert();             			
            			break;

            		case '6':
            			document.getElementById('list-user').style.display = 'none';
        				document.getElementById('ldap-tips').style.display = 'none';
        				document.getElementById('user-msg-error').style.display = 'block';
            			document.getElementById("user-msg-erro").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";  
            			document.getElementById('btn-check').style.display = 'block';
            			document.getElementById('btn-add-user').style.display = 'none';
            			break;

            	}

            	
            }
		});

	}

	var resizeEventsTrigger = (function () {
	    function triggerResizeStart($el) {
	        $el.trigger('resizestart');
	        isStart = !isStart;
	    }

	    function triggerResizeEnd($el) {
	        clearTimeout(timeoutId);
	        timeoutId = setTimeout(function () {
	            $el.trigger('resizeend');
	            isStart = !isStart;
	        }, delay);
	    }

	    var isStart = true;
	    var delay = 200;
	    var timeoutId;

	    return function ($el) {
	        isStart ? triggerResizeStart($el) : triggerResizeEnd($el);
    	};

	})();

	$("body").on('resizestart', function () {
	    // console.log('resize start');
	});
	$("body").on('resizeend', function () {
		refwindow();
	});

	window.onresize = function () {
	    resizeEventsTrigger( $("body") );
	};

	function refwindow(){
		console.log("wheight:"+window.innerHeight);
	    // console.log('resize end');
	    totalusertable = window.innerHeight;
	    console.log("TotalUsertable"+totalusertable);
	    headerheight = 56;
	    titleheight = 31;
	    breadcrumheight = 0;
	    searchheight = 24;
	    navigatorheight = 41;
	    usertable = totalusertable - headerheight- headerheight - titleheight - breadcrumheight - searchheight - navigatorheight;
	    document.getElementById("user-table").style.maxHeight = usertable+"px";
	    console.log("usertable:"+usertable);
	}

	$(window).resize(function()
	{   

	    refwindow();
	});
	
	$( document ).ready(function() {
		
	    refwindow();

	});


</script>