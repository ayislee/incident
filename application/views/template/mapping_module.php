<style type="text/css">
    .area-save {
        margin-left: 25px;
        width: 1000px;

    }
    .f-r{
        float: right;
    }

    .mod-col-2{
        width: 760px;
    }
</style>
<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
<div class="area-save w1000">
    <div class="f-r">
        <button class="std-btn bkgr-green" onclick="module_save()"><?php echo $this->lang->line('save')?></button>
        <button class="std-btn bkgr-red" onclick="module_cancel()"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>
<?php } ?>

<div id="dt-module"></div>
<div class="del-access-module" id="save-module">
    <div class="modify-title">
        <?php echo $this->lang->line('Notify'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Configuration has been saved'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onClick="location.reload()"><?php echo $this->lang->line('ok'); ?></button>
    </div>
</div>




<script type="text/javascript">
    var id_module_delete;
    var id_group_delete;
    function delete_module(id_module,id){
        pop('disable-background','del-map-module');
        id_module_delete = id_module;
        id_group_delete = id;
        
    }

    function deletemodule(){
        //alert('ok delete '+ id_module_delete + 'group : '+ id_group_delete);
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/delete_module_map",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                id_group: id_group_delete,
                id_module: id_module_delete,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                location.reload();
                //alert(data);
            }
        });     

    }

    function add_map_module(){
        pop('disable-background','add-map-module');
    }

    function add_module(){
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_module",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                id_group: "<?php echo $id_group?>",
                id_module: $("#select-module").val(),
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                location.reload();
                //alert(data);
            }
        });         
    }

    function module_change(id_module,id_group){
        //alert($("#module_"+id_module+"_group_"+id_group).prop('checked'));
        if($("#module_"+id_module+"_group_"+id_group).prop('checked')){
            $.ajax({
                type: "POST",  
                url: "<?php echo base_url()?>index.php/admin/add_module",  
                contentType: 'application/x-www-form-urlencoded',
                data: { 
                    id_group: id_group,
                    id_module: id_module,
                    sess: "<?php echo session_id()?>"
                },
                dataType: "text",
                beforeSend: function(){

                },
                complete: function(){
                    
                },
                success: function(data){
                    //location.reload();
                    //alert(data);
                }
            });
        }else{
            $.ajax({
                type: "POST",  
                url: "<?php echo base_url()?>index.php/admin/delete_module_map",  
                contentType: 'application/x-www-form-urlencoded',
                data: { 
                    id_group: id_group,
                    id_module: id_module,
                    sess: "<?php echo session_id()?>"
                },
                dataType: "text",
                beforeSend: function(){

                },
                complete: function(){
                    
                },
                success: function(data){
                    //location.reload();
                    //alert(data);
                }
            });
        }
    }

    function module_save(){
        var json = '<?php echo json_encode($module_list->result_array())?>';
        var obj = JSON.parse(json);
        //alert(obj.length);
        //alert("jsdkj");
        obj.forEach(function(item, index){
            var list_module = item.id;
            module_change(item.id,<?php echo $id_group ?>); 
        });

        pop('disable-background','save-module');

    }

    function module_cancel(){
        location.reload();
    }

    function reload_page(){

    }


</script>