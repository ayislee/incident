<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('admin_menu_opco_configuration')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url()?>assets/datepicker/css/datepicker.css">
		<script src="<?php echo base_url()?>assets/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.number.js"></script>

	</head>

	<body>
		<?php echo $sidemenu ?>
		<div class="main-content">
			<?php echo $header ?>
			<?php echo $title ?>
			<?php //echo $content_title ?>
			<?php echo $config_list ?>
			
		</div>
		
		
		<div class="content"></div>
	</body>
</html>