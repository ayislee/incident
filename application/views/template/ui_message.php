<style type="text/css">
      select { 
            width: 100px;
            text-align-last: center; 
      }
      .c-option {
            padding-left: 20px;
      }
</style>

<div class="ontop" id="disable-background"></div>
<div class="ontop2" id="disable-background2"></div>

<div class="add-modify-ui-detail" id="add-revenue-stream">
      <div class="modify-title" id="title-add-revenue-stream"></div>
      <div class"rows">
            <div class="center11" id="add-revenue-stream-message"></div>
      </div>

      <div class="rows">
            <input class="input-ui-detail" id="new-revenue-stream-name" name="new-revenue-stream-name" type="text">
      </div>

      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-add-revenue-stream"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','add-revenue-stream')"><?php echo $this->lang->line('cancel')?></button> 
      </div>
</div>

<div class="add-modify-ui-detail" id="modify-revenue-stream">
      <div class="modify-title" id="title-modify-revenue-stream"></div>
      <div class"rows">
            <div class="center11" id="modify-revenue-stream-message"></div>
      </div>

      <div class="rows">
            <input class="input-ui-detail" id="modify-revenue-stream-name" name="modify-revenue-stream-name" type="text">
      </div>

      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-revenue-stream"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-revenue-stream')"><?php echo $this->lang->line('cancel')?></button> 
      </div>
</div>




<div class="add-modify-ui-detail" id="add-ui-detail">
	<div class="modify-title" id="title-add-detail"></div>
	<div class"rows">
		<div class="center11" id="add-ui-detail-message"></div>
	</div>

	<div class="rows">
		<input class="input-ui-detail" id="new-ui-detail-name" name="new-ui-detail-name" type="text">
            <input class="input-ui-detail" id="new-ui-detail-value" name="new-ui-detail-value" type="text">
	</div>

	<div class="rows center11">
		<button class="std-btn bkgr-green" id="btn-save-add-ui-detail"><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','add-ui-detail')"><?php echo $this->lang->line('cancel')?></button> 
	</div>
</div>



<style type="text/css">
      .btn {
            width: auto;
            border-color: #FFC711;
            height: 35px;
      }
      .center {
            text-align: center;
      }
      .wid {
            width: 117px;
            margin-left: 0px;
            margin-right: 0px;
      }

      .alert-error {
            color: red;
            text-align: center;
      }
</style>

<div class="add-modify-ui-detail" id="add-control-coverage">
      <div class="modify-title" id="title-add-control-coverage"></div>
      <div class"rows">
            <div class="center11" id="add-control-coverage-message"><?php echo $this->lang->line('Please add Control Coverage name that you desire')?></div>
      </div>

      <div class="rows center">
            <input class="input-ui-detail" id="new-control-coverage-name" name="new-control-coverage-name" type="text">
            <input class="input-ui-detail wid" id="new-start-date" name="new-start-date" type="text">
            <a href="#" class="btn" id="dp4" data-date-format="yyyy-mm-dd" data-date=""><i class="fa fa-calendar" aria-hidden="true"></i></a>
            <input class="input-ui-detail wid" id="new-end-date" name="new-end-date" type="text">
            <a href="#" class="btn" id="dp5" data-date-format="yyyy-mm-dd" data-date=""><i class="fa fa-calendar" aria-hidden="true"></i></a>
      </div>
      <div class="alert alert-error" id="alert">
            <strong></strong>
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-add-control-coverage"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','add-control-coverage')"><?php echo $this->lang->line('cancel')?></button> 
      </div>
</div>


<div class="add-modify-ui-detail" id="add-currency">
      <div class="modify-title" id="title-add-detail"><?php echo $this->lang->line('Currency')?></div>
      <div class="rows center21">
            <select class="std-select" id="select_currency" name="select_currency" onchange="ch_country()">
            <?php foreach ($country as $key => $value) {
                  # code...
                  echo '<option value="'.$key.'">'.$value.'</option>';

            }?>
            </select>
            <p id="symbol" style="margin-top: 10px;margin-bottom:20px;font-weight: bold;"></p>
            <input class="input-ui-detail" id="currencyname" name="currencyname" type="hidden">
            <input class="input-ui-detail" id="currencysymbol" name="currencysymbol" type="hidden">

      </div>
      <script type="text/javascript">
            function ch_country(){
                  document.getElementById('symbol').innerHTML = $("#select_currency").val();
                  $("#currencysymbol").val($("#select_currency").val());
                  $("#currencyname").val($( "#select_currency option:selected" ).text());

            }
      </script>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-add-currency" onclick="add_group()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','add-currency')"><?php echo $this->lang->line('cancel')?></button> 
      </div>
</div>

<div class="add-modify-ui-detail" id="modify-ui-detail">
	<div class="modify-title" id="title-modify-ui-detail"></div>
	<div class="rows">
		<input class="input-ui-detail" id="modify-ui-detail-name" name="modify-ui-detail-name" type="text">
            <input class="input-ui-detail" id="modify-ui-detail-value" name="modify-ui-detail-value" type="text">
	</div>
	<div class="rows center11">
		<button class="std-btn bkgr-green" id="btn-save-modify-ui-detail" onclick=""><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','modify-ui-detail')"><?php echo $this->lang->line('cancel')?></button> 
	</div>

</div>

<div class="add-modify-ui-detail" id="modify-opco-detail">
      <div class="modify-title" id="title-modify-opco-detail"><?php echo $this->lang->line('Modify OPCO')?></div>
      <div class="rows">
            <input class="input-ui-detail"  id="modify-opco-detail-name" name="modify-opco-detail-name" type="text">
            <div class="center11"><?php echo $this->lang->line('Default Currency')?></div>
            <div style='text-align:center'>
                  <select class="std-select"  id="modify-opco-detail-value" name="modify-opco-detail-value">
                  </select>
            </div>
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-opco-detail" onclick=""><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-opco-detail')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>


<div class="add-modify-ui-detail" id="add-opco-detail">
      <div class="modify-title" id="title-add-detail"><?php echo $this->lang->line('Add OPCO')?></div>
      <div class"rows">
            <div class="center11" id="add-opco-detail-message"></div>
      </div>

      <div class="rows">
            <input class="input-ui-detail" id="new-opco-detail-name" name="new-opco-detail-name" type="text">
            <div class="center11"><?php echo $this->lang->line('Default Currency')?></div>
            <div style='text-align:center'>
                  <select class="std-select"  id="new-opco-detail-value" name="new-opco-detail-value">
                  </select>
            </div>
      </div>

      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-add-opco-detail"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','add-opco-detail')"><?php echo $this->lang->line('cancel')?></button> 
      </div>
</div>


<div class="del-confirm" id="msg-error">
	<div class="modify-title">
		<?php echo $this->lang->line("Notify"); ?>
	</div>
	<div class="confirm-message" id="notify-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','msg-error');" id="btn-ok-msg-error"><?php echo $this->lang->line('ok'); ?></button>
	</div>

</div>

<div class="del-confirm" id="win-del-confirm">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message" id="ui-detail-delete-confirm"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-red" id="btn-delete-ui-detail"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-grey" onClick="hide('disable-background','win-del-confirm')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="del-confirm" id="win-upload">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="upload-message"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-grey" onClick="hide('disable-background','win-upload')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>




<script type="text/javascript">

      function add_control_coverage_list(){
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/add_control_coverage",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        ccname: $("#new-control-coverage-name").val(),
                        start_date: $("#new-start-date").val(),
                        end_date: $("#new-end-date").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','add-control-coverage');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });            
      }

      function modify_control_coverage(id){
            //alert("hahah")
            
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/modify_control_coverage",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        ccname: $("#new-control-coverage-name").val(),
                        start_date: $("#new-start-date").val(),
                        end_date: $("#new-end-date").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','add-control-coverage');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });      
                  
      }

      function delete_control_coverage(id){
            //alert(id);
            
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_control_coverage",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        var obj = jQuery.parseJSON(data);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','add-control-coverage');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                        
                  }
            });

      }

      function delete_revenue(id){
            //alert(id);
            
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_revenue",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','add-revenue-stream');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                        
                  }
            });

      }

      function delete_currency(id){
            $.ajax({
                  type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/delete_currency",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                  id: id,
                  sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                  
            },
            success: function(data){
                  window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/1';
            }
            });
      }

      function delete_detection(id){
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_detection",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        switch(obj.status){
                        case 0:
                              window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/2';
                              break;
                        default :
                              //hide('disable-background','add-control-coverage');
                              document.getElementById("notify-error").innerHTML = obj.msg;
                              pop("disable-background","msg-error");
                              break;
      
                        }
                  }
            });
      }

      function delete_opco(id){
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_opco",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        var obj = jQuery.parseJSON(data);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/3';
                                    break;
                              default :
                                    hide('disable-background','add-control-coverage');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });
      }


      function add_group(){

            $.ajax({
                  type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_currency",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                  currencyname: $("#currencyname").val(),
                  currencysymbol: $("#currencysymbol").val(),
                  sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                  
            },
            success: function(data){
                  //location.reload();
                  //alert(data);
                  switch(data){
                        case '0':
                              window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/1';
                              break;
                        case '1':
                              hide('disable-background','add-currency');
                              document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('Currency already exist')?>";
                              pop("disable-background","msg-error");
                              break;
                        case '2':
                              document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('name_blank')?>";
                              hide("disable-background","add-ui-detail");

                              pop("disable-background","msg-error");
                              break;
                        case '3':
                              document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                              hide("disable-background","add-ui-detail");
                              pop("disable-background","msg-error");
                              break;      
                  }
            }
            });         
      }

      function add_detect(){

            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/add_detection",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        dtname: $("#new-ui-detail-name").val(),
                        dtcost: $("#new-ui-detail-value").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/2';
                                    break;
                              default :
                                    hide('disable-background','add-ui-detail');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });         
      }


      function add_opco_list(){

            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/add_opco",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        opconame: $("#new-opco-detail-name").val(),
                        currency_id:$("#new-opco-detail-value").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/3';
                                    break;
                              default :
                                    hide('disable-background','add-ui-detail');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });         
      }

      function add_revenue(){

            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/add_revenue_stream",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        revenue_stream_name: $("#new-revenue-stream-name").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','add-revenue-stream');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });         
      }



	function modify_currency(id){

		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/update_currency",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id: id,
            	currencyname: $("#modify-ui-detail-name").val(),
                  currencysymbol: $("#modify-ui-detail-value").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//location.reload();
            	//alert(data);
            	switch(data){
            		case '0':
                              //alert("shdjs");
            			window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/<?php echo $acc_active?>';
            			break;
            		case '1':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('fail_update_ui')?>";
            			pop("disable-background","msg-error");
            			break;
            		case '2':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('name_blank')?>";
            			hide("disable-background","modify-ui-detail");
            			document.getElementById("btn-ok-msg-error").onclick = function(){
            				hide('disable-background','msg-error');
            				pop('disable-background','modify-ui-detail');
            			};
            			pop("disable-background","msg-error");
            			break;
            		case '3':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			hide("disable-background","modify-ui-detail");
            			pop("disable-background","msg-error");
            			break;	
            	}
            }
		});
	}

      function modify_detection(id){

            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/update_detection",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        dtid: id,
                        dtname: $("#modify-ui-detail-name").val(),
                        dtcost: $("#modify-ui-detail-value").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/2';
                                    break;
                              default :
                                    hide('disable-background','modify-ui-detail');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }
                  }
            });
      }


      function modify_opco(id){
            //alert("hahah");
            
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/update_opco",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        opcoid: id,
                        opconame: $("#modify-opco-detail-name").val(),
                        currency_id: $("#modify-opco-detail-value").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/3';
                                    break;
                              default :
                                    hide('disable-background','modify-ui-detail');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }

                  }
            });

      }

      function modify_revenue(id){
            //alert("hahah");
            
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/update_revenue",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        id: id,
                        revenue_stream_name: $("#modify-revenue-stream-name").val(),
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //location.reload();
                        //alert(data);
                        var obj = jQuery.parseJSON(data);
                        //alert(obj.status);
                        switch(obj.status){
                              case 0:
                                    window.location.href = '<?php echo base_url()?>index.php/admin/ui_management/4';
                                    break;
                              default :
                                    hide('disable-background','modify-revenue-stream');
                                    document.getElementById("notify-error").innerHTML = obj.msg;
                                    pop("disable-background","msg-error");
                                    break;
            
                        }

                  }
            });

      }


	$( document ).ready(function() {

            var startDate = new Date();
            var endDate = new Date();
            $('#dp4').datepicker()
                  .on('changeDate', function(ev){
                        if (ev.date.valueOf() > endDate.valueOf()){
                              $('#alert').show().find('strong').text("<?php echo $this->lang->line('The start date can not be greater then the end date') ?>");
                        } else {
                              $('#alert').hide();
                              startDate = new Date(ev.date);
                              $('#new-start-date').val($('#dp4').data('date'));
                        }
                        $('#dp4').datepicker('hide');
                  });
            $('#dp5').datepicker()
                  .on('changeDate', function(ev){
                        if (ev.date.valueOf() < startDate.valueOf()){
                              $('#alert').show().find('strong').text("<?php echo $this->lang->line('The end date can not be less then the start date') ?>");
                        } else {
                              $('#alert').hide();
                              endDate = new Date(ev.date);
                              $('#new-end-date').val($('#dp5').data('date'));
                        }
                        $('#dp5').datepicker('hide');
                  });


            $("#opco-list").collapse("show");
            active = "<?php echo $acc_active?>";
            switch(active){
                  case "1" :
                        $("#detail-group-currency").collapse("show");                        
                        
                        break;
                  case "2" :
                        $("#detail-detection-tool").collapse("show");
                        break;
                  case "3" :
                        $("#detail-opco").collapse("show");
                        break;
                  case "4" :
                        $("#detail-revenue-stream").collapse("show");
                        break;

                  
    	       
            }
            
	});

      function get_sysdate(){

          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!

          var yyyy = today.getFullYear();
          if(dd<10){
              dd='0'+dd
          } 
          if(mm<10){
              mm='0'+mm
          } 
          var today = yyyy+'-'+mm+'-'+dd;
          return today;
      }

</script>