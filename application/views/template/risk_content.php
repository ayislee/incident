<style type="text/css">
.reff-number {
	display: block;
	text-align: center;
	width: calc(100% - 300px);
	height: 35px;
	border-radius: 3px;
	border: 1px solid #ffc711;
	margin-left: 150px;
	margin-right: 40px;
	margin-bottom: 10px;
	outline: none;
}

.rut-col-1-2 {
	display: inline-block;
	width: 100px;
	padding-left: 20px;
	vertical-align: middle;
}

.rut_content {
  margin-left: 25px;
  margin-top: 5px;
  display: block;
  background-color: #ffc711;
  padding-top: 1px;
  padding-bottom: 1px;
  padding-left: 20px;
  border-radius: 5px;
  width: 850px;
}

.rut-btn-block {
  margin-left: 25px;
  margin-top: 10px;
  display: block;
  background-color: white;
  padding-top: 1px;
  padding-bottom: 1px;
  padding-left: 20px;
  border-radius: 5px;
  width: 850px;
  text-align: right;
}

.table-risk {
  margin-left: 25px;
  width: 850px;
  display: block;
}	

.rut-col-2-2 {
    display: inline-block;
    padding-left: 20px;
    vertical-align: middle;
    width: 350px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
</style>
<div class="rut_content">
	<div class="label-item width10">
		<?php echo $this->lang->line('Risk Universe Type')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="rutid" id="rutid">
			<?php foreach ($rut->result() as $row) { 
				if($selected_rut==$row->rutid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->rutid?>" <?php echo $selected?>><?php echo $row->rutname?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Section Title')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="stid" id="stid">
			<?php foreach ($st->result() as $row) { 
				if($selected_st==$row->stid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->stid?>" <?php echo $selected?>><?php echo $row->stname?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="rut_content margin-top-3" style="display:none">
	<div class="label-item width10">
		<?php echo $this->lang->line('Operator Category')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="ocid" id="ocid">
			<?php foreach ($oc->result() as $row) { 
				if($selected_oc==$row->ocid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->ocid?>" <?php echo $selected?>><?php echo $row->ocname?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Ref Number')?>
	</div>
	<div class="detail-item width250">
		<input class="std-input width250" name="ref_number" id="ref_number">
	</div>
</div>

<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Risk')?>
	</div>
	<div class="detail-item width250">
		<input class="std-input width250" name="rcname" id="rcname">
	</div>
</div>


<div class="rut_content margin-top-3">
	<div class="label-item width10" style="vertical-align: top">
		<?php echo $this->lang->line('Description')?>
	</div>
	<div class="detail-item width250">
		<textarea class="std-input width250" style="height:60px;" name="rcdec" id="rcdec"></textarea>
	</div>
</div>

<div class="rut-btn-block">
      <button class="std-btn bkgr-green" onclick="insert_rc()"><?php echo $this->lang->line('add')?>
</div>

<div class="rut-title">
      <?php echo $this->lang->line('Risk')?>
</div>
<div class="table-risk">
      <div class="table-head">
            <div class="table-head-item rut-col-1-2"><?php echo $this->lang->line('Ref Number')?></div>
            <div class="table-head-item rut-col-2-2"><?php echo $this->lang->line('Risk')?></div>
            <div class="table-head-item rut-col-3"><?php echo $this->lang->line('Description')?></div>
      </div>
      <?php
            foreach ($rc->result() as $row) {
      ?>
      <div class="table-body">
            <div class="table-body-item rut-col-1-2"><?php echo $row->ref_number?></div>
            <div class="table-body-item rut-col-2-2"><?php echo $row->rcname?></div>
            <div class="table-body-item rut-col-3"><?php echo $row->rcdec?></div>
            <div class="table-body-item right"><button class="std-btn bkgr-red" onclick="delete_id(<?php echo $row->rcid?>)"><?php echo $this->lang->line('delete')?></button></div>
            <div class="table-body-item right"><button class="std-btn bkgr-blue" onclick="modify_id(<?php echo $row->rcid?>)"><?php echo $this->lang->line('modify')?></button></div>
      </div>
      <?php
             } 
      ?>

</div>


<div class="ontop" id="disable-background"></div>

<div class="add-access-module" id="win-msg">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="detail-group-error"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-green" onClick="hide('disable-background','win-msg')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>

<div class="del-access-module" id="del-rc">
    <div class="modify-title">
        <?php echo $this->lang->line('delete'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Are you sure to delete'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onClick="remove_rc()"><?php echo $this->lang->line('delete'); ?></button>
        <button class="std-btn bkgr-grey" onClick="hide('disable-background','del-rc')"><?php echo $this->lang->line('cancel'); ?></button>
    </div>
</div>

<div class="add-modify-ui-detail" id="modify-rc">
      <div class="modify-title" id="title-modify-detail"></div>
      <div class="rows">
      		<input type="text" class="reff-number" id="modify-reff-number">
            <input type="hidden" name="modify-rcid" id="modify-rcid">
            <input class="input-ui-detail" id="modify-rcname" name="modify-rcname" type="text">
            <textarea class="input-ui-detail" style="height:60px;" name="modify-rcdec" id="modify-rcdec"></textarea>
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-st" onclick="update_rc()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-rc')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>

<script type="text/javascript">
	$( "#rutid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/risk/"+$("#rutid").val();
	});

	$( "#stid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/risk/"+$("#rutid").val()+"/"+$("#stid").val();
	});	

	$( "#ocid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/risk/"+$("#rutid").val()+"/"+$("#stid").val()+"/"+$("#ocid").val();
	});	


	var delete_rc_id;

	function pop(div,div2) {
	    document.getElementById(div).style.display = 'block';
	    document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
	    document.getElementById(div).style.display = 'none';
	    document.getElementById(div2).style.display = 'none';
	}


	function insert_rc(){
		//alert('hahh');
		$.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/insert_rc",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 

				stid: $("#stid").val(),
				ocid: $("#ocid").val(),
				ref_number: $("#ref_number").val(),
				rcname: $("#rcname").val(),
				rcdec: $("#rcdec").val(),
				sess: "<?php echo session_id()?>"
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
				
			},
			success: function(data){
			    //alert(data);
				switch(data){
					case '0':
						location.reload();
						break;
					case '1':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to save data')?>";
						pop('disable-background','win-msg');
						break;
					case '2':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Please fill in all fields')?>";
						pop('disable-background','win-msg');
						break;
					case '3':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
						pop('disable-background','win-msg');
						break;
					case '-2':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Reference Number Error')?>";
						pop('disable-background','win-msg');
						break;
				}
			}
		});

	}

	function delete_id(id){
		delete_rc_id = id;
        pop('disable-background','del-rc');
	}

	function remove_rc(){
	    //alert(delete_rut_id);
	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/admin/delete_rc",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	                rcid: delete_rc_id,
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	                //alert(data);
	                switch(data){
	                      case '0':
	                            location.reload();
	                            break;
	                      case '1':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to delete data')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                      case '2':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                }
	          }
	    });
	}

	function modify_id(id){
		//alert(rcdec);
		//alert(id);
		
		 $.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/rc",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 
			    rcid: id,
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
			    
			},
			success: function(data){
			    //alert(data);
			    var obj = JSON.parse(data);
			    switch(obj.status){
			          case 0:
			          		$("#modify-reff-number").val(obj.ref_number);
						    $("#modify-rcname").val(obj.rcname);
						    $("#modify-rcid").val(obj.rcid);
						    $("#modify-rcdec").val(obj.rcdec);
						    pop('disable-background','modify-rc');
			                
			                break;
			          default:
			                break;
			    }
			}
		});



	}

	function update_rc(){
	    $.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/update_rc",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 
				ocid: $("#ocid").val(),
				ref_number: $("#modify-reff-number").val(),
			    rcid: $("#modify-rcid").val(),
			    rcname: $("#modify-rcname").val(),
			    rcdec: $("#modify-rcdec").val(),
			    stid: $("#stid").val(),
			    sess: "<?php echo session_id()?>"
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
			    
			},
			success: function(data){
			    //alert(data);
			    switch(data){
			          case '0':
			                location.reload();
			                break;
			          case '1':
			                hide('disable-background','modify-rc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to update data')?>";
			                pop('disable-background','win-msg');
			                break;
			          case '2':
			                hide('disable-background','modify-rc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Please fill in all fields')?>";
			                pop('disable-background','win-msg');
			                break;
			          case '3':
			                hide('disable-background','modify-rc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
			                pop('disable-background','win-msg');
			                break;
			          case '-2':
			          		hide('disable-background','modify-rc');
							document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Reference Number Error')?>";
							pop('disable-background','win-msg');
						break;
			    }
			}
		});
	}
</script>