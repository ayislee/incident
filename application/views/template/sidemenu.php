<style type="text/css">
.sidemenu {
	position: absolute;
	display: inline-block;
	
	width: 300px;
	height: 100%;
	min-height: 665px;
	padding-top: 56px;
	padding-left: 11px;
	color: white;
	overflow: hidden; 

}
.sidemenu-2 {
	position: fixed;
	top: 0px;
	left: 0px;
	display: inline-block;
	background-color: #4a4c4e;
	width: 300px;
	height: 100%;
	min-height: 665px;
	padding-top: 56px;
	padding-left: 11px;
	color: white;
	overflow: hidden; 
	z-index: 3000;

}

</style>
<div class="sidemenu">
</div>
<div class="sidemenu-2">
	<?php
	if($this->session->userdata('get_privilege')[4]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
	?>
	<div class="menu-item">
		<i class="fa fa-user" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/admin/user_management" class="menu-link">
			<?php echo $this->lang->line('admin_menu_user_management'); ?>
		</a>
	</div>
	<?php } ?>
	<?php 
	if($this->session->userdata('get_privilege')[0]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
	<div class="menu-item" id="privilege-1">
		<i class="fa fa-users" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/admin/group_management" class="menu-link">
			<?php echo $this->lang->line('admin_menu_group_management'); ?>
		</a>
	</div>
	<?php } ?>

	<?php
	if($this->session->userdata('get_privilege')[5]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ 
	?>
	<div class="menu-item">
		<i class="fa fa-keyboard-o" aria-hidden="true"></i>
		<button  class="menu-link btn-sidemenu bkgr-black" data-toggle="collapse" data-target="#risk-list">
			<?php echo $this->lang->line('admin_menu_risk_universe_mapping'); ?>
		</button>
		<div class="sidemenu-sub collapse" id="risk-list">
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/rut" class="menu-link indent10">
					<?php echo $this->lang->line('Risk Universe Type'); ?>
				</a>
			</div>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/st" class="menu-link indent10">
					<?php echo $this->lang->line('Section Title'); ?>
				</a>
			</div>
			<!--
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/oc" class="menu-link indent10">
					<?php echo $this->lang->line('Operator Category'); ?>
				</a>
			</div>
			-->
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/risk" class="menu-link indent10">
					<?php echo $this->lang->line('Risk'); ?>
				</a>
			</div>
		</div>
	</div>
	<?php } ?>
	
	<div class="menu-item">
		<i class="fa fa-sitemap" aria-hidden="true"></i>
		<button class="menu-link btn-sidemenu bkgr-black" data-toggle="collapse" data-target="#root-category">
			<?php echo $this->lang->line('root_cause_category'); ?>
		</button>
		<div class="sidemenu-sub collapse" id="root-category">
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/root_cause_category" class="menu-link indent10">
					<?php echo $this->lang->line('category'); ?>
				</a>
			</div>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/root_type" class="menu-link indent10">
					<?php echo $this->lang->line('type'); ?>
				</a>
			</div>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/root_name" class="menu-link indent10">
					<?php echo $this->lang->line('name'); ?>
				</a>
			</div>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/root_action" class="menu-link indent10">
					<?php echo $this->lang->line('action'); ?>
				</a>
			</div>
		</div>
	</div>

	<!-- CR003 -->
	<div class="menu-item">
		<i class="fa fa-cog" aria-hidden="true"></i>
		<button class="menu-link btn-sidemenu bkgr-black" data-toggle="collapse" data-target="#kpi-list">
			<?php echo $this->lang->line('control_coverage_kpi'); ?>
		</button>
		<div class="sidemenu-sub collapse" id="kpi-list">
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/controlkpi/cckpi" class="menu-link indent10">
					<?php echo $this->lang->line('control_coverage_kpi'); ?>
				</a>
			</div>
			<div class="sub-menu-item">
				<a href="" class="menu-link indent10">
					<?php echo $this->lang->line('history'); ?>
				</a>
			</div>
			
		</div>
		
	</div>
	<!-- END CR003 -->
	

	<div class="menu-item">
		<i class="fa fa-cogs" aria-hidden="true"></i>
		<button class="menu-link btn-sidemenu bkgr-black" data-toggle="collapse" data-target="#opco-list">
			<?php echo $this->lang->line('Configuration'); ?>
		</button>
		<div class="sidemenu-sub collapse" id="opco-list">
			<?php if($this->session->userdata('get_privilege')[6]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/ui_management" class="menu-link indent10">
					<?php echo $this->lang->line('General Configuration'); ?>
				</a>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('get_privilege')[7]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/opco_config_table" class="menu-link indent10">
					<?php echo $this->lang->line('admin_menu_opco_configuration'); ?>
				</a>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('get_privilege')[8]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/report_config" class="menu-link indent10">
					<?php echo $this->lang->line('admin_menu_report_configuration'); ?>
				</a>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('get_privilege')[9]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
			<div class="sub-menu-item">
				<a href="<?php echo base_url()?>index.php/admin/exchange_rate" class="menu-link indent10">
					<?php echo $this->lang->line('admin_menu_exchange_configuration'); ?>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>

	<?php if($this->session->userdata('get_privilege')[3]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
	<div class="menu-item" id="privilege-4">
		<i class="fa fa-list-ul" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/admin/audit_trail" class="menu-link">
			<?php echo $this->lang->line('admin_menu_audit_trail'); ?>
		</a>
	</div>
	<?php } ?>
	<div class="menu-item">
		<i class="fa fa-question-circle" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/admin/faq" class="menu-link">
			<?php echo $this->lang->line('admin_menu_faq'); ?>
		</a>
	</div>
	<div class="menu-item item-last">
		<i class="fa fa-sign-out" aria-hidden="true"></i>
		<a href="<?php echo base_url()?>index.php/login/logout" class="menu-link">
			<?php echo $this->lang->line('admin_menu_logout'); ?>
		</a>
	</div>

	
</div>


<script type="text/javascript">
	$( document ).ready(function() {

	});

	function set_privilege(id){
		$.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/module_privilege",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                privilege_id: id,
                group_id: "<?php echo $this->session->userdata('group_id')?>",
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //location.reload();
                if(data=="0"){
                	$("#privilege-"+id).addClass("hide");
                	$('[name="privilege-'+id+'"]').addClass("hide");
                }
            }
        });		
	}
</script>

