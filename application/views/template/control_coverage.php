    <!-- This accordion Control Coverage-->
    <!--
    <div class="ui-menu-item">
        <div class="ui-menu-item-title"><?php echo $this->lang->line('Control Coverage')?></div>
        <button class="btn-down " id="btn-down-group-currency" data-toggle="collapse" data-target="#detail-control-coverage"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_cc()"></i></i></button>
    </div>
    <style type="text/css">
    .wid75 {
        width:75px;
    }
    .wid200 {
        width:180px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        vertical-align: middle;
        margin-right: 5px;

    }
    </style>

   
    <div id="detail-control-coverage" class="ui-detail collapse">
        <div class="ui-detail-content">
        <?php
            foreach ($control_coverage->result() as $control_coverage) { ?>
            <div class="ui-detail-row">
                <div class="ui-detail-item wid200"><?php echo $control_coverage->ccname?></div>
                <div class="ui-detail-item wid75"><?php echo $control_coverage->start_date?></div>
                <div class="ui-detail-item wid75"><?php echo $control_coverage->end_date?></div>
                <button class="std-btn bkgr-red ui-detail-item-btn" onclick="delete_control_coverage_<?php echo $control_coverage->ccid?>()"><?php echo $this->lang->line('delete')?></button>
                <button class="std-btn bkgr-green ui-detail-item-btn" onclick="modify_control_coverage_<?php echo $control_coverage->ccid?>()"><?php echo $this->lang->line('modify')?></button>
            </div>


             <script type="text/javascript">
                    
                function modify_control_coverage_<?php echo $control_coverage->ccid?>(){
                    document.getElementById("title-add-control-coverage").innerHTML="<?php echo $this->lang->line('Modify for').' '.$control_coverage->ccname?>";
                    
                    $("#new-control-coverage-name").val("<?php echo $control_coverage->ccname?>");
                    $("#new-start-date").val("<?php echo $control_coverage->start_date?>");
                    $("#new-end-date").val("<?php echo $control_coverage->end_date?>");
                    document.getElementById("btn-save-add-control-coverage").onclick = function() {modify_control_coverage("<?php echo $control_coverage->ccid?>")};
                    pop("disable-background","add-control-coverage");
                }
                
               function delete_control_coverage_<?php echo $control_coverage->ccid?>(){
                    //alert("haha");
                    document.getElementById("ui-detail-delete-confirm").innerHTML="<?php echo $this->lang->line('Are you sure to delete')?>";
                    document.getElementById("btn-delete-ui-detail").onclick = function() {delete_control_coverage("<?php echo $control_coverage->ccid ?>")};
                    pop("disable-background","win-del-confirm");
                }
           </script>
        <?php
             }
        ?>
        </div>
        <div class="ui-control">
            <button class="std-btn bkgr-blue" onclick="add_control_coverage()"><?php echo $this->lang->line('add')?></button> 
        </div>  
    </div>
    <script>
        function func_cc(){ 
            $(".collapse").collapse("hide");
        }

        function add_control_coverage(){
            document.getElementById("title-add-control-coverage").innerHTML="<?php echo $this->lang->line('add_new').' '.$this->lang->line('Control Coverage')?>";
            document.getElementById("add-control-coverage-message").innerHTML="<?php echo $this->lang->line('Please add').' '.$this->lang->line('Control Coverage').' '.$this->lang->line('name that you desire')?>";
            document.getElementById("new-control-coverage-name").placeholder = "<?php echo $this->lang->line('Type Control Coverage name ...')?>";
            document.getElementById("new-ui-detail-value").style.display = 'none';
            $("#new-control-coverage-name").val("");
            $("#new-start-date").val(get_sysdate());
            $("#new-end-date").val(get_sysdate());
            $('#dp4').data(get_sysdate());
            $('#dp5').data(get_sysdate());
            $('#alert').hide();

            document.getElementById("btn-save-add-control-coverage").onclick = function() {add_control_coverage_list()};
            pop("disable-background","add-control-coverage");
        }

      function get_sysdate(){

          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!

          var yyyy = today.getFullYear();
          if(dd<10){
              dd='0'+dd
          } 
          if(mm<10){
              mm='0'+mm
          } 
          var today = yyyy+'-'+mm+'-'+dd;
          return today;
      }

    </script>      
    -->