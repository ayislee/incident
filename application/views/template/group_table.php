<div class="group-table">
	<div class="table-head">
		<div class="table-head-item col-74"><?php echo $this->lang->line('id')?></div>
		<div class="table-head-item col-133"><?php echo $this->lang->line('group_name')?></div>
		<div class="table-head-item col-97"><?php echo $this->lang->line('group_status')?></div>
		<!--
		<div class="table-head-item col-81"><?php echo $this->lang->line('group_type')?></div>
		<div class="table-head-item col-97"><?php echo $this->lang->line('authentification_type')?></div>
		-->
		<div class="table-head-item col-122"><?php echo $this->lang->line('date_created')?></div>
		<div class="table-head-item col-93"><?php echo $this->lang->line('created_by')?> </div>
		<div class="table-head-item col-133"><?php echo $this->lang->line('date_update')?> </div>
		<div class="table-head-item col-93"><?php echo $this->lang->line('update_by')?> </div>

	</div>

	<?php echo $group_table ?>
	

</div>

<div class="navigator">
	<?php echo $pagination ?>
	<div class="command">
		<?php if($this->session->userdata('get_privilege')[2]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
		<button class="std-btn bkgr-green" onclick="add_group()" id="privilege-3"><?php echo $this->lang->line('add')?></button>
		<?php } ?>
	</div>
</div>

<div class="ontop" id="disable-background"></div>

<div class="pop-user-modify" id="add-new-group">
	<div class="modify-title">
		<?php echo $this->lang->line('add_group'); ?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group_name')?>
			</div>
			:
			<div class="detail-item ">
				<input class="std-input" id="new-group_name" name="new-group_name" class="input-items" type="text">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group_status')?>
			</div>
			:
			<div class="detail-item">
				<select class="std-input" id="new-group-status" name="new-group-status">
					<option value="1" selected><?php echo $this->lang->line('enable')?></option>
					<option value="0"><?php echo $this->lang->line('disable')?></option>
				</select>
			</div>
		</div>
		<!--
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group_type')?>
			</div>
			:
			<div class="detail-item">
				<select class="std-input" id="new-group-type" name="new-group-type">
					<?php foreach ($group_type->result() as $row) { ?>
					<option value="<?php echo $row->id?>"><?php echo $row->group_type_name?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('authentification_type')?>
			</div>
			:
			<div class="detail-item">
				<select class="std-input" id="new-group-authen" name="new-group-authen">
					<?php foreach ($group_authen->result() as $row) { ?>
					<option value="<?php echo $row->id?>"><?php echo $row->group_authen_name?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		-->
		<div class="rows center10">
			<button class="std-btn bkgr-green" onclick="add_new_group()"><?php echo $this->lang->line('add')?></button>
			<button class="std-btn bkgr-red" onclick="hide('disable-background','add-new-group')"><?php echo $this->lang->line('cancel')?></button>
		</div>
	</div>

</div>

<div class="add-access-module" id="result-group-error">
	<div class="modify-title">
		<?php echo $this->lang->line('Notify'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','result-group-error');pop('disable-background','add-new-group');"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<script type="text/javascript">
	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}

	function add_group(){
		$("#new-group_name").val('');
		$("#new-group-status").val('1');
		$("#new-group-type").val('1');
		$("#new-group-authen").val('1');
		pop('disable-background','add-new-group');
	}

	function add_new_group(){
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_new_group",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	group_name: $("#new-group_name").val(),
            	//group_type: $("#new-group-type").val(),
            	//group_authen: $("#new-group-authen").val(),
            	group_type: null,
            	group_authen: null,
            	group_status: $("#new-group-status").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	switch(data){
            		case '0':
            			window.location.href = "<?php echo base_url()?>index.php/admin/group_management";
            			break;
            		case '1':
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('fail_add_group')?>";
            			hide("disable-background","add-new-group");
            			pop("disable-background","result-group-error");
            			break;
            		case '2':
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('group_name_blank')?>";
            			hide("disable-background","add-new-group");
            			pop("disable-background","result-group-error");
            			break;
            		case '3':
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			hide("disable-background","add-new-group");
            			pop("disable-background","result-group-error");
            			break;
            	}
            }
		});
	}

</script>
