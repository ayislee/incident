<div class="log-table" id="log-table">
	<div class="table-head">
		<div class="table-head-item col-nomor"><?php echo $this->lang->line('number')?></div>
		<div class="table-head-item col-username"><?php echo $this->lang->line('date')?></div>
		<div class="table-head-item col-username"><?php echo $this->lang->line('user')?></div>
		<div class="table-head-item col-username"><?php echo $this->lang->line('module')?></div>
		<div class="table-head-item col-username"><?php echo $this->lang->line('activity')?></div>
		<div class="table-head-item col-username"><?php echo $this->lang->line('description')?></div>
	</div>

	<?php echo $log_table ?>

</div>
<div class="navigator">
	<?php echo $pagination ?>
</div>


<script type="text/javascript">
	
	var resizeEventsTrigger = (function () {
	    function triggerResizeStart($el) {
	        $el.trigger('resizestart');
	        isStart = !isStart;
	    }

	    function triggerResizeEnd($el) {
	        clearTimeout(timeoutId);
	        timeoutId = setTimeout(function () {
	            $el.trigger('resizeend');
	            isStart = !isStart;
	        }, delay);
	    }

	    var isStart = true;
	    var delay = 200;
	    var timeoutId;

	    return function ($el) {
	        isStart ? triggerResizeStart($el) : triggerResizeEnd($el);
    	};

	})();

	$("body").on('resizestart', function () {
	    // console.log('resize start');
	});
	$("body").on('resizeend', function () {
		refwindow();
	});

	window.onresize = function () {
	    resizeEventsTrigger( $("body") );
	};

	function refwindow(){
		console.log("wheight:"+window.innerHeight);
	    // console.log('resize end');
	    totalusertable = window.innerHeight;
	    console.log("TotalUsertable"+totalusertable);
	    headerheight = 56;
	    titleheight = 31;
	    breadcrumheight = 31;
	    searchheight = 120;
	    navigatorheight = 41;
	    usertable = totalusertable - headerheight- headerheight - titleheight - breadcrumheight - searchheight - navigatorheight;
	    document.getElementById("log-table").style.maxHeight = usertable+"px";
	    console.log("usertable:"+usertable);
	}

	$(window).resize(function()
	{   

	    refwindow();
	});
	
	$( document ).ready(function() {
		
	    refwindow();

	});
</script>