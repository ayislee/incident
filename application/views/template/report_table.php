<style type="text/css">

	.tbl-msg{
		text-align: center;
		padding-left: 10px;
		padding-right: 10px;
		padding-top: 0px;
		padding-bottom: 10px;
	}

	.c1 {
		padding-left: 10px;
		width: 50px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		vertical-align: middle;

	}
	.c2 {
		padding-left: 10px;
		width: 300px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		vertical-align: middle;
	}
	.c3 {
		padding-left: 10px;
		width: 400px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		vertical-align: middle;
	}
	.ct1 {
		padding-left: 10px;
		width: 150px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		vertical-align: middle;
		float: left;
		text-align: left;
	}

	.ct2 {
		padding-left: 10px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		vertical-align: middle;
		float: left;
	}

	.w-input{
		width: 300px;
	}
	.w100 {
		width: 100px;
	}



	.std-input:disabled {
		background-color: transparent;
		border-style: none;

	}
	.right-align{
		float: right;
		padding-right: 10px;
	}

	.wfix {
		max-width: 100px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}
</style>

<div class="group-table">
	<div class="table-head">
		<div class="table-head-item c1"><?php echo $this->lang->line('id')?></div>
		<div class="table-head-item c2"><?php echo $this->lang->line('report_name')?></div>
		<div class="table-head-item c3"><?php echo $this->lang->line('report_path')?></div>
	</div>
	<?php foreach ($reports->result() as $report): ?>
	<div class="table-body">
		<div class="table-body-item c1">#<?php echo $report->reportid?></div>
		<div class="table-body-item c2"><?php echo $report->report_name?></div>
		<div class="table-body-item c3"><?php echo $report->report_path?></div>
		<div class="table-body-item right-align"><button class="std-btn bkgr-blue wfix" id="btn-modify-<?php echo  $report->reportid?>"><?php echo $this->lang->line('modify')?></button></div>
		<div class="table-body-item right-align"><button class="std-btn bkgr-red wfix" id="btn-delete-<?php echo  $report->reportid?>"><?php echo $this->lang->line('delete')?></button></div>
	</div>
	<script type="text/javascript">
		$("#btn-delete-<?php echo $report->reportid?>").click(function(){
			$("#btn-delete-yes").click(function(){ do_delete(<?php echo $report->reportid?>);});
			pop('disable-background','w-delete');
		});

		$("#btn-modify-<?php echo $report->reportid?>").click(function(){
			get_profile(<?php echo $report->reportid?>);
			pop('disable-background','modify-report');
		});

	</script>
	<?php endforeach ?>
</div>

<div class="navigator">
	<div class="command">
		<button class="std-btn bkgr-green" onclick="add_report()" id="privilege-3"><?php echo $this->lang->line('add')?></button>
	</div>
</div>

<div class="ontop" id="disable-background"></div>
<div class="ontop2" id="disable-background2"></div>


<div class="add-report" id="add-report">
	<div class="modify-title">
		<?php echo $this->lang->line('Add Report'); ?>
	</div>
	<div class="report-form" id="detail-group-error">
		<div class="rows">
			<div class="c-label"><?php echo $this->lang->line('report_name')?></div>
			<div class="c-input" ><input class="std-input w-input" id="r-name"></div>
		</div>
		<div class="rows">
			<div class="c-label"><?php echo $this->lang->line('report_path')?></div>
			<div class="c-input" ><input class="std-input w-input" id="r-path"></div>

		</div>
		<div class="rows">
			<div class="c-label"></div>
			<div class="c-input"><button class="std-btn bkgr-green" onClick="get_params(0)"><?php echo $this->lang->line('Get Parameters')?></button></div>
		</div>
	</div>
	<div class="tbl-msg" id='msg'></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green w100" onClick="save_report(0);"><?php echo $this->lang->line('save'); ?></button>
		<button class="std-btn bkgr-red w100" onClick="hide('disable-background','add-report');"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="add-report" id="modify-report">
	<div class="modify-title">
		<?php echo $this->lang->line('Modify Report'); ?>
	</div>
	<div class="report-form" >
		<div class="rows">
			<input type="hidden" id="m-id">
			<div class="c-label"><?php echo $this->lang->line('report_name')?></div>
			<div class="c-input" ><input class="std-input w-input" id="m-name"></div>
		</div>
		<div class="rows">
			<div class="c-label"><?php echo $this->lang->line('report_path')?></div>
			<div class="c-input" ><input class="std-input w-input" id="m-path"></div>

		</div>
		<div class="rows">
			<div class="c-label"></div>
			<div class="c-input"><button class="std-btn bkgr-green" onClick="get_params(1)"><?php echo $this->lang->line('Get Parameters')?></button></div>
		</div>
	</div>
	<div class="tbl-msg" id='modify-msg'></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green w100" onClick="save_report(1);"><?php echo $this->lang->line('save'); ?></button>
		<button class="std-btn bkgr-red w100" onClick="hide('disable-background','modify-report');"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>


<div class="add-access-module" id="w-msg">
	<div class="modify-title" id="w-title"></div>
	<div class="confirm-message" id="w-messages"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" id="w-btn"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div class="add-access-module" id="w-delete">
	<div class="modify-title">
		<?php echo $this->lang->line('Warning'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('delete_report_confirm_message')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-red" id="btn-delete-yes"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-green" onClick="hide('disable-background','w-delete')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<script type="text/javascript">
	var num_params=0;
	// kesalahan di pembacaan jumlah parameter ... kerjakan blog !!!!

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}

	function add_report(){
		$("#r-name").val('');
		$('#r-path').val('');
		document.getElementById('msg').innerHTML = '';
		pop('disable-background','add-report');
	}

	function get_params(type){
		document.getElementById('disable-background2').style.display = 'block';

		if(type==0){
			path= $("#r-path").val();
			name= $("#r-name").val();
		}else{
			path= $("#m-path").val();
			name= $("#m-name").val();
		}
		
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/get_params",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	path: path,
            	name: name,
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	var obj = jQuery.parseJSON(data);

            	switch(obj.status) {
            		case 0:
            			//alert(obj.params);
            			document.getElementById('disable-background2').style.display = 'none';
            			num_params = obj.params.length; 
            			generate_table(obj.params,type);


            			break;
            		default:
            			document.getElementById('disable-background2').style.display = 'none';
            			if(type==0){
            				document.getElementById('msg').innerHTML = obj.msg;
            			}else{
            				document.getElementById('modify-msg').innerHTML = obj.msg;
            			}
            			break;
            	}
            }
		});
	}

	function generate_table(parameters,type){
		//alert(JSON.stringify(parameters));
		//document.getElementById('msg').innerHTML = JSON.stringify(parameters);
		$.ajax({
			type: "POST",  

            url: "<?php echo base_url()?>index.php/admin/generate_params_table",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	type: type,
            	parameters: JSON.stringify(parameters)
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	if(type==0){
            		document.getElementById('msg').innerHTML=data;
            		document.getElementById('modify-msg').innerHTML='';
            	}else{
            		document.getElementById('msg').innerHTML='';
            		document.getElementById('modify-msg').innerHTML=data;
            	}
            	
            }
		});

	}

	function save_report(type){
		// alert(num_params);

		if(num_params >0){
			arr_params = [];
			if(type==0){
				for(i=1;i<=num_params;i++){
					if($("#p_"+i).is(":checked")){
						checked=1;
					}else{
						checked=0;
					}

					r_params = {name: $("#param_name_"+i).val(),multiselect:checked};
					arr_params.push(r_params);
				}
			}else{
				for(i=1;i<=num_params;i++){
					if($("#e_p_"+i).is(":checked")){
						checked=1;
					}else{
						checked=0;
					}

					r_params = {name: $("#e_param_name_"+i).val(),multiselect:checked};
					arr_params.push(r_params);
				}
			}

			json = JSON.stringify(arr_params);
			//document.getElementById('msg').innerHTML=json;
			document.getElementById('disable-background2').display="block";
			if(type==0){ 
				url = "<?php echo base_url()?>index.php/admin/insert_report";
				p = {
						json: json,
	            		name: $("#r-name").val(),
	            		path: $("#r-path").val(),
	            		sess: "<?php echo session_id()?>"
	            	};
			}else{
				url = "<?php echo base_url()?>index.php/admin/update_report";
				p = {
						id: $("#m-id").val(),
						json: json,
	            		name: $("#m-name").val(),
	            		path: $("#m-path").val(),
	            		sess: "<?php echo session_id()?>"
	            	};
			}
			// alert(JSON.stringify(p));

			$.ajax({
				type: "POST",  
	            url: url,  
	            contentType: 'application/x-www-form-urlencoded',
	            data: p,
	            dataType: "text",
	            beforeSend: function(){

	            },
	            complete: function(){
	            	
	            },
	            success: function(data){
	            	var obj = jQuery.parseJSON(data);
	            	//alert(data);
	            	//document.getElementById('msg').innerHTML=data;
	            	switch(obj.status){
	            		case 0:
	            			document.getElementById('disable-background2').display="none";
	            			if(type==0){
		            			hide('disable-background','add-report');
	            			}else{
	            				hide('disable-background','modify-report');
	            			}
	            			document.getElementById('w-title').innerHTML = "<?php echo $this->lang->line('Message')?>";
	            			document.getElementById('w-messages').innerHTML = "<?php echo $this->lang->line('Configuration has been saved')?>";
	            			document.getElementById("w-btn").onclick = function () { location.reload(); };
	            			pop('disable-background','w-msg');
	            			
	            			break;
	            		default:
	            			document.getElementById('disable-background2').display="none";
	            			if(type==0){
	            				hide('disable-background','add-report');
	            			}else{
	            				hide('disable-background','modify-report');
	            			}
	            			
	            			document.getElementById('w-title').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
	            			document.getElementById('w-messages').innerHTML = obj.msg;
	            			document.getElementById("w-btn").onclick = function () { hide('disable-background','w-msg'); };
	            			pop('disable-background','w-msg');
	            			break;
	            	}
	            }
			});

		}
	}

	function do_delete(id){
		//alert("do delete");
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/delete_report",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id: id,
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	var obj = jQuery.parseJSON(data);
            	//alert(data);
            	//document.getElementById('msg').innerHTML=data;
            	switch(obj.status){
            		case 0:
            			
            			hide('disable-background','w-delete');
            			document.getElementById('w-title').innerHTML = "<?php echo $this->lang->line('Message')?>";
            			document.getElementById('w-messages').innerHTML = "<?php echo $this->lang->line('Report deleted')?>";
            			document.getElementById("w-btn").onclick = function () { location.reload(); };
            			pop('disable-background','w-msg');
            			
            			break;
            		default:
            			document.getElementById('disable-background2').display="none";
            			hide('disable-background','add-report');
            			document.getElementById('w-title').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
            			document.getElementById('w-messages').innerHTML = obj.msg;
            			document.getElementById("w-btn").onclick = function () { hide('disable-background','w-msg'); };
            			pop('disable-background','w-msg');
            			break;
            	}
            }
		});
	}

	function get_profile(id){
		//alert(id);
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/get_report",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id: id,
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	var obj = jQuery.parseJSON(data);
            	//document.getElementById('modify-msg').innerHTML=data;

            	switch(obj.status) {
            		case 0:
            			//alert(obj.params);
            			$("#m-id").val(obj.report['report_id']);
            			$("#m-name").val(obj.report['report_name']);
            			$("#m-path").val(obj.report['report_path']);
            			//$('#m-name').val(obj.)
            			document.getElementById('modify-msg').innerHTML=obj.report['parameters'];
            			num_params = obj.report['num_params'];
            			break;
            		default:
            			break;
            	}
            }
		});
	}

	$(document).ready(function(){
		$("#opco-list").collapse("show");
	});

</script>