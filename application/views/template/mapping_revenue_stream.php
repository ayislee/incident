<style type="text/css">
    .area-save {
        margin-left: 25px;
        width: 1000px;

    }
    .f-r{
        float: right;
    }
    .report-col-1{
        width: 820px;
    }
</style>
<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
<div class="area-save w1000">
    <div class="f-r">
        <button class="std-btn bkgr-green" onclick="revenue_save()"><?php echo $this->lang->line('save')?></button>
        <button class="std-btn bkgr-red" onclick="revenue_cancel()"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>
<?php } ?>
<div id="query"></div>

<div class="del-access-module" id="save-module">
    <div class="modify-title">
        <?php echo $this->lang->line('Notify'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Configuration has been saved'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onClick="location.reload()"><?php echo $this->lang->line('ok'); ?></button>
    </div>
</div>
<script type="text/javascript">
    function change_revenue_stream(revenue_id,group_id){
        if($("#revenue_stream_"+revenue_id+"_group_"+group_id+"_view").prop('checked')){
            val_view = 1;
        }else{
            val_view = 0;
        }
        
        var obj_report = {
            group_id: group_id,
            revenue_id: revenue_id,
            view: val_view
        }
        //console.log(obj_report);
        return obj_report;

        
        
    }

    function revenue_save(){
        //alert("haha");
        var json = '<?php echo json_encode($revenue_list->result_array())?>';
        var obj = JSON.parse(json);
        console.log(obj);

        //alert(obj.length);
        //alert("jsdkj");
        var all_revenue = [];
        obj.forEach(function(item, index){
            var list_module = item.opcoid;
            all_revenue.push(change_revenue_stream(item.id,<?php echo $id_group ?>));
            //alert(item.opconame); 
        });
        var json_str = JSON.stringify(all_revenue);
        console.log(json_str);

        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/change_map_revenue",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                json: json_str
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                console.log(data);
                //location.reload();
                document.getElementById('query').innerHTML=data;
                //alert(data);
                pop('disable-background','save-module');
            }
        });

        
    }

    function report_cancel(){
        location.reload();
    }
</script>