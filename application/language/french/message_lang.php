<?php
$lang['welcome_message'] 	= 'Bienvenue';
$lang['login']				= 'connectez-vous';
$lang['username']			= "nom d'utilisateur";
$lang['password']			= 'mot de passe';
$lang['invalid_login']		= "Nom d'utilisateur ou mot de passe est invalide";
$lang['user_password_required']		= "Nécessite une entrée de nom d'utilisateur et mot de passe";
$lang['user_status_disable']		= "L'utilisateur ou le groupe est désactivé";
$lang['user_not_allowed']			= "Utilisateur non autorisé à accéder à ce module";
$lang['wrong_password']				= 'Mot de passe entré est incorrect';

$lang['disable_module']		= 'Module est désactivé';

$lang['admin_menu_user_management']			= "Gestion de l'utilisateur";
$lang['admin_menu_group_management']		= 'La direction du groupe';
$lang['admin_menu_information_management']	= "Gestion de l'information Univers";
$lang['admin_menu_audit_trail']				= 'Audit Trail';
$lang['admin_menu_faq']						= "Mode d'emploi";
$lang['admin_menu_logout']					= 'De mensonges';
$lang['admin_menu_risk_universe_mapping']	= 'Cartographie des risques Univers';
$lang['admin_menu_opco_configuration']		= 'Configuration OpCo';


$lang['hello']								= 'Salut';
$lang['please_login']						= "S'il vous plaît vous connecter";

$lang['number']								= 'nº';
$lang['name']								= 'Prénom';
$lang['group']								= 'Groupe';
$lang['status']								= 'statut';
$lang['last_login']							= 'Dernière connexion';
$lang['all']								= 'Tout';

$lang['add']								= 'Ajouter';
$lang['delete']								= 'Effacer';

$lang['enable']								= 'Activer';
$lang['disable']							= 'Désactiver';
$lang['user_not_found']						= 'Utilisateur non trouvé';

$lang['id']									= 'identité';
$lang['group_name']							= 'nom du groupe';
$lang['group_status']						= 'le statut de groupe';
$lang['group_type']							= 'Type de groupe';
$lang['authentification_type']				= 'Authentication type';
$lang['date_created']						= 'date de création';
$lang['created_by']							= 'créé par';
$lang['date_update']						= 'Date mise à jour';
$lang['update_by']							= 'mise à jour par';
$lang['group_not_found']					= 'groupe introuvable';
$lang['VimpelCom Group Currency']			= 'VimpelCom Group devise';

$lang['module']								= 'Module';
$lang['start_date']							= 'Date de début';
$lang['end_date']							= 'Date de fin';
$lang['user_admin']							= 'administration des utilisateurs';
$lang['incident']							= 'Incident';
$lang['filter']								= 'Filtre';

$lang['date']								= 'Date';
$lang['user']								= 'Utilisateur';
$lang['activity']							= 'Activité';
$lang['description']						= 'La description';

$lang['confirm']							= 'Confirmer';
$lang['confirm_message']					= 'Etes-vous sûr désactivera compte?';
$lang['confirm_message_en']					= "Êtes-vous sûr d'activer le compte?";
$lang['yes']								= 'Oui';
$lang['cancel']								= 'Annuler';

$lang['email']								= 'Email';
$lang['first_name']							= 'Prénom';
$lang['last_name']							= 'Nom de famille';

$lang['modify']								= 'Modifier';
$lang['save']								= 'sauvegarder';
$lang['Not a valid e-mail address']			= 'Adresse courriel invalide';
$lang['Change Password']					= 'Mot de passe';
$lang['ldap_user']							= "Cet utilisateur à partir d'Active Directory Service Server, ne peut pas changer le mot de passe";
$lang['ok']									= "D'accord";
$lang["Can't change password"]				= "Vous ne pouvez pas changer le mot de passe";
$lang['Change password']					= 'Changer le mot de passe';
$lang['New password']						= 'Nouveau mot de passe';
$lang['Retype password']					= 'retaper le mot de passe';
$lang['Password you entered is not the same']	= 'Mot de passe que vous avez entré est pas le même';
$lang['Update Password success']				= 'succès Mise à jour du mot de passe';
$lang['Update Password fail']					= 'Mise à jour de mot de passe sûr';
$lang['Notify']									= 'Notifier';
$lang['Add User']								= 'Ajouter un utilisateur';
$lang['Check user']								= "Vérifier l'utilisateur";
$lang['Active Directory Service User']			= 'Active Directory Service utilisateur';
$lang['First name']								= 'Prénom';
$lang['Last name']								= 'Nom de famille';
$lang['Local Database User']					= 'Base de données locale utilisateur';
$lang['Password']								= 'Mot de passe';
$lang['Error Message']							= "Message d'erreur";
$lang['Username already registered']			= "il Pseudonyme vous choisissez ou a été existent déjà de type <br> S'il vous plaît vérifier ou tapez un autre nom d'utilisateur que vous désirez, <br> Et vérifier la disponibilité du nom d'utilisateur";
$lang['Invalid command']						= 'session expirée';
$lang['User created']							= 'utilisateur Créé';
$lang['Invalid email']							= 'email invalide';
$lang['Invalid password']						= 'Mot de passe incorrect';
$lang['LDAP binding Failed']					= 'LDAP liaison Échec';
$lang['Group Detail']							= 'Groupe Détail';
$lang['Module Access']							= 'Accès Module';
$lang['delete_group_confirm_message']			= 'Etes-vous sûr supprimera groupe ?';
$lang['Group name already use']					= 'Nom du groupe déjà utilisé';
$lang['remove_module']							= "Vous allez supprimer le module d'accès du groupe, <br> Etes-vous sûr?";
$lang['add_module']								= "Ajouter module d'accès ";
$lang['add_group']								= 'Ajouter un nouveau groupe ';
$lang['fail_add_group']							= 'Échec ajouter un nouveau groupe ';
$lang['group_name_blank']						= 'Nom du groupe est vide ';
$lang['Are you sure to delete']				    = 'Êtes-vous sûr de vouloir supprimer ?';
$lang['add_ui_title']						    = 'Ajouter Univers information Détail';
$lang['fail_add_ui']							= "Échec ajouter de nouveaux détails de l'interface utilisateur ";
$lang['name_blank']								= 'Nom UI est vide ';
$lang['modify_ui_title']						= 'Modifier Univers information Détail';
$lang['fail_update_ui']							= "Échec mise à jour détaillée de l'interface utilisateur ";
$lang['privilege']								= 'privilège';

$lang['You can modify this Group Detail below'] = 'Vous pouvez modifier ce groupe Détail ci-dessous';
$lang['Save privilege is complete'] 			= 'Enregistrer privilège est complète';
$lang['type_username'] 			  				= "S'il vous plaît entrez le nom d'utilisateur que le désir, <br> Et vérifier la disponibilité du nom d'utilisateur";
$lang['Username is blank']						= "Nom d'utilisateur est vide";

$lang['similar_name']							= "Voici un nom similaire qui existent sur notre base de données <br>, fait l'un d'entre eux sont les vôtres?";
$lang['add_new']								= 'Ajouter nouvelle pour ';
$lang['Modify for']								= 'Modifier pour ';
$lang['Available user']							= 'Disponible utilisateur';
$lang['[Active Directory User]']				= '[Active Directory utilisateur]';
$lang['[Available user]']						= '[Disponible utilisateur]';

// Incident

$lang['Incident Management']					= 'gestion des incidents';
$lang['Report']									= 'rapport';

//report
$lang['MFS Report']							= 'MFS Report';
$lang['RA Leakage per Month']					= 'RA Leakage per Month';
$lang['RA % Leakage']							= 'RA % Leakage';
$lang['FM Leakage per Month']					= 'FM Leakage per Month';
$lang['FM % Leakage']							= 'FM % Leakage';
$lang['Combined RAFM Leakage']					= 'Combined RAFM Leakage';
$lang['Combined RAFM % Leakage by Month']		= 'Combined RAFM % Leakage by Month';
$lang['RA by OpCo']							= 'RA by OpCo';
$lang['Comparison RA Leakage, Prevented & Recovery']	= 'Comparison RA Leakage, Prevented & Recovery';
$lang['FM Leakage per OpCo']							= 'FM Leakage per OpCo';
$lang['Comparison FM Leakage, Prevented & Recovery']	= 'Comparison FM Leakage, Prevented & Recovery';
$lang['Combined Leakage per OpCo']						= 'Combined Leakage per OpCo';
$lang['Combined Comparison Leakage, Prevented & Recovery']	= 'Combined Comparison Leakage, Prevented & Recovery';
$lang['Leakage, Prevented & Recovery by OpCo']				= 'Leakage, Prevented & Recovery by OpCo';
$lang['Leakage, Saving By OpCo']							= 'Leakage, Saving By OpCo';
$lang['Incidents by OpCo']									= 'Incidents by OpCo';
$lang['RA % Leakage by OpCo']								= 'RA % Leakage by OpCo';
$lang['FM % Leakage by OpCo']								= 'FM % Leakage by OpCo';
$lang['Combined % Leakage by OpCo']						= 'Combined % Leakage by OpCo';
$lang['Bad Debt by OpCo']									= 'Bad Debt by OpCo';
$lang['Resource by % Saving']								= 'Resource by % Saving';
$lang['Bad Debt by Month']									= 'Bad Debt by Month';
$lang['Leakage Calculation per Month']						= 'Leakage Calculation per Month';
$lang['Leakage V Saving']									= 'Leakage V Saving';
$lang['Incidents Reported By Month']						= 'Incidents Reported By Month';
$lang['RA, FM and Combined Dashboard by Month']			= 'RA, FM and Combined Dashboard by Month';
$lang['RA Leakage Calculation by Month']					= 'RA Leakage Calculation by Month';
$lang['FM Leakage Calculation by Month']					= 'FM Leakage Calculation by Month';

$lang['No']						= 'No';
$lang['OpCo']					= 'Opco';
$lang['Open Incident']			= 'Ouvrir incident';
$lang['Close Incident']			= 'Fermer incident';
$lang['Total']					= 'Total';
$lang['Records']				= 'Enregistrements';

$lang['Risk Universe Type']		= 'Risque Univers type';
$lang['Section Title']			= 'Titre de la section';
$lang['Operator Category']		= "Catégorie de l'opérateur";
$lang['Risk']					= 'Risque';
$lang['VimpelCom Group Currency'] 	= 'VimpelCom Group devise';
$lang['Detection Tool']				= 'Outils de détection';
$lang['OpCo']						= 'OpCo';
$lang['Revenue Stream']				= 'Flux de revenus';
$lang['General']					= 'Général';
$lang['ByPass']						= 'By-pass';
$lang['MFS']						= 'MFS';
$lang['General']					= 'General';
$lang['ByPass']						= 'By-pass';
$lang['MFS']						= 'MFS';
$lang['Configuration UI']			= 'UI Configuration';
$lang['Configuration']				= 'Configuration';
$lang['Type Currency name ...']		= 'Tapez le nom Devise ...';
$lang['Type Currency symbol ...']	= 'Tapez le symbole de devises ...';
$lang['Type Detection tool name ...']		= "Tapez Détection nom de l'outil ...";
$lang['Type Detection tool cost ...']		= "Tapez Détection coût de l'outil ...";

$lang['January']							= 'Janvier';
$lang['February']							= 'Février';
$lang['March']								= 'Mars';
$lang['April']								= 'Avril';
$lang['May']								= 'Mai';
$lang['June']								= 'Juin';
$lang['July']								= 'Juillet';
$lang['August']								= 'Août';
$lang['September']							= 'Septembre';
$lang['October']							= 'Octobre';
$lang['November']							= 'Novembre';
$lang['December']							= 'Décembre';

$lang['Month']								= 'Mois';
$lang['Year']								= 'An';
$lang['Get Opco Configuration']				= 'Obtenez Configuration';

$lang['OpCo no configuration, configuration values set to default'] = 'OpCo aucune configuration, les valeurs de configuration définies par défaut';
$lang['Message']							= 'Message';
$lang['Add OpCo Configuration']				= 'Ajouter une configuration OpCo';
$lang['Period']								= 'Période';
$lang['Configuration in selected already exists, the process is canceled'] = 'Configuration dans déjà sélectionné existe, <par> le processus est annulé';
$lang['Configuration has been saved']		= 'Configuration a été enregistré';
$lang['Are you sure to cancel the operation?'] 	= "Etes-vous sûr d'annuler l'opération?";

$lang['Risk Universe Type List'] 				= 'Risque Univers Liste';
$lang['Fail to save data']						= 'Défaut de sauvegarder les données';
$lang['Fail to update data']					= 'Défaut de mettre à jour les données';
$lang['Risk Universe Type Name is blank']		= 'Risk Universe Type Nom est vide';
$lang['Section Title List'] 					= 'Titre de la section Liste';
$lang['Section Title  Name is blank']			= 'Titre de la section Nom est vide';
$lang['Operator Category List']					= 'Opérateur Liste des catégories';
$lang['Operator Category Name is blank']		= "Catégorie de l'opérateur Nom est vide";
$lang['Description']							= 'La description';
$lang['Risk Category or Description is blank']	= 'Catégorie de risque ou description est vide';
$lang['Please fill in all fields']				= "S'il vous plaît remplir tous les champs ou entrée Date non valide";
$lang['Currency']								= 'Devise';
$lang['Currency Symbol']						= 'Symbole de la monnaie';
$lang['Tools Name']								= 'Outils Nom';
$lang['Cost']									= 'Coût';
$lang['Open Incident']							= 'Incidente Ouverte';
$lang['Close Incident']							= "Près l'incident";

$lang['Search']									= 'Chercher';
$lang['All']									= 'TOUT';
$lang['Open']									= 'OUVERTE';
$lang['Close']									= 'FERMER';
$lang['Filter']									= 'Filtre';
$lang['Incident ID']							= 'Titre';
$lang['External ID']							= 'identité externe';
$lang['Last Update']							= 'Dernière mise à jour';
$lang['No Records']								= 'nº Enregistrements';
$lang['UPLOAD']									= 'TÉLÉCHARGER';
$lang['?']										= '?';
$lang['New Incident']							= 'Nouvel incident';
$lang['risk_tab']								= 'Risque';
$lang['Attachment']								= 'Attachement';
$lang['History']								= 'Histoire';
$lang['Note']									= 'commentaires';
$lang['Impact Severity']						= 'Incidence gravité';
$lang['Detection Date']							= 'incident a commencé';
$lang['Case Start Date']						= 'incident détecté';
$lang['Case End Date']							= 'incident résolu';
$lang['Leakage One Day']						= 'Fuite un jour';
$lang['Recovery']								= 'Récupération';	
$lang['Leakage Frequency']						= 'Fréquence de fuite';
$lang['Leakage Timing']							= 'Timing fuites';	
$lang['Leakage ID Type']						= "Fuites Type d'identité";	
$lang['Recovery Type']							= 'Type de récupération ';	
$lang['Incident Status']						= 'état incidents';	
$lang['Final Leakage']							= 'fuites final';	
$lang['Recovered Value']						= 'Valeur récupérée';	
$lang['Prevented Value']						= 'Valeur empêché';	
$lang['Potential Leakage']						= 'fuites potentiel';	
$lang['Description']							= 'La description';	
$lang['Investigation Note']						= 'Investigation Remarque';	
$lang['Under Investigation']					= 'Dans le cadre des enquêtes';
$lang['Date Time']								= 'Date Heure';
$lang['User']									= 'Utilisateur';
$lang['Filename']								= 'Nom de fichier';
$lang['File Description']						= 'Description du fichier';

$lang['No Files']								= 'nº Fichiers';
$lang['Action']									= 'Action';
$lang['Comments']								= 'Commentaires';
$lang['No Histories']							= 'nº Histoires';
$lang['Upload attachment']						= 'Télécharger la pièce jointe';
$lang['Upload']									= 'Télécharger';
$lang['Create Incident Succes']					= 'Créer un incident Succès';
$lang['Incident already saved']					= 'Incident déjà enregistré';
$lang['View Incident']							= 'Voir incident';
$lang['Update Incident']						= "Mise à jour de l'incident";
$lang['Update']									= 'Mettre à jour';
$lang['Risk Category already selected']			= 'Catégorie de risque déjà sélectionné';
$lang['Under Investigation']					= 'SOUS ENQUÊTE';
$lang['Under Investigation2']					= 'Sous Enquête';

$lang['General Configuration']					= 'Configuration générale';
$lang['rut reset']								= 'Etes-vous sûr de changer Type de risque Univers? <br> Catégorie de risque réinitialisera';
$lang['No']										= 'Nº';
$lang['Contact']								= 'Contact';
$lang['view']									= 'Vue';
$lang['BULK UPLOAD']							= 'TRANSFERT GROUPÉ';
$lang['Currency already exist']					= 'Monnaie existent déjà';
$lang['Upload bulk file']						= 'Télécharger le fichier en vrac';
$lang['Uploading file ... please wait']			= "fichier en cours ... s'il vous plaît patienter";
$lang['upload_error']							= "Quelque chose a mal tourné lors de l'enregistrement du fichier, s'il vous plaît assurez-vous que le fichier csv et essayez à nouveau";
$lang['process_bulk']							= "Essayez d'insérer des données à partir du fichier csv, s'il vous plaît attendre";
$lang['error_1']								= 'champs Numéro correspondent pas à la ligne ';
$lang['insert bulk file complete']				= 'insérer complète des fichiers en vrac';
$lang['Invalid Format csv file']				= 'Format non valide fichier csv';
$lang['Examples']								= 'Examples';
$lang['Example Upload bulk CSV file']			= 'Exemple Télécharger un fichier CSV en vrac';
$lang['line_error']								= 'champ Numéro dans la ligne% l est% f champs), devrait avoir 19 champs';
$lang['Invalid field value']					= 'valeur de champ non valide dans la ligne';
$lang['Upload Result']							= 'Télécharger erreur';

$lang['First']									= 'Premier';
$lang['Previous']								= 'Précédent';
$lang['Last']									= 'Dernier';
$lang['Next']									= 'Prochain';

$lang['Invalid date close status']				= "S'il vous plaît remplir la date correcte, <br> Fermer la date doit être supérieure ou égale à démarrer ou date de fin";
$lang['Invalid date rules']						= "S'il vous plaît remplir la date de détection correcte ou la date de début, la date <br> de détection doit être supérieure ou égale à la date de début";
$lang['Invalid date close status']				= 'règles de date non valide (entrée Future date)';
$lang['Create incident Failed']					= 'Créer un incident Échec';
$lang['Create Incident Success']				= 'Créer un incident Succès';
$lang['Update Incident Success']				= "Mise à jour de l'incident Succès";
$lang['Update incident Failed']					= "Mise à jour de l'incident a échoué";

$lang['detection date empty']					= 'detektion la date empti';
$lang['case start date empty']					= 'date de début heure empti';
$lang['Risk Date']								= 'date de risque';
$lang['risk date empty']						= 'Risque date de empti';
$lang['Control Coverage']						= 'contrôle Soverage';
$lang['Type Control Coverage name ...']			= 'Type de contrôle Soverage Nam ...';
$lang['Please add Control Coverage name that you desire']		= "S'il vous plaît ajouter le contrôle Soverage Nam Thap Desire tripler";
$lang['The start date can not be greater then the end date'] 	= 'Date Tkhe commencer san note baa plus alors Tkhe Date de fin';
$lang['The end date can not be less then the start date']		= 'Tkhe Fin san note baa loess puis Tkhe date de début';
$lang['Control coverage is blank']								= 'Le formulaire IC de contrôle';
$lang['Session expired']										= 'session ekspired';
$lang['Control coverage already saved']							= 'Soverage de commande déjà enregistrée';
$lang['Control coverage already deleted']						= 'Soverage de contrôle déjà supprimé';

$lang['admin_menu_report_configuration']						= 'Rapport de configuration';
$lang['report_name']											= 'prénom';
$lang['report_path']											= 'Chemin';
$lang['Add Report']												= 'Ajouter un rapport';
$lang['Get Parameters']											= 'Obtenez Paramètres';
$lang['Report Path is blank']									= "S'il vous plaît remplir Rapport nom et chemin";
$lang['Report Name is blank']									= "S'il vous plaît remplir Rapport nom et chemin";
$lang['Multi Select']											= 'Sélection multiple';
$lang['Parameter']												= 'Paramètres)';
$lang['Type']													= 'Type';
$lang['Report already saved']									= 'Rapport déjà enregistré';
$lang['Warning']												= 'Attention !';
$lang['delete_report_confirm_message']							= 'Etes-vous sûr supprimera rapport?';
$lang['Report deleted']											= 'Rapport supprimé';
$lang['Modify Report']											= 'Modifier Rapport';
$lang['Report Name']											= 'Nom du rapport';
$lang['Export To']												= 'Exporter vers';
$lang['Export']													= 'Télécharger';
$lang['Download Report']										= 'Télécharger le rapport';
$lang['filename']												= 'Nom de fichier';
$lang['View']													= 'Vue';
$lang['get_params_fail']										= "Impossible de lire le paramètre, essayez de vérifier l'adresse ou communiquer avec les rapports d'administrateur";
$lang['Check all']												= 'Vérifie tout';
$lang['Uncheck All']											= 'Décocher tout';
$lang['Select an Option']										= 'Selekt une option';
$lang['Selected']												= 'Selekted';

$lang['Page Width']												= 'Largeur de page';
$lang['Page Height']											= 'Hauteur de page';
$lang['Margin Top']												= 'Marge supérieure';
$lang['Margin Left']											= 'Marge gauche';
$lang['Margin Right']											= 'droit de la marge';
$lang['Margin Bottom']											= 'en bas de la marge';
$lang['inches']													= 'pouces';

$lang['admin_menu_exchange_configuration']						= 'Configuration Taux de change';
$lang['Module Privelege']										= 'Privilège Module';
$lang['Menu Privelege']											= 'Menu Privilège';
$lang['OpCo Configuration Privelege']							= 'Privilège Configuration OpCo';
$lang['Report Privelege']										= 'Rapport Privilège';
$lang['opco_view']												= 'Vue';
$lang['opco_add']												= 'Ajouter';
$lang['opco_update']											= 'Mettre à jour';
$lang['add_all_month']											= 'Ajouter tout le mois';
$lang['Report Privelege']										= 'Rapport Privilège';
$lang['Incident Management Privelege']							= 'Privilège de gestion des incidents';
$lang['Module']													= 'Module';
$lang['Access']													= 'Accès';
$lang['update_all_month']										= 'Mettre à jour tout le mois';
$lang['Incident Management Privelege']							= 'Privilège de gestion des incidents';
$lang['bulk_incident']											= 'Masse';

$lang['Welcome to VIP']											= 'Bienvenue au VIP';
$lang['cant_modify']											= 'Vous ne pouvez modifier au mois en cours seulement';
$lang['leakage']												= 'Fuite';
$lang['Revenue Stream']											= 'Flux de revenus';
$lang['no_report']												= "Désolé, vous n'êtes pas autorisé à afficher les rapports";


$lang['Exchange Rate'] 											= "Taux de change pour USD";
$lang['something_wrong']										= "Impossible de stocker une partie ou la totalité des données, s'il vous plaît contacter administrateur";
$lang['Add Exchange Rate']										= 'Add Exchange Rate';
$lang['not_allowed']											= 'Vous ne pouvez pas ajuster le taux de change futur';
$lang['invalid_rate']											= 'valeur de débit non valide';
$lang['already_exist']											= 'mois sélectionné existe déjà';

$lang['Add OPCO']												= 'Ajouter OPCO';
$lang['Default Currency']										= 'devise par défauty';
$lang['Modify OPCO']											= 'Modifier OPCO';

$lang['access_denied']											= 'Accès refusé';
$lang['access_denied_msg']										= "Désolé, vous avez pas la permission d'accéder à cette page";
$lang['tooltip']												= 'Tooltip to show description of field, and another information about user input';


$lang['actual_leakage']											= 'Une fuite réelle';

$lang['Bad Debts']												= 'Mauvaises dettes';
$lang['Total Operating Revenue']								= "Total des revenus d'exploitation";
$lang['Service Revenue']										= 'Revenus de services';
$lang['Roaming & Interconnect Cost']							= 'Roaming & Interconnexion Coût';

$lang['actual_leakage']											= 'fuites Actual';

$lang['bypass']													= 'Bypass';
$lang['Yes']													= 'Yes';

$lang['On-net Terminated minutes']								= 'On-Net minutes terminées';
$lang['Prevented']												= 'Empêché';
$lang['on_net_suspended_sims']									= 'SIMs On-Net suspendus';
$lang['average_retail_on_net']									= 'Prix moyen On-Net par Minutes';
$lang['detection_cost']											= 'Coût de détection';
$lang['regulated_international_ic_price']						= 'Réglementé Prix IC international';
$lang['on_net_price_arbitrage']									= 'On-net Prix Arbitrage';
$lang['off_net_terminated_minutes']								= 'Off-Net minutes terminées';
$lang['off_net_suspensed_sims']									= 'SIMs Off-Net suspendus';
$lang['average_retail_off_net']									= 'Prix moyen Off-Net par Minutes';
$lang['average_disconecting_duration']							= 'Moyenne Disconnecting Durée (Minutes)';
$lang['off_net_price_arbitrage']								= 'Off-net Prix Arbitrage';

$lang['over_charging']											= 'Au cours de charge';
$lang['revenue_loss']											= 'Perte de revenus';
$lang['recovered']												= 'Rétabli';
$lang['prevented_loss']											= 'Perte empêché';

$lang['Please select']											= "S'il vous plaît remplir le titre";
$lang['error_external_id']										= "S'il vous plaît remplir ID externe";

$lang['error_detection_tools']									= "S'il vous plaît sélectionner les outils de détection";
$lang['error_rut']												= "S'il vous plaît sélectionner Type de risque Univers";
$lang['error_st']												= "S'il vous plaît sélectionnez Section Titre";
$lang['error_rc']												= "S'il vous plaît sélectionner Risque";
$lang['error_revenue_stream']									= "S'il vous plaît sélectionner Revenu flux";
$lang['error_description']										= "S'il vous plaît remplir la description";
$lang['error_note']											    = "S'il vous plaît remplir Commentaires";
$lang['error_impact_severity']								    = "S'il vous plaît sélectionnez l'impact Gravité";

$lang['error_leakage_freq']								    	= "S'il vous plaît sélectionner Fréquence Fuites";
$lang['error_leakage_timing']								    = "S'il vous plaît sélectionner Timing Fuites";
$lang['error_leakage_id_type']								    = "S'il vous plaît sélectionner Fuites type";
$lang['error_recovery_type']								    =  "S'il vous plaît sélectionnez le type de récupération";

$lang['error_filesize']											= 'File size more than 10MB';

$lang['Ref Number']											    = 'Numéro de réference'; 
$lang['Reference Number Error']									= 'Referenco Nombro Eraro';

$lang['Recursive Frequency']									= 'Fréquence récursive';
$lang['incident_leakage']										= 'incident fuites';
$lang['incident_leakage_summary']								= 'Incident de fuite (Revenu de fuite)';
$lang['actual_loss']											= 'Perte réelle';

$lang['negative_value']											= "S'il vous plaît ne pas entrer une valeur négative";

$lang['Daily']													= 'Tous les jours';
$lang['Weekly']													= 'Hebdomadaire';
$lang['Monthly']												= 'Mensuel';

$lang['Personal Setting']										= 'Cadre personnel';

$lang['pass_rule']												= 'au moins 8 caractères, <br> doivent contenir au moins 1 lettre majuscule, 1 lettre minuscule, et 1 chiffre <br> Peut contenir des caractères spéciaux';
$lang['error_recursive_frequency']								= "S'il vous plaît sélectionner Fréquence récursive";
$lang['Old password']											= 'Ancien mot de passe';

$lang['close_date']												= 'Close Date';

$lang['amount'] = "Montant";
$lang['no_selected'] = "Pas de sélection";
$lang['min_value'] = "Min Value";
$lang['max_value'] = "Max Value";
$lang['error_incident_leakage'] = "La fuite d'incident doit être supérieure à 0 <br>la fuite d'incident doit être supérieure ou égale à la récupération";
$lang['confirm_delete'] = "Supprimer Confirmer";
$lang['confirm_delete_message'] = "Tapez Mot de passe pour confirmer";
$lang['delete_success'] = "L'incident a déjà été supprimé";
$lang['delete_fail'] = "Échec de suppression d'incident";
$lang['no_delete'] = "Vous n'avez pas la permission de supprimer Incident";
$lang['forget_password'] = 'Mot de passe oublié?';
$lang['insert_email'] = 'Veuillez insérer un courriel';
$lang['reset'] = 'Réinitialiser';
$lang['invalid_email'] = 'Veuillez insérer un courriel valide';
$lang['not_found'] = 'Email non enregistré';
$lang['Already Sent'] = 'Demande Réinitialiser le mot de passe déjà envoyé à votre email, si vous ne recevez pas de courriel, cliquez ';
$lang['sent_link_success'] = "Réinitialiser le mot de passe confirmé déjà envoyé à votre email, s'il vous plaît Vérifiez votre adresse e-mail";
$lang['internal_error'] = 'Erreur de commande interne';
$lang['here'] = 'ici';
$lang['fail_create_link'] = "Échec de la création d'un lien";
$lang['cannot_blank'] = 'Le mot de passe ne peut pas être vide';
$lang['success_change_password'] = "Votre mot de passe a déjà changé, s'il vous plaît <a href='".base_url()."'> connexion </a>";
$lang['error_code']='Code invalide';
$lang['link_invalid']='Code expiré';

$lang['Page'] = 'Page';
$lang['English'] = 'Anglais';
$lang['French'] = 'français';
$lang['Russian'] = 'russe';

$lang['admin'] = 'Admin';
$lang['upload_success'] = 'Télécharger le succès';
$lang['upload_failed'] = 'Échec du téléchargement';

$lang['aml_sox'] = 'AML/SOX';
$lang['aml'] = 'AML';
$lang['sox'] = 'SOX';

// CR002
$lang['root_cause_dec'] = 'Description de cause racine';
$lang['error_correction_dec'] = "Description de la correction d'erreur";

$lang['root_cause_category'] = 'Catégorie de cause racine';
$lang['category'] = 'Catégorie';
$lang['category_list'] = 'Liste des catégories';
$lang['type'] = 'Type';
$lang['type_list'] = 'Type List';
$lang['name'] = 'prénom';
$lang['action'] = 'Action';
$lang['Liquidate damages claim'] = 'Liquidation des dommages et intérêts $';

$lang['name_list'] = 'Liste de nom';
$lang['action_list'] = "Liste d'action";

$lang['error_rcc'] = "Sélectionnez Catégorie de cause racine";
$lang['error_rct'] = "Sélectionnez Type de cause racine";
$lang['error_rcn'] = "Sélectionnez Nom racine racine";
$lang['error_rca'] = "Sélectionnez Action Root Cause";

$lang['error_liquidated'] = "La demande en dommages-intérêts est vide";
$lang['error_root_cause_dec'] = "Root Cause Description est vide";
$lang['error_correction_dec_msg'] = "Correction d'erreur La description est vide";

$lang['Normal'] = "Normal";
$lang['FM'] = "FM";

// CR003
$lang["control_coverage_kpi"] 	= "KPI de couverture de contrôle";
$lang["Revenue Stream Privilege"] = "Privilège Revenue Stream"; 
$lang["Control"] = "Contrôle";
$lang["history"] = "Histoire";
