<?php
$lang['welcome_message'] 	= 'Welcome';
$lang['login']				= 'Login';
$lang['username']			= 'Username';
$lang['password']			= 'Password';
$lang['invalid_login']		= 'Username or password invalid';
$lang['user_password_required']		= 'Requires a username and password input';
$lang['user_status_disable']		= 'User or group is disable';
$lang['user_not_allowed']			= 'User not allowed to access module';
$lang['wrong_password']				= 'incorrectly entering the password';

$lang['disable_module']		= 'Module is disable';

$lang['admin_menu_user_management']			= 'User Management';
$lang['admin_menu_group_management']		= 'Group Management';
$lang['admin_menu_information_management']	= 'Universe Information Management';
$lang['admin_menu_audit_trail']				= 'Audit Trail';
$lang['admin_menu_faq']						= 'FAQ';
$lang['admin_menu_logout']					= 'Log Out';
$lang['admin_menu_risk_universe_mapping']	= 'Risk Universe Mapping';
$lang['admin_menu_opco_configuration']		= 'OpCo Configuration';


$lang['hello']								= 'Hi';
$lang['please_login']						= 'Please Login';

$lang['number']								= 'No';
$lang['name']								= 'Name';
$lang['group']								= 'Group';
$lang['status']								= 'Status';
$lang['last_login']							= 'Last Login';
$lang['all']								= 'All';

$lang['add']								= 'ADD';
$lang['delete']								= 'DELETE';

$lang['enable']								= 'Enable';
$lang['disable']							= 'Disable';
$lang['user_not_found']						= 'User not found';

$lang['id']									= 'ID';
$lang['group_name']							= 'Group Name';
$lang['group_status']						= 'Group Status';
$lang['group_type']							= 'Group Type';
$lang['authentification_type']				= 'Authentification Type';
$lang['date_created']						= 'Date Created';
$lang['created_by']							= 'Created by';
$lang['date_update']						= 'Date Update';
$lang['update_by']							= 'Update by';
$lang['group_not_found']					= 'group not found';
$lang['VimpelCom Group Currency']			= 'VimpelCom Group Currency List';

$lang['module']								= 'Module';
$lang['start_date']							= 'Start Date';
$lang['end_date']							= 'End Date';
$lang['user_admin']							= 'User Admin';
$lang['incident']							= 'Incident';
$lang['filter']								= 'Filter';

$lang['date']								= 'Date';
$lang['user']								= 'User';
$lang['activity']							= 'Activity';
$lang['description']						= 'Description';

$lang['confirm']							= 'Confirm';
$lang['confirm_message']					= 'Are you sure will disable account ?';
$lang['yes']								= 'Yes';
$lang['cancel']								= 'Cancel';

$lang['email']								= 'Email';
$lang['first_name']							= 'First name';
$lang['last_name']							= 'Last name';

$lang['modify']								= 'Modify';
$lang['save']								= 'Save';
$lang['Not a valid e-mail address']			= 'Not a valid e-mail address';
$lang['Change Password']					= 'Password';

$lang['ldap_user']							= 'This user from Active Directory Service Server, cant change the password';
$lang['ok']									= 'Ok';
$lang["Can't change password"]				= "Can't change password";
$lang['Change password']					= 'Change password';
$lang['New password']						= 'New password';
$lang['Retype password']					= 'Retype password';
$lang['Password you entered is not the same']	= 'Password you entered is not the same';
$lang['Update Password success']				= 'Update Password success';
$lang['Update Password fail']					= 'Update Password fail';
$lang['Notify']									= 'Notify';
$lang['Add User']								= 'Add User';
$lang['Check user']								= 'Check user';
$lang['Active Directory Service User']			= 'Active Directory Service User';
$lang['First name']								= 'First name';
$lang['Last name']								= 'Last name';
$lang['Local Database User']					= 'Local Database User';
$lang['Password']								= 'Password';
$lang['Error Message']							= 'Error Message';
$lang['Username already registered']			= 'The Username you choose or type was already exist<br>Please check or type other username that you desire,<br>And check username availabilities';
$lang['Invalid command']						= 'session timed out';
$lang['User created']							= 'User Created';
$lang['Invalid email']							= 'invalid email';
$lang['Invalid password']						= 'Invalid password';
$lang['LDAP binding Failed']					= 'LDAP binding Failed';
$lang['Group Detail']							= 'Group Detail';
$lang['Module Access']							= 'Module Access';
$lang['delete_group_confirm_message']			= 'Are you sure will delete group ?';
$lang['Group name already use']					= 'Group name already use';
$lang['remove_module']							= 'You will remove access module from group, <br>are you sure?';
$lang['add_module']								= 'Add access module ';
$lang['add_group']								= 'Add New Group ';
$lang['fail_add_group']							= 'Fail add new group ';
$lang['group_name_blank']						= 'Group name is blank ';
$lang['Are you sure to delete']				    = 'Are you sure to delete ?';
$lang['add_ui_title']						    = 'Add Universe Information Detail';
$lang['fail_add_ui']							= 'Fail add new UI detail ';
$lang['name_blank']								= 'UI name is blank ';
$lang['modify_ui_title']						= 'Modify Universe Information Detail';
$lang['fail_update_ui']							= 'Fail update UI detail ';
$lang['privilege']								= 'Privilege';

$lang['You can modify this Group Detail below'] = 'You can modify this Group Detail below';
$lang['Save privilege is complete'] 			= 'Save privilege is complete';
$lang['type_username'] 			  				= 'Please type username that desire, <br>And check username availabilities';
$lang['Username is blank']						= 'Username is blank';
$lang['similar_name']							= 'Here some similar name that exist on our<br>databse, does one of these are yours?';
$lang['add_new']								= 'Add new for ';
$lang['Please add']								= 'Please add';
$lang['name that you desire']					= 'name that you desire';
$lang['Modify for']								= 'Modify for ';
$lang['Available user']							= 'Available user';
$lang['[Active Directory User]']				= '[Active Directory User]';
$lang['[Available user]']						= '[Available user]';


// Incident
$lang['Administrator Module']								= 'Administrator Module';
$lang['Incident Module']									= 'Incident Module';

$lang['Incident Management']								= 'Incident Management';
$lang['Report']												= 'Report';

//report
$lang['MFS Report']											= 'MFS Report';
$lang['RA Leakage per Month']								= 'RA Leakage per Month';
$lang['RA % Leakage']										= 'RA % Leakage';
$lang['FM Leakage per Month']								= 'FM Leakage per Month';
$lang['FM % Leakage']										= 'FM % Leakage';
$lang['Combined RAFM Leakage']								= 'Combined RAFM Leakage';
$lang['Combined RAFM % Leakage by Month']					= 'Combined RAFM % Leakage by Month';
$lang['RA by OpCo']											= 'RA by OpCo';
$lang['Comparison RA Leakage, Prevented & Recovery']		= 'Comparison RA Leakage, Prevented & Recovery';
$lang['FM Leakage per OpCo']								= 'FM Leakage per OpCo';
$lang['Comparison FM Leakage, Prevented & Recovery']		= 'Comparison FM Leakage, Prevented & Recovery';
$lang['Combined Leakage per OpCo']							= 'Combined Leakage per OpCo';
$lang['Combined Comparison Leakage, Prevented & Recovery']	= 'Combined Comparison Leakage, Prevented & Recovery';
$lang['Leakage, Prevented & Recovery by OpCo']				= 'Leakage, Prevented & Recovery by OpCo';
$lang['Leakage, Saving By OpCo']							= 'Leakage, Saving By OpCo';
$lang['Incidents by OpCo']									= 'Incidents by OpCo';
$lang['RA % Leakage by OpCo']								= 'RA % Leakage by OpCo';
$lang['FM % Leakage by OpCo']								= 'FM % Leakage by OpCo';
$lang['Combined % Leakage by OpCo']							= 'Combined % Leakage by OpCo';
$lang['Bad Debt by OpCo']									= 'Bad Debt by OpCo';
$lang['Resource by % Saving']								= 'Resource by % Saving';
$lang['Bad Debt by Month']									= 'Bad Debt by Month';
$lang['Leakage Calculation per Month']						= 'Leakage Calculation per Month';
$lang['Leakage V Saving']									= 'Leakage V Saving';
$lang['Incidents Reported By Month']						= 'Incidents Reported By Month';
$lang['RA, FM and Combined Dashboard by Month']				= 'RA, FM and Combined Dashboard by Month';
$lang['RA Leakage Calculation by Month']					= 'RA Leakage Calculation by Month';
$lang['FM Leakage Calculation by Month']					= 'FM Leakage Calculation by Month';


$lang['No']						= 'No';
$lang['OpCo']					= 'Opco';
$lang['Open Incident']			= 'Open Incident';
$lang['Close Incident']			= 'Close Incident';
$lang['Total']					= 'Total';
$lang['Records']				= 'Records';

$lang['Risk Universe Type']		= 'Risk Universe Type';
$lang['Section Title']			= 'Section Title';
$lang['Operator Category']		= 'Operator Category';
$lang['Risk']					= 'Risk Category';
$lang['VimpelCom Group Currency'] 	= 'VimpelCom Group Currency List';
$lang['Detection Tool']				= 'Detection Tool';
$lang['OpCo']						= 'OpCo';
$lang['Revenue Stream']				= 'Revenue Stream';
$lang['General']					= 'General';
$lang['ByPass']						= 'ByPass';
$lang['MFS']						= 'MFS';
$lang['Configuration UI']			= 'General Configuration';
$lang['Configuration']				= 'Configuration';
$lang['Type Currency name ...']		= 'Type Currency name ...';
$lang['Type Currency symbol ...']	= 'Type Currency symbol ...';
$lang['Type Detection tool name ...']		= 'Type Detection tool name ...';
$lang['Type Detection tool cost ...']		= 'Type Detection tool cost ...';

$lang['January']							= 'January';
$lang['February']							= 'February';
$lang['March']								= 'March';
$lang['April']								= 'April';
$lang['May']								= 'May';
$lang['June']								= 'June';
$lang['July']								= 'July';
$lang['August']								= 'August';
$lang['September']							= 'September';
$lang['October']							= 'October';
$lang['November']							= 'November';
$lang['December']							= 'December';

$lang['Month']								= 'Month';
$lang['Year']								= 'Year';
$lang['Get Opco Configuration']				= 'Get Configuration';

$lang['OpCo no configuration, configuration values set to default'] = 'OpCo no configuration, configuration values set to default';
$lang['Message']							= 'Message';
$lang['Add OpCo Configuration']				= 'Add OpCo Configuration';
$lang['Period']								= 'Period';
$lang['Configuration in selected already exists, the process is canceled'] = 'Configuration in selected already exists, <br>the process is canceled';
$lang['Configuration has been saved']		= 'Configuration has been saved';
$lang['Are you sure to cancel the operation?'] 	= 'Are you sure to cancel the operation?';

$lang['Risk Universe Type List'] 				= 'Risk Universe Type List';
$lang['Fail to save data']						= 'Fail to save data';
$lang['Fail to update data']					= 'Fail to update data';
$lang['Risk Universe Type Name is blank']		= 'Risk Universe Type Name is blank';
$lang['Section Title List'] 					= 'Section Title List';
$lang['Section Title  Name is blank']			= 'Section Title  Name is blank';
$lang['Operator Category List']					= 'Operator Category List';
$lang['Operator Category Name is blank']		= 'Operator Category Name is blank';
$lang['Description']							= 'Description';
$lang['Risk Category or Description is blank']	= 'Risk Category or Description is blank';
$lang['Please fill in all fields']				= 'Please fill in all fields';
$lang['Currency']								= 'Currency';
$lang['Currency Symbol']						= 'Currency Symbol';
$lang['Tools Name']								= 'Tools Name';
$lang['Cost']									= 'Cost';
$lang['Open Incident']							= 'Open Incident';
$lang['Close Incident']							= 'Close Incident';

$lang['Search']									= 'Search';
$lang['All']									= 'All';
$lang['Open']									= 'OPEN';
$lang['Close']									= 'CLOSE';
$lang['Filter']									= 'Filter';
$lang['Incident ID']							= 'Incident ID';
$lang['External ID']							= 'External ID';
$lang['Last Update']							= 'Last Update';
$lang['No Records']								= 'No Records';
$lang['UPLOAD']									= 'UPLOAD';
$lang['?']										= '?';
$lang['New Incident']							= 'New Incident';
$lang['risk_tab']								= 'Risk';
$lang['Attachment']								= 'Attachment';
$lang['History']								= 'History';
$lang['Note']									= 'Note';
$lang['Impact Severity']						= 'Impact Severity';
$lang['Detection Date']							= 'Detection Date';
$lang['Case Start Date']						= 'Case Start Date';
$lang['Case End Date']							= 'Case End Date';
$lang['Leakage One Day']						= 'Leakage One Day';
$lang['Recovery']								= 'Recovery';	
$lang['Leakage Frequency']						= 'Leakage Frequency';
$lang['Leakage Timing']							= 'Leakage Timing';	
$lang['Leakage ID Type']						= 'Leakage ID Type';	
$lang['Recovery Type']							= 'Recovery Type ';	
$lang['Incident Status']						= 'Incident Status';	
$lang['Final Leakage']							= 'Final Leakage';	
$lang['Recovered Value']						= 'Recovered Value';	
$lang['Prevented Value']						= 'Prevented Value';	
$lang['Potential Leakage']						= 'Potential Leakage';	
$lang['Description']							= 'Description';	
$lang['Investigation Note']						= 'Investigation Note';	
$lang['Under Investigation']					= 'Under Investigation';
$lang['Date Time']								= 'Date Time';
$lang['User']									= 'User';
$lang['Filename']								= 'Filename';
$lang['File Description']						= 'File Description';

$lang['No Files']								= 'No Files';
$lang['Action']									= 'Action';
$lang['Comments']								= 'Comments';
$lang['No Histories']							= 'No Histories';
$lang['Upload attachment']						= 'Upload Attacment';
$lang['Upload']									= 'Upload';
$lang['Create Incident Succes']					= 'Create Incident Succes';
$lang['Incident already saved']					= 'Incident already saved';
$lang['View Incident']							= 'View Incident';
$lang['Update Incident']						= 'Upadate Incident';
$lang['Update']									= 'Update';
$lang['Risk Category already selected']			= 'Risk Category already selected';
$lang['Under Investigation']					= 'UNDER INVESTIGATION';
$lang['Under Investigation2']					= 'Under Investigation';

$lang['General Configuration']					= 'General Configuration';
$lang['rut reset']								= 'Are you sure to change Risk Universe Type ? <br>Risk Category will reset';
$lang['No']										= 'No';
$lang['Contact']								= 'Contact';
$lang['view']									= 'View';
$lang['BULK UPLOAD']							= 'BULK UPLOAD';
$lang['Currency already exist']					= 'Currency already exist';
$lang['Upload bulk file']						= 'Upload bulk file';
$lang['Uploading file ... please wait']			= 'Uploading file ... please wait';
$lang['upload_error']							= 'Something went wrong when saving the file, please make sure csv file and try again';
$lang['process_bulk']							= 'Try to insert data from csv file, please wait';
$lang['error_1']								= 'Number fields not match in line ';
$lang['insert bulk file complete']				= 'insert bulk file complete';
$lang['Invalid Format csv file']				= 'Invalid Format csv file';
$lang['Examples']								= 'Examples';
$lang['Example Upload bulk CSV file']			= 'Example Upload bulk CSV file';
$lang['line_error']								= 'Number field in line %l is %f field(s), should have 19 fields';
$lang['Invalid field value']					= 'Invalid field value in line';
$lang['Upload Result']							= 'Upload Result';

