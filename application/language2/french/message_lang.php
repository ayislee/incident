<?php
$lang['welcome_message'] 	= 'Bienvenue';
$lang['login']				= 'connectez-vous';
$lang['username']			= "nom d'utilisateur";
$lang['password']			= 'mot de passe';
$lang['invalid_login']		= "Nom d'utilisateur ou mot de passe est invalide";
$lang['user_password_required']		= "Nécessite une entrée de nom d'utilisateur et mot de passe";
$lang['user_status_disable']		= "L'utilisateur ou le groupe est désactivé";
$lang['user_not_allowed']			= "Utilisateur non autorisé à accéder le module";
$lang['wrong_password']				= 'entrer le mot de passe de manière incorrecte';

$lang['disable_module']		= 'Module est désactivé';

$lang['admin_menu_user_management']			= "Gestion de l'utilisateur";
$lang['admin_menu_group_management']		= 'La direction du groupe';
$lang['admin_menu_information_management']	= "Gestion de l'information Univers";
$lang['admin_menu_audit_trail']				= 'Audit Trail';
$lang['admin_menu_faq']						= 'FAQ';
$lang['admin_menu_logout']					= 'De mensonges';
$lang['admin_menu_risk_universe_mapping']	= 'Cartographie des risques Univers';
$lang['admin_menu_opco_configuration']		= 'Configuration OpCo';


$lang['hello']								= 'Salut';
$lang['please_login']						= "S'il vous plaît vous connecter";

$lang['number']								= 'Nombre';
$lang['name']								= 'Prénom';
$lang['group']								= 'Groupe';
$lang['status']								= 'statut';
$lang['last_login']							= 'Dernière connexion';
$lang['all']								= 'Tout';

$lang['add']								= 'Ajouter';
$lang['delete']								= 'Effacer';

$lang['enable']								= 'Activer';
$lang['disable']							= 'Désactiver';
$lang['user_not_found']						= 'Utilisateur non trouvé';

$lang['id']									= 'identité';
$lang['group_name']							= 'nom du groupe';
$lang['group_status']						= 'le statut de groupe';
$lang['group_type']							= 'Type de groupe';
$lang['authentification_type']				= 'Authentication type';
$lang['date_created']						= 'date de création';
$lang['created_by']							= 'créé par';
$lang['date_update']						= 'Date mise à jour';
$lang['update_by']							= 'mise à jour par';
$lang['group_not_found']					= 'groupe introuvable';
$lang['VimpelCom Group Currency']			= 'VimpelCom Group devise';

$lang['module']								= 'Module';
$lang['start_date']							= 'Date de début';
$lang['end_date']							= 'Date de fin';
$lang['user_admin']							= 'administration des utilisateurs';
$lang['incident']							= 'Incident';
$lang['filter']								= 'Filtre';

$lang['date']								= 'Date';
$lang['user']								= 'Utilisateur';
$lang['activity']							= 'Activité';
$lang['description']						= 'La description';

$lang['confirm']							= 'Confirmer';
$lang['confirm_message']					= 'Etes-vous sûr désactivera compte?';
$lang['yes']								= 'Oui';
$lang['cancel']								= 'Annuler';

$lang['email']								= 'Email';
$lang['first_name']							= 'Prénom';
$lang['last_name']							= 'Nom de famille';

$lang['modify']								= 'Modifier';
$lang['save']								= 'sauvegarder';
$lang['Not a valid e-mail address']			= 'Adresse courriel invalide';
$lang['Change Password']					= 'Mot de passe';
$lang['ldap_user']							= "Cet utilisateur à partir d'Active Directory Service Server, ne peut pas changer le mot de passe";
$lang['ok']									= "D'accord";
$lang["Can't change password"]				= "Vous ne pouvez pas changer le mot de passe";
$lang['Change password']					= 'Changer le mot de passe';
$lang['New password']						= 'Nouveau mot de passe';
$lang['Retype password']					= 'retaper le mot de passe';
$lang['Password you entered is not the same']	= 'Mot de passe que vous avez entré est pas le même';
$lang['Update Password success']				= 'succès Mise à jour du mot de passe';
$lang['Update Password fail']					= 'Mise à jour de mot de passe sûr';
$lang['Notify']									= 'Notifier';
$lang['Add User']								= 'Ajouter un utilisateur';
$lang['Check user']								= "Vérifier l'utilisateur";
$lang['Active Directory Service User']			= 'Active Directory Service utilisateur';
$lang['First name']								= 'Prénom';
$lang['Last name']								= 'Nom de famille';
$lang['Local Database User']					= 'Base de données locale utilisateur';
$lang['Password']								= 'Mot de passe';
$lang['Error Message']							= "Message d'erreur";
$lang['Username already registered']			= "il Pseudonyme vous choisissez ou a été existent déjà de type <br> S'il vous plaît vérifier ou tapez un autre nom d'utilisateur que vous désirez, <br> Et vérifier la disponibilité du nom d'utilisateur";
$lang['Invalid command']						= 'session expirée';
$lang['User created']							= 'utilisateur Créé';
$lang['Invalid email']							= 'email invalide';
$lang['Invalid password']						= 'Mot de passe incorrect';
$lang['LDAP binding Failed']					= 'LDAP liaison Échec';
$lang['Group Detail']							= 'Groupe Détail';
$lang['Module Access']							= 'Accès Module';
$lang['delete_group_confirm_message']			= 'Etes-vous sûr supprimera groupe ?';
$lang['Group name already use']					= 'Nom du groupe déjà utilisé';
$lang['remove_module']							= "Vous allez supprimer le module d'accès du groupe, <br> Etes-vous sûr?";
$lang['add_module']								= "Ajouter module d'accès ";
$lang['add_group']								= 'Ajouter un nouveau groupe ';
$lang['fail_add_group']							= 'Échec ajouter un nouveau groupe ';
$lang['group_name_blank']						= 'Nom du groupe est vide ';
$lang['Are you sure to delete']				    = 'Êtes-vous sûr de vouloir supprimer ?';
$lang['add_ui_title']						    = 'Ajouter Univers information Détail';
$lang['fail_add_ui']							= "Échec ajouter de nouveaux détails de l'interface utilisateur ";
$lang['name_blank']								= 'Nom UI est vide ';
$lang['modify_ui_title']						= 'Modifier Univers information Détail';
$lang['fail_update_ui']							= "Échec mise à jour détaillée de l'interface utilisateur ";
$lang['privilege']								= 'privilège';

$lang['You can modify this Group Detail below'] = 'Vous pouvez modifier ce groupe Détail ci-dessous';
$lang['Save privilege is complete'] 			= 'Enregistrer privilège est complète';
$lang['type_username'] 			  				= "S'il vous plaît entrez le nom d'utilisateur que le désir, <br> Et vérifier la disponibilité du nom d'utilisateur";
$lang['Username is blank']						= "Nom d'utilisateur est vide";

$lang['similar_name']							= "Voici un nom similaire qui existent sur notre base de données <br>, fait l'un d'entre eux sont les vôtres?";
$lang['add_new']								= 'Ajouter nouvelle pour ';
$lang['Modify for']								= 'Modifier pour ';
$lang['Available user']							= 'Disponible utilisateur';
$lang['[Active Directory User]']				= '[Active Directory utilisateur]';
$lang['[Available user]']						= '[Disponible utilisateur]';

// Incident

$lang['Incident Management']					= 'gestion des incidents';
$lang['Report']									= 'rapport';

//report
$lang['MFS Report']							= 'MFS Report';
$lang['RA Leakage per Month']					= 'RA Leakage per Month';
$lang['RA % Leakage']							= 'RA % Leakage';
$lang['FM Leakage per Month']					= 'FM Leakage per Month';
$lang['FM % Leakage']							= 'FM % Leakage';
$lang['Combined RAFM Leakage']					= 'Combined RAFM Leakage';
$lang['Combined RAFM % Leakage by Month']		= 'Combined RAFM % Leakage by Month';
$lang['RA by OpCo']							= 'RA by OpCo';
$lang['Comparison RA Leakage, Prevented & Recovery']	= 'Comparison RA Leakage, Prevented & Recovery';
$lang['FM Leakage per OpCo']							= 'FM Leakage per OpCo';
$lang['Comparison FM Leakage, Prevented & Recovery']	= 'Comparison FM Leakage, Prevented & Recovery';
$lang['Combined Leakage per OpCo']						= 'Combined Leakage per OpCo';
$lang['Combined Comparison Leakage, Prevented & Recovery']	= 'Combined Comparison Leakage, Prevented & Recovery';
$lang['Leakage, Prevented & Recovery by OpCo']				= 'Leakage, Prevented & Recovery by OpCo';
$lang['Leakage, Saving By OpCo']							= 'Leakage, Saving By OpCo';
$lang['Incidents by OpCo']									= 'Incidents by OpCo';
$lang['RA % Leakage by OpCo']								= 'RA % Leakage by OpCo';
$lang['FM % Leakage by OpCo']								= 'FM % Leakage by OpCo';
$lang['Combined % Leakage by OpCo']						= 'Combined % Leakage by OpCo';
$lang['Bad Debt by OpCo']									= 'Bad Debt by OpCo';
$lang['Resource by % Saving']								= 'Resource by % Saving';
$lang['Bad Debt by Month']									= 'Bad Debt by Month';
$lang['Leakage Calculation per Month']						= 'Leakage Calculation per Month';
$lang['Leakage V Saving']									= 'Leakage V Saving';
$lang['Incidents Reported By Month']						= 'Incidents Reported By Month';
$lang['RA, FM and Combined Dashboard by Month']			= 'RA, FM and Combined Dashboard by Month';
$lang['RA Leakage Calculation by Month']					= 'RA Leakage Calculation by Month';
$lang['FM Leakage Calculation by Month']					= 'FM Leakage Calculation by Month';

$lang['No']						= 'Non';
$lang['OpCo']					= 'Opco';
$lang['Open Incident']			= 'Ouvrir incident';
$lang['Close Incident']			= 'Fermer incident';
$lang['Total']					= 'Total';
$lang['Records']				= 'Enregistrements';

$lang['Risk Universe Type']		= 'Risque Univers type';
$lang['Section Title']			= 'Titre de la section';
$lang['Operator Category']		= "Catégorie de l'opérateur";
$lang['Risk']					= 'Risque';
$lang['VimpelCom Group Currency'] 	= 'VimpelCom Group devise';
$lang['Detection Tool']				= 'Detection Tool';
$lang['OpCo']						= 'OpCo';
$lang['Revenue Stream']				= 'Flux de revenus';
$lang['General']					= 'Général';
$lang['ByPass']						= 'By-pass';
$lang['MFS']						= 'MFS';
$lang['General']					= 'General';
$lang['ByPass']						= 'By-pass';
$lang['MFS']						= 'MFS';
$lang['Configuration UI']			= 'UI Configuration';
$lang['Configuration']				= 'Configuration';
$lang['Type Currency name ...']		= 'Tapez le nom Devise ...';
$lang['Type Currency symbol ...']	= 'Tapez le symbole de devises ...';
$lang['Type Detection tool name ...']		= "Tapez Détection nom de l'outil ...";
$lang['Type Detection tool cost ...']		= "Tapez Détection coût de l'outil ...";

$lang['January']							= 'Janvier';
$lang['February']							= 'Février';
$lang['March']								= 'Mars';
$lang['April']								= 'Avril';
$lang['May']								= 'Mai';
$lang['June']								= 'Juin';
$lang['July']								= 'Juillet';
$lang['August']								= 'Août';
$lang['September']							= 'Septembre';
$lang['October']							= 'Octobre';
$lang['November']							= 'Novembre';
$lang['December']							= 'Décembre';

$lang['Month']								= 'Mois';
$lang['Year']								= 'An';
$lang['Get Opco Configuration']				= 'Obtenez Configuration';

$lang['OpCo no configuration, configuration values set to default'] = 'OpCo aucune configuration, les valeurs de configuration définies par défaut';
$lang['Message']							= 'Message';
$lang['Add OpCo Configuration']				= 'Ajouter une configuration OpCo';
$lang['Period']								= 'Période';
$lang['Configuration in selected already exists, the process is canceled'] = 'Configuration dans déjà sélectionné existe, <par> le processus est annulé';
$lang['Configuration has been saved']		= 'Configuration a été enregistré';
$lang['Are you sure to cancel the operation?'] 	= "Etes-vous sûr d'annuler l'opération?";

$lang['Risk Universe Type List'] 				= 'Risque Univers Liste';
$lang['Fail to save data']						= 'Défaut de sauvegarder les données';
$lang['Fail to update data']					= 'Défaut de mettre à jour les données';
$lang['Risk Universe Type Name is blank']		= 'Risk Universe Type Nom est vide';
$lang['Section Title List'] 					= 'Titre de la section Liste';
$lang['Section Title  Name is blank']			= 'Titre de la section Nom est vide';
$lang['Operator Category List']					= 'Opérateur Liste des catégories';
$lang['Operator Category Name is blank']		= "Catégorie de l'opérateur Nom est vide";
$lang['Description']							= 'La description';
$lang['Risk Category or Description is blank']	= 'Catégorie de risque ou description est vide';
$lang['Please fill in all fields']				= 'Merci de remplir tous les champs';
$lang['Currency']								= 'Devise';
$lang['Currency Symbol']						= 'Symbole de la monnaie';
$lang['Tools Name']								= 'Outils Nom';
$lang['Cost']									= 'Coût';
$lang['Open Incident']							= 'Open Incident';
$lang['Close Incident']							= 'Close Incident';

$lang['Search']									= 'Search';
$lang['All']									= 'All';
$lang['Open']									= 'OPEN';
$lang['Close']									= 'CLOSE';
$lang['Filter']									= 'Filter';
$lang['Incident ID']							= 'Incident ID';
$lang['External ID']							= 'External ID';
$lang['Last Update']							= 'Last Update';
$lang['No Records']								= 'No Records';
$lang['UPLOAD']									= 'UPLOAD';
$lang['?']										= '?';
$lang['New Incident']							= 'New Incident';
$lang['risk_tab']								= 'Risk';
$lang['Attachment']								= 'Attachment';
$lang['History']								= 'History';
$lang['Note']									= 'Note';
$lang['Impact Severity']						= 'Impact Severity';
$lang['Detection Date']							= 'Detection Date';
$lang['Case Start Date']						= 'Case Start Date';
$lang['Case End Date']							= 'Case End Date';
$lang['Leakage One Day']						= 'Leakage One Day';
$lang['Recovery']								= 'Recovery';	
$lang['Leakage Frequency']						= 'Leakage Frequency';
$lang['Leakage Timing']							= 'Leakage Timing';	
$lang['Leakage ID Type']						= 'Leakage ID Type';	
$lang['Recovery Type']							= 'Recovery Type ';	
$lang['Incident Status']						= 'Incident Status';	
$lang['Final Leakage']							= 'Final Leakage';	
$lang['Recovered Value']						= 'Recovered Value';	
$lang['Prevented Value']						= 'Prevented Value';	
$lang['Potential Leakage']						= 'Potential Leakage';	
$lang['Description']							= 'Description';	
$lang['Investigation Note']						= 'Investigation Note';	
$lang['Under Investigation']					= 'Under Investigation';
$lang['Date Time']								= 'Date Time';
$lang['User']									= 'User';
$lang['Filename']								= 'Filename';
$lang['File Description']						= 'File Description';

$lang['No Files']								= 'No Files';
$lang['Action']									= 'Action';
$lang['Comments']								= 'Comments';
$lang['No Histories']							= 'No Histories';
$lang['Upload attachment']						= 'Upload Attacment';
$lang['Upload']									= 'Upload';
$lang['Create Incident Succes']					= 'Create Incident Succes';
$lang['Incident already saved']					= 'Incident already saved';
$lang['View Incident']							= 'View Incident';
$lang['Update Incident']						= 'Upadate Incident';
$lang['Update']									= 'Update';
$lang['Risk Category already selected']			= 'Risk Category already selected';
$lang['Under Investigation']					= 'UNDER INVESTIGATION';
$lang['Under Investigation2']					= 'Under Investigation';

$lang['General Configuration']					= 'General Configuration';
$lang['rut reset']								= 'Are you sure to change Risk Universe Type ? <br>Risk Category will reset';
$lang['No']										= 'No';
$lang['Contact']								= 'Contact';
$lang['view']									= 'View';
$lang['BULK UPLOAD']							= 'BULK UPLOAD';
$lang['Currency already exist']					= 'Currency already exist';
$lang['Upload bulk file']						= 'Upload bulk file';
$lang['Uploading file ... please wait']			= 'Uploading file ... please wait';
$lang['upload_error']							= 'Something went wrong when saving the file, please make sure csv file and try again';
$lang['process_bulk']							= 'Try to insert data from csv file, please wait';
$lang['error_1']								= 'Number fields not match in line ';
$lang['insert bulk file complete']				= 'insert bulk file complete';
$lang['Invalid Format csv file']				= 'Invalid Format csv file';
$lang['Examples']								= 'Examples';
$lang['Example Upload bulk CSV file']			= 'Example Upload bulk CSV file';
$lang['line_error']								= 'Number field in line %l is %f field(s), should have 19 fields';
$lang['Invalid field value']					= 'Invalid field value in line';
$lang['Upload Result']							= 'Upload Result';