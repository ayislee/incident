<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();   
        require_once(APPPATH.'libraries/config.php');
        $this->session->set_userdata('loaded_module',USERADMIN_MODULE);
        if ($this->session->has_userdata('language')){

            
            $this->lang->load("message",$this->session->userdata('language'));
        }else{
            $this->session->set_userdata('language','english');
            $this->lang->load("message",$this->session->userdata('language'));
        }; 

        
        
        $this->load->model('mglobal');
        $this->load->model('mlog');

        $module_granted = $this->mglobal->group_to_module($this->session->userdata('group_id'),USERADMIN_MODULE);
        //echo $module_granted;
        //die;
        $module_status = $this->mglobal->module_status();
        if($module_status==0){

            $language['language'] =  $this->session->userdata('language');
            $data['language_switcher'] = $this->load->view('header\language',$language,true);
            $this->load->view('system/disablemodule',$data);
   
        }else{
            //header('Location: '.base_url().'index.php/admin/useradmin_start');
            // check login or not
            if(!($this->session->userdata('islogin') /*&& $this->session->userdata('module')==$this->session->userdata('loaded_module') */)){
                header('Location: '.base_url().'index.php/login/login');
            }else if($module_granted==0){
                header('Location: '.base_url().'index.php/login/login/user_not_allowed');
            }else if($this->mlog->check_expire_pwd($this->session->userdata('id_user'))){
                header('Location: '.base_url().'index.php/login/pwd_expire');
            }


        }
    }

	public function index()
	{
        //print_r($this->session->all_userdata());
        //$this->session->sess_destroy();
        header('Location: '.base_url().'index.php/admin/welcome');
	}

    public function userlist(){

    }

    public function welcome(){
        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Welcome to VIP');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$title,TRUE);
        $this->load->view('template/welcome',$data);
    }

    public function user_management(){
        $s = $this->input->get('s');
        $page=$this->input->get('page');
        if($page==''){
            $page=1;
        }

        if($s==''){
            $us = '%';
        }else {
            $us = '%'.$s.'%';
        }
        
        if($this->session->userdata('get_privilege')[4]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }


        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_user_management');
        $content_title['title'] = ' ';
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $user_search['user_search'] = $s;
        $data['user_search'] = $this->load->view('template/user_search',$user_search,TRUE); 
       

        //get user table

        $this->load->model('madmin');


        $pages = $this->madmin->pages_user($us);
        $total_row_user = $this->madmin->user_total_row($us);

        $dt = $this->madmin->getuser($page,ROW_PER_PAGE,$us);
        $js= '';
        $user_table['user_table']='';
        if($dt->num_rows()>=1){
            
            $user_count = 0;
            $js .='<script>';
            $js .=' var ids = [];';



            foreach ($dt->result() as $row) {
                # code...
                if($row->id != 1){
                    $user_count +=1;
                }
                if($row->user_status==1){
                    $status=$this->lang->line('enable');
                }else{
                    $status=$this->lang->line('disable');                    
                }
                $js .= '    ids['.($user_count-1).'] = '.$row->id.';';
                $no_user = (ROW_PER_PAGE*$page)-ROW_PER_PAGE+$user_count;
                $user_table['user_table'] .= '<div class="table-body">';
                $user_table['user_table'] .= '  <div class="table-body-item col-no">#'.$row->id.'</div>';
                $user_table['user_table'] .= '  <div class="table-body-item col-name">'.$row->first_name.' '.$row->last_name.'</div>';
                if($row->id != 1){
                    $user_table['user_table'] .= '  <div class="table-body-item col-username"><a href="'.base_url().'index.php/admin/user_management_detail/'.$row->id.'">'.$row->user_name.'</a></div>';
                }else {
                    $user_table['user_table'] .= '  <div class="table-body-item col-username"><a href="#">'.$row->user_name.'</a></div>';
                }
                $user_table['user_table'] .= '  <div class="table-body-item col-group">'.$row->group_name.'</div>';
                $user_table['user_table'] .= '  <div class="table-body-item col-status">'.$status.'</div>';
                $user_table['user_table'] .= '  <div class="table-body-item col-last">'.$row->last_login.'</div>';
                if($row->id!=1){
                $user_table['user_table'] .= '  <div class="table-body-item col-all"><input type="checkbox" class ="check-item" name="check-id-'.$row->id.'" id="check-id-'.$row->id.'"></div>';    
                }
                
                $user_table['user_table'] .= '</div>';

            }
            $js .=' var idshow='.$user_count.';';
            $js .='</script>';
        }else{
            $user_table['user_table'] .= '<div class="table-body">';
            $user_table['user_table'] .= '  <div class="table-body-item col-not-found">'.$this->lang->line('user_not_found').'</div>';
            $user_table['user_table'] .= '</div>';
        }

        $user_table['js1'] = $js;
        
        // $user_table['pagination'] = '';
        // if($pages>1){
        //     $last_page=$pages;
        //     $first_page=1;
        //     if($page==$first_page) { 
        //         $prev_page = $first_page;
        //     }else{
        //         $prev_page = $page - 1;
        //     }

        //     if($page==$last_page){
        //         $next_page = $last_page;
        //     }else{
        //         $next_page = $page + 1;
        //     }

        //     $user_table['pagination'] .= '<div class="page">';
        //     $user_table['pagination'] .= '    <ul class="pagination">';
        //     $user_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/user_management/'.$first_page.'?s='.$s.'"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';
        //     $user_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/user_management/'.$prev_page.'?s='.$s.'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
        //     $user_table['pagination'] .= '        <li><a href="#">'.$page.'</a></li>';
        //     $user_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/user_management/'.$next_page.'?s='.$s.'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
        //     $user_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/user_management/'.$last_page.'?s='.$s.'"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
        //     $user_table['pagination'] .= '    </ul>';
        //     $user_table['pagination'] .= '</div>';
        // }else{

        // }

        //echo $num_record;
        $this->load->library('pagination');


        //$config['per_page'] = 2;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['page_query_string'] = TRUE;
        //$config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo; '.$this->lang->line('First');
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = $this->lang->line('Last').' &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = $this->lang->line('Next').' &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; '.$this->lang->line('Previous');
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        // $config['display_pages'] = FALSE;
        // 
        $config['anchor_class'] = 'follow_link';
        $config['base_url'] = base_url()."index.php/admin/user_management?s=".$s;
        
        $config['total_rows'] = $total_row_user;
        $config['per_page'] = ROW_PER_PAGE;
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $user_table['pagination'] = $this->pagination->create_links();


        // group
        $user_table['group'] = $this->madmin->get_group();
        $data['user_table']  = $this->load->view('template/user_table',$user_table,TRUE);

        $this->load->view('template/user_management',$data);

    }

    public function user_management_detail($id){
        
        if($this->session->userdata('get_privilege')[4]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_user_management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$title,TRUE);

        $data['detail']  = $this->madmin->get_user_detail($id);
        $this->load->view('template/user_management_detail',$data);        
    }

    public function group_management($page=1){
        $s = $this->input->get('s');
        if($s==''){
            $us = '%';
        }else {
            $us = $s.'%';
        }

        if($this->session->userdata('get_privilege')[0]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_group_management');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title'] = ' ';
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        
        $select_filter['select_status']  = '<select class="select-status" id="select_status" name="select_status">';
        if($s==''){
            $select_filter['select_status'] .= '    <option value="" selected></option>';
            $select_filter['select_status'] .= '    <option value="1" >'.$this->lang->line('enable').'</option>';
            $select_filter['select_status'] .= '    <option value="0" >'.$this->lang->line('disable').'</option>';
        }else if($s==1){
            $select_filter['select_status'] .= '    <option value=""></option>';
            $select_filter['select_status'] .= '    <option value="1" selected>'.$this->lang->line('enable').'</option>';
            $select_filter['select_status'] .= '    <option value="0" >'.$this->lang->line('disable').'</option>';
        }else{
            $select_filter['select_status'] .= '    <option value=""></option>';
            $select_filter['select_status'] .= '    <option value="1" >'.$this->lang->line('enable').'</option>';
            $select_filter['select_status'] .= '    <option value="0" selected>'.$this->lang->line('disable').'</option>';

        }
        $select_filter['select_status'] .= '</select>';
        $data['status_filter']=$this->load->view('template/group_status_filter',$select_filter,TRUE);

        // get group table
        $this->load->model('madmin');
        $dt = $this->madmin->getgroup($page,ROW_PER_PAGE,$us);
        //print_r($dt);
        $pages = $this->madmin->pages_group($us);


        $group_table['group_table']='';
        if($dt->num_rows()>=1){
            
            $group_count = 0;



            foreach ($dt->result() as $row) {
                # code...
                $group_count +=1;
                if($row->group_status==1){
                    $status=$this->lang->line('enable');
                }else{
                    $status=$this->lang->line('disable');                    
                }

                $group_table['group_table'] .= '<div class="table-body">';
                $group_table['group_table'] .= '  <div class="table-body-item col-74">#'.$row->id.'</div>';
                if($row->id != 1){
                    $group_table['group_table'] .= '  <div class="table-body-item col-133"><a href="'.base_url().'index.php/admin/group_management_detail/'.$row->id.'">'.$row->group_name.'</a></div>';
                }else{
                    $group_table['group_table'] .= '  <div class="table-body-item col-133"><a href="#">'.$row->group_name.'</a></div>';
                }
                
                $group_table['group_table'] .= '  <div class="table-body-item col-97">'.$status.'</div>';
                //$group_table['group_table'] .= '  <div class="table-body-item col-81">'.$row->group_type.'</div>';
                //$group_table['group_table'] .= '  <div class="table-body-item col-97">'.$row->authen_type.'</div>';
                $group_table['group_table'] .= '  <div class="table-body-item col-122">'.$row->date_created.'</div>';
                $group_table['group_table'] .= '  <div class="table-body-item col-93">'.$row->user_created.'</div>';
                $group_table['group_table'] .= '  <div class="table-body-item col-133">'.$row->date_update.'</div>';
                $group_table['group_table'] .= '  <div class="table-body-item col-93">'.$row->user_updated.'</div>';
                $group_table['group_table'] .= '</div>';

            }
        }else{
            $group_table['group_table'] .= '<div class="table-body">';
            $group_table['group_table'] .= '  <div class="table-body-item col-not-found">'.$this->lang->line('group_not_found').'</div>';
            $group_table['group_table'] .= '</div>';
        }

        $group_table['pagination'] = '';
        if($pages>1){
            $last_page=$pages;
            $first_page=1;
            if($page==$first_page) { 
                $prev_page = $first_page;
            }else{
                $prev_page = $page - 1;
            }

            if($page==$last_page){
                $next_page = $last_page;
            }else{
                $next_page = $page + 1;
            }

            $group_table['pagination'] .= '<div class="page">';
            $group_table['pagination'] .= '    <ul class="pagination">';
            $group_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/group_management/'.$first_page.'?s='.$s.'"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';
            $group_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/group_management/'.$prev_page.'?s='.$s.'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
            $group_table['pagination'] .= '        <li><a href="#">'.$page.'</a></li>';
            $group_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/group_management/'.$next_page.'?s='.$s.'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
            $group_table['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/group_management/'.$last_page.'?s='.$s.'"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
            $group_table['pagination'] .= '    </ul>';
            $group_table['pagination'] .= '</div>';
        }else{

        }

        $group_table['group_type'] = $this->madmin->get_group_type();
        $group_table['group_authen'] = $this->madmin->get_group_authen();

        $data['group_table'] = $this->load->view('template/group_table',$group_table,TRUE);

        $this->load->view('template/group_management',$data);
    }

    public function group_management_detail($id,$privilege_menu=1){

        if($this->session->userdata('get_privilege')[0]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        //$dt = $this->madmin->getuser($page,ROW_PER_PAGE);
        //print_r($dt);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_group_management');
        $sub['title']   = $this->lang->line('Group Detail');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$sub,TRUE);
         
        $data['detail'] = $this->madmin->get_group_detail($id,$privilege_menu);
        switch ($privilege_menu) {
            case 1:
                $data['privilege_map'] = $this->madmin->mapping_module($id);
                $map['title'] = $this->lang->line('Module Privelege');
                $data['map_title']= $this->load->view('template/content-title3',$map,TRUE);
                break;
            case 3:
                $data['privilege_map'] = $this->madmin->mapping_opco($id);
                $map['title'] = $this->lang->line('OpCo Configuration Privelege');
                $data['map_title']= $this->load->view('template/content-title3',$map,TRUE);
                break;
            case 4:
                $data['privilege_map'] = $this->madmin->mapping_incident($id);
                $map['title'] = $this->lang->line('Incident Management Privelege');
                $data['map_title']= $this->load->view('template/content-title3',$map,TRUE);
                break;
            case 5:
                $data['privilege_map'] = $this->madmin->mapping_report($id);
                $map['title'] = $this->lang->line('Report Privelege');
                $data['map_title']= $this->load->view('template/content-title3',$map,TRUE);
                break;
            
            default:
                $data['privilege_map'] = $this->madmin->mapping_module($id);
                $map['title'] = $this->lang->line('Module Privilege');
                $data['map_title']= $this->load->view('template/content-title3',$map,TRUE);
                break;
        }
        

        $this->load->view('template/group_management_detail',$data);

    }


    public function ui_management($active=1){

        if($this->session->userdata('get_privilege')[6]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        //$dt = $this->madmin->getuser($page,ROW_PER_PAGE);
        //print_r($dt);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Configuration');
        $content_title['title'] = $this->lang->line('Configuration UI');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $menu['ui_content_menu'] = $this->madmin->get_ui_menu($active);
        
        //$menu['ui_detail'] = $this->madmin->get_ui_detail($active);
        //$menu['active']=$active;
        $data['ui_content'] = $this->load->view('template/ui_content',$menu,TRUE);

        


        $this->load->view('template/ui_management',$data);
    }

    public function faq_upload(){
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'pdf';
        //$config['max_size']             = 1000;
        $config['file_name'] = $this->input->get('name',true); 
        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        //print_r($this->upload->do_upload('fileupload'));
        if ( ! $this->upload->do_upload('fileupload'))
        {
                $error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
                //print_r($error) ;
                //echo "eror" . $this->input->get('name',true);
                echo '<script>pop("disable-background","win-upload");</script>'; 
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                //$this->load->view('upload_success', $data);
                //echo "berhasil" . $this->input->get('name',true);
                echo '<script><script>pop("disable-background","win-upload");</script>'; 
        }

    }



    public function audit_trail(){

        if($this->session->userdata('get_privilege')[3]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }


        $user   = $this->input->get('user');
        $module = $this->input->get('module');
        $start  = $this->input->get('start');
        $end    = $this->input->get('end');
        $page   = $this->input->get('page');

        if($page=='') $page=1;

        

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_audit_trail');
        $content_title['title'] = ' ';
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $audit_trail_filter['user']     = $this->madmin->get_user_trail($user);
        $audit_trail_filter['module']   = $this->madmin->get_module($module);
        $audit_trail_filter['page']     = $page;
        $audit_trail_filter['start']    = $start;
        $audit_trail_filter['end']      = $end;
        $data['audit_trail_filter']     = $this->load->view('template/audit_trail_filter',$audit_trail_filter,TRUE);

        $data['audit_trail_table']      = $this->madmin->get_user_log($page,$user,$module,$start,$end);


        $this->load->view('template/audit_trail',$data);
    }

    public function faq($page=1){
        $this->load->model('madmin');
        $dt = $this->madmin->getuser($page,ROW_PER_PAGE);
        //print_r($dt);
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_faq');
        $data['title']  = $this->load->view('template/title',$title,TRUE);

        $data['faqcontent'] = $this->load->view('template/faqcontent',$header,TRUE);

        $this->load->view('template/faq',$data);
    }

    public function disable_acc(){
        $this->load->model('madmin');
        $json = $this->input->post('users');
        $s = $this->input->post('s');
        if($s==session_id()){
            $users = json_decode($json);
            if(count($users)>0){
                foreach ($users as $value) {
                    # code...
                    //echo $value;
                    $this->madmin->disable_user($value,$this->session->userdata('id_user'));
                }
            }
        }else{
            echo "illegal";
        }
    }

    public function enable_acc(){
        $this->load->model('madmin');
        $json = $this->input->post('users');
        $s = $this->input->post('s');
        if($s==session_id()){
            $users = json_decode($json);
            if(count($users)>0){
                foreach ($users as $value) {
                    # code...
                    //echo $value;
                    $this->madmin->enable_user($value,$this->session->userdata('id_user'));
                }
            }
        }else{
            echo "illegal";
        }
    }
    public function save_user(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $firstname = $this->input->post('f');
        $lastname  = $this->input->post('l');
        $group     = $this->input->post('g'); 
        $status    = $this->input->post('stat');
        $s         = $this->input->post('s');
        //$output = "id : ".$id .'<br>'."email: ".$email.'<br>firstname:'.$firstname.'<br>lastname: '.$lastname.'<br>group: '.$group.'<br>status:'.$status;

        //echo $output;
        if($s==session_id()){
            $this->madmin->update_user($id,$email,$firstname,$lastname,$group,$status,$this->session->userdata('id_user'));
        }
    }

    public function check_ldap(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $s         = $this->input->post('s');
        if($s==session_id()){
            if($this->madmin->check_ldap($id)){
                // ldp user
                echo 1;
            }else{
                // db user
                echo 0;
            }
        }

    }

    public function change_pass(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $p1 = $this->input->post('p1');
        $p2 = $this->input->post('p2');
        $s  = $this->input->post('s');
        if($s == session_id()){
            if($p1 == $p2){
                $regex_text = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/m';
                $a = preg_match($regex_text, $p1);
                if($a){
                    echo $this->madmin->ch_pass($id,$p1,$this->session->userdata('id_user'));
                }else{
                    echo $this->lang->line('pass_rule');
                }
                
            }else{
                echo $this->lang->line('Password you entered is not the same');
            }
        }
    }

    public function change_pass2(){
        $this->load->model('madmin');
        $id = $this->input->post('id');
        $p0 = $this->input->post('p0');
        $p1 = $this->input->post('p1');
        $p2 = $this->input->post('p2');
        $s  = $this->input->post('s');
        if($s == session_id()){
            if($p1 == $p2){
                $regex_text = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/m';
                $a = preg_match($regex_text, $p1);
                if($a){
                    echo $this->madmin->ch_pass2($id,$p1,$p0,$this->session->userdata('id_user'));
                }else{
                    echo $this->lang->line('pass_rule');
                }
                
            }else{
                echo $this->lang->line('Password you entered is not the same');
            }
        }
    }

    public function test_regex(){
        $regex_text = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/m';

        $a = preg_match($regex_text, '1234abcS');

        if($a){
            echo "benar";
        }else {
            echo "salah";
        }

    }
	public function check_ldap_user(){
        $user = $this->input->post('user');
        $sess = $this->input->post('sess');
        
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($user)!=''){
                /*
                $localuser = $this->madmin->check_local_user($user); 
                if($localuser){
                    // user registered in db
                    $ldap_user = array('code'=>'2','ldap_users'=>array());
                    echo json_encode($ldap_user);
                }else{
                    // check exist user from ldap server
                    $ldap_user = $this->madmin->check_ldap_user($user);
                    echo json_encode($ldap_user);
                }
                */
                $ldap_user = $this->madmin->check_ldap_user($user);
                echo json_encode($ldap_user);
            }else{
                // No input user
                $ldap_user = array('code'=>'5','ldap_users'=>array());
                echo json_encode($ldap_user);
            }
        }else{
            // invalid session
            $ldap_user = array('code'=>'6','ldap_users'=>array());
            echo json_encode($ldap_user);
        }

    }

    public function add_ldap_user(){
        $username = $this->input->post('username');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $email = $this->input->post('email');
        $group = $this->input->post('group');
        $status = $this->input->post('status');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            // echo $username.' '.$firstname.' '.$lastname.' '.$email.' '.$group.' '.$status.' '.$sess;
            $this->madmin->add_ldap_user($username,$firstname,$lastname,$email,$group,$status,$this->session->userdata('id_user'));
            
        }
    }

    public function add_local_user(){
        $username = $this->input->post('username');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $email = $this->input->post('email');
        $group = $this->input->post('group');
        $status = $this->input->post('status');
        $p1 = $this->input->post('p1');
        $p2 = $this->input->post('p2');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            if(($p1!=='') && ($p1 == $p2)){
                //check email
                $this->load->helper('email');
                if (valid_email($email)){
                    // code here $username,$firstname,$lastname,$email,$group,$p1,$status,$id_user
                    $this->madmin->add_local_user($username,$firstname,$lastname,$email,$group,$p1,$status,$this->session->userdata('id_user'));
                    echo '1'; //'user created';
                }else {
                    echo '2'; //'email is not valid';
                }
            }else {
                echo '3'; //password is not valid';    
            }
        }else {
            echo '4'; //  Command Invalid
        }

    }

    public function update_group(){
        $id = $this->input->post('id');
        $group_name = $this->input->post('group_name');
        $group_type = $this->input->post('group_type');
        $authen_type = $this->input->post('authen_type');
        $group_status = $this->input->post('group_status');
        $sess = $this->input->post('sess');
        //echo $sess;
        
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($group_name) != ''){
                $status = $this->madmin->update_group($id,$group_name,$group_type,$authen_type,$group_status,$this->session->userdata('id_user'));
            
                if($status){
                    echo '0';
                }else{
                    echo '1';
                }
            }else{
                echo '3';
            }
            
            
        }else{
            echo '2';
        }
        

    }

    public function delete_module_map(){
        $id_group = $this->input->post('id_group');
        $id_module = $this->input->post('id_module');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            //echo "group: ".$id_group." moudule".$id_module;
            $this->madmin->delete_module_map($id_group,$id_module,$this->session->userdata('id_user'));
            
        }

    }

    public function add_module(){
        $id_group = $this->input->post('id_group');
        $id_module = $this->input->post('id_module');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            //echo "group: ".$id_group." moudule: ".$id_module;
            $status = $this->madmin->add_module_map($id_group,$id_module,$this->session->userdata('id_user'));
            
        }
    }

    public function add_new_group(){
        $group_name = $this->input->post('group_name');
        $group_type = $this->input->post('group_type');
        $group_authen = $this->input->post('group_authen'); 
        $group_status = $this->input->post('group_status');       
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($group_name)!=''){
                $status = $this->madmin->add_group($group_name,$group_type,$group_authen,$group_status,$this->session->userdata('id_user'));
                if($status){
                    echo '0';
                }else{
                    echo '1';
                }                
            }else{
                echo '2';
            }
            
        }else{
            echo '3';
        }

    }

    public function delete_currency(){
        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $this->madmin->delete_currency($id,$this->session->userdata('id_user'));
        }
    }

    public function delete_detection(){

        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->delete_detection($id,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }

    public function delete_opco(){
        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');

        if($sess == session_id()){
            $out = $this->madmin->delete_opco($id,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }


    public function add_currency(){
        $currencyname = $this->input->post('currencyname');
        $currencysymbol = $this->input->post('currencysymbol');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($currencyname)!='' && trim($currencysymbol)!=''){
                $status = $this->madmin->add_currency($currencyname,$currencysymbol,$this->session->userdata('id_user'));
                if($status){
                    echo '0';
                }else{
                    echo '1';
                }                
            }else{
                echo '2';
            } 
        }else{
            echo '3';
        }
    }


    public function add_detection(){
        $dtname = $this->input->post('dtname');
        $dtcost = $this->input->post('dtcost');
        $sess = $this->input->post('sess');

        if($sess == session_id()){
            $this->load->model('madmin');
            $out = $this->madmin->add_detection($dtname,$dtcost,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);

    }



    public function add_opco(){
        $opconame = $this->input->post('opconame');
        $currency_id = $this->input->post('currency_id'); 
        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->add_opco($opconame,$currency_id,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);

    }


    
    public function update_currency(){

        $id =$this->input->post('id');
        $currencyname = $this->input->post('currencyname');
        $currencysymbol = $this->input->post('currencysymbol');

        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($currencyname)!='' && trim($currencysymbol)!=''){
                $status = $this->madmin->update_currency($id,$currencyname,$currencysymbol,$this->session->userdata('id_user'));
                if($status){
                    echo '0';
                }else{
                    echo '1';
                }                
            }else{
                echo '2';
            } 
        }else{
            echo '3';
        }
    }

    public function update_detection(){

        $dtid =$this->input->post('dtid');
        $dtname = $this->input->post('dtname');
        $dtcost = $this->input->post('dtcost');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->update_detection($dtid,$dtname,$dtcost,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }

    public function test_detect(){
        $this->load->model('madmin');
        $out = $this->madmin->update_opco(3,"S",$this->session->userdata('id_user'));
        print_r($out);
    }

    public function update_opco(){

        $opcoid =$this->input->post('opcoid');
        $opconame = $this->input->post('opconame');
        $currency_id = $this->input->post('currency_id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){

            $out = $this->madmin->update_opco($opcoid,$opconame, $currency_id,$this->session->userdata('id_user'));

        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);

    }


    public function privilege($id){

        if($this->session->userdata('get_privilege')[0]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $group_name = $this->madmin->get_group_name($id);
        $title['title'] = $this->lang->line('admin_menu_group_management');
        $content_title['title'] = $group_name.' '.$this->lang->line('Menu Privilege');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);

        $data['content'] = $this->madmin->content_privilege($id);
        
        $this->load->view('template/privilege',$data);

    }

    public function set_map_privilege(){
        $privilege_id =$this->input->post('privilege_id');       
        $group_id = $this->input->post('group_id');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');        
        if($sess == session_id()){
            $this->madmin->set_map_privilege($privilege_id,$group_id,$this->session->userdata('id_user'));
        }
    }

    public function del_map_privilege(){
        $privilege_id =$this->input->post('privilege_id');       
        $group_id = $this->input->post('group_id');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');        
        if($sess == session_id()){
            $this->madmin->del_map_privilege($privilege_id,$group_id,$this->session->userdata('id_user'));
        }
    }

    public function module_privilege(){
        $privilege_id =$this->input->post('privilege_id');       
        $group_id = $this->input->post('group_id');
        $sess = $this->input->post('sess');
        //echo "privilege_id:".$privilege_id.' - group_id:'.$group_id;
        
        $this->load->model('madmin');        
        if($sess == session_id()){
            if($this->madmin->get_map_privilege($group_id,$privilege_id)){
                echo "1";
            }else{
                echo "0";
            }
        }else{
            echo "0";    
        } 
        
                       
    }
    public function available($user){
        $this->load->model('madmin');
        $status = $this->madmin->check_ldap_user($user);
        $output = '';
        $output .= '<div class="table-head">';
        $output .= '    <div class="table-head-item col-username2">';
        $output .=  $this->lang->line('username');
        $output .= '    </div>';
        $output .= '</div>';

        $output .= '<div class="user-available" id="user-available">';
        $i=1;
        foreach ($status['ldap_users'] as $row) {
            # code...
            if($i==1){
                $checked='checked';
            }else{
                $checked='';
            }
            if($row['ldap_user']){
                $user_source = $this->lang->line('[Active Directory User]');
            }else{
                $user_source = $this->lang->line('[Available user]');
            }
            $output .= '<div class="table-body radio-area">';
            $output .= '    <div class="table-body-item col-username2">'.$row['username'].'</div><div class="source-user">'.$user_source.'</div><input class="std-radio" type="radio" name="username" id="username" value="'.$row['username'].'" '.$checked.'>';
            $output .= '</div>';
            $i++;    
        }
        $output .= '</div>';
        echo $output;
        //echo json_encode($localuser);
        //print_r($localuser);
    }




    public function verify_ldap_user(){
        $user = $this->input->post('user');
        $sess = $this->input->post('sess');
        
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($user)!=''){
                
                $localuser = $this->madmin->check_local_user($user); 
                if($localuser){
                    // user registered in db
                    echo '{"code":"2","username":"","firstname":"","lastname":"","email":""}';
                }else{
                    // check exist user from ldap server
                    $ldap_user = $this->madmin->verify_ldap_user($user);
                    //echo '{"'.$ldap_user['code'].'","'.$ldap_user['username'].'","'.$ldap_user['firstname'].'","'.$ldap_user['lastname'].'","'.$ldap_user['email'].'"}';
                    //header("Content-type:application/json");
                    echo json_encode($ldap_user);


                }

            }else{
                // No input user
                echo '{"code":"5","username":"","firstname":"","lastname":"","email":""}';
            }
        }else{
            // invalid session
            echo '{"code":"6","username":"","firstname":"","lastname":"","email":""}';
        }

    }









    function get_current_month(){
        $now = new \DateTime('now');
        $month = $now->format('m');
        return $month;
    }

    function get_current_year(){
        $now = new \DateTime('now');
        $year = $now->format('Y');
        return $year;
    }

    public function opco_config($opco_id=1,$month='00',$year='0000'){

        if($this->session->userdata('get_privilege')[7]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        if($month=='00'){
            $month==$this->get_current_month();
        }
        if($year=='0000'){
            $year=$this->get_current_year();
        }

        $mode = $this->input->get('mode');

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('admin_menu_opco_configuration');
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['content'] = $this->madmin->opco_config($opco_id,$month,$year,$mode);
        $this->load->view('template/opco_config',$data);           
    }

    public function check_opco_config(){
        $opcoid =$this->input->post('opcoid');       
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            echo $this->madmin->check_opco_config($opcoid,$month,$year);
        }else{
            echo '2';
        }
    }    

    public function add_opco_config(){
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');    
        if($sess == session_id()){
            echo $this->madmin->add_opco_config($json,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
    }

    public function update_opco_config(){
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        //echo $json;
        
        $this->load->model('madmin');    
        if($sess == session_id()){
            echo $this->madmin->update_opco_config($json,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
        
    }

    public function test_update_config(){
        $this->load->model('madmin');
        echo $this->madmin->update_opco_config('[{"config_date":"undefined-undefined-01 00:00:00","variable":"general_numbers_of_subscribers_prepaid","value":"1"},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_numbers_of_subscribers_postpaid","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_gross_adds_prepaid","value":"1"},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_gross_adds_postpaid","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_no_of_mo_voice_minutes_prepaid","value":"1"},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_no_of_mo_voice_minutes_postpaid","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_inbound_roaming_revenue","value":"1"},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_international_interconnect_revenue","value":"1"},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_total_mt_international_minutes","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_international_interconnect_cost","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_total_mo_international_minutes","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_total_bu_revenue","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_in_system","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_gl_accounting_system","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_third_parties_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_third_parties_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_postpaid_defaulters_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_postpaid_defaulters_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_prepaid_customers_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_prepaid_customers_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_local_interconnect_partners_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_local_interconnect_partners_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_international_interconnect_partners_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_international_interconnect_partners_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_roaming_partners_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_roaming_partners_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_mfs_provision","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_mfs_bad_debts","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_avg_disconnecting_duration","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_on_net_terminated_minutes","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_off_net_terminated_minutes","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_on_net_suspended_sims","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_off_net_suspended_sims","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_leakage_per_sim","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_leakage","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_recovery","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_prevented","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"bypass_detection_cost","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_payments_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_payments_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_payments_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_transfers_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_transfers_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_transfers_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_accounts_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_accounts_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_accounts_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_loans_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_loans_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_loans_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_insurance_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_insurance_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_insurance_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_merchant_services_no_of_fraud_incidents","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_merchant_services_financial_losses_to_customers","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"mfs_merchant_services_financial_losses_to_bu","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"currency_currency_symbol","value":"5"},{"config_date":"undefined-undefined-01 00:00:00","variable":"currency_rate","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_over_charging","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"general_deferred_balance","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"control_coverage_fm","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"control_coverage_ra","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"control_coverage_mfs","value":""},{"config_date":"undefined-undefined-01 00:00:00","variable":"control_coverage_bypass","value":""}]',$this->session->userdata('id_user'));
    }

    public function rut(){

        if($this->session->userdata('get_privilege')[5]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_risk_universe_mapping');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('Risk Universe Type');
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $rut = $this->madmin->rut();
        $content['rut'] = $rut;
        $data['content'] = $this->load->view('template/rut_content',$content,TRUE);
        
        $this->load->view('template/rut',$data);   
    }

    public function save_rut(){
        $rutname = $this->input->post('rutname');
        $bypass = $this->input->post('bypass');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($rutname)!=''){
                echo $this->madmin->add_rut($rutname,$bypass,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }

    }   

    public function delete_rut(){
        $rutid = $this->input->post('rutid');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            echo $this->madmin->delete_rut($rutid,$this->session->userdata('id_user'));
            
        }else{
            echo '2';
        }

    }    

    public function update_rut(){
        $rutid = $this->input->post('rutid');
        $rutname = $this->input->post('rutname');
        $bypass = $this->input->post('bypass');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($rutname)!=''){
                echo $this->madmin->update_rut($rutid,$rutname,$bypass,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }

    }         

    public function st($rutid=-1){

        if($this->session->userdata('get_privilege')[5]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_risk_universe_mapping');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('Section Title');
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $content['rut'] = $this->madmin->get_rut();
        if($rutid==-1){
            $rutid = $this->madmin->get_first_rut_id();
        }
        $content['st'] = $this->madmin->get_st($rutid);
        $content['selected_rut'] = $rutid;
        $data['content'] = $this->load->view('template/st_content',$content,TRUE);
        $this->load->view('template/st',$data);   
    }

    public function insert_st(){
        $rutid = $this->input->post('rutid');
        $stname = $this->input->post('stname');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($stname)!='' && $rutid != ''){
                echo $this->madmin->insert_st($rutid,$stname,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }
    }

    function test_insert_st(){
        $this->load->model('madmin');
        echo $this->madmin->insert_st(1,'Customer Management',$this->session->userdata('id_user'));

    }

 

    public function delete_st(){
        $stid = $this->input->post('stid');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            echo $this->madmin->delete_st($stid,$this->session->userdata('id_user'));
            
        }else{
            echo '2';
        }
    }         

    public function update_st(){
        $rutid = $this->input->post('rutid'); 
        $stid = $this->input->post('stid');
        $stname = $this->input->post('stname');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($stname)!=''){
                echo $this->madmin->update_st($rutid,$stid,$stname,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }

    } 

    public function oc($rutid=-1,$stid=-1){

        if($this->session->userdata('get_privilege')[5]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_risk_universe_mapping');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('Operator Category');

        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $rut = $this->madmin->rut();
        $content['rut'] = $rut;
        if($rutid==-1){
            $rutid = $this->madmin->get_first_rut_id();
        }
        if($stid==-1){
            $stid = $this->madmin->get_first_st_id($rutid);
        }

                

        $content['st'] = $this->madmin->get_st($rutid);
        $content['selected_rut'] = $rutid;
        $content['selected_st'] = $stid;
        $content['oc'] = $this->madmin->get_oc($stid);

        $data['content'] = $this->load->view('template/oc_content',$content,TRUE);

        $this->load->view('template/oc',$data);   
    }

    public function insert_oc(){
        $stid = $this->input->post('stid');
        $ocname = $this->input->post('ocname');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($ocname)!='' && $stid != ''){
                echo $this->madmin->insert_oc($stid,$ocname,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }
    }    

    public function test_oc(){
        $this->load->model('madmin');
        echo $this->madmin->insert_oc(1,"Fixed & Mobile",$this->session->userdata('id_user'));
    }

    public function delete_oc(){
        $ocid = $this->input->post('ocid');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            echo $this->madmin->delete_oc($ocid,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
    }         

    public function update_oc(){
        $stid = $this->input->post('stid');
        $ocid = $this->input->post('ocid');
        $ocname = $this->input->post('ocname');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($ocname)!=''){
                echo $this->madmin->update_oc($stid,$ocid,$ocname,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }
    } 


    public function risk($rutid=-1,$stid=-1,$ocid=-1){

        if($this->session->userdata('get_privilege')[5]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_risk_universe_mapping');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $content_title['title']= $this->lang->line('Risk');
        $rut = $this->madmin->rut();
        $content['rut'] = $rut;
        if($rutid==-1){
            $rutid = $this->madmin->get_first_rut_id();
        }
        if($stid==-1){
            $stid = $this->madmin->get_first_st_id($rutid);
        }
         if($ocid==-1){
            $ocid = $this->madmin->get_first_oc_id($stid);
        }

        $content['st'] = $this->madmin->get_st($rutid);
        $content['selected_rut'] = $rutid;
        $content['selected_st'] = $stid;
        $content['selected_oc'] = $ocid;
        $content['oc'] = $this->madmin->get_oc($stid);
        $content['rc'] = $this->madmin->get_rc($stid,$ocid);
        $data['content'] = $this->load->view('template/risk_content',$content,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);

        $this->load->view('template/risk',$data);   
    } 

    public function test_insert_rc(){
            $this->load->model('madmin');
            echo $this->madmin->insert_rc(54,'','RA Partner Management3','RA Partner Management3 Description',$this->session->userdata('id_user'));

    }

    public function insert_rc(){
        $stid = $this->input->post('stid');
        $ocid = $this->input->post('ocid');
        $ref_number = $this->input->post('ref_number');
        $rcname = $this->input->post('rcname');
        $rcdec = $this->input->post('rcdec');
        $sess = $this->input->post('sess');

        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($rcname)!='' && trim($rcdec)!='' && $stid != ''){
                echo $this->madmin->insert_rc($ref_number,$stid,$ocid,$rcname,$rcdec,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }
    }   

    public function delete_rc(){
        $rcid = $this->input->post('rcid');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            echo $this->madmin->delete_rc($rcid,$this->session->userdata('id_user'));
        }else{
            echo '2';
        }
    }         


    public function update_rc(){
        $ocid = $this->input->post('ocid');
        $rcid = $this->input->post('rcid');
        $ref_number = $this->input->post('ref_number');
        $rcname = $this->input->post('rcname');
        $rcdec = $this->input->post('rcdec');
        $sess = $this->input->post('sess');
        $stid = $this->input->post('stid');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($rcname)!='' && trim($rcdec)!=''){
                echo $this->madmin->update_rc($stid,$ref_number,$ocid,$rcid,$rcname,$rcdec,$this->session->userdata('id_user'));
            }else{
                echo '2';
            }
        }else{
            echo '3';
        }
    } 

    public function test(){
        $this->load->model('madmin');
        echo $this->madmin->update_rc(17,'AMC','DEC',$this->session->userdata('id_user'));
        //$this->madmin->get_opco_config('1','06','2016');
    }          

    public function add_control_coverage(){
        $ccname = $this->input->post('ccname');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date'); 
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($ccname)!=''){
                $out = $this->madmin->add_control_coverage($ccname,$start_date,$end_date,$this->session->userdata('id_user'));
            }else{
                $out['status'] = 2;
                $out['msg'] = $this->lang->line('Control coverage is blank');
            }
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);       
    }

    public function delete_control_coverage(){
        $ccid = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->delete_control_coverage($ccid,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }

    public function modify_control_coverage(){
        $ccid = $this->input->post('id');
        $ccname = $this->input->post('ccname');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date'); 
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->modify_control_coverage($ccid,$ccname,$start_date,$end_date,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }

    public function report_config(){

        if($this->session->userdata('get_privilege')[8]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_report_configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$title,TRUE);
        $report_table['reports'] = $this->madmin->report_list();
        $data['report_table'] = $this->load->view('template/report_table',$report_table,TRUE); 
        //$data['detail']  = $this->madmin->get_user_detail($id);
        $this->load->view('template/report',$data);         
    }

    public function get_params(){
        $path = $this->input->post('path');

        $name = $this->input->post('name');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($path)==''){
                $out['status'] = 1;
                $out['msg'] = $this->lang->line('Report Path is blank');
                echo json_encode($out);
                exit;
            }

            if(trim($name)==''){
                $out['status'] = 2;
                $out['msg'] = $this->lang->line('Report Name is blank');
                echo json_encode($out);
                exit;
            }

            $out = $this->ssrs_get_params($path);

            echo json_encode($out);

        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
            echo json_encode($out);
            exit;
        }

                
    }

    public function test_params(){
         $params =  $this->ssrs_get_params('/VIP_Dashboard/2.1 VimpelCom GRAFM General Dashboard');
         print_r($params);
    }

    public function ssrs_get_params($report){
        require_once(APPPATH.'libraries/config.php');
        require_once(APPPATH.'libraries/bin/SSRSReport.php');
        try {
            $ssrs_report = new SSRSReport(new Credentials(UID, PWD), SERVICE_URL);
            $reportParameters = $ssrs_report ->GetReportParameters($report, null, true, null,     null);
            $parameters = array();
            foreach($reportParameters as $reportParameter) {
                $parameters[] = array(
                                        "Name" => $reportParameter->Name,
                                        "ValidValues" => $reportParameter->ValidValues,
                                        "PromptUser"=>$reportParameter->PromptUser,
                                        "MultiValue"=>$reportParameter->MultiValue
                                      );       
            }
        }

        catch(SSRSReportException $serviceException)
        {
            //$out = array('status'=>-1,'msg' =>$serviceException->GetErrorMessage(),'parameters'=>array());
            $out = array('status'=>-1,'msg' =>$this->lang->line('get_params_fail'),'parameters'=>array());
            return $out;
            exit();
        }
        $out = array('status'=>0,'msg' =>'','parameters'=>$parameters);

        return $this->ssrs_valid_params($out);
    }

    public function ssrs_valid_params($params){
        if($params['status']==0){
            $num = 0;
            $new_params = array();
            foreach ($params['parameters'] as $param) {
                # code...
                //echo $param['Name'].'<br>';
                //print_r($param['ValidValues']); echo '<br>-------<br>';
                if($param['PromptUser']==1) array_push($new_params, $param);
            }
            //echo '<br>'.$num;
            $out=array('status'=>$params['status'],'msg'=>$params['msg'],'params'=>$new_params);
            return $out;
        }else{
            $out=array('status'=>$params['status'],'msg'=>$params['msg'],'params'=>$params['parameters']);
            return $out;
        }
    }

    public function generate_params_table(){
        $parameters = $this->input->post('parameters');
        $type = $this->input->post('type');
        //$parameters = '[{"Name":"Year","ValidValues":[{"Label":"2016","Value":"2016"},{"Label":"2017","Value":"2017"}]},{"Name":"OpCo","ValidValues":[{"Label":"Algeria","Value":"24"},{"Label":"Armenia","Value":"17"},{"Label":"Bangladesh","Value":"27"},{"Label":"France","Value":"14"},{"Label":"Georgia","Value":"19"},{"Label":"Germany","Value":"13"},{"Label":"Indonesia","Value":"31"},{"Label":"Italy","Value":"23"},{"Label":"Kazakhstan","Value":"18"},{"Label":"Kyrgyzstan","Value":"20"},{"Label":"Pakistan","Value":"26"},{"Label":"Russia","Value":"16"},{"Label":"Tajikistan","Value":"21"},{"Label":"UK","Value":"15"},{"Label":"Ukraine","Value":"25"},{"Label":"Uzbekistan","Value":"22"},{"Label":"Zimbabwe","Value":"28"}]}]';
        $html = '';
        $i = 0;
        $p = json_decode($parameters);
        if($type==0){
            $html .= '<div class="group-table">';
            $html .= '  <div class="table-head">';
            $html .= '      <div class="table-head-item ct1">'.$this->lang->line('Parameter').'</div>';
            $html .= '      <div class="table-head-item ct2">'.$this->lang->line('Type').'</div>';
            $html .= '  </div>';
            
            foreach ($p as $p) {
                # code...
                $i++;
                if($p->MultiValue==1) $checked= "checked"; else $checked='';
                $html .= '  <div class="table-body">';
                $html .= '      <div class="table-body-item ct1">';
                $html .= '         <input class="std-input" id="param_name_'.$i.'" value="'.$p->Name.'" disabled>';
                $html .= '     </div>';
                $html .= '      <div class="table-body-item ct2">';
                $html .= '          ';
                $html .= '          <input type="checkbox" id="p_'.$i.'" name="p_'.$i.'" disabled '.$checked.'> '.$this->lang->line('Multi Select');
                $html .= '      </div>';
                $html .= '  </div>';
            }
            $html .= '  </div>';
            $html .= '</div>';

            $html .= '<script>';
            $html .= '</script>';
        }else{
            $html .= '<div class="group-table">';
            $html .= '  <div class="table-head">';
            $html .= '      <div class="table-head-item ct1">'.$this->lang->line('Parameter').'</div>';
            $html .= '      <div class="table-head-item ct2">'.$this->lang->line('Type').'</div>';
            $html .= '  </div>';
            
            foreach ($p as $p) {
                # code...
                $i++;
                if($p->MultiValue==1) $checked= "checked"; else $checked='';
                $html .= '  <div class="table-body">';
                $html .= '      <div class="table-body-item ct1">';
                $html .= '         <input class="std-input" id="e_param_name_'.$i.'" value="'.$p->Name.'" disabled>';
                $html .= '     </div>';
                $html .= '      <div class="table-body-item ct2">';
                $html .= '          ';
                $html .= '          <input type="checkbox" id="e_p_'.$i.'" name="p_'.$i.'" disabled '.$checked.'> '.$this->lang->line('Multi Select');
                $html .= '      </div>';
                $html .= '  </div>';
            }

            $html .= '  </div>';
            $html .= '</div>';

            $html .= '<script>';
            $html .= '</script>';            
        }

        echo $html;
        
    }


    public function insert_report(){
        $json = $this->input->post('json');
        $name = $this->input->post('name');
        $path = $this->input->post('path');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($path)==''){
                $out['status'] = 1;
                $out['msg'] = $this->lang->line('Report Path is blank');
                echo json_encode($out);
                exit;
            }

            if(trim($name)==''){
                $out['status'] = 2;
                $out['msg'] = $this->lang->line('Report Name is blank');
                echo json_encode($out);
                exit;
            }

            $out = $this->madmin->save_report($name,$path,$json,$this->session->userdata('id_user'));

            echo json_encode($out);
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
            echo json_encode($out);
            exit;
        }


    }

    public function delete_report(){
        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->delete_report($id,$this->session->userdata('id_user'));
            echo json_encode($out);
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
            echo json_encode($out);
            exit;
        }
    }

    public function get_report(){
        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            $out = $this->madmin->get_report($id);
            echo json_encode($out);
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
            echo json_encode($out);
            exit;
        }
    }

    public function test_get_report(){
        $this->load->model('madmin');
        $r = $this->madmin->get_report(6);
        print_r($r);

    }

    public function update_report(){
        $json = $this->input->post('json');
        $id   = $this->input->post('id');
        $name = $this->input->post('name');
        $path = $this->input->post('path');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            if(trim($path)==''){
                $out['status'] = 1;
                $out['msg'] = $this->lang->line('Report Path is blank');
                echo json_encode($out);
                exit;
            }

            if(trim($name)==''){
                $out['status'] = 2;
                $out['msg'] = $this->lang->line('Report Name is blank');
                echo json_encode($out);
                exit;
            }

            $out = $this->madmin->update_report($id,$name,$path,$json,$this->session->userdata('id_user'));

            echo json_encode($out);
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
            echo json_encode($out);
            exit;
        }


    }

    public function opco_config_table($opco=-1){

        if($this->session->userdata('get_privilege')[7]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){}else{
            header('Location: '.base_url().'index.php/admin/permission');
        }

        $this->load->model('madmin');

        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $content_title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['config_list'] = $this->madmin->get_opco_config_table($opco);
        
        //$data['ui_content'] = $this->load->view('template/opco_config_table',$menu,TRUE);

        


        $this->load->view('template/opco_config_table',$data);        
    }

    public function group_opco(){
        $this->load->model('madmin');

        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $content_title['title'] = $this->lang->line('admin_menu_opco_configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title',$content_title,TRUE);
        $data['config_list'] = $this->madmin->get_opco_config_table($opco);
        
        //$data['ui_content'] = $this->load->view('template/opco_config_table',$menu,TRUE);

        


        $this->load->view('template/opco_config_table',$data);      
    }

    public function change_map_opco2(){
        $group_id = $this->input->post('group_id');
        $opco_id   = $this->input->post('opco_id');
        $view = (int) $this->input->post('view');
        $add = (int) $this->input->post('add');
        $update = (int) $this->input->post('update');
        $add_all_month = (int) $this->input->post('add_all_month');
        $update_all_month = (int) $this->input->post('update_all_month');

        $view_incident = (int) $this->input->post('view_incident');
        $create_incident = (int) $this->input->post('create_incident');
        $update_incident = (int) $this->input->post('update_incident');
        $bulk_incident = (int) $this->input->post('bulk_incident');

        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            //echo "view : ".$view."--add:".$add."--update:".$update;
            
           $this->madmin->change_map_opco($group_id,$opco_id,$view,$add,$update,$add_all_month,$update_all_month,$view_incident,$create_incident,$update_incident,$bulk_incident,$this->session->userdata('id_user')); 
           
            
        }else{

        }
    }

    public function change_map_opco(){
        $json = $this->input->post('json');
        $this->load->model('madmin');
        //echo $json;
        
        $obj = json_decode($json);
        foreach ($obj as $row) {
            $this->madmin->change_map_opco($row->group_id,$row->opco_id,$row->view,$row->add,$row->update,$row->add_all_month,$row->update_all_month,$row->view_incident,$row->create_incident,$row->update_incident,$row->bulk_incident,$this->session->userdata('id_user'));
        }
        
    }

    public function test_change_map_opco(){
        $json = '[{"group_id":12,"opco_id":"13","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"14","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"15","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"16","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"17","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"18","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"19","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"20","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"21","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"22","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"23","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"24","view":1,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"25","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"26","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"27","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"28","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"31","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"},{"group_id":12,"opco_id":"32","view":0,"add":0,"update":0,"add_all_month":0,"update_all_month":0,"view_incident":"0","create_incident":"0","update_incident":"0","bulk_incident":"0"}]';

         $this->load->model('madmin');
         $obj = json_decode($json);
        foreach ($obj as $row) {
            $this->madmin->change_map_opco($row->group_id,$row->opco_id,$row->view,$row->add,$row->update,$row->add_all_month,$row->update_all_month,$row->view_incident,$row->create_incident,$row->update_incident,$row->bulk_incident,$this->session->userdata('id_user'));
        }

    }

    public function change_map_report2(){


        $group_id = $this->input->post('group_id');
        $report_id   = $this->input->post('report_id');
        $view = (int) $this->input->post('view');
        
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        if($sess == session_id()){
            //echo "view : ".$view."--add:".$add."--update:".$update;
            $this->madmin->change_map_report($group_id,$report_id,$view,$this->session->userdata('id_user'));
            
           
            
        }else{

        }
    }

    public function change_map_report(){

        $json = $this->input->post('json');
        $this->load->model('madmin');
        //echo $json;
        
        $obj = json_decode($json);

        $this->load->model('madmin');
        foreach ($obj as $row) {
            $this->madmin->change_map_report($row->group_id,$row->report_id,$row->view,$this->session->userdata('id_user'));    
        }
        
    }


    public function test_change_opco(){
        $this->load->model('madmin');
        $this->madmin->change_map_opco(3,1,0,0,0,0,0,0,0,0,0,$this->session->userdata('id_user'));
    }

    public function get_add_all_month(){
        $group_id = $this->input->post('group_id');
        $opco_id   = $this->input->post('opco_id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        $sql='  SELECT
                    map_group_opco.group_id,
                    map_group_opco.opco_id,
                    map_group_opco.add_all_month
                FROM
                    map_group_opco
                WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_all_month=1';
        $dt = $this->db->query($sql,array($group_id,$opco_id));
        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
            echo '1';
        }else{
            echo '0';
        }
    }

    public function update_all_month(){
        $group_id = $this->input->post('group_id');
        $opco_id   = $this->input->post('opco_id');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');
        $sql='  SELECT
                    map_group_opco.group_id,
                    map_group_opco.opco_id,
                    map_group_opco.update_all_month
                FROM
                    map_group_opco
                WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_all_month=1';
        $dt = $this->db->query($sql,array($group_id,$opco_id));
        if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
            echo '1';
        }else{
            $c_month = (int) date('m');
            $c_year = (int) date('Y');

            if(($c_month==$month && $c_year==$year)){
                echo '1';
            }else{
                echo '0';
                //echo $c_year.' '.$c_month.' --- '.$month.' '.$year;
            }

        }
    }

    public function under_construction(){
        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Welcome to VIP');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$title,TRUE);
        $this->load->view('template/under_construction',$data);
    }

    public function permission(){
        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('Welcome to VIP');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$title,TRUE);
        $this->load->view('template/permission',$data);
    }

    public function get_list_currency(){
        $curr_currency = (int) $this->input->post('curr_currency');
        $sess = $this->input->post('sess');
        if($sess == session_id()){
            $sql = 'SELECT * FROM map_vipgroup_currency ORDER BY id';
            $dt = $this->db->query($sql);
            $out = '';
            foreach ($dt->result() as $row) {
                # code...
                if($row->id== $curr_currency) $selected='selected'; else $selected='';
                $out .=  '<option class="c-option" value="'.$row->id.'" '.$selected.'>'.$row->currencysymbol.'</option>';
            }
            echo $out;

        }else{
            echo '';
        }

    }

    public function add_revenue_stream(){
        $this->load->model('madmin');
        $revenue_stream_name=  $this->input->post('revenue_stream_name');
        $sess = $this->input->post('sess');
        if($sess == session_id()){
            $out = $this->madmin->add_revenue_stream($revenue_stream_name,$this->session->userdata('id_user'));
        }else{
            $out['status'] =-1;
            $out['msg'] = $this->lang->line('Invalid command');
        }
        echo json_encode($out);
    }

    public function delete_revenue(){

        $id = $this->input->post('id');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');

        if($sess == session_id()){
            $out = $this->madmin->delete_revenue($id,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);
    }

    public function update_revenue(){
        $id = $this->input->post('id');
        $revenue_stream_name = $this->input->post('revenue_stream_name');
        $sess = $this->input->post('sess');
        $this->load->model('madmin');

        if($sess == session_id()){
            $out = $this->madmin->update_revenue($id,$revenue_stream_name,$this->session->userdata('id_user'));
        }else{
            $out['status'] = 3;
            $out['msg'] = $this->lang->line('Session expired');
        }

        echo json_encode($out);        
    }

    public function exchange_rate(){
        $this->load->model('madmin');
        $header['language'] = $this->session->userdata('language');
        $header['firstname'] = $this->session->userdata('firstname');
        $data['header']   = $this->load->view('template/header',$header,TRUE);
        $data['sidemenu'] = $this->load->view('template/sidemenu','',TRUE);

        $title['title'] = $this->lang->line('admin_menu_exchange_configuration');
        $data['title']  = $this->load->view('template/title',$title,TRUE);
        $data['content_title']= $this->load->view('template/content-title2',$title,TRUE);
        $data['table_rate'] = $this->madmin->table_rate();
        $this->load->view('template/exchange',$data);        
    }

    public function get_rate_form(){
        $this->load->model('madmin');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sess = $this->input->post('sess');
        if($sess == session_id()){
            echo json_encode($this->madmin->get_rate_form($year,$month)->result());
        }
    }

    public function test_update_rate(){
        $this->load->model('madmin');
       print_r($this->madmin->update_rate('[{"year":2016,"month":9,"currency_id":"14","rate":"1.22"},{"year":2016,"month":9,"currency_id":"15","rate":"1"},{"year":2016,"month":9,"currency_id":"16","rate":"0.23"},{"year":2016,"month":9,"currency_id":"17","rate":"0.11"}]',1));
    }

    public function update_rate(){
        $this->load->model('madmin');
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        if($sess == session_id()){
            $out = $this->madmin->update_rate($json,$this->session->userdata('id_user'));
        }else{
            $out = array('status'=>-100,'msg'=>$this->lang->line('Invalid command'));
        }
        echo json_encode($out); 
    }

    public function add_rate(){
        $this->load->model('madmin');
        $json = $this->input->post('json');
        $sess = $this->input->post('sess');
        if($sess == session_id()){
            
            $out = $this->madmin->add_rate($json,$this->session->userdata('id_user'));

        }else{
            $out = array('status'=>-100,'msg'=>$this->lang->line('Invalid command'));
        }
        //echo $json;
        echo json_encode($out);
    }

    public function test_add_rate(){
        $this->load->model('madmin');
        $m = $this->madmin->add_rate('[{"year":2016,"month":5,"currency_id":"1","rate":""},{"year":2016,"month":5,"currency_id":"14","rate":""},{"year":2016,"month":5,"currency_id":"16","rate":""}]',1);
        print_r($m);
    }

    public function get_rate(){

        $this->load->model('madmin');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $opcoid = $this->input->post('opcoid');
        $sess = $this->input->post('sess');
        
        $out = $this->madmin->get_rate($month,$year,$opcoid);
        echo json_encode($out);
        //echo $year;
    }

    public function rc(){
         $rcid = $this->input->post('rcid');
         $sql = 'SELECT * FROM cms_risk_rc WHERE rcid=?';
         $dt = $this->db->query($sql,array($rcid));
         if($dt->num_rows()>0){
            $row = $dt->row();
            $out = array('status'=>0,'rcid'=>$row->rcid, 'ref_number'=>$row->ref_number,'rcname'=>$row->rcname,'rcdec'=>$row->rcdec);
         }else{
            $out = array('status'=>-1,'rcid'=>'', 'ref_number'=>'','rcname'=>'','rcdec'=>'');
         }
         echo json_encode($out);
    }



    public function up_cur(){
        $this->load->model('madmin');
        $out = $this->madmin->run_procedure(2016,9);
    }

}

