<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
    public function __construct() {
        parent::__construct(); 
        require_once(APPPATH.'libraries/config.php');
        if ($this->session->has_userdata('language')){

            
            $this->lang->load("message",$this->session->userdata('language'));
        }else{
            $this->session->set_userdata('language','english');
            $this->lang->load("message",$this->session->userdata('language'));
        };
    }

    public function login($message=''){
        //print_r($this->session->all_userdata());
        //die;
        $language['language'] =  $this->session->userdata('language');
        //echo $data['language'];
        $data['message'] = $this->lang->line($message);
        $data['language_switcher'] = $this->load->view('header/language',$language,true);
        $data['action'] = base_url().'index.php/login/authentication';
        $this->load->view('login/login',$data);    

        
    }

    public function authentication(){
        
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $encrytion_pass = KEY.$password;
        $this->load->model('mglobal');
        $this->load->model('mlog');
        $this->load->model('madmin');
        if($username=='' || $password==''){
            header('Location: '.base_url().'index.php/login/login/user_password_required');
        }else{
            $auth = $this->mglobal->auth($username);
            // check status user and group
            if(($auth['status'] != 1) || ($auth['group_status'] != 1)){

                header('Location: '.base_url().'index.php/login/login/user_status_disable');
            }else{
                
                // map group to module
                //echo  $this->mglobal->group_to_module($auth['group'],$this->session->userdata('loaded_module'));
                //die;
                if($this->mglobal->group_to_module($auth['group'],$this->session->userdata('loaded_module'))){
                // log to user LDAP 
                    if($this->mglobal->ldap_login($username,$password)){
                        // echo 'success';
                        // write log
                        $priv = $this->madmin->get_privilege($auth['group']);
                        

                        $sessiondata = array(
                            'username'  => $username,
                            'email'     => $auth['email'],
                            'islogin' => TRUE,
                            'module' => $this->session->userdata('loaded_module'),
                            'id_user'=> $auth['id'],
                            'firstname' => $auth['first_name'],
                            'group_id' => $auth['group'],
                            'get_privilege'=>$priv
                        );

                        $this->madmin->update_lastlogin($username);
                        $this->session->set_userdata($sessiondata);
                        $activity = 'login';
                        $decription = 'log from '. $_SERVER['REMOTE_ADDR'];
                        $this->mlog->userlog($auth['id'],$activity,$this->session->userdata('loaded_module'),$decription);

                        $expire_pwd = $this->mlog->check_expire_pwd($auth['id']);
                        if($expire_pwd){

                            // start here
                            
                            header('Location: '.base_url().'index.php/login/pwd_expire');
                        }else{
                            switch ($this->session->userdata('loaded_module')) {
                                case '1':
                                    # code...
                                    //echo session_id();
                                    header('Location: '.base_url().'index.php/admin');
                                    break;
                                
                                case '2':
                                    # code...
                                    header('Location: '.base_url().'index.php/incident');
                                    break;
                            }
                        }
                         //redirect($_SERVER['HTTP_REFERER']);
                        //$this->session->set_userdata('islogin',false);
                        // set session
                    }else{

                        $activity = 'login';
                        $decription = 'Fail login from '. $_SERVER['REMOTE_ADDR'].' because wrong password';
                        $this->mlog->userlog($auth['id'],$activity,$this->session->userdata('loaded_module'),$decription);
                        //$this->session->sess_destroy();
                        header('Location: '.base_url().'index.php/login/login/wrong_password');

                        //$this->session->set_userdata('islogin',false);
                        // clear session
                    }

                }else{
                    header('Location: '.base_url().'index.php/login/login/user_not_allowed');
                }
            }
        }

    }

    public function pwd_expire(){
        if(!($this->session->userdata('islogin'))){
            header('Location: '.base_url().'index.php/login/login');
        }else{
            $data['action'] = base_url().'index.php/login/generate';
            $language['language'] =  $this->session->userdata('language');
            $data['language_switcher'] = $this->load->view('header/language',$language,true);
            header('Location: '.base_url().'index.php/login/generate/2');

            

        }
    }

    public function logout(){
        $this->load->model('mlog');
        $module = $this->session->userdata('loaded_module');
        
        $activity = 'logout';
        $desc = 'logout user '. $this->session->userdata('username');
        $sql_log = 'INSERT INTO cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,?,?,?,NOW())';
        $this->db->query($sql_log,array($this->session->userdata('id_user'),$module,$activity,$desc));

        $this->session->sess_destroy();
        switch ($module) {
            case '1':
                # code...
                header('Location: '.base_url().'index.php/admin');
                break;
            
            case '2':
                # code...
                header('Location: '.base_url().'index.php/incident');
                break;
        }
    }

    public function reset_password($message=""){
        $data['action'] = base_url().'index.php/login/generate';
        $language['language'] =  $this->session->userdata('language');
        $data['language_switcher'] = $this->load->view('header/language',$language,true);
        $data['message'] = $this->lang->line($message);
        $data['invalid_email'] = $this->lang->line('invalid_email');
        $this->load->view('login/reset_password',$data);  
    }

    public function generate($flag=1){

        $emailPost = $this->input->post('email');

        if($emailPost == ''){
            $email = $this->session->userdata('email');
        }else{
            $email = $emailPost;
        }

        $this->load->model('mlog');

        $email_ok = $this->mlog->find_email($email);
        if($email_ok){
            // check prev reset
            $reset = $this->mlog->reset_pass($email,$flag);
            switch ($reset['status']) {
                case 0:
                    //print_r($reset);
                    $status = $this->sent_email($reset['email'],$reset['code']);
                    // echo $status;
                    // die;
                    switch ($status) {
                        case '0':
                            # code...
                            header('Location: '.base_url().'index.php/login/sent?e='.$reset['email'].'&c='.$reset['code'].'&desc='.$reset['desc']);
                            break;

                        case '1':
                            # code...
                            header('Location: '.base_url().'index.php/login/sent?e='.$reset['email'].'&c='.$reset['code'].'&desc='.$reset['desc']);
                            break;
                        
                        default:
                            # code...
                            header('Location: '.base_url().'index.php/login/sent?e='.$reset['email'].'&c='.$reset['code'].'&desc=internal_error');
                            break;
                    }
                    break;
                case -1:
                    header('Location: '.base_url().'index.php/login/sent?e='.$reset['email'].'&c='.$reset['code'].'&desc='.$reset['fail_create_link']);
                    break;
                
                case -2:
                    $url = base_url().'index.php/login/sent?e='.$reset['email'].'&c='.$reset['code'].'&desc='.$reset['desc'].'&link='.base_url().'index.php/login/resent/'.urlencode($reset['email']).'/'.$reset['code'];
                    header('Location: '.$url);
                    
                    # code...
                    break;
            }
        }else{
            header('Location: '.base_url().'index.php/login/reset_password/not_found');
        }

    }



    public function sent(){
        $language['language'] =  $this->session->userdata('language');
        $data['language_switcher'] = $this->load->view('header/language',$language,true);
        $this->load->view('login/sent',$data);
    }

    public function sent_email($email,$code){
        // ./forgetPasswordMail.sh <email> <URL>

        $Executable = SEND_EMAIL;
        $params = " ".$email." ".base_url()."index.php/login/rp?c=".$code;
        //$params = '';
        exec($Executable.$params, $output,$return);
        // var_dump($output);
        return $return;
    }

    public function resent($encode_email,$code){
        // echo $encode_email;

        $email = urldecode($encode_email);
        // echo $email;
        // die;
        $this->sent_email($email,$code);
        header('Location: '.base_url().'index.php/login/sent?e='.$email.'&c='.$code.'&desc=sent_link_success');

    }

    public function urle(){
        echo urlencode('ayislee@gmail.com');
    }

    public function rp(){
        $code = $this->input->get('c');
        // echo $code;
        $this->load->model('mlog');
        $status = $this->mlog->check_code($code);

        switch ($status['status']) {
            case 0:
                // Redirect to change password
                $email = $this->mlog->get_email_from_code($code);
                $data['email'] = $email;
                $language['language'] =  $this->session->userdata('language');
                //echo $data['language'];
                $data['language_switcher'] = $this->load->view('header/language',$language,true);
                $data['action'] = base_url()."index.php/login/change_password?sid=".session_id();
                $data['code']=$code;
                $this->load->view('login/changepass',$data);
                break;
            case -1:
                # code...
                header('Location: '.base_url().'index.php/login/reset_result?m='.$status['desc']);
                break;
            
            case -2:
                # code...
                header('Location: '.base_url().'index.php/login/reset_result?m='.$status['desc']);
                break;
        }

    }

    public function change_password(){
        $sid = $this->input->get('sid');
        $email = $this->input->post('email');
        $new_password = $this->input->post('new-password');
        $retype_password = $this->input->post('retype-password');
        $code=$this->input->post('code');

        if($sid == session_id()){
            if($new_password == $retype_password){

                $regex_text = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/m';
                $a = preg_match($regex_text, $new_password);

                if($a){
                    $this->load->model('mlog');
                    $st = $this->mlog->change_password($email,$new_password,$code);
                    // print_r($st);
                    header('Location: '.base_url().'index.php/login/reset_result?m='.$st['desc']);

                }else{
                    header('Location:'.base_url().'index.php/login/rp?c='.$code.'&msg='.$this->lang->line('Invalid password'));  
                }
            }else{
                header('Location:'.base_url().'index.php/login/rp?c='.$code.'&msg='.$this->lang->line('Password you entered is not the same'));
            }
        }else{
            // password rule
            echo "invalid session";
            
        }

    }

    public function reset_result(){
        $m = $this->input->get('m');
        $language['language'] =  $this->session->userdata('language');
                //echo $data['language'];
        $data['language_switcher'] = $this->load->view('header/language',$language,true);

        $this->load->view('login/reset_result',$data);


    }

}