<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();     
    }
 
    function index($language = "") {
        
		$this->session->set_userdata('language', $language);        
        //echo $this->session->userdata('language');
        redirect($_SERVER['HTTP_REFERER']);
        
    }
}