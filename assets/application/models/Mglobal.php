<?php 
class Mglobal extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }


    public function auth($username){

    	$sql = 'SELECT
					*
				FROM
					cms_user
				WHERE cms_user.user_name = ?';

		$ds_user = $this->db->query($sql,array($username));
		$user = array();
		if($ds_user->num_rows()>0){
			foreach ($ds_user->result() as $dt) {
				# code...
				$user['found'] 		= true;
				$user['id'] 		= $dt->id;
				$user['username']	= $dt->user_name;
				$user['status'] 	= $dt->user_status;
				$user['group'] 		= $dt->user_group;
				$user['first_name'] = $dt->first_name;
				$user['last_name'] 	= $dt->last_name;
				$user['email']		= $dt->email;
				// group status
				$sql = 'SELECT * FROM cms_group WHERE id = ?';
				$ds_group = $this->db->query($sql,array($user['group']));
				if($ds_group->num_rows()>0){
					foreach ($ds_group->result() as $dt_group) {
						# code...
						$user['group_status']=$dt_group->group_status;
						$user['group_type']=$dt_group->group_type;
					}
				}else{
					$user['group_status'] 	= '';
					$user['group_type']	= '';
				}

			}
			
		}else{
				$user['found'] 			= false;
				$user['id'] 			= '';
				$user['username']		= '';
				$user['status'] 		= '';
				$user['group'] 			= '';
				$user['first_name'] 	= '';
				$user['last_name'] 		= '';
				$user['group_status'] 	= '';
				$user['group_type']		= '';
				$user['email']			= '';

		}


		return $user;




    }

	public function module_status(){
		$sql ='SELECT * FROM cms_module WHERE id = 1';
		$ds_module_status =  $this->db->query($sql);
		$status = 0;
		foreach ($ds_module_status->result() as $status_module) {
			# code...
			$status = $status_module->module_status;
		}

		return $status;
	}

	public function group_to_module($id_group,$id_module){
		$sql = 'SELECT * FROM map_group_module WHERE id_group =? AND id_module=?';
		//echo 'group:'.$id_group.'<br>';
		//echo 'module:'.$id_module.'<br>';
		$dt = $this->db->query($sql,array($id_group,$id_module));
		if($dt->num_rows()>0) {
			return 1;
		}else{
			return 0;
		}

	}

	public function ldap_login($username,$password){
		
		$sql = 'SELECT
					cms_user.id,
					cms_user.user_name,
					cms_authen_password.`password`,
					cms_authen_password.password_status
				FROM
					cms_user
				INNER JOIN cms_authen_password ON cms_user.id = cms_authen_password.id
				WHERE 
					cms_user.user_name = ? AND 
					cms_authen_password.`password` = md5(?)';
		$ds_ldap = $this->db->query($sql,array($username,KEY.$password));
		
		if($ds_ldap->num_rows()>0){
			return true;
		}else{
			// check local username in database
			/*
			$sql = 'SELECT * FROM cms_user WHERE cms_user.user_name = ?';
			$dt = $this->db->query($sql,array($username));
			if($dt->num_rows()>0){
				return $this->ldap_con($username,$password);
			}else{
				return false;
			}
			*/
			return false;
		}
	}

	public function ldap_con($username,$password){


		$ldap_connection = ldap_connect(LDAP_SERVER, LDAP_PORT);

		if (! $ldap_connection)
		{
		    return FALSE;
		}else {
			ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
			$ldap_bind = @ldap_bind($ldap_connection, $username.LDAP_DOMAIN, $password);
			if (! $ldap_bind)
			{
			    return FALSE;
			}else{
				return TRUE;
			}
		}

	}


}