<?php 
class Mincident extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

    public function get_by_opco($page=1,$num){
    	$row_count = $num;
    	$offset = ($page - 1)*$num ;
    	if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
	    	$sql = 'SELECT
					SUM(CASE WHEN v1.incident_status=0 THEN 1 ELSE 0 END) AS open,
					SUM(CASE WHEN v1.incident_status=1 THEN 1 ELSE 0 END) AS close,
					SUM(CASE WHEN v1.incident_status=2 THEN 1 ELSE 0 END) AS investigation,
					cms_opco_list.opconame,
					cms_opco_list.opcoid
					FROM
					cms_opco_list
					LEFT OUTER JOIN v1 ON cms_opco_list.opcoid = v1.opcoid
					GROUP BY opconame,opcoid
					ORDER BY opcoid LIMIT ?,?';
			$data['by_opco'] = $this->db->query($sql,array($offset,$row_count));
		}else{
			$sql = 'SELECT
					SUM(CASE WHEN v1.incident_status=0 THEN 1 ELSE 0 END) AS open,
					SUM(CASE WHEN v1.incident_status=1 THEN 1 ELSE 0 END) AS close,
					SUM(CASE WHEN v1.incident_status=2 THEN 1 ELSE 0 END) AS investigation,
					cms_opco_list.opconame,
					cms_opco_list.opcoid
					FROM
					cms_opco_list
					LEFT OUTER JOIN v1 ON cms_opco_list.opcoid = v1.opcoid
					INNER JOIN map_group_opco ON cms_opco_list.opcoid = map_group_opco.opco_id
					WHERE map_group_opco.group_id=? AND map_group_opco.view_incident=1
					GROUP BY opconame,opcoid
					ORDER BY opcoid LIMIT ?,?';
			$data['by_opco'] = $this->db->query($sql,array($this->session->userdata('group_id'),$offset,$row_count));
			//echo $this->db->last_query();
			//die;
		}
		if($this->session->userdata('loaded_module')==2 AND $this->agent->is_mobile()){
			return $this->load->view('mobile/opco_filter_table',$data,TRUE);
		}else return $this->load->view('incident/opco_filter_table',$data,TRUE);
    }

    public function get_opco($opcoid){
    	$sql = 'SELECT * FROM cms_opco_list WHERE opcoid= ?';
    	$data = $this->db->query($sql,array($opcoid));
    	if($data->num_rows()>0){
    		foreach ($data->result() as $row) {
    			$out = array('found'=>true, 'opcoid'=>$row->opcoid, 'opconame'=>$row->opconame);
    		}
    		
    	}else{
    		$out = array('found'=>false, 'opcoid'=>$opcoid, 'opconame'=>null);
    	}

    	return $out;
    }

    public function num_incident($status,$opcoid,$s){
    	/*
    	$sql = 'SELECT
					Count(cms_incident.id) AS `num_record`
				FROM
					cms_incident
				LEFT OUTER JOIN cms_incident_detail ON cms_incident.id = cms_incident_detail.incident_id
				WHERE
					cms_incident.incident_status = ? AND
					cms_incident_detail.opcoid = ? AND 
					cms_incident.incident_num LIKE ?';
		$data = $this->db->query($sql,array($status,$opcoid,$s.'%'));

		foreach ($data->result() as $row) {
			$num = $row->num_record;
		}
		*/
		$sql = 'SELECT DISTINCT
					cms_incident.id,
					cms_incident.incident_num,
					cms_incident.external_id,
					cms_incident.incident_status,
					cms_incident.create_date,
					cms_incident.update_date,
					cms_incident_detail.opcoid
				FROM
					cms_incident
				LEFT OUTER JOIN cms_incident_detail ON cms_incident.id = cms_incident_detail.incident_id
				WHERE cms_incident_detail.opcoid =? AND cms_incident.incident_status LIKE ? AND cms_incident.incident_num LIKE ?
				ORDER BY cms_incident.create_date DESC';

		$dt = $this->db->query($sql,array($opcoid,$status,$s.'%'));

		return $dt->num_rows();

    }

    public function get_incident($opcoid,$status,$page,$row_per_page,$s){
    	$num_record = $this->num_incident($status,$opcoid,$s);
    	//echo $num_record;
    	$this->load->library('pagination');


		//$config['per_page'] = 2;
		$config['uri_segment'] = 3;
		$config['num_links'] = 9;
		$config['page_query_string'] = TRUE;
		//$config['use_page_numbers'] = TRUE;
		$config['query_string_segment'] = 'page';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = '&laquo; '.$this->lang->line('First');
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = $this->lang->line('Last').' &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = $this->lang->line('Next').' &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; '.$this->lang->line('Previous');
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		// $config['display_pages'] = FALSE;
		// 
		$config['anchor_class'] = 'follow_link';




		$config['base_url'] = base_url() .'index.php/incident/incident_table/'.urlencode($status).'/'.$opcoid.'/';
		$config['total_rows'] = $num_record;
		$config['per_page'] = $row_per_page;
		$config['use_page_numbers'] = TRUE;

		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$row_count = $row_per_page;
    	$offset = ($page - 1)*$row_per_page ;
		$sql = 'SELECT DISTINCT
					cms_incident.id,
					cms_incident.incident_num,
					cms_incident.external_id,
					cms_incident.incident_status,
					cms_incident.create_date,
					cms_incident.update_date,
					cms_incident_detail.opcoid
				FROM
					cms_incident
				LEFT OUTER JOIN cms_incident_detail ON cms_incident.id = cms_incident_detail.incident_id
				WHERE cms_incident_detail.opcoid =? AND cms_incident.incident_status LIKE ? AND cms_incident.incident_num LIKE ?
				ORDER BY cms_incident.create_date DESC, cms_incident.incident_num DESC 
				LIMIT ?,?';

		$dt = $this->db->query($sql,array($opcoid,$status,$s.'%',$offset,$row_count));
		$data['data'] = $dt;
		$select_status = '';
		$select_status .= '<select class="std-input mobile-select" id="filter-status" name="filter-status">';
		switch ($status) {
			case '%':
				$select_status .= '<option value="%" selected>'.$this->lang->line('All').'</option>';
				$select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
				$select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
				$select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
				break;
			case '0':
				$select_status .= '<option value="%" selected>'.$this->lang->line('All').'</option>';
				$select_status .= '<option value="0" selected>'.$this->lang->line('Open').'</option>';
				$select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
				$select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
				break;
			case '1':
				$select_status .= '<option value="%" selected>'.$this->lang->line('All').'</option>';
				$select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
				$select_status .= '<option value="1" selected>'.$this->lang->line('Close').'</option>';
				$select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
				break;
			case '2':
				$select_status .= '<option value="%" selected>'.$this->lang->line('All').'</option>';
				$select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
				$select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
				$select_status .= '<option value="2" selected>'.$this->lang->line('Under Investigation').'</option>';
				break;
		}

		$select_status .= '</select>';

		$data['select_status'] = $select_status;
		$data['opcoid'] = $opcoid;
		$data['page'] = $page;

		$sql='SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=? AND create_incident=1';
		$dt = $this->db->query($sql,array($this->session->userdata('group_id'),$opcoid));
		if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			$data['create_incident'] = true;
		}else{
			$data['create_incident'] = false;
		}

		$sql='SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=? AND bulk_incident=1';
		$dt = $this->db->query($sql,array($this->session->userdata('group_id'),$opcoid));
		if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			$data['bulk_incident'] = true;
		}else{
			$data['bulk_incident'] = false;
		}

		if($this->session->userdata('loaded_module')==2 AND $this->agent->is_mobile()){
			return $this->load->view('mobile/incident_list',$data,TRUE);
		}else return $this->load->view('incident/incident_list',$data,TRUE);



    }


    public function form_incident($opcoid,$mode,$id,$cancel_operation_link,$detail_id=-1){
    	switch ($mode) {
    		case 'NEW':
    			# code...
    			$sql = 'SELECT * FROM cms_opco_list WHERE opcoid = ?';
    			$st = $this->db->query($sql,array($opcoid));
    			foreach ($st->result() as $row) {
    				# code...
    				$opconame = $row->opconame;
    			}
				$detail['id']='';
				$detail['incident_num']='';
				if(get_cookie('external_id')=='') $detail['external_id']='';else $detail['external_id']= get_cookie('external_id');
				$detail['create_date']='';
				$detail['create_by']='';
				$detail['update_date']='';
				$detail['updated_by']='';
				$detail['incident_detail_id']='';
				$detail['incident_id']='';
				$detail['incident_date']='';
				$detail['opcoid']=$opcoid;
				//echo get_cookie('detection_tool');
				//die;
				if(get_cookie('detection_tool')=='') $detail['detection_tool']='';else $detail['detection_tool']= (int) get_cookie('detection_tool'); 
				if(get_cookie('rutid')=='') $detail['rutid']='';else $detail['rutid']= (int) get_cookie('rutid');
				if(get_cookie('stid')=='') $detail['stid']=''; else $detail['stid']= (int) get_cookie('stid');
				if(get_cookie('ocid')=='') $detail['ocid']=''; else $detail['ocid']= (int) get_cookie('ocid');
				if(get_cookie('revenue_stream')=='') $detail['revenue_stream']=''; else $detail['revenue_stream']= (int) get_cookie('revenue_stream');
				if(get_cookie('impact_severity')=='') $detail['impact_severity']=''; else $detail['impact_severity']= (int) get_cookie('impact_severity');
				if(get_cookie('detection_date')=='') $detail['detection_date']=date("Y-m-d");else $detail['detection_date'] = DateTime::createFromFormat('Y-m-d', get_cookie('detection_date'))->format('Y-m-d');
				if(get_cookie('case_start_date')=='') $detail['case_start_date']=date("Y-m-d");else $detail['case_start_date'] = DateTime::createFromFormat('Y-m-d', get_cookie('case_start_date'))->format('Y-m-d');
				if(get_cookie('case_end_date')=='')$detail['case_end_date']='';else $detail['case_end_date'] = DateTime::createFromFormat('Y-m-d', get_cookie('case_end_date'))->format('Y-m-d');
				if(get_cookie('leakage_one_day')=='') $detail['leakage_one_day']='';else $detail['leakage_one_day']= (int) get_cookie('leakage_one_day');
				if(get_cookie('leakage_one_day_lc')=='') $detail['leakage_one_day_lc']='';else $detail['leakage_one_day_lc']= (int) get_cookie('leakage_one_day_lc');

				if(get_cookie('recovery')=='') $detail['recovery']=''; else $detail['recovery'] = (int) get_cookie('recovery');
				if(get_cookie('recovery_lc')=='') $detail['recovery_lc']=''; else $detail['recovery_lc'] = (int) get_cookie('recovery_lc');
				if(get_cookie('leakage_freq')=='') $detail['leakage_freq']=''; else $detail['leakage_freq'] = (int) get_cookie('leakage_freq');
				if(get_cookie('leakage_timing')=='') $detail['leakage_timing']=''; else $detail['leakage_timing'] = (int) get_cookie('leakage_timing');
				if(get_cookie('leakage_id_type')=='') $detail['leakage_id_type']='';else $detail['leakage_id_type'] = (int) get_cookie('leakage_id_type');
				if(get_cookie('recovery_type')=='')$detail['recovery_type']=''; else $detail['recovery_type'] = (int) get_cookie('recovery_type');
				if(get_cookie('incident_status')=='') $detail['incident_status']=0; $detail['incident_status']= (int) get_cookie('incident_status');
				if(get_cookie('incident_status_detail')=='') $detail['incident_status_detail']=0;else $detail['incident_status_detail']=(int) get_cookie('incident_status_detail');
				$detail['c_final_leakage']='';
				$detail['c_final_leakage_lc']='';
				$detail['c_recovered']='';
				$detail['c_recovered_lc']='';
				$detail['c_prevented']='';
				$detail['c_prevented_lc']='';
				$detail['c_potential']='';
				$detail['c_potential_lc']='';
				$detail['action']='';
				//echo get_cookie('note');
				//die;
				if(get_cookie('note')=='') $detail['note']='';else $detail['note']=get_cookie('note');

				//$detail['note']='';
				
				if(get_cookie('description')=='') $detail['description']='';else $detail['description']=get_cookie('description');
				$detail['update_date']='';
				$detail['update_by']='';
				$detail['correct_date']='';
				$detail['corerct_by']='';
				$detail['close_date']='';
				$detail['close_by']='';
				$detail['user_create']='';
				$detail['user_update']='';
				$detail['opconame']=$opconame;
				$detail['rutname']='';
				$detail['stname']='';
				$detail['ocname']='';
				$detail['leakage_freq_name']='';
				$detail['leakage_timing_name']='';
				$detail['recovery_type_name']='';
				$detail['leakage_id_type_name']='';

				if(get_cookie('list_rc')=='') $categories=array(); else {
					$json = json_decode(get_cookie('list_rc'));
					//print_r($json);
					//die;
					$categories = array();
					foreach ($json as $value) {
						# code...
						array_push($categories, array('incident_detail_id'=>$value->incident_detail_id,'rcid'=>$value->rcid,'rcname'=>$value->rcname));
					}
						
				}

				// attactment 
				$data['attachment'] = array();
				$data['history'] = array();

				$year = (int) date('Y');
				$month = (int) date('m');
				$sql = 'SELECT
	                    cms_opco_list.opcoid,
	                    cms_opco_list.opconame,
	                    cms_opco_list.currency_id,
	                    cms_currency_exchange.`year`,
	                    cms_currency_exchange.`month`,
	                    cms_currency_exchange.rate,
	                    map_vipgroup_currency.currencysymbol
	                FROM
	                    cms_opco_list
	                LEFT OUTER JOIN cms_currency_exchange ON cms_opco_list.currency_id = cms_currency_exchange.currency_id
	                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
	                WHERE cms_opco_list.opcoid=? AND cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? ';
				$dt_rate = $this->db->query($sql,array($opcoid,$year,$month));
		        //echo $this->db->last_query();
		        //die;
		        if($dt_rate->num_rows()>0){
		            $row = $dt_rate->row();
		            $data['rate'] = $row->rate;
		        }else{
		            $data['rate'] = 0;
		        }

		        $sql = 'SELECT
		                cms_opco_list.opcoid,
		                cms_opco_list.opconame,
		                cms_opco_list.currency_id,
		                map_vipgroup_currency.currencysymbol
		                FROM
		                cms_opco_list
		                INNER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
		                 WHERE opcoid=?';
		        $dt_opco = $this->db->query($sql,array($opcoid));

		        $d = $dt_opco->row();
		        $data['symbol'] = $d->currencysymbol;
		        $data['currency_id']=$d->currency_id;


    			break;
    		
    		default:
    			# code...
    			$incident_id = $id;
    			if($detail_id == -1 ){
    				$where_detail = '';
    				$arr = array($incident_id);
    			}else{
    				$where_detail = ' AND cms_incident_detail.id = ? ';
    				$arr = array($incident_id,$detail_id);
    			}
    			$sql = 'SELECT
							cms_incident.id,
							cms_incident.incident_num,
							cms_incident.external_id,
							cms_incident.create_date,
							cms_incident.create_by,
							cms_incident.update_date,
							cms_incident.updated_by,
							cms_incident.incident_status,
							cms_incident_detail.id AS incident_detail_id,
							cms_incident_detail.incident_id,
							cms_incident_detail.incident_date,
							cms_incident_detail.opcoid,
							cms_incident_detail.detection_tool,
							cms_incident_detail.rutid,
							cms_incident_detail.stid,
							cms_incident_detail.ocid,
							cms_incident_detail.revenue_stream,
							cms_incident_detail.impact_severity,
							DATE_FORMAT(cms_incident_detail.detection_date,"%Y-%m-%d") AS detection_date,
							DATE_FORMAT(cms_incident_detail.case_start_date,"%Y-%m-%d") AS case_start_date,
							DATE_FORMAT(cms_incident_detail.case_end_date,"%Y-%m-%d") AS case_end_date,
							cms_incident_detail.leakage_one_day,
							cms_incident_detail.leakage_one_day_lc,
							cms_incident_detail.recovery,
							cms_incident_detail.recovery_lc,
							cms_incident_detail.leakage_freq,
							cms_incident_detail.leakage_timing,
							cms_incident_detail.leakage_id_type,
							cms_incident_detail.recovery_type,
							cms_incident_detail.incident_status AS incident_status_detail,
							cms_incident_detail.c_final_leakage,
							cms_incident_detail.c_final_leakage_lc,
							cms_incident_detail.c_recovered,
							cms_incident_detail.c_recovered_lc,
							cms_incident_detail.c_prevented,
							cms_incident_detail.c_prevented_lc,
							cms_incident_detail.c_potential,
							cms_incident_detail.c_potential_lc,
							cms_incident_detail.action,
							cms_incident_detail.note,
							cms_incident_detail.description,
							cms_incident_detail.update_date,
							cms_incident_detail.update_by,
							cms_incident_detail.correct_date,
							cms_incident_detail.corerct_by,
							cms_incident_detail.close_date,
							cms_incident_detail.close_by,
							usrc.user_name AS user_create,
							usru.user_name AS user_update,
							cms_opco_list.opconame,
							cms_risk_rut.rutname,
							cms_risk_st.stname,
							cms_risk_oc.ocname,
							map_leakage_freq.`name` AS leakage_freq_name,
							map_leakage_timing.`name` AS leakage_timing_name,
							map_recovery_type.`name` AS recovery_type_name,
							map_leakage_id_type.`name` AS leakage_id_type_name
						FROM
							cms_incident
						LEFT OUTER JOIN cms_incident_detail ON cms_incident.id = cms_incident_detail.incident_id
						LEFT OUTER JOIN cms_user AS usrc ON cms_incident.create_by = usrc.id
						LEFT OUTER JOIN cms_user AS usru ON cms_incident.updated_by = usru.id
						LEFT OUTER JOIN cms_opco_list ON cms_incident_detail.opcoid = cms_opco_list.opcoid
						LEFT OUTER JOIN cms_risk_rut ON cms_incident_detail.rutid = cms_risk_rut.rutid
						LEFT OUTER JOIN cms_risk_st ON cms_incident_detail.stid = cms_risk_st.stid
						LEFT OUTER JOIN cms_risk_oc ON cms_incident_detail.ocid = cms_risk_oc.ocid
						LEFT OUTER JOIN map_leakage_freq ON cms_incident_detail.leakage_freq = map_leakage_freq.id
						LEFT OUTER JOIN map_leakage_timing ON cms_incident_detail.leakage_timing = map_leakage_timing.id
						LEFT OUTER JOIN map_leakage_id_type ON cms_incident_detail.leakage_id_type = map_leakage_id_type.id
						LEFT OUTER JOIN map_recovery_type ON cms_incident_detail.recovery_type = map_recovery_type.id
						WHERE
							cms_incident.id = ? '.$where_detail.
						'ORDER BY
							incident_detail_id DESC
						LIMIT 0, 1
						';

				$dt = $this->db->query($sql,$arr);
				
				//print_r($dt->result());
				//die;
				if($dt->num_rows()>0){
					$categories = array();


					foreach ($dt->result_array() as $detail) {
						# code...

						$sqlc = 'SELECT
									cms_incident_risk_mapping.incident_detail_id,
									cms_incident_risk_mapping.rcid,
									cms_risk_rc.rcname
								FROM
									cms_incident_risk_mapping
								INNER JOIN cms_risk_rc ON cms_incident_risk_mapping.rcid = cms_risk_rc.rcid
								WHERE cms_incident_risk_mapping.incident_detail_id =?';
						$cat = $this->db->query($sqlc,array($detail['incident_detail_id']));
						foreach ($cat->result() as $row) {
							# code...
							array_push($categories, array('incident_detail_id'=>$row->incident_detail_id,'rcid'=>$row->rcid,'rcname'=>$row->rcname));
						}

						// attachment 
						if($mode=='VIEW'){
							$sql = 'SELECT
									cms_incident_attachment.id,
									cms_incident_attachment.incident_detail_id,
									cms_incident_attachment.file_id,
									cms_incident_attachment.file_description,
									cms_data_file.type,
									cms_data_file.`name`,
									cms_incident_detail.correct_date,
									cms_incident_detail.corerct_by,
									cms_user.user_name,
									cms_user.email,
									cms_user.first_name,
									cms_user.last_name
								FROM
									cms_incident_attachment
								LEFT OUTER JOIN cms_data_file ON cms_incident_attachment.file_id = cms_data_file.id
								LEFT OUTER JOIN cms_incident_detail ON cms_incident_attachment.incident_detail_id = cms_incident_detail.id
								LEFT OUTER JOIN cms_user ON cms_incident_detail.corerct_by = cms_user.id
								WHERE cms_incident_attachment.incident_detail_id =?';
							$dt_a =  $this->db->query($sql,array($detail['incident_detail_id']));
							$attachment = array();
							foreach ($dt_a->result_array() as $attch) {
								# code...
								array_push($attachment,$attch);
							}
						}else{
							$attachment = array();
						}
						

						// history
						//echo $detail['id'].'<br>';
						$sql = 'SELECT
								cms_incident_detail.incident_id,
								cms_incident_detail.corerct_by,
								cms_incident_detail.action,
								cms_incident_detail.correct_date,
								cms_user.user_name,
								cms_incident_detail.id,
								cms_incident_detail.note

								FROM
								cms_incident_detail
								LEFT OUTER JOIN cms_user ON cms_incident_detail.corerct_by = cms_user.id
								WHERE cms_incident_detail.incident_id=? 
								ORDER BY cms_incident_detail.id DESC';

						$dt_h =  $this->db->query($sql,array($detail['id']));
						//echo $sql;
						//print_r($dt_h->result());
						//die;	
						$history = array();	
						foreach ($dt_h->result_array() as $his) {
							# code...
							array_push($history,$his);
						}
					}
				}

				$year = (int) DateTime::createFromFormat('Y-m-d', $detail['detection_date'])->format('Y');
				$month = (int) DateTime::createFromFormat('Y-m-d', $detail['detection_date'])->format('m');
				//echo $year.'--'.$month;die;
				

		        $sql = 'SELECT
		                cms_opco_list.opcoid,
		                cms_opco_list.opconame,
		                cms_opco_list.currency_id,
		                map_vipgroup_currency.currencysymbol
		                FROM
		                cms_opco_list
		                INNER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
		                 WHERE opcoid=?';
		        $dt_opco = $this->db->query($sql,array($opcoid));

		        $d = $dt_opco->row();
		        $data['symbol'] = $d->currencysymbol;
		        $data['currency_id']=  $d->currency_id;

		        $sql = 'SELECT * FROM cms_currency_exchange
	                WHERE cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? AND currency_id=?';
				$dt_rate = $this->db->query($sql,array($year,$month,$data['currency_id']));
		        //echo $this->db->last_query();
		        //die;
		        if($dt_rate->num_rows()>0){
		            $row = $dt_rate->row();
		            $data['rate'] = $row->rate;
		        }else{
		            $data['rate'] = 0;
		        }


				$data['attachment'] = $attachment;
				$data['history'] = $history;

    			break;

    	}


    	$data['categories']	= $categories;

    	$sqldt = 'SELECT * FROM cms_detection_tool';
    	$data['select_detection_tool'] = $this->db->query($sqldt);

    	$sqlrut = 'SELECT * FROM cms_risk_rut ORDER BY rutid';
    	$data['select_rut'] = $this->db->query($sqlrut);
    	
    	if($mode=='NEW' && get_cookie('rutid')==''){
    		$first_rut = 0;
    		foreach ($data['select_rut']->result() as $row) {
    			# code...
    			if($first_rut==0){
    				$detail['rutid']=$row->rutid;
    			}
    			$first_rut++;
    		}
    	}
    	// extend

    	$st_data = $this->get_st($detail['rutid']);
		$data['select_st'] = $st_data['data'];


		//echo get_cookie('stid');
		//die;
		if($mode=='NEW' && get_cookie('stid')==''){
    		$first_st = 0;
    		foreach ($data['select_st']->result() as $row) {
    			# code...
    			if($first_st==0){
    				$detail['stid']=$row->stid;
    			}
    			$first_st++;
    		}

    		//echo $detail['stid'];
    		//die;
    	}

		$oc_data = $this->get_oc($detail['stid']);
		$data['select_oc'] = $oc_data['data'];

		if($mode=='NEW' && get_cookie('ocid')==''){
    		$first_oc = 0;
    		foreach ($data['select_oc']->result() as $row) {
    			# code...
    			if($first_oc==0){
    				$detail['ocid']=$row->ocid;
    			}
    			$first_oc++;
    		}
    	}

    	$rc_data = $this->get_rc($detail['ocid']);
		$data['select_rc'] = $rc_data['data'];

		if($mode=='NEW' || $mode=='EDIT'){
    		$first_rc = 0;
    		foreach ($data['select_rc']->result() as $row) {
    			# code...
    			if($first_rc==0){
    				$detail['rcid']=$row->rcid;
    			}
    			$first_rc++;
    		}
    	}

    	// mapping 
    	$sql = 'SELECT * FROM map_leakage_freq';
    	$data['leakage_freq'] = $this->db->query($sql);

    	$sql = 'SELECT * FROM map_leakage_timing';
    	$data['leakage_timing'] = $this->db->query($sql);

    	$sql = 'SELECT * FROM map_leakage_id_type';
    	$data['leakage_id_type'] = $this->db->query($sql);

    	$sql = 'SELECT * FROM map_recovery_type';
    	$data['recovery_type'] = $this->db->query($sql);

    	$data['detail']  	= $detail;

    	$data['cancel_operation_link'] = $cancel_operation_link;
		$data['mode'] = $mode;

		$sql = 'SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=? AND update_incident=1';
		$dt = $this->db->query($sql,array($this->session->userdata('group_id'),$opcoid));

		if($dt->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			$data['update_incident'] = true;
		}else{
			$data['update_incident'] = false;
		}

		$sql = 'SELECT * FROM cms_revenue_stream';

		// get

		$data['select_revenue_stream']=$this->db->query($sql);
		$data['opcoid']=$opcoid;
		if($this->session->userdata('loaded_module')==2 AND $this->agent->is_mobile()){
			return $this->load->view('mobile/incident_form',$data,TRUE);
		}else return $this->load->view('incident/incident_form',$data,TRUE);

    }

    public function get_st($rutid){
    	$sqlst = 	'SELECT
						cms_risk_mapping.mid,
						cms_risk_mapping.rutid,
						cms_risk_mapping.stid,
						cms_risk_st.stname
					FROM
						cms_risk_mapping
					INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
					WHERE cms_risk_mapping.rutid=?
					ORDER BY cms_risk_mapping.mid ASC';
		$dt = $this->db->query($sqlst,array($rutid));
		$out['data'] = $dt;
		$out['option']  = '';
		foreach ($dt->result() as $row) {
			# code...
			$out['option'] .= '<option value="'.$row->stid.'">'.$row->stname.'</option>';
		}
		return $out;

    }

    public function get_oc($stid){
    	$sqloc = 	'SELECT
						cms_so_mapping.soid,
						cms_so_mapping.stid,
						cms_so_mapping.ocid,
						cms_risk_oc.ocname
					FROM
						cms_so_mapping
					INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
					WHERE cms_so_mapping.stid=?
					ORDER BY cms_so_mapping.soid ASC';
		$dt = $this->db->query($sqloc,array($stid));
		$out['data'] = $dt;
		$out['option']  = '';
		foreach ($dt->result() as $row) {
			# code...
			$out['option'] .= '<option value="'.$row->ocid.'">'.$row->ocname.'</option>';
		}
		return $out;

    }

    public function get_rc($ocid){
    	$sqlrc = 	'SELECT
						cms_risk_rc.rcid,
						cms_risk_rc.soid,
						cms_risk_rc.rcname,
						cms_risk_rc.rcdec,
						cms_so_mapping.ocid
					FROM
						cms_risk_rc
					INNER JOIN cms_so_mapping ON cms_risk_rc.soid = cms_so_mapping.soid
					WHERE cms_so_mapping.ocid=?';
		$dt = $this->db->query($sqlrc,array($ocid));
		$out['data'] = $dt;
		$out['option']  = '';
		foreach ($dt->result() as $row) {
			# code...
			$out['option'] .= '<option value="'.$row->rcid.'">'.$row->rcname.'</option>';
		}
		return $out;

    }

    public function insert_incident($json,$mode,$user_id){

    	$incident = json_decode($json);
    	$incident_date = date('Y-m-d H:i:s');

    	// Validate


		if($incident->detection_date=='' || $incident->detection_date=='0000-00-00'){
			$out['status'] = -2;
			$out['msg'] = $this->lang->line('detection date empty');
			return $out;
    		exit();
		}

		if($incident->case_start_date=='' || $incident->case_start_date=='0000-00-00'){
			$out['status'] = -3;
			$out['msg'] = $this->lang->line('case start date empty');
			return $out;
    		exit();
		}

    	if($incident->incident_status_detail == '1' && ($incident->case_end_date < $incident->case_start_date)){
    		$out['status'] = -4;
			$out['msg'] = $this->lang->line('Invalid date close status');
			return $out;
    		exit();
    	}


    	if($incident->detection_date > $incident->case_start_date ){
    		$out['status'] = -5;
			$out['msg'] = $this->lang->line('Invalid date rules');
			return $out;
    		exit();
    	}



    	if($incident->detection_date > $incident_date){
			$out['status'] = -6;
			$out['msg'] = $this->lang->line('Invalid date rules (Future date input)');
			return $out;
    		exit();
		}

    	if( $incident->case_start_date > $incident_date){
			$out['status'] = -7;
			$out['msg'] = $this->lang->line('Invalid date rules (Future date input)');
			return $out;
    		exit();
		}

		if( $incident->case_end_date > $incident_date){
			$out['status'] = -8;
			$out['msg'] = $this->lang->line('Invalid date rules (Future date input)');
			return $out;
    		exit();
		}




    	// ($leakage_one_day,$recovery,$case_start_date,$case_end_date,$leakage_id_type,$leakage_freq,$leakage_timing)
    	$c = $this->calculation(
    			$incident->c_final_leakage,
    			$incident->recovery,
    			$incident->case_start_date,
    			$incident->case_end_date,
    			$incident->leakage_id_type,
    			$incident->leakage_freq,
    			$incident->leakage_timing,
    			$incident->detection_date);

    	$c_lc = $this->calculation(
    			$incident->c_final_leakage_lc,
    			$incident->recovery_lc,
    			$incident->case_start_date,
    			$incident->case_end_date,
    			$incident->leakage_id_type,
    			$incident->leakage_freq,
    			$incident->leakage_timing,
    			$incident->detection_date);

    	

    	
    	switch ($mode) {
    		case 'NEW':
    			$action = 'Create Incident';
    			switch ($incident->incident_status) { 
    				case '0': // Open
    					$incident->leakage_one_day = null;
    					$incident->leakage_one_day_lc = null;
		    			$incident->c_recovered = null;
		    			$incident->c_recovered_lc = null;
		    			$incident->c_prevented = null;
		    			$incident->c_prevented_lc = null;
		    			$incident->c_potential = null;
		    			$incident->c_potential_lc = null;
		    			$close_date = null;
		    			$close_by = null;
		    			$update_date = null;
		    			$update_by = null;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			/*
		    			$sql = 'SELECT `GET_INCIDENTID_SEQUENCE`("'.$incident->opconame.'") AS incident_num;';
			    		//$sql = 'SELECT * FROM cms_incident';
			    		$dt = $this->db->query($sql);
			    		foreach ($dt->result() as $row) {
			    			$incident_num = $row->incident_num;
			    		}
			    		*/
			    		$incident_num = $this->app_incidentid_sequence($incident->opconame);

			    		$sql = 'INSERT INTO cms_incident(
    								incident_num,
    								external_id,
    								incident_status,
    								create_date,
    								create_by) VALUES (?,?,?,NOW(),?)';

						$da_inc = $this->db->query($sql,array($incident_num,$incident->external_id,$incident->incident_status,$user_id));


						if($da_inc){
							$insert_id = $this->db->insert_id();

							

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$insert_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents($attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($insert_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Create Incident Success');
							
							$desc = 'Create new incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Create incident Failed');
						}
    					break;
    				
    				case '1': // Close
    					$incident->leakage_one_day = $c['leakage_one_day'];
    					$incident->leakage_one_day = $c_lc['leakage_one_day'];
		    			$incident->c_recovered = $c['c_recovered'];
		    			$incident->c_recovered_lc = $c_lc['c_recovered'];
		    			$incident->c_prevented = $c['c_prevented'];
		    			$incident->c_prevented_lc = $c_lc['c_prevented'];
		    			$incident->c_potential = $c['c_potential'];
		    			$incident->c_potential_lc = $c_lc['c_potential'];
		    			$close_date = date('Y-m-d H:i:s');
		    			$close_by = $user_id;
		    			$update_date = date('Y-m-d H:i:s');
		    			$update_by =$user_id;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			$sql = 'SELECT `GET_INCIDENTID_SEQUENCE`("'.$incident->opconame.'") AS incident_num;';
			    		//$sql = 'SELECT * FROM cms_incident';
			    		$dt = $this->db->query($sql);
			    		foreach ($dt->result() as $row) {
			    			$incident_num = $row->incident_num;
			    		}

			    		$sql = 'INSERT INTO cms_incident(
    								incident_num,
    								external_id,
    								incident_status,
    								create_date,
    								create_by) VALUES (?,?,?,NOW(),?)';

						$da_inc = $this->db->query($sql,array($incident_num,$incident->external_id,$incident->incident_status,$user_id));

						if($da_inc){
							$insert_id = $this->db->insert_id();

							

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$insert_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									//$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents(UPLOAD_FOLDER.chr(47).$attachment->name)));
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents( $attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($insert_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Create Incident Success');

							$desc = 'Create new incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Create incident Failed');
						}
    					break;

    				case '2': // Under Invesigation
    					$incident->leakage_one_day = null;
    					$incident->leakage_one_day_lc = null;
		    			$incident->c_recovered = null;
		    			$incident->c_recovered_lc = null;
		    			$incident->c_prevented = null;
		    			$incident->c_prevented_lc = null;
		    			$incident->c_potential = null;
		    			$incident->c_potential_lc = null;
		    			$close_date = null;
		    			$close_by = null;
		    			$update_date = date('Y-m-d H:i:s');
		    			$update_by = $user_id;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			$sql = 'SELECT `GET_INCIDENTID_SEQUENCE`("'.$incident->opconame.'") AS incident_num;';
			    		//$sql = 'SELECT * FROM cms_incident';
			    		$dt = $this->db->query($sql);
			    		foreach ($dt->result() as $row) {
			    			$incident_num = $row->incident_num;
			    		}

			    		$sql = 'INSERT INTO cms_incident(
    								incident_num,
    								external_id,
    								incident_status,
    								create_date,
    								create_by) VALUES (?,?,?,NOW(),?)';

						$da_inc = $this->db->query($sql,array($incident_num,$incident->external_id,$incident->incident_status,$user_id));

						if($da_inc){
							$insert_id = $this->db->insert_id();

							

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$insert_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents($attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($insert_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Create Incident Success');

							$desc = 'Create new incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Create incident Failed');
						}
    					break;
    			}
    			break;
    		
    		case 'UPDATE':
    			# code...
    			$action = 'Update Incident';
    			switch ($incident->incident_status) { 
    				case '0': // Open
    					$incident->leakage_one_day = null;
    					$incident->leakage_one_day_lc = null;
		    			$incident->c_recovered = null;
		    			$incident->c_recovered_lc = null;
		    			$incident->c_prevented = null;
		    			$incident->c_prevented_lc = null;
		    			$incident->c_potential = null;
		    			$incident->c_potential_lc = null;
		    			$close_date = null;
		    			$close_by = null;
		    			$update_date =date('Y-m-d H:i:s');
		    			$update_by = $user_id;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			
			    		$sql = 'SELECT * FROM cms_incident 
			    				WHERE id=?';

						$da_inc = $this->db->query($sql,array($incident->incident_id));
						if($da_inc->num_rows()>0){
							foreach ($da_inc->result() as $row) {
								# code...
								$old_status = $row->incident_status;
								$incident_num = $row->incident_num;
							}
							/*
							if($old_status != intval($incident->incident_status)){
								$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
										WHERE id=?';
								$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							}
							*/
							$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
									WHERE id=?';
							$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$incident->incident_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									/****
									Cari tau
									*/
									//echo UPLOAD_FOLDER.chr(47).$attachment->name;
									//echo FCPATH;
									//die;
									//$location = FCPATH;
									//echo $attachment->fullpath;
									//die;
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents( $attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($incident->incident_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Update Incident Success');

							$desc = 'Update incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Update incident Failed');
						}
    					break;
    				
    				case '1': // Close
    					$incident->leakage_one_day = $c['leakage_one_day'];
    					$incident->leakage_one_day_lc = $c_lc['leakage_one_day'];
		    			$incident->c_recovered = $c['c_recovered'];
		    			$incident->c_recovered_lc = $c_lc['c_recovered'];
		    			$incident->c_prevented = $c['c_prevented'];
		    			$incident->c_prevented_lc = $c_lc['c_prevented'];
		    			$incident->c_potential = $c['c_potential'];
		    			$incident->c_potential_lc = $c_lc['c_potential'];
		    			$close_date = date('Y-m-d H:i:s');
		    			$close_by = $user_id;
		    			$update_date = date('Y-m-d H:i:s');
		    			$update_by = $user_id;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			$sql = 'SELECT * FROM cms_incident 
			    				WHERE id=?';

						$da_inc = $this->db->query($sql,array($incident->incident_id));

						if($da_inc->num_rows()>0){
							foreach ($da_inc->result() as $row) {
								# code...
								$old_status = $row->incident_status;
								$incident_num = $row->incident_num;
							}
							
							/*
							if($old_status != intval($incident->incident_status)){
								$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
										WHERE id=?';
								$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							}
							*/
							$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
									WHERE id=?';
							$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$incident->incident_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents( $attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($incident->incident_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Update Incident Success');

							$desc = 'Update incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Update incident Failed');
						}
    					break;

    				case '2': // Under Invesigation
    					$incident->leakage_one_day = null;
    					$incident->leakage_one_day_lc = null;
		    			$incident->c_recovered = null;
		    			$incident->c_recovered_lc = null;
		    			$incident->c_prevented = null;
		    			$incident->c_prevented_lc = null;
		    			$incident->c_potential = null;
		    			$incident->c_potential_lc = null;
		    			$close_date = null;
		    			$close_by = null;
		    			$update_date =  date('Y-m-d H:i:s');
		    			$update_by = $user_id;
		    			$correct_date = date('Y-m-d H:i:s');
		    			$corerct_by = $user_id;

		    			$sql = 'SELECT * FROM cms_incident 
			    				WHERE id=?';

						$da_inc = $this->db->query($sql,array($incident->incident_id));

						if($da_inc->num_rows()>0){
							foreach ($da_inc->result() as $row) {
								# code...
								$old_status = $row->incident_status;
								$incident_num = $row->incident_num;
							}
							
							/*
							if($old_status != intval($incident->incident_status)){
								$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
										WHERE id=?';
								$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							}
							*/
							$sql = 'UPDATE cms_incident SET incident_status=?, update_date=NOW(), updated_by=?
									WHERE id=?';
							$this->db->query($sql,array($incident->incident_status,$user_id,$incident->incident_id));

							 $sql_inc_det = 'INSERT INTO cms_incident_detail (
							 					incident_id,
							 					incident_date,
							 					opcoid,
							 					detection_tool,
							 					rutid,
							 					stid,
							 					ocid,
							 					revenue_stream,
							 					impact_severity,
							 					detection_date,
							 					case_start_date,
							 					case_end_date,
							 					leakage_one_day,
							 					leakage_one_day_lc,
							 					recovery,
							 					recovery_lc,
							 					leakage_freq,
							 					leakage_timing,
							 					leakage_id_type,
							 					recovery_type,
							 					incident_status,
							 					c_final_leakage,
							 					c_final_leakage_lc,
							 					c_recovered,
							 					c_recovered_lc,
							 					c_prevented,
							 					c_prevented_lc,
							 					c_potential,
							 					c_potential_lc,
							 					note,
							 					description,
							 					action,
							 					update_date,
							 					update_by,
							 					correct_date,
							 					corerct_by,
							 					close_date,
							 					close_by) 
											VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

							$da_inc_det = $this->db->query($sql_inc_det,array(
												$incident->incident_id,
							 					$incident_date,
							 					$incident->opcoid,
							 					$incident->detection_tool,
							 					$incident->rutid,
							 					$incident->stid,
							 					$incident->ocid,
							 					$incident->revenue_stream,
							 					$incident->impact_severity,
							 					$incident->detection_date,
							 					$incident->case_start_date,
							 					$incident->case_end_date,
							 					$incident->leakage_one_day,
							 					$incident->leakage_one_day_lc,
							 					$incident->recovery,
							 					$incident->recovery_lc,
							 					$incident->leakage_freq,
							 					$incident->leakage_timing,
							 					$incident->leakage_id_type,
							 					$incident->recovery_type,
							 					$incident->incident_status_detail,
							 					$incident->c_final_leakage,
							 					$incident->c_final_leakage_lc,
							 					$incident->c_recovered,
							 					$incident->c_recovered_lc,
							 					$incident->c_prevented,
							 					$incident->c_prevented_lc,
							 					$incident->c_potential,
							 					$incident->c_potential_lc,
							 					$incident->note,
							 					$incident->description,
							 					$action,
							 					$update_date,
							 					$update_by,
							 					$correct_date,
							 					$corerct_by,
							 					$close_date,
							 					$close_by));
							if($da_inc_det){
								$insert_detail_id = $this->db->insert_id();
								// insert map risk
								foreach ($incident->list_rc as $rc) {
									# code...
									$sql = 'INSERT INTO cms_incident_risk_mapping(incident_detail_id,rcid) VALUES (?,?)';
									$this->db->query($sql,array($insert_detail_id, $rc->rcid));

								}

								foreach ($incident->attachment as $attachment) {
									# code...
									$sql = 'INSERT INTO cms_data_file(type,name,data) VALUES(?,?,?)';
									$da_af =$this->db->query($sql,array($attachment->type,$attachment->name,file_get_contents( $attachment->fullpath)));
									if($da_af){
										$insert_file_id = $this->db->insert_id();
										$sql = 'INSERT INTO cms_incident_attachment(incident_detail_id,file_id,file_description) VALUES (?,?,?)';
										$this->db->query($sql,array($insert_detail_id,$insert_file_id,$attachment->file_description));
									}
								}
							}

							$sql = 'INSERT INTO cms_incident__log(id_incident,user_id,action,note,log_time) VALUES (?,?,?,?,NOW())';
							$this->db->query($sql,array($incident->incident_id,$user_id,$action,$incident->note));
							$out['status'] = 1;
							$out['msg'] = $this->lang->line('Update Incident Success');

							$desc = 'Update incident ID '. $incident_num;
					        $activity = $action;
					        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,2,?,?,NOW())';
					        $this->db->query($sql_log,array($corerct_by,$activity,$desc));

						}else {
							$out['status'] = -1;
							$out['msg'] = $this->lang->line('Update incident Failed');
						}
    					break;
    			}
    			break;

    		case 'VIEW':
    			# code...
    			break;
    	}
    	return $out;
		    	
    }

    public function calculation($c_final_leakage,$recovery,$case_start_date,$case_end_date,$leakage_id_type,$leakage_freq,$leakage_timing,$detection_date){
        $format = 'Y-m-d';
        //$date1 = '2011-02-15';
        //$date2 = '2009-02-20';


        //$diff2 = ((abs(strtotime ($detection_date) - strtotime ($case_end_date)))/(60*60*24))+1;
        

        $different_days = ((abs(strtotime ($case_start_date) - strtotime ($case_end_date)))/(60*60*24))+1;
        echo $different_days;
        die;
        $leakage_one_day = $c_final_leakage /  $different_days;
        

        $a = ($c_final_leakage + $recovery) /  $different_days;
        $b = $recovery;
        $c = ((abs(strtotime ($case_start_date) - strtotime ($case_end_date)))/(60*60*24))+1;

        //echo 'a = '. $a. '<br>'; 
        //echo 'b = '. $b. '<br>';
        //echo 'c = '. $c. '<br>';
        switch ($leakage_id_type) {
        	case '1': // proactive
        		switch ($leakage_freq) {
        			case '1': // recrusive
        				switch ($leakage_timing) {
        				 	case '1': // Realtime
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $210,000
								[Formula = A*C-B]

								2.	Recovery: $100,000
								[Formula = B]

								3.	Prevented: $3,340,000
								[Formula = A*(365-C)]

								4.	Potential leakage: $3,650,000
								[Formula = 1 + 2 + 3]
								*/
								$out['leakage_one_day'] = $a;
								$out['c_recovered'] = $b;
								$out['c_prevented'] = $a * (365 - $c);
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];

        				 		break;
        				 	
        				 	case '2': // Post-Even
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $210,000
								[Formula = A*C-B]

								2.	Recovery: $100,000
								[Formula = B]

								3.	Prevented: $3,340,000
								[Formula = A*(365-C)]

								4.	Potential leakage: $3,650,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = $b;
								$out['c_prevented'] = $a * (365 - $c);
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 } 
        				break;
        			
        			case '2': // One-Off
        				switch ($leakage_timing) {
        				 	case '1': // Realtime
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: N/A

								2.	Recovery: N/A

								3.	Prevented: $310,000
								[Formula = A*C]

								4.	Potential leakage: $310,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = 0;
								$out['c_prevented'] = $a * $c;
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 	
        				 	case '2': // Post-Even
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: N/A

								2.	Recovery: N/A

								3.	Prevented: $310,000
								[Formula = A*C]

								4.	Potential leakage: $310,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = 0;
								$out['c_prevented'] = $a * $c;
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 }
        				break;
        		}
        		break;
        	
        	case '2': // reaktive
        		switch ($leakage_freq) {
        			case '1': // recrusive
        				switch ($leakage_timing) {
        				 	case '1': // Realtime
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $210,000
								[Formula = A*C-B]

								2.	Recovery: $100,000
								[Formula = B]

								3.	Prevented: $3,340,000
								[Formula = A*(365-C)]

								4.	Potential leakage: $3,650,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = $b;
								$out['c_prevented'] = $a * (365 - $c);
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 	
        				 	case '2': // Post-Even
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $210,000
								[Formula = A*C-B]

								2.	Recovery: $100,000
								[Formula = B]

								3.	Prevented: $3,340,000
								[Formula = A*(365-C)]

								4.	Potential leakage: $3,650,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = $b;
								$out['c_prevented'] = $a * (365 - $c);
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 } 
        				break;
        			
        			case '2': // One-off
        				switch ($leakage_timing) {
        				 	case '1': // Realtime
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $310,000
								[Formula = A*C]

								2.	Recovery: N/A

								3.	Prevented: N/A

								4.	Potential leakage: $310,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = 0;
								$out['c_prevented'] =0;
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 	
        				 	case '2': // Post-Even
        				 		# code...
        				 		/*
        				 		1.	Final Leakage: $210,000
								[Formula = A*C-B]

								2.	Recovery: $100,000
								[Formula = B]

								3.	Prevented: N/A

								4.	Potential leakage: $310,000
								[Formula = 1 + 2 + 3]

        				 		*/
        				 		$out['leakage_one_day'] = $a;
								$out['c_recovered'] = $b;
								$out['c_prevented'] =0;
								$out['c_potential'] = $c_final_leakage + $out['c_recovered'] + $out['c_prevented'];
        				 		break;
        				 }
        				break;
        		}
        		break;
        }

        return $out;
        
    }

    public function get_opco_from_incident($incident_id){
    	$sql = 'SELECT DISTINCT
				cms_incident_detail.opcoid,
				cms_opco_list.opconame
				FROM
				cms_incident
				INNER JOIN cms_incident_detail ON cms_incident.id = cms_incident_detail.incident_id
				INNER JOIN cms_opco_list ON cms_incident_detail.opcoid = cms_opco_list.opcoid
				WHERE cms_incident.id=?';
		$data = $this->db->query($sql,array($incident_id));
		$out['found']=false;
		$out['opconame'] = '';
		$out['opcoid'] = -1;
		
		foreach ($data->result() as $row) {
			# code...
			$out['found']=true;
			$out['opconame'] = $row->opconame;
			$out['opcoid']= $row->opcoid;
		}
		return $out;
    }

    public function get_file($file_id){
    	$sql = 'SELECT * FROM cms_data_file WHERE id= ?';
    	return $this->db->query($sql,array($file_id));
    }


    public function find_opco($opconame){
    	$sql = 'SELECT * FROM cms_opco_list WHERE UPPER(opconame)=?';
    	$dt = $this->db->query($sql,array(strtoupper($opconame)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$opcoid = $row->opcoid;
    		}

    	}else{
    		$status = false;
    		$opcoid = null;
    	}
    	return array('status'=>$status,'opcoid'=>$opcoid);
    }

    public function find_detection_tool($dtname){
    	$sql = 'SELECT * FROM cms_detection_tool WHERE UPPER(dtname)=?';
    	$dt = $this->db->query($sql,array(strtoupper($dtname)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$dtid = $row->dtid;
    		}

    	}else{
    		$status = false;
    		$dtid = null;
    	}
    	return array('status'=>$status,'dtid'=>$dtid);
    }

    public function find_rut($rutname){
    	$sql = 'SELECT * FROM cms_risk_rut WHERE UPPER(rutname)=?';
    	$dt = $this->db->query($sql,array(strtoupper($rutname)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$rutid = $row->rutid;
    		}

    	}else{
    		$status = false;
    		$rutid = null;
    	}
    	return array('status'=>$status,'rutid'=>$rutid);
    }

    public function find_st($stname,$rutid){
    	$sql = 'SELECT
				cms_risk_mapping.rutid,
				cms_risk_st.stid,
				cms_risk_st.stname
				FROM
				cms_risk_mapping
				INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
				WHERE UPPER(stname)=? AND cms_risk_mapping.rutid=?';

    	$dt = $this->db->query($sql,array(strtoupper($stname),$rutid));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$stid = $row->stid;
    		}

    	}else{
    		$status = false;
    		$stid = null;
    	}
    	return array('status'=>$status,'stid'=>$stid);
    }

    public function find_oc($ocname,$stid){
    	$sql = 'SELECT
				cms_so_mapping.soid,
				cms_so_mapping.stid,
				cms_so_mapping.ocid,
				cms_risk_oc.ocname
				FROM
				cms_so_mapping
				INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
				WHERE UPPER(ocname)=? AND cms_so_mapping.stid=?';

    	$dt = $this->db->query($sql,array(strtoupper($ocname),$stid));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$ocid = $row->ocid;
    		}

    	}else{
    		$status = false;
    		$ocid = null;
    	}
    	return array('status'=>$status,'ocid'=>$ocid);
    }

    public function find_rc($rcname,$ocid,$stid){
    	$sql = 'SELECT
				cms_risk_rc.rcid,
				cms_risk_rc.soid,
				cms_risk_rc.rcname,
				cms_risk_rc.rcdec,
				cms_so_mapping.stid,
				cms_so_mapping.ocid
				FROM
				cms_risk_rc
				INNER JOIN cms_so_mapping ON cms_risk_rc.soid = cms_so_mapping.soid
				WHERE UPPER(rcname)=? AND cms_so_mapping.ocid=? AND cms_so_mapping.stid=?';

    	$dt = $this->db->query($sql,array(strtoupper($rcname),$ocid,$stid));

    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$rcid = $row->rcid;
    		}

    	}else{
    		$status = false;
    		$rcid = null;
    	}
    	return array('status'=>$status,'rcid'=>$rcid);
    }
    
    public function find_leakage_freq($name){
    	$sql = 'SELECT * FROM map_leakage_freq WHERE UPPER(name)=?';
    	$dt = $this->db->query($sql,array(strtoupper($name)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$id = $row->id;
    		}

    	}else{
    		$status = false;
    		$id = null;
    	}
    	return array('status'=>$status,'id'=>$id);
    }

    public function find_leakage_timing($name){
    	$sql = 'SELECT * FROM map_leakage_timing WHERE UPPER(name)=?';
    	$dt = $this->db->query($sql,array(strtoupper($name)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$id = $row->id;
    		}

    	}else{
    		$status = false;
    		$id = null;
    	}
    	return array('status'=>$status,'id'=>$id);
    }

    public function find_leakage_id_type($name){
    	$sql = 'SELECT * FROM map_leakage_id_type WHERE UPPER(name)=?';
    	$dt = $this->db->query($sql,array(strtoupper($name)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$id = $row->id;
    		}

    	}else{
    		$status = false;
    		$id = null;
    	}
    	return array('status'=>$status,'id'=>$id);
    }

    public function find_recovery_type($name){
    	$sql = 'SELECT * FROM map_recovery_type WHERE UPPER(name)=?';
    	$dt = $this->db->query($sql,array(strtoupper($name)));
    	if($dt->num_rows()>0){
    		foreach ($dt->result() as $row) {
    			# code...
    			$status = true;
    			$id = $row->id;
    		}

    	}else{
    		$status = false;
    		$id = null;
    	}
    	return array('status'=>$status,'id'=>$id);
    }    

    public function app_incidentid_sequence($opconame){
    	$sql =  'SELECT * FROM app_incidentid_sequence';
    	$data = $this->db->query($sql);
    	foreach ($data->result() as $row) {
    		# code...
    		$current_seq = $row->CURRENTID;
    		$current_date = $row->CDATE;
    	}
    	$next_seq = $current_seq+1;
    	if($next_seq>9999) $next_seq = 1;
    	$sql = 'UPDATE app_incidentid_sequence SET CDATE=NOW(), CURRENTID=?';
    	$this->db->query($sql,array($next_seq));

    	$sql =  'SELECT * FROM app_incidentid_sequence';
    	$data = $this->db->query($sql);
    	foreach ($data->result() as $row) {
    		# code...
    		$current_seq = $row->CURRENTID;
    		$current_date = $row->CDATE;
    	}

    	$DateTime = DateTime::createFromFormat('Y-m-d', $current_date);
		$formatdate = $DateTime->format('dmY');

    	//return $formatdate;
    	return strtoupper($opconame).'-'.$formatdate.'-'.str_pad($current_seq,4,'0',STR_PAD_LEFT);
    }

    public function get_opco_list(){
    	$sql = 'SELECT * FROM cms_opco_list';
    	return $this->db->query($sql);
    }

    public function get_report($report,$ps){
    	//echo FCPATH.'imagestemp/';
    	//exit;
    	$settings = parse_ini_file(APPPATH.'libraries/app.config', 1);
    	try{
    		$rs = new SSRSReport(new Credentials(UID, PWD), SERVICE_URL);
    		if (isset($_REQUEST['rs:Command'])){
    			switch($_REQUEST['rs:Command']){
		            case 'Sort':
		                $rs->Sort2($_REQUEST['rs:SortId'],
		                           $_REQUEST['rs:SortDirection'],
		                           $_REQUEST['rs:ClearSort'],
		                           PageCountModeEnum::$Estimate,
		                           $ReportItem,
		                           $ExecutionInfo);
		                  break;
		            default:
		            	$out['status']=-1;
		            	$out['msg'] = 'Unknown :' . $_REQUEST['rs:Command'];
		            	$out['html_result'];
		            	return $out;
		                exit;
		        }
    		}else {
		        $executionInfo = $rs->LoadReport2($report, NULL);
		        $parameters = array();
		        $p = json_decode($ps);
		        //print_r($p);
		        $i=0;
		        foreach ($p as $params) {
		        	# code...
		        	$parameters[$i] = new ParameterValue();
		        	$parameters[$i]->Name = $params->Name;
		        	$parameters[$i]->Value = $params->Value;
		        	$i++;
		        }
		        //print_r($parameters);

		        $rs->SetExecutionParameters2($parameters);
		    }
		    $renderAsHTML = new RenderAsHTML();

		    $renderAsHTML->ReplacementRoot = $this->getPageURL();
		    $renderAsHTML->StreamRoot = base_url().'imagestemp/';
		    $result_html = $rs->Render2($renderAsHTML,
		                                 PageCountModeEnum::$Actual,
		                                 $Extension,
		                                 $MimeType,
		                                 $Encoding,
		                                 $Warnings,
		                                 $StreamIds);

		    foreach($StreamIds as $StreamId)
		    {
		        $renderAsHTML->StreamRoot = null;
		        $result_png = $rs->RenderStream($renderAsHTML,
		                                    $StreamId,
		                                    $Encoding,
		                                    $MimeType);

		        if (!$handle = fopen("imagestemp/" . $StreamId, 'wb'))
		        {
	            	$out['status']=-2;
	            	$out['msg'] = "Cannot open file for writing output";
	            	$out['html_result']='';
	            	return $out;
	                exit;

		        }

		        if (fwrite($handle, $result_png) === FALSE)
		        {
		        	$out['status']=-3;
	            	$out['msg'] = "Cannot write to file";
	            	$out['html_result']='';
	            	return $out;
	                exit;

		        }
		        fclose($handle);
		    }


    	}catch(SSRSReportException $serviceExcprion)
		{
        	$out['status']=-2;
        	$out['msg'] = $serviceExcprion->GetErrorMessage();
        	$out['html_result']='';
        	return $out;
        	exit;			
 
		}
		 
		
		$out['status']=0;
    	$out['msg'] = 'Success';
    	$out['html_result']=$result_html;
    	return $out;
    }

    function getPageURL()
	{
	    $PageUrl = 'http://';
	    $uri = $_SERVER["REQUEST_URI"];
	    $index = strpos($uri, '?');
	    if($index !== false)
	    {
		$uri = substr($uri, 0, $index);
	    }
	    $PageUrl .= $_SERVER["SERVER_NAME"] .
	                ":" .
	                $_SERVER["SERVER_PORT"] .
	                $uri;
	    return $PageUrl;
	}

	public function select_report(){

		if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
			$sql = 'SELECT * FROM cms_report';
			$s = $this->db->query($sql);	
		}else {
			$sql = 'SELECT
						cms_report.reportid,
						cms_report.report_name,
						cms_report.report_path
					FROM
						map_group_report
					INNER JOIN cms_report ON map_group_report.report_id = cms_report.reportid
					WHERE map_group_report.group_id=? AND map_group_report.p_view=1';
			$s = $this->db->query($sql,array($this->session->userdata('group_id')));
		}

		return $s;
	}

	public function privilege_report($id){
		$sql = 'SELECT
					cms_report.reportid,
					cms_report.report_name,
					cms_report.report_path
				FROM
					map_group_report
				INNER JOIN cms_report ON map_group_report.report_id = cms_report.reportid
				WHERE map_group_report.group_id=? AND map_group_report.p_view=1 AND cms_report.reportid=?';
		$s = $this->db->query($sql,array($this->session->userdata('group_id'),$id));
		return $s;

	}

	public function get_first_report($id){
		if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
			$sql = 'SELECT * FROM cms_report limit 1';
			$s = $this->db->query($sql);	
		}else {
			$sql = 'SELECT
						cms_report.reportid,
						cms_report.report_name,
						cms_report.report_path
					FROM
						map_group_report
					INNER JOIN cms_report ON map_group_report.report_id = cms_report.reportid
					WHERE map_group_report.group_id=? AND map_group_report.p_view=1';
			$s = $this->db->query($sql,array($this->session->userdata('group_id')));
		}
		
		if($s->num_rows()>0){
			foreach ($s->result() as $key) {
				# code...
				$first = $key->reportid;
			}
		}else{
			$first = $id;
		}
		return $first;
	}

	function get_report_profile($id){
		$sql = 'SELECT
				cms_report.reportid,
				cms_report.report_name,
				cms_report.report_path,
				cms_report_parameter.`name`,
				cms_report_parameter.multiselect
				FROM
				cms_report
				LEFT OUTER JOIN cms_report_parameter ON cms_report.reportid = cms_report_parameter.report_id
				WHERE cms_report.reportid=?';
		$s = $this->db->query($sql,array($id));
		$out = array();
		$p=array();
		foreach ($s->result() as $key) {
			$report = $key->report_path;
			$title	= $key->report_name;
			array_push($p, array('name'=>$key->name,'multiselect'=>$key->multiselect));
			# code...
			
		}
		$out = array('report'=>$report,'title'=>$title,'parameters'=>$p);
		return $out;
	}


public function export_report($filename,$report,$ps,$export_to){
    	//echo FCPATH.'imagestemp/';
		//$filename = date("Ymdhis");
    	//exit;
    	$settings = parse_ini_file(APPPATH.'libraries/app.config', 1);
    	try{
    		$rs = new SSRSReport(new Credentials(UID, PWD), SERVICE_URL);
    		if (isset($_REQUEST['rs:Command'])){
    			switch($_REQUEST['rs:Command']){
		            case 'Sort':
		                $rs->Sort2($_REQUEST['rs:SortId'],
		                           $_REQUEST['rs:SortDirection'],
		                           $_REQUEST['rs:ClearSort'],
		                           PageCountModeEnum::$Estimate,
		                           $ReportItem,
		                           $ExecutionInfo);
		                  break;
		            default:
		            	$out['status']=-1;
		            	$out['msg'] = 'Unknown :' . $_REQUEST['rs:Command'];
		            	$out['html_result'];
		            	return $out;
		                exit;
		        }
    		}else {
		        $executionInfo = $rs->LoadReport2($report, NULL);
		        //print_r($executionInfo);
		        $parameters = array();
		        $p = json_decode($ps);
		        //print_r($p);
		        $i=0;
		        foreach ($p as $params) {
		        	# code...
		        	$parameters[$i] = new ParameterValue();
		        	$parameters[$i]->Name = $params->Name;
		        	$parameters[$i]->Value = $params->Value;
		        	$i++;
		        }
		        //print_r($parameters);

		        $rs->SetExecutionParameters2($parameters);
		    }

		    if($export_to=='EXCEL'){
			    $renderAsEXCEL = new RenderAsEXCEL();
			    $result_EXCEL = $rs->Render2($renderAsEXCEL, 
	                              PageCountModeEnum::$Estimate, 
	                              $Extension, 
	                              $MimeType, 
	                              $Encoding, 
	                              $Warnings, 
	                              $StreamIds);

			    $handle = fopen("exportto/".$filename.".xls", 'wb'); 
			    $ext = '.xls';
				fwrite($handle, $result_EXCEL); 

		    }else{
		    	$renderAsPDF = new RenderAsPDF();
		    	//print_r($renderAsPDF);
		    	//die;
		    	//if($page_width != null) $renderAsPDF->PageWidth = $page_width ."in";
		    	//$renderAsPDF->PageWidth = $executionInfo->ReportPageSettings->PaperSize->Width;
		    	//if($page_height != null) $renderAsPDF->PageHeight = $page_height."in";
		    	//$renderAsPDF->PageHeight = $executionInfo->ReportPageSettings->PaperSize->Height;

		    	//if($margin_top != null) $renderAsPDF->MarginTop = $margin_top."in";
		    	//if($margin_right != null) $renderAsPDF->MarginRight = $margin_right."in";
		    	//if($margin_right != null) $renderAsPDF->MarginLeft = $margin_left."in";
		    	//if($margin_bottom != null) $renderAsPDF->MarginBottom = $margin_bottom."in";
			    $result_PDF = $rs->Render2($renderAsPDF, 
	                              PageCountModeEnum::$Estimate, 
	                              $Extension, 
	                              $MimeType, 
	                              $Encoding, 
	                              $Warnings, 
	                              $StreamIds);

			    $handle = fopen("exportto/".$filename.".pdf", 'wb'); 
			    $ext = '.pdf';
				fwrite($handle, $result_PDF);
		    }
			fclose($handle);
    	}catch(SSRSReportException $serviceExcprion)
		{
        	$out['status']=-2;
        	$out['msg'] = $serviceExcprion->GetErrorMessage();
        	$out['html_result']='';
        	return $out;
        	exit;			
 
		}
		 
		
		$out['status']=0;
    	$out['msg'] = 'Success';
    	$out['filepath']=FCPATH.'exportto/'.$filename.$ext;
    	return $out;
    }

    public function get_report_id($path,$id){
    	//echo $path;
    	$sql = 'SELECT * FROM cms_report WHERE report_path = ?';
    	$dt = $this->db->query($sql,array($path));
    	if($dt->num_rows()>0){
    		$row = $dt->row();
    		$id = $row->reportid;
    	}

    	return $id;
    }


    public function get_opco_config_table($opco){
        // select opco
        if($opco == -1){

            if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
                $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame  ASC limit 1 ';
                $query = $this->db->query($sql);
                if($query->num_rows()>0){
                     $row = $query->row(1);
                     $opco = $row->opcoid;
                }
            }else{
                $sql = 'SELECT
                            cms_opco_list.opconame,
                            cms_opco_list.opcoid,
                            map_group_opco.group_id
                        FROM
                            map_group_opco
                        INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                        WHERE map_group_opco.group_id=? AND map_group_opco.view_opco_config=1 ORDER BY cms_opco_list.opconame  ASC limit 1';
                $query = $this->db->query($sql,array($this->session->userdata('group_id')));
                if($query->num_rows()>0){
                     $row = $query->row(1);
                     $opco = $row->opcoid;
                }
            }

        }
        if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
            $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame';
            $data['select_opco'] = $this->db->query($sql);
        }else{
            $sql = 'SELECT
                            cms_opco_list.opconame,
                            cms_opco_list.opcoid,
                            map_group_opco.group_id
                        FROM
                            map_group_opco
                        INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                        WHERE map_group_opco.group_id=?  AND view_opco_config ORDER BY cms_opco_list.opconame';
            $data['select_opco'] = $this->db->query($sql,array($this->session->userdata('group_id')));
        }
        $data['opco_id'] = $opco;

        $sql = "SELECT
                cms_opco_config.config_date,
                    DATE_FORMAT(cms_opco_config.config_date,'%m') AS `Month`,
                    YEAR(cms_opco_config.config_date) AS `Year`,
                    cms_opco_config.opcoid
                FROM
                    cms_opco_config
                WHERE opcoid=?
                GROUP BY
                    cms_opco_config.config_date
                ORDER BY
                    cms_opco_config.config_date DESC";
        $data['data'] = $this->db->query($sql,array($opco));
        //echo $this->db->last_query();
        //die;
        $data['current_month'] = date('m');
        $data['current_year'] = date('Y');

        return $this->load->view('incident/opco_config_table_content',$data,TRUE);

    }

    public function opco_config($opco_id,$month,$year,$mode){
        if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
            $sql = 'SELECT * FROM cms_opco_list';
            $opco_list = $this->db->query($sql);
        }else{
            $sql=   'SELECT
                        cms_opco_list.opcoid,
                        cms_opco_list.opconame,
                        map_group_opco.add_opco_config
                        FROM
                        map_group_opco
                    INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                    WHERE map_group_opco.group_id=? AND map_group_opco.add_opco_config=1';
            $opco_list = $this->db->query($sql,array($this->session->userdata('group_id')));
        }
        
        // gate rate information from cms_currency_exhange
        $sql = 'SELECT
                    cms_opco_list.opcoid,
                    cms_opco_list.opconame,
                    cms_opco_list.currency_id,
                    cms_currency_exchange.`year`,
                    cms_currency_exchange.`month`,
                    cms_currency_exchange.rate,
                    map_vipgroup_currency.currencysymbol
                FROM
                    cms_opco_list
                LEFT OUTER JOIN cms_currency_exchange ON cms_opco_list.currency_id = cms_currency_exchange.currency_id
                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                WHERE cms_opco_list.opcoid=? AND cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? ';

        $dt_rate = $this->db->query($sql,array($opco_id,$year,$month));
        //echo $this->db->last_query();
        //die;
        if($dt_rate->num_rows()>0){
            $row = $dt_rate->row();
            $config['rate'] = $row->rate;
        }else{
            $config['rate'] = 0;
        }
        //echo $this->db->last_query();
        //echo $config['rate'];
        //die;
        //-----

        // get Default 

        $sql = 'SELECT
                cms_opco_list.opcoid,
                cms_opco_list.opconame,
                cms_opco_list.currency_id,
                map_vipgroup_currency.currencysymbol
                FROM
                cms_opco_list
                INNER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                 WHERE opcoid=?';
        $dt_opco = $this->db->query($sql,array($opco_id));

        $d = $dt_opco->row();
        $config['symbol'] = $d->currencysymbol;

        
        $select_opco = '<select class="std-select" name="id-opco" id="id-opco" disabled>';
        $select_opco2 = '<select class="std-select width250" name="new-id-opco" id="new-id-opco">';
        foreach ($opco_list->result() as $row) {
            if($row->opcoid == $opco_id) $opco_selected = 'selected'; else $opco_selected ='';
            $select_opco .= '<option value="'.$row->opcoid.'" '.$opco_selected.'>'.$row->opconame.'</option>';
            $select_opco2 .= '<option value="'.$row->opcoid.'" '.$opco_selected.'>'.$row->opconame.'</option>';
        }

        $select_opco .= '</select>';
        $select_opco2 .= '</select>';
        $config['select_opco'] = $select_opco;
        $config['select_opco2'] = $select_opco2;

        $array_month = array();
        $array_month[0] = array('month_name'=>$this->lang->line('January'),'month' => '01');
        $array_month[1] = array('month_name'=>$this->lang->line('February'),'month' => '02');
        $array_month[2] = array('month_name'=>$this->lang->line('March'),'month' => '03');
        $array_month[3] = array('month_name'=>$this->lang->line('April'),'month' => '04');
        $array_month[4] = array('month_name'=>$this->lang->line('May'),'month' => '05');
        $array_month[5] = array('month_name'=>$this->lang->line('June'),'month' => '06');
        $array_month[6] = array('month_name'=>$this->lang->line('July'),'month' => '07');
        $array_month[7] = array('month_name'=>$this->lang->line('August'),'month' => '08');
        $array_month[8] = array('month_name'=>$this->lang->line('September'),'month' => '09');
        $array_month[9] = array('month_name'=>$this->lang->line('October'),'month' => '10');
        $array_month[10] = array('month_name'=>$this->lang->line('November'),'month' => '11');
        $array_month[11] = array('month_name'=>$this->lang->line('December'),'month' => '12');

        $select_month ='<select class="std-select" name="month" id="month" disabled>';
        $select_month2 ='<select class="std-select" name="month" id="new-month">';
        foreach ($array_month as $row) {
            if($row['month']==$month) {
                $month_selected = 'selected'; 
            }else {
                $month_selected='';
            }
            $select_month .= '<option value="'.$row['month'].'" '.$month_selected.'>'.$row['month_name'].'</option>';
            $select_month2 .= '<option value="'.$row['month'].'" '.$month_selected.'>'.$row['month_name'].'</option>'; 
        }
        $select_month .= '</select>';
        $select_month2 .= '</select>';


        $config['select_month']= $select_month;
        $config['select_month2']= $select_month2;

        $array_year     = array('2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026');
        $select_year    = '<select class="std-select" name="year" id="year" disabled>';
        $select_year2   = '<select class="std-select" name="year" id="new-year">';
        
        foreach ($array_year as $row) {
            # code...
            if($row==$year) $year_selected='selected'; else $year_selected='';
            $select_year .= '<option value='.$row.' '.$year_selected.'>'.$row.'</option>';
            $select_year2 .= '<option value='.$row.' '.$year_selected.'>'.$row.'</option>';
        }
        $select_year .= '</select>';
        $select_year2 .= '</select>';

        $config['select_year'] = $select_year;
        $config['select_year2'] = $select_year2;


        $select_date = $year.'-'.$month.'-01 00:00:00';


        $sql = 'SELECT * FROM cms_opco_config
                WHERE opcoid=? AND config_date=?';
        $dt_opco_config = $this->db->query($sql,array($opco_id,$select_date));

        
        if($dt_opco_config->num_rows()>0){
            $config['message'] = 0;
            $opco_config = $this->get_opco_config($opco_id,$month,$year);

        }else{
            $config['message'] = 1;
            $opco_config = $this->default_config();
        }

        $sql = 'SELECT * FROM map_vipgroup_currency';
        $dt = $this->db->query($sql);
        $config['currency'] = $dt;

        $config['config'] = $opco_config;
        $config['mode']=$mode;
        
        
        $config['current_month'] = date("m");
        if(strlen($config['current_month'])==1) $config['current_month'] = '0'.$config['current_month'];
        $config['current_year'] = date("Y");
        $config['opco_id'] = $opco_id;
        $sql = 'SELECT * FROM cms_opco_list WHERE opcoid=?';
        $dt = $this->db->query($sql,array($opco_id));
        if($dt->num_rows()>0){
            $ret = $dt->row();
            $config['opco_name'] = $ret->opconame;
        }else{
            $config['opco_name'] = '';
        }
        $config['month'] = $month;
        $config['year'] = $year;


        $output = $this->load->view('incident/opco_config_detail',$config,TRUE);
        return $output;
            
    }

    public function get_opco_config($opco_id,$month,$year){
        /*
        $opco_config[0]= array('id' => '1', 'variable' => 'general_numbers_of_subscribers_prepaid','desc' => 'Numbers of Subscribers Postpaid', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        */

        $date_config = $year.'-'.$month.'-01 00:00:00';
        $sql = 'SELECT * FROM cms_opco_config WHERE opcoid=? AND config_date=?';
        $data = $this->db->query($sql,array($opco_id,$date_config));
        $opco_config = $this->default_config();
        
        foreach ($data->result() as $row) {
            $key = array_search($row->config_key, array_column($opco_config, 'variable'));
            if($key >= 0) {
                //echo $row->config_value;
                $opco_config[$key]['value']=$row->config_value;
                $opco_config[$key]['id']=$row->opcoid;
            }
        }
        return $opco_config;
    }

    public function default_config(){
        $opco_config[0]= array('id' => '', 'variable' => 'general_numbers_of_subscribers_prepaid','desc' => 'Numbers of Subscribers', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[1]= array('id' => '', 'variable' => 'general_numbers_of_subscribers_postpaid','desc' => 'Numbers of Subscribers', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[2]= array('id' => '', 'variable' => 'general_gross_adds_prepaid','desc' => 'Gross-Adds', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[3]= array('id' => '', 'variable' => 'general_gross_adds_postpaid','desc' => 'Gross-Adds', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[4]= array('id' => '', 'variable' => 'general_no_of_mo_voice_minutes_prepaid','desc' => 'No. of MO Voice Minutes', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[5]= array('id' => '', 'variable' => 'general_no_of_mo_voice_minutes_postpaid','desc' => 'No. of MO Voice Minutes', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[6]= array('id' => '', 'variable' => 'general_inbound_roaming_revenue','desc' => 'Inbound Roaming Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[7]= array('id' => '', 'variable' => 'general_international_interconnect_revenue','desc' => 'International Interconnect Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[8]= array('id' => '', 'variable' => 'general_total_mt_international_minutes','desc' => 'Total MT International minutes', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[9]= array('id' => '', 'variable' => 'general_international_interconnect_cost','desc' => 'International Interconnect Cost', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[10]= array('id' => '', 'variable' => 'general_total_mo_international_minutes','desc' => 'Total MO International minutes', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[11]= array('id' => '', 'variable' => 'general_total_bu_revenue','desc' => 'Total BU Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[12]= array('id' => '', 'variable' => 'general_in_system','desc' => 'IN system', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[13]= array('id' => '', 'variable' => 'general_gl_accounting_system','desc' => 'GL Accounting System', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[14]= array('id' => '', 'variable' => 'general_third_parties_provision','desc' => 'Third parties', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[15]= array('id' => '', 'variable' => 'general_third_parties_bad_debts','desc' => 'Third parties', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[16]= array('id' => '', 'variable' => 'general_postpaid_defaulters_provision','desc' => 'Postpaid defaulters', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[17]= array('id' => '', 'variable' => 'general_postpaid_defaulters_bad_debts','desc' => 'Postpaid defaulters', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[18]= array('id' => '', 'variable' => 'general_prepaid_customers_provision','desc' => 'Prepaid Customers', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[19]= array('id' => '', 'variable' => 'general_prepaid_customers_bad_debts','desc' => 'Prepaid Customers', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[20]= array('id' => '', 'variable' => 'general_local_interconnect_partners_provision','desc' => 'Local Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[21]= array('id' => '', 'variable' => 'general_local_interconnect_partners_bad_debts','desc' => 'Local Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[22]= array('id' => '', 'variable' => 'general_international_interconnect_partners_provision','desc' => 'International Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[23]= array('id' => '', 'variable' => 'general_international_interconnect_partners_bad_debts','desc' => 'International Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[24]= array('id' => '', 'variable' => 'general_roaming_partners_provision','desc' => 'Roaming Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[25]= array('id' => '', 'variable' => 'general_roaming_partners_bad_debts','desc' => 'Roaming Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[26]= array('id' => '', 'variable' => 'general_mfs_provision','desc' => 'MFS', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[27]= array('id' => '', 'variable' => 'general_mfs_bad_debts','desc' => 'MFS', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');


        //group bypass
        $opco_config[28]= array('id' => '', 'variable' => 'bypass_avg_disconnecting_duration','desc' => 'Avg. Disconnecting Duration (Minutes)', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[29]= array('id' => '', 'variable' => 'bypass_on_net_terminated_minutes','desc' => 'On-net Terminated minutes', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[30]= array('id' => '', 'variable' => 'bypass_off_net_terminated_minutes','desc' => 'Off-net Terminated minutes', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[31]= array('id' => '', 'variable' => 'bypass_on_net_suspended_sims','desc' => 'On-net Suspended SIMs', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[32]= array('id' => '', 'variable' => 'bypass_off_net_suspended_sims','desc' => 'Off-net Suspended SIMs', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[33]= array('id' => '', 'variable' => 'bypass_leakage_per_sim','desc' => 'Leakage Per Sim', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[34]= array('id' => '', 'variable' => 'bypass_leakage','desc' => 'Leakage', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[35]= array('id' => '', 'variable' => 'bypass_recovery','desc' => 'Recovery', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[36]= array('id' => '', 'variable' => 'bypass_prevented','desc' => 'Prevented', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[37]= array('id' => '', 'variable' => 'bypass_detection_cost','desc' => 'Detection Cost', 'value_desc' => '', 'value' => '', 'group' => 'bypass');

        //group MFS
        $opco_config[38]= array('id' => '', 'variable' => 'mfs_payments_no_of_fraud_incidents','desc' => 'Payments', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[39]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_customers','desc' => 'Payments', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[40]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_bu','desc' => 'Payments', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        
        $opco_config[41]= array('id' => '', 'variable' => 'mfs_transfers_no_of_fraud_incidents','desc' => 'Transfers', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[42]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_customers','desc' => 'Transfers', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[43]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_bu','desc' => 'Transfers', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[44]= array('id' => '', 'variable' => 'mfs_accounts_no_of_fraud_incidents','desc' => 'Accounts', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[45]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_customers','desc' => 'Accounts', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[46]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_bu','desc' => 'Accounts', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[47]= array('id' => '', 'variable' => 'mfs_loans_no_of_fraud_incidents','desc' => 'Loans', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[48]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_customers','desc' => 'Loans', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[49]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_bu','desc' => 'Loans', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[50]= array('id' => '', 'variable' => 'mfs_insurance_no_of_fraud_incidents','desc' => 'Insurance', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[51]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_customers','desc' => 'Insurance', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[52]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_bu','desc' => 'Insurance', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[53]= array('id' => '', 'variable' => 'mfs_merchant_services_no_of_fraud_incidents','desc' => 'Merchant Services', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[54]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_customers','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[55]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_bu','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        //group currency
        $opco_config[56]= array('id' => '', 'variable' => 'currency_currency_symbol','desc' => 'Currency Symbol', 'value_desc' => '', 'value' => '', 'group' => 'currency');
        $opco_config[57]= array('id' => '', 'variable' => 'currency_rate','desc' => 'Rate', 'value_desc' => '', 'value' => '', 'group' => 'currency');

        //EXTEND
        $opco_config[58]= array('id' => '', 'variable' => 'general_over_charging','desc' => 'Over Charging', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[59]= array('id' => '', 'variable' => 'general_deferred_balance','desc' => 'Deferred Balance', 'value_desc' => '', 'value' => '', 'group' => 'general');

        //group Control Coverage
        $opco_config[60]= array('id' => '', 'variable' => 'control_coverage_fm','desc' => 'FM', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[61]= array('id' => '', 'variable' => 'control_coverage_ra','desc' => 'RA', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[62]= array('id' => '', 'variable' => 'control_coverage_mfs','desc' => 'MFS', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[63]= array('id' => '', 'variable' => 'control_coverage_bypass','desc' => 'ByPass', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');

        // USD Mirror
        $opco_config[64]= array('id' => '', 'variable' => 'general_inbound_roaming_revenue_lc','desc' => 'Inbound Roaming Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        
        $opco_config[65]= array('id' => '', 'variable' => 'general_international_interconnect_revenue_lc','desc' => 'International Interconnect Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[66]= array('id' => '', 'variable' => 'general_international_interconnect_cost_lc','desc' => 'International Interconnect Cost', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[67]= array('id' => '', 'variable' => 'general_total_bu_revenue_lc','desc' => 'Total BU Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[68]= array('id' => '', 'variable' => 'general_in_system_lc','desc' => 'IN system', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[69]= array('id' => '', 'variable' => 'general_gl_accounting_system_lc','desc' => 'GL Accounting System', 'value_desc' => '', 'value' => '', 'group' => 'general');
                
        $opco_config[70]= array('id' => '', 'variable' => 'general_over_charging_lc','desc' => 'Over Charging', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[71]= array('id' => '', 'variable' => 'general_deferred_balance_lc','desc' => 'Deferred Balance', 'value_desc' => '', 'value' => '', 'group' => 'general');
        
        $opco_config[72]= array('id' => '', 'variable' => 'general_third_parties_provision_lc','desc' => 'Third parties', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[73]= array('id' => '', 'variable' => 'general_third_parties_bad_debts_lc','desc' => 'Third parties', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');

        $opco_config[74]= array('id' => '', 'variable' => 'general_postpaid_defaulters_provision_lc','desc' => 'Postpaid defaulters', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[75]= array('id' => '', 'variable' => 'general_postpaid_defaulters_bad_debts_lc','desc' => 'Postpaid defaulters', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[76]= array('id' => '', 'variable' => 'general_prepaid_customers_provision_lc','desc' => 'Prepaid Customers', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[77]= array('id' => '', 'variable' => 'general_prepaid_customers_bad_debts_lc','desc' => 'Prepaid Customers', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[78]= array('id' => '', 'variable' => 'general_local_interconnect_partners_provision_lc','desc' => 'Local Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[79]= array('id' => '', 'variable' => 'general_local_interconnect_partners_bad_debts_lc','desc' => 'Local Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[80]= array('id' => '', 'variable' => 'general_international_interconnect_partners_provision_lc','desc' => 'International Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[81]= array('id' => '', 'variable' => 'general_international_interconnect_partners_bad_debts_lc','desc' => 'International Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[82]= array('id' => '', 'variable' => 'general_roaming_partners_provision_lc','desc' => 'Roaming Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[83]= array('id' => '', 'variable' => 'general_roaming_partners_bad_debts_lc','desc' => 'Roaming Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[84]= array('id' => '', 'variable' => 'general_mfs_provision_lc','desc' => 'MFS', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[85]= array('id' => '', 'variable' => 'general_mfs_bad_debts_lc','desc' => 'MFS', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');



        
        $opco_config[86]= array('id' => '', 'variable' => 'bypass_leakage_per_sim_lc','desc' => 'Leakage Per Sim', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[87]= array('id' => '', 'variable' => 'bypass_leakage_lc','desc' => 'Leakage', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[88]= array('id' => '', 'variable' => 'bypass_recovery_lc','desc' => 'Recovery', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[89]= array('id' => '', 'variable' => 'bypass_prevented_lc','desc' => 'Prevented', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[90]= array('id' => '', 'variable' => 'bypass_detection_cost_lc','desc' => 'Detection Cost', 'value_desc' => '', 'value' => '', 'group' => 'bypass');


        
        $opco_config[91]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_customers_lc','desc' => 'Payments', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        
        $opco_config[92]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_bu_lc','desc' => 'Payments', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[93]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_customers_lc','desc' => 'Transfers', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[94]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_bu_lc','desc' => 'Transfers', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[95]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_customers_lc','desc' => 'Accounts', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[96]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_bu_lc','desc_lc' => 'Accounts', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[97]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_customers_lc','desc' => 'Loans', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[98]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_bu_lc','desc' => 'Loans', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[99]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_customers_lc','desc' => 'Insurance', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[100]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_bu_lc','desc' => 'Insurance', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[101]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_customers_lc','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[102]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_bu_lc','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');     








        return $opco_config;

    }    

    public function get_rate_form($year,$month){
            $sql = 'SELECT * FROM cms_currency_exchange WHERE `year`=? AND `month`=?';
            $dt = $this->db->query($sql,array($year,$month));
        return $dt;
    }


    public function check_opco_config($opcoid,$month,$year){
        $date_data = $year.'-'.$month.'-01 00:00:00';
        
        $sql = 'SELECT * FROM cms_opco_config WHERE config_date=? AND opcoid=?';
        $data = $this->db->query($sql,array($date_data,$opcoid));
        
        if($data->num_rows()>0){
            return '1';
        }else{
            return '0';
        }
    }

    public function add_opco_config($json,$id_user){
        $json_obj = json_decode($json);
        $out = ''; 
        $date_check = $json_obj[0]->config_date;
        $opcoid_check = $json_obj[0]->opcoid;
        $sql = 'SELECT * FROM cms_opco_config WHERE config_date=? AND opcoid=?';
        $data = $this->db->query($sql,array($date_check,$opcoid_check));

        if($data->num_rows()>0){
            return '1';
        }else{
            //prosess 
            foreach ($json_obj as $row) {
                # code...
                $sql = 'INSERT INTO cms_opco_config(config_date,opcoid,config_key,config_value) VALUES (?,?,?,?)';
                $this->db->query($sql,array($row->config_date,$row->opcoid,$row->variable,$row->value));

            }

            $desc = 'Add OpCo Configuration id# '.$opcoid_check;
            $activity = 'Add OpCo Configuration';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }

    }

    public function update_opco_config($json,$id_user){
        $this->db->db_debug = true;
        $json_obj = json_decode($json);
        $out = ''; 
        $date_check = $json_obj[0]->config_date;
        $opcoid_check = $json_obj[0]->opcoid;
        //prosess 
        foreach ($json_obj as $row) {
            # code...
            //$sql2 = 'UPDATE cms_opco_config SET config_value="'.$row->value.'" WHERE opcoid="'.$row->opcoid.'" AND config_date="'.$row->config_date.'" AND config_key="'.$row->variable.'"';
            //echo $sql2.'<br>';

            // search
            $sql = 'SELECT * FROM cms_opco_config WHERE opcoid=? AND config_date=? AND config_key=?';
            $s = $this->db->query($sql,array($row->opcoid,$row->config_date,$row->variable)); 
            if($s->num_rows()>0){
                $sql = 'UPDATE cms_opco_config SET config_value=? WHERE opcoid=? AND config_date=? AND config_key=?';
                $this->db->query($sql,array($row->value,$row->opcoid,$row->config_date,$row->variable));
            }else{
                $sql = 'INSERT INTO cms_opco_config(config_date,opcoid,config_key,config_value) VALUES (?,?,?,?)';
                $this->db->query($sql,array($row->config_date,$row->opcoid,$row->variable,$row->value));
            }

        }

        $desc = 'Update OpCo Configuration id# '.$opcoid_check;
        $activity = 'Update OpCo Configuration';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
        return '0'; 
        

    }

    public function get_rate($month,$year,$opcoid){
        $sql = 'SELECT
                cms_opco_list.opcoid,
                cms_opco_list.opconame,
                cms_opco_list.currency_id,
                cms_currency_exchange.rate,
                cms_currency_exchange.`year`,
                cms_currency_exchange.`month`,
                map_vipgroup_currency.currencysymbol
                FROM
                cms_opco_list
                LEFT OUTER JOIN cms_currency_exchange ON cms_currency_exchange.currency_id = cms_opco_list.currency_id
                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                WHERE cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? AND cms_opco_list.opcoid=?';
        $dt = $this->db->query($sql,array($year,$month,$opcoid));
        //echo $this->db->last_query();
        
        if($dt->num_rows()>0){
            $row= $dt->row();
            $out = array($row->rate,$row->currencysymbol);
        }else {
            $out = array(0,'');
        }
        return $out;
    }    

}