<?php 
class Madmin extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

    public function getuser($page=1,$num,$s='%'){

    	$row_count = $num;
    	$offset = ($page - 1)*$num ;
		
    	
    	$sql = 'SELECT
					cms_user.id,
					cms_user.user_name,
					cms_user.user_status,
					cms_user.user_group,
					cms_user.email,
					cms_user.first_name,
					cms_user.last_name,
					cms_user.date_created,
					cms_user.created_by,
					cms_user.date_updated,
					cms_user.updated_by,
					cms_user.last_login,
					cms_group.group_name
				FROM
					cms_user
				INNER JOIN cms_group ON cms_user.user_group = cms_group.id
				WHERE cms_user.user_name LIKE ? || cms_group.group_name LIKE ? || concat(trim(cms_user.first_name)," ",trim(cms_user.last_name)) LIKE ?
				ORDER BY cms_user.id
				LIMIT ?,?';
		return  $this->db->query($sql,array($s,$s,$s,$offset,$row_count));
    }

    public function get_user_detail($id){
        $sql = 'SELECT
                    cms_user.id,
                    cms_user.user_name,
                    cms_user.user_status,
                    cms_user.user_group,
                    cms_user.email,
                    cms_user.first_name,
                    cms_user.last_name,
                    cms_user.date_created,
                    cms_user.created_by,
                    cms_user.date_updated,
                    cms_user.updated_by,
                    cms_user.last_login,
                    cms_group.group_name
                FROM
                    cms_user
                INNER JOIN cms_group ON cms_user.user_group = cms_group.id
                WHERE cms_user.id =?';
        $dt = $this->db->query($sql,array($id));
        foreach ($dt->result() as $row) {
            # code...
            $data['id']             = $row->id;
            $data['user_name']      = $row->user_name;
            $data['user_status']    = $row->user_status;
            if($row->user_status==1){
                 $data['status']    = 'Enable';
            }else{
                 $data['status']    = 'Disable';    
            }
            $data['group_name']     = $row->group_name;
            $data['email']          = $row->email;
            $data['first_name']     = $row->first_name;
            $data['last_name']      = $row->last_name;
            $data['date_created']   = $row->date_created;
            $data['last_login']     = $row->last_login;
            $data['user_group']     = $row->user_group;
        }

        $sql = 'SELECT * FROM cms_group';
        $ds = $this->db->query($sql);
        $select_group = '<select class="std-input" id="grp" name="grp" class="">';
        foreach ($ds->result() as $r) {
            # code...
            if($data['user_group']==$r->id){
                $selected = 'selected';
            }else{
                $selected = '';
            }
            $select_group .= '<option value="'.$r->id.'" '.$selected.'>'.$r->group_name.'</option>';
        }
        $select_group .= '</select>';
        $data['select_group'] = $select_group;

        $status  = '<select  class="std-input"  id="status" name="status">';
        if($data['user_status']==1){
            $selected = "selected";
        }else{
            $selected = "";
        }
        $status .= '    <option value=1 '.$selected.'>'.$this->lang->line('enable').'</option>';
        if($data['user_status']==0){
            $selected = "selected";
        }else{
            $selected = "";
        }        
        $status .= '    <option value=0 '.$selected.'>'.$this->lang->line('disable').'</option>';
        $status .= '</select>';

        $data['status_radio'] = $status;
        return $this->load->view('template/user_detail',$data,TRUE);
    }

    public function update_lastlogin($username){
    	$sql = 'UPDATE cms_user SET last_login=NOW() WHERE user_name=?';
    	$this->db->query($sql,array($username));
    }

    public function pages_user($s='%'){
    	// $sql = 'SELECT COUNT(id) AS co FROM cms_user WHERE cms_user.user_name LIKE ?';
        $sql = 'SELECT
                    COUNT(cms_user.id) AS co
                FROM
                    cms_user
                INNER JOIN cms_group ON cms_user.user_group = cms_group.id
                WHERE cms_user.user_name LIKE ? || cms_group.group_name LIKE ? || concat(trim(cms_user.first_name)," ",trim(cms_user.last_name)) LIKE ?';


    	$dt = $this->db->query($sql,array($s,$s,$s));
    	foreach ($dt->result() as $row) {
    		# code...
    		$total = $row->co;
    	}

    	return ceil($total/ROW_PER_PAGE);
    }

    public function user_total_row($s='%'){
        $sql = 'SELECT
                    COUNT(cms_user.id) AS co
                FROM
                    cms_user
                INNER JOIN cms_group ON cms_user.user_group = cms_group.id
                WHERE cms_user.user_name LIKE ? || cms_group.group_name LIKE ? || concat(trim(cms_user.first_name)," ",trim(cms_user.last_name)) LIKE ?';


        $dt = $this->db->query($sql,array($s,$s,$s));
        $row = $dt->row();
        return $row->co;
    }

    public function getgroup($page,$num,$s='%'){
    	$row_count = $num;
    	$offset = ($page - 1)*$num ;

    	$sql = 	'SELECT
                    cms_group.id,
                    cms_group.group_name,
                    cms_group.group_status,
                    cms_group.group_type,
                    cms_group.authen_type,
                    cms_group.date_created,
                    cms_group.created_by,
                    cms_group.date_update,
                    cms_group.updated_by,
                    u1.user_name AS user_created,
                    u2.user_name AS user_updated
                FROM
                    cms_group
                LEFT OUTER JOIN cms_user AS u1 ON cms_group.created_by = u1.id
                LEFT OUTER JOIN cms_user AS u2 ON cms_group.updated_by = u2.id
 
                WHERE cms_group.group_status LIKE ?
    			 LIMIT ?,?';

    	$r = $this->db->query($sql,array($s,$offset,$row_count));
    	//print_r($r);
    	//die;
    	return $r;

    }

    public function pages_group($s='%'){
    	$sql = 'SELECT COUNT(id) AS co FROM cms_group WHERE group_status LIKE ?';
    	$dt = $this->db->query($sql,array($s));
    	foreach ($dt->result() as $row) {
    		# code...
    		$total = $row->co;
    	}

    	return ceil($total/ROW_PER_PAGE);
    }

    public function get_ui_menu($active=1){
        $country = array (
            'ALL' => 'Albania Lek',
            'DZD' => 'Algeria Dinar',
            'AFN' => 'Afghanistan Afghani',
            'ARS' => 'Argentina Peso',
            'AMD' => 'Armenian Dram',
            'AWG' => 'Aruba Guilder',
            'AUD' => 'Australia Dollar',
            'AZN' => 'Azerbaijan New Manat',
            'BSD' => 'Bahamas Dollar',
            'BBD' => 'Barbados Dollar',
            'BDT' => 'Bangladeshi taka',
            'BYR' => 'Belarus Ruble',
            'BZD' => 'Belize Dollar',
            'BMD' => 'Bermuda Dollar',
            'BOB' => 'Bolivia Boliviano',
            'BAM' => 'Bosnia and Herzegovina Convertible Marka',
            'BWP' => 'Botswana Pula',
            'BGN' => 'Bulgaria Lev',
            'BRL' => 'Brazil Real',
            'BND' => 'Brunei Darussalam Dollar',
            'KHR' => 'Cambodia Riel',
            'CAD' => 'Canada Dollar',
            'KYD' => 'Cayman Islands Dollar',
            'CLP' => 'Chile Peso',
            'CNY' => 'China Yuan Renminbi',
            'COP' => 'Colombia Peso',
            'CRC' => 'Costa Rica Colon',
            'HRK' => 'Croatia Kuna',
            'CUP' => 'Cuba Peso',
            'CZK' => 'Czech Republic Koruna',
            'DKK' => 'Denmark Krone',
            'DOP' => 'Dominican Republic Peso',
            'XCD' => 'East Caribbean Dollar',
            'EGP' => 'Egypt Pound',
            'SVC' => 'El Salvador Colon',
            'EEK' => 'Estonia Kroon',
            'EUR' => 'Euro Member Countries',
            'FKP' => 'Falkland Islands (Malvinas) Pound',
            'FJD' => 'Fiji Dollar',
            'GEL' => 'Georgian Lari',
            'GHC' => 'Ghana Cedis',
            'GIP' => 'Gibraltar Pound',
            'GTQ' => 'Guatemala Quetzal',
            'GGP' => 'Guernsey Pound',
            'GYD' => 'Guyana Dollar',
            'HNL' => 'Honduras Lempira',
            'HKD' => 'Hong Kong Dollar',
            'HUF' => 'Hungary Forint',
            'ISK' => 'Iceland Krona',
            'INR' => 'India Rupee',
            'IDR' => 'Indonesia Rupiah',
            'IRR' => 'Iran Rial',
            'IMP' => 'Isle of Man Pound',
            'ILS' => 'Israel Shekel',
            'JMD' => 'Jamaica Dollar',
            'JPY' => 'Japan Yen',
            'JEP' => 'Jersey Pound',
            'KZT' => 'Kazakhstan Tenge',
            'KPW' => 'Korea (North) Won',
            'KRW' => 'Korea (South) Won',
            'KGS' => 'Kyrgyzstan Som',
            'LAK' => 'Laos Kip',
            'LVL' => 'Latvia Lat',
            'LBP' => 'Lebanon Pound',
            'LRD' => 'Liberia Dollar',
            'LTL' => 'Lithuania Litas',
            'MKD' => 'Macedonia Denar',
            'MYR' => 'Malaysia Ringgit',
            'MUR' => 'Mauritius Rupee',
            'MXN' => 'Mexico Peso',
            'MNT' => 'Mongolia Tughrik',
            'MZN' => 'Mozambique Metical',
            'NAD' => 'Namibia Dollar',
            'NPR' => 'Nepal Rupee',
            'ANG' => 'Netherlands Antilles Guilder',
            'NZD' => 'New Zealand Dollar',
            'NIO' => 'Nicaragua Cordoba',
            'NGN' => 'Nigeria Naira',
            'NOK' => 'Norway Krone',
            'OMR' => 'Oman Rial',
            'PKR' => 'Pakistan Rupee',
            'PAB' => 'Panama Balboa',
            'PYG' => 'Paraguay Guarani',
            'PEN' => 'Peru Nuevo Sol',
            'PHP' => 'Philippines Peso',
            'PLN' => 'Poland Zloty',
            'QAR' => 'Qatar Riyal',
            'RON' => 'Romania New Leu',
            'RUB' => 'Russia Ruble',
            'SHP' => 'Saint Helena Pound',
            'SAR' => 'Saudi Arabia Riyal',
            'RSD' => 'Serbia Dinar',
            'SCR' => 'Seychelles Rupee',
            'SGD' => 'Singapore Dollar',
            'SBD' => 'Solomon Islands Dollar',
            'SOS' => 'Somalia Shilling',
            'ZAR' => 'South Africa Rand',
            'LKR' => 'Sri Lanka Rupee',
            'SEK' => 'Sweden Krona',
            'CHF' => 'Switzerland Franc',
            'SRD' => 'Suriname Dollar',
            'SYP' => 'Syria Pound',
            'TWD' => 'Taiwan New Dollar',
            'TJS' => 'Tajikistani Somoni',
            'THB' => 'Thailand Baht',
            'TTD' => 'Trinidad and Tobago Dollar',
            'TRY' => 'Turkey Lira',
            'TRL' => 'Turkey Lira',
            'TVD' => 'Tuvalu Dollar',
            'UAH' => 'Ukraine Hryvna',
            'GBP' => 'United Kingdom Pound',
            'UGX' => 'Uganda Shilling',
            'USD' => 'United States Dollar',
            'UYU' => 'Uruguay Peso',
            'UZS' => 'Uzbekistan Som',
            'VEF' => 'Venezuela Bolivar',
            'VND' => 'Viet Nam Dong',
            'YER' => 'Yemen Rial',
            'ZWD' => 'Zimbabwe Dollar'
        );
        $acc_active['country'] = $country;

        $sql = 'SELECT * FROM map_vipgroup_currency';
        $currency = $this->db->query($sql);
        $configuration['group_currency'] = $currency;

        $sql = 'SELECT * FROM cms_detection_tool';
        $detection_tool = $this->db->query($sql);
        $configuration['detection_tool'] = $detection_tool;

        $sql = 'SELECT
                    cms_opco_list.opcoid,
                    cms_opco_list.opconame,
                    cms_opco_list.currency_id,
                    map_vipgroup_currency.currencyname,
                    map_vipgroup_currency.currencysymbol
                FROM
                    cms_opco_list
                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id ORDER BY cms_opco_list.opconame ASC';
        $opco_list = $this->db->query($sql);
        $configuration['opco_list'] = $opco_list;

        //$sql = 'SELECT ccid,ccname, DATE_FORMAT(start_date,"%Y-%m-%d") AS start_date, DATE_FORMAT(end_date,"%Y-%m-%d") AS end_date FROM cms_control_coverage';
        //$control_coverage = $this->db->query($sql);
        //$configuration['control_coverage'] = $control_coverage;

        $sql = 'SELECT * FROM cms_revenue_stream ORDER BY id ASC';
        $configuration['revenue_stream'] = $this->db->query($sql);

        $output = $this->load->view('template/ui_management_content',$configuration,TRUE);
        $acc_active['acc_active'] = $active;
        $output .= '</div>';
        $output .= $this->load->view('template/ui_message',$acc_active,TRUE);
        return $output;
    }

    public function get_user_trail($default){
        $sql = 'SELECT * FROM cms_user';
        $dt = $this->db->query($sql);
        if($default==''){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $select =   '<select class="filter_item" id="username">';
        $select .=  '   <option value="" '.$selected.'></option>';
        if($dt->num_rows()>0){
            foreach ($dt->result() as $row) {
                # code...
                if($default==$row->user_name){
                    $selected='selected';
                }else{
                    $selected='';
                }
                $select .= '    <option '.$selected.'>'.$row->user_name.'</option>';
            }
        }
        $select .= '</select>';

        return $select;
    }

    public function get_module($default){
        $module     = '<select class="filter_item" id="module">';
        if($default=='') {
            $selected = 'selected';
        } else{
            $selected = '';
        }
        $module    .= '     <option '.$selected.'></option>';

        if($default==1) {
            $selected = 'selected';
        } else{
            $selected = '';
        }
        $module    .= '     <option '.$selected.' value=1>'.$this->lang->line('user_admin').'</option>';
        if($default==2) {
            $selected = 'selected';
        } else{
            $selected = '';
        }
        $module    .= '     <option '.$selected.' value=2>'.$this->lang->line('incident').'</option>';
        $module    .= '</select>';

        return $module;
    }

    public function get_user_log($page,$user,$module,$start,$end){
        
       
        $row_count = ROW_PER_PAGE;
        $offset = ($page-1)*ROW_PER_PAGE ;    
        $ss = $user;
        $m = $module;
        if($user == ''){
            $user = '%';
        }

        if($module == ''){
            $module = '%';
        }

        if($start==''){
            $where_start = '';
        }else{
            $where_start = 'AND cms_user_log.log_time >= "'.$start.'" ';
        }

        if($end==''){
            $where_end = '';
        }else{
            $where_end = 'AND cms_user_log.log_time <= "'.$end.' 23:59:59" ';
        }

        $sql = 'SELECT COUNT(cms_user_log.id) AS total FROM
                cms_user_log
                INNER JOIN cms_user ON cms_user_log.id_user = cms_user.id
                
                WHERE 
                    cms_user.user_name LIKE ? AND
                    cms_user_log.id_module LIKE ? '.$where_start.$where_end;
        $dt_total = $this->db->query($sql,array($user,$module));

        $dt_total_row = $dt_total->row();


        $sql = 'SELECT
                    cms_user_log.id,
                    cms_user_log.id_user,
                    cms_user_log.id_module,
                    cms_user_log.activity,
                    cms_user_log.description,
                    cms_user_log.log_time,
                    cms_user.user_name,
                    cms_user.date_created,
                    cms_user.created_by,
                    cms_user.date_updated,
                    cms_user.updated_by,
                    cms_user.last_login
                FROM
                cms_user_log
                INNER JOIN cms_user ON cms_user_log.id_user = cms_user.id
                
                WHERE 
                    cms_user.user_name LIKE ? AND
                    cms_user_log.id_module LIKE ? '.$where_start.$where_end.'
                ORDER BY cms_user_log.log_time DESC
                LIMIT ?,?';   
        $dt = $this->db->query($sql,array($user,$module,$offset,$row_count));

        // echo $this->db->last_query();
        // die;
        $data['log_table'] = '';
        $data['pagination'] = '';
        $no_log = 0;

        if($dt->num_rows()>0){
            foreach ($dt->result() as $row) {
                # code...
                if($row->id_module==1){
                    $modulename = $this->lang->line('user_admin');
                }else{
                    $modulename = $this->lang->line('incident');
                }
                
                $no_log += 1;
                $no = (ROW_PER_PAGE*$page)-ROW_PER_PAGE+$no_log;
                $data['log_table'] .= '<div class="table-body">';
                $data['log_table'] .= '     <div class="table-body-item col-nomor">#'.$no.'</div>';
                $data['log_table'] .= '     <div class="table-body-item col-username">'.$row->log_time.'</div>';
                $data['log_table'] .= '     <div class="table-body-item col-username">'.$row->user_name.'</div>';
                $data['log_table'] .= '     <div class="table-body-item col-username">'.$modulename.'</div>';
                $data['log_table'] .= '     <div class="table-body-item col-username">'.$row->activity.'</div>';
                $data['log_table'] .= '     <div class="table-body-item col-desc">'.$row->description.'</div>';
                $data['log_table'] .= '</div>';
            }
            
             $pages = $this->pages_log($user,$module,$start,$end);
            




            // if($pages>1){
            //     $last_page=$pages;
            //     $first_page=1;
            //     if($page==$first_page) { 
            //         $prev_page = $first_page;
            //     }else{
            //         $prev_page = $page - 1;
            //     }

            //     if($page==$last_page){
            //         $next_page = $last_page;
            //     }else{
            //         $next_page = $page + 1;
            //     }

            //     $data['pagination'] .= '<div class="page">';
            //     $data['pagination'] .= '    <ul class="pagination">';
            //     $data['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/audit_trail/'.$first_page.'?users='.$ss.'&module='.$m.'&start='.$start.'&end='.$end.'"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';
            //     $data['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/audit_trail/'.$prev_page.'?users='.$ss.'&module='.$m.'&start='.$start.'&end='.$end.'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
            //     $data['pagination'] .= '        <li><a href="#">'.$page.'</a></li>';
            //     $data['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/audit_trail/'.$next_page.'?users='.$ss.'&module='.$m.'&start='.$start.'&end='.$end.'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
            //     $data['pagination'] .= '        <li><a href="'.base_url().'index.php/admin/audit_trail/'.$last_page.'?users='.$ss.'&module='.$m.'&start='.$start.'&end='.$end.'"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
            //     $data['pagination'] .= '    </ul>';
            //     $data['pagination'] .= '</div>';
            // }else{

            // }

        }else{

        }

        $this->load->library('pagination');


        //$config['per_page'] = 2;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['page_query_string'] = TRUE;
        //$config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo; '.$this->lang->line('First');
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = $this->lang->line('Last').' &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = $this->lang->line('Next').' &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; '.$this->lang->line('Previous');
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        // $config['display_pages'] = FALSE;
        // 
        $config['anchor_class'] = 'follow_link';
        $config['base_url'] = base_url()."index.php/admin/audit_trail?users=".$ss."&module=".$m;
        
        $config['total_rows'] = $dt_total_row->total;
        $config['per_page'] = ROW_PER_PAGE;
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $output = $this->load->view('template/audit_trail_table',$data,TRUE);
        return $output;
    }


    public function pages_log($user,$module,$start,$end){

        if($start==''){
            $where_start = '';
        }else{
            $where_start = 'AND cms_user_log.log_time >= "'.$start.'" ';
        }

        if($end==''){
            $where_end = '';
        }else{
            $where_end = 'AND cms_user_log.log_time <= "'.$end.' 23:59:59" ';
        }

        $sql = 'SELECT
                    COUNT(cms_user_log.id) AS co
                FROM
                cms_user_log
                INNER JOIN cms_user ON cms_user_log.id_user = cms_user.id
                
                WHERE 
                    cms_user.user_name LIKE ? AND
                    cms_user_log.id_module LIKE ? '.$where_start.$where_end;
        
        $dt = $this->db->query($sql,array($user,$module));
        foreach ($dt->result() as $row) {
            # code...
            $total = $row->co;
        }

        return ceil($total/ROW_PER_PAGE);
    }

    public function disable_user($id,$id_user){
        
        $sql = 'UPDATE cms_user SET user_status=0,date_updated=NOW(),updated_by=? WHERE id=?';
        
        
        $this->db->query($sql,array($id_user,$id));
        $desc = 'Disable user id#'.$id;
        $activity = 'Update User Status';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
    }

    public function enable_user($id,$id_user){
        
        $sql = 'UPDATE cms_user SET user_status=1,date_updated=NOW(),updated_by=? WHERE id=?';
        
        
        $this->db->query($sql,array($id_user,$id));
        $desc = 'Disable user id#'.$id;
        $activity = 'Update User Status';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
    } 

    public function update_user($id,$email,$firstname,$lastname,$group,$status,$id_user){
        $sql = 'UPDATE cms_user SET email= ?, first_name=?, last_name=?, user_group=?, user_status=?, date_updated=NOW(), updated_by=? WHERE id=?';
        $dt = $this->db->query($sql,array($email, $firstname, $lastname, $group, $status, $id_user, $id));
        // log
        $desc = 'Update user id#'.$id;
        $activity = 'Update User';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));

    }

    public function check_ldap($id){
        $sql = 'SELECT * FROM cms_authen_password WHERE id=?';
        $dt = $this->db->query($sql,array($id));
        if($dt->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    public function ch_pass($id,$p,$id_user){
        $ps = KEY.$p;
        $sql = 'UPDATE cms_authen_password SET password=md5(?) WHERE id=?';
        if($this->db->query($sql,array($ps,$id))){
            $desc = 'Update password user id#'.$id;
            $activity = 'Update password';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc)); 
            return $this->lang->line('Update Password success');
        }else{
            return $this->lang->line('Update Password fail');
        }
            

    }

    public function ch_pass2($id,$p,$p0,$id_user){
        $old_pass = KEY.$p0;
        $sql = 'SELECT * FROM cms_authen_password WHERE id=? AND password=md5(?)';
        $dt =  $this->db->query($sql,array($id_user,$old_pass));
        if($dt->num_rows()>0){

            $ps = KEY.$p;
            $sql = 'UPDATE cms_authen_password SET password=md5(?) WHERE id=?';
            if($this->db->query($sql,array($ps,$id))){
                $desc = 'Update password user id#'.$id;
                $activity = 'Update password';
                $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
                $this->db->query($sql_log,array($id_user,$activity,$desc)); 
                return $this->lang->line('Update Password success');
            }else{
                return $this->lang->line('Update Password fail');
            }

        }else{
            return $this->lang->line('wrong_password');
        }

        
            

    }

    public function check_local_user($user){
        $sql = 'SELECT * FROM cms_user WHERE user_name = ?';
        $dt = $this->db->query($sql,array($user));
        if($dt->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    public function check_ldap_user($user){

        $username   = LDAP_USER;
        $password   = LDAP_PASSWORD;
        $server     = LDAP_SERVER;
        $domain     = LDAP_DOMAIN;
        $port       = LDAP_PORT;      
        // $ldap_connection = ldap_connect($server, $port);  
        //$ldap_connection = ldap_connect(LDAP_SERVER, LDAP_PORT);
        $ldap_connection = true;
        $data_ldap = array();

        /* this for disale LDAP*/

        
        if (! $ldap_connection)
        {
            $data_ldap = array(
                'code'=>'3',
                'username'=>'',
                'firstname'=>'',
                'lastname'=>'',
                'email'=>'');
            return $data_ldap;
            //echo '<p>LDAP SERVER CONNECTION FAILED</p>';
        }else{
            // Help talking to AD
            //ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            //ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            
            //ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            //ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            
            //$ldap_bind = @ldap_bind($ldap_connection, $username.$domain, $password);

            //$ldap_bind = @ldap_bind($ldap_connection, LDAP_USER.LDAP_DOMAIN, LDAP_PASSWORD);
            $ldap_bind = true;
            if (! $ldap_bind)
            {
                $data_ldap = array(
                    'code'=>'4',
                    'username'=>'',
                    'firstname'=>'',
                    'lastname'=>'',
                    'email'=>'');
                //echo '<p>LDAP BINDING FAILED</p>';
                return $data_ldap;
            }else{
                $ldap_users=array();
                //$filter_person = '(&(sAMAccountName='.$user.'*)(objectCategory=user))';
                //$sr_person = ldap_search($ldap_connection,'CN=Users,DC='.LDAP_DOMAIN_DC1.',DC='.LDAP_DOMAIN_DC2,$filter_person);
                //$sr = ldap_get_entries($ldap_connection, $sr_person);

                $i = 0;
                //$filter_person2 = '(&(sAMAccountName='.$user.')(objectCategory=user))';
                //$sr_person2 = ldap_search($ldap_connection,'CN=Users,DC='.LDAP_DOMAIN_DC1.',DC='.LDAP_DOMAIN_DC2,$filter_person2);
                //$sr2 = ldap_get_entries($ldap_connection, $sr_person2);
                $sr2['count'] = 0;
                if($sr2['count']==0){
                    $i++; 
                    $user_exist = $this->check_local_user($user);
                    if(!$user_exist){
                        array_push($ldap_users, array(  'no_user'=>$i,
                                                        'username'=>$user,
                                                        'firstname'=>'',
                                                        'lastname'=>'',
                                                        'email'=>'',
                                                        'ldap_user'=>0,
                                                        'user_exist'=>$user_exist ));    
                    }
                }
                $sr['count']=0;
                if($sr['count']>0){
                    // LDAP user Found
                    // Get all information
                    /*
                    $count_user = $sr['count'];
                    
                    foreach ($sr as $array_user) {
                        # code...
                        if(is_array($array_user)){
                            if(in_array('samaccountname',$array_user)){
                                $username = $array_user['samaccountname'][0];
                            }else{
                                $firstname = '';
                            }
                            
                            if(in_array('givenname',$array_user)){
                                $firstname = $array_user['givenname'][0];
                            }else{
                                $firstname = '';
                            }

                            if(in_array('sn',$array_user)){
                                $lastname = $array_user['sn'][0];
                            }else{
                                $lastname = '';
                            }

                            if(in_array('userprincipalname',$array_user)){
                                $email = $array_user['userprincipalname'][0];
                            }else{
                                $email = '';
                            }
                            $user_exist = $this->check_local_user($username);
                            if(!$user_exist){
                                array_push($ldap_users, array(  'no_user'=>$i,
                                                                'username'=>$username,
                                                                'firstname'=>$firstname,
                                                                'lastname'=>$lastname,
                                                                'email'=>$email,
                                                                'ldap_user'=>1 ,
                                                                'user_exist'=>$user_exist ));    
                            }
                            
                        }

                        $i++;

                    }
                    */
                    // check username that type is exist ?
                    $data_ldap = array(
                        'code'=>'0',
                        'ldap_users'=>$ldap_users);
                    return $data_ldap;

                }else{
                    // LDAP user not found
                    $user_exist = $this->check_local_user($user);
                    if(!$user_exist){

                        $data_ldap = array(
                            'code'=>'1',
                            'ldap_users'=>$ldap_users);    
                    }else{
                        $data_ldap = array(
                            'code'=>'2',
                            'ldap_users'=>array());
                    }
                    
                    return $data_ldap;

                }

            }

        }
        
    }

    public function get_group(){
        $sql = 'SELECT * FROM cms_group';
        $dt = $this->db->query($sql);

        $group = '<select class="std-input2" id="ldap-group" name="ldap-group">';
        foreach ($dt->result() as $row) {
            # code...
            $group .= '<option value="'.$row->id.'">'.$row->group_name.'</option>';
        }
        $group .= '</select>';
        return $group;
    }

    public function add_ldap_user($username,$firstname,$lastname,$email,$group,$status,$id_user){
        $sql = 'INSERT INTO cms_user(user_name,user_status,user_group,email,first_name,last_name,date_created,created_by) 
                VALUES (?,?,?,?,?,?,NOW(),?)';
        $this->db->query($sql,array($username,$status,$group,$email,$firstname,$lastname,$id_user));

        $desc = 'Add LDAP user '.$username;
        $activity = 'Create User';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
        // log 
    }

    public function add_local_user($username,$firstname,$lastname,$email,$group,$p1,$status,$id_user){
        $sql = 'INSERT INTO cms_user(user_name,user_status,user_group,email,first_name,last_name,date_created,created_by,last_cpwd,pwd_expire) 
                VALUES (?,?,?,?,?,?,NOW(),?,NOW(),NOW())';
        $this->db->query($sql,array($username,$status,$group,$email,$firstname,$lastname,$id_user));
        $id = $this->db->insert_id();
        $entity_type = 1;
        $entity_id = 1;
        $password_status = 1;
        $pass = KEY.$p1;
        $sql = 'INSERT INTO cms_authen_password(id,entity_type,entity_id,password,password_status,date_created)
                VALUES (?,?,?,md5(?),?,NOW())';
        $this->db->query($sql,array($id,$entity_type,$entity_id,$pass,$password_status));



        $desc = 'Add Local user '.$username;
        $activity = 'Create User';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
        // log 
    }

    public function get_group_detail($id,$privilege_menu){
        $sql = 'SELECT
                    cms_group.id,
                    cms_group.group_name,
                    cms_group.group_status,
                    cms_group.group_type,
                    cms_group.authen_type,
                    cms_group.date_created,
                    cms_group.created_by,
                    cms_group.date_update,
                    cms_group.updated_by,
                    group_type.group_type_name,
                    group_authen.group_authen_name
                FROM
                    cms_group
                LEFT OUTER JOIN group_type ON cms_group.group_type = group_type.id
                LEFT OUTER JOIN group_authen ON cms_group.authen_type = group_authen.id
                WHERE cms_group.id = ?';
        $dt = $this->db->query($sql,array($id));

        $output = '<div class="group_detail">';
        foreach ($dt->result() as $row) {
            # code...
            $data['id'] = $row->id;
            $data['group_name']         = $row->group_name;
            $data['group_type']         = $row->group_type;
            $data['group_type_name']    = $row->group_type_name;
            $data['authen_type']        = $row->authen_type;
            $data['authen_type_name']   = $row->group_authen_name;
            $data['date_created']       = $row->date_created;
 
            $sqld = 'SELECT * FROM cms_user WHERE id = ?';
            $du = $this->db->query($sqld,array($row->created_by));
            if($du->num_rows()>0){
                foreach ($du->result() as $row_created) {
                    # code...
                    $data['created_by'] = $row_created->user_name;
                }
            }else{
                $data['created_by'] = '';
            }
            $data['date_update']    = $row->date_update;

            $sqlu = 'SELECT * FROM cms_user WHERE id = ?';
            $du2 = $this->db->query($sqld,array($row->updated_by));
            if($du2->num_rows()>0){
                foreach ($du2->result() as $row_updated) {
                    # code...
                    $data['updated_by'] = $row_updated->user_name;
                }
            }else{
                $data['updated_by'] = '';
            }

            if($row->group_status == 1 ){
                $group_status_name = $this->lang->line('enable');
            }else{
                $group_status_name = $this->lang->line('disable');
            }
            $data['group_status'] = $row->group_status;
            $data['group_status_name'] = $group_status_name;
        }

        $select_group_type  =   '<select class="std-input" id="group_type" name="group_type">';
        $sql_group_type = 'SELECT * FROM group_type';
        $dtgt = $this->db->query($sql_group_type);
        
        foreach ($dtgt->result() as $row_group_type) {
            # code...
            if($row_group_type->id==$data['group_type']){
                $selected_group_type = 'selected';
            }else{
                $selected_group_type = '';
            }
            $select_group_type .= '<option value="'.$row_group_type->id.'">'.$row_group_type->group_type_name.'</option>';    
        }

        $select_group_type  .=  '</select>';
        $data['select_group_type'] = $select_group_type;

        $select_group_authen  =   '<select class="std-input" id="authen_type" name="authen_type">';
        $sql_group_authen = 'SELECT * FROM group_authen';
        $dtga = $this->db->query($sql_group_authen);
        
        foreach ($dtga->result() as $row_group_authen) {
            # code...
            if($row_group_authen->id==$data['authen_type']){
                $selected_group_authen = 'selected';
            }else{
                $selected_group_authen = '';
            }
            $select_group_authen .= '<option value="'.$row_group_authen->id.'">'.$row_group_authen->group_authen_name.'</option>';    
        }

        $select_group_authen  .=  '</select>';
        $data['select_authen_type'] = $select_group_authen;  

        $select_status  = '<select class="std-input" id="group_status" name="group_status">';
        if($data['group_status']=="1"){
            $select_status .= '<option value="1" selected>'.$this->lang->line('enable').'</option>';
            $select_status .= '<option value="0" >'.$this->lang->line('disable').'</option>';
        }else{
            $select_status .= '<option value="1" >'.$this->lang->line('enable').'</option>';
            $select_status .= '<option value="0" selected>'.$this->lang->line('disable').'</option>';
        }
        $select_status .=  '</select>';             
        $data['select_status'] = $select_status;
        $data['privilege_menu'] = $privilege_menu; 
        $output = $this->load->view('template/group_detail',$data,TRUE);
        return $output;
    }

    public function mapping_module($id){
        $sql = 'SELECT * FROM cms_module ORDER BY id';
        $dt = $this->db->query($sql);

        /*
        $sql = 'SELECT
                    map_group_module.id_group,
                    map_group_module.id_module,
                    cms_module.module_name
                FROM
                    map_group_module
                INNER JOIN cms_module ON map_group_module.id_module = cms_module.id
                WHERE map_group_module.id_group = ? ';
        $dt = $this->db->query($sql,array($id));
        */

        $output = '<div class="table-module">';
        $output .= '<div class="table-head w1000">';
        $output .= '    <div class="table-body-item mod-col-1 head-table-text">'.$this->lang->line('id').'</div>';
        $output .= '    <div class="table-body-item mod-col-2 head-table-text">'.$this->lang->line('Module').'</div>';
        $output .= '    <div class="table-body-item mod-col-3 head-table-text">'.$this->lang->line('Access').'</div>';
        $output .= '</div>';
        foreach ($dt->result() as $row) {
            # code...

            $output .= '<div class="table-body padding-top w1000">';

            $output .= '    <div class="mod-col-1">#'.$row->id.'</div>';
            $output .= '    <div class="mod-col-2">'.$row->module_name.'</div>';
            $sql = 'SELECT
                        map_group_module.id_group,
                        map_group_module.id_module,
                        cms_module.module_name
                    FROM
                        map_group_module
                    INNER JOIN cms_module ON map_group_module.id_module = cms_module.id
                    WHERE map_group_module.id_group = ? AND map_group_module.id_module=?';
            $pv = $this->db->query($sql,array($id,$row->id));
            //print_r($pv);
            //die;
            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
                $disabled = ''; 
            }else{
                $disabled = 'disabled';
            }
            if($pv->num_rows()>0){
                $ret = $pv->row();
                $output .= '<div class="mod-col-3">';
                $output .= '     <input id="module_'.$row->id.'_group_'.$id.'" type="checkbox" checked '.$disabled.' onchange="">';
                $output .= '</div>';

            }else{
                $output .= '<div class="mod-col-3">';
                $output .= '     <input id="module_'.$row->id.'_group_'.$id.'" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';

            }
            

            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ 

                //$output .= '    <div class="mod-col-3 right"><button class="std-btn bkgr-red" onclick="delete_module('.$row->id_module.','.$id.')" name="privilege-2">'.$this->lang->line('delete').'</button ></div>';
            }
            $output .= '</div>'; 
        }
        /*
        $output .= '<div class="rows">';
        if($this->session->userdata('get_privilege')[1]['map_group']){
            $output .= '    <div class=""><button class="std-btn bkgr-green" onclick="add_map_module()" name="privilege-2">'.$this->lang->line('add').'</button></div>';
        }
        $output .= '</div>'; 
        */
        $output .= '</div>';

        $sql_module = 'SELECT * FROM cms_module';
        $dtm = $this->db->query($sql_module);
        
        $module_list['module_list'] = $dtm;
        $module_list['id_group'] = $id;
        $output .= $this->load->view('template/mapping_module',$module_list,TRUE);
        return $output;
    }

    public function mapping_opco($id){
        $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame ASC';
        $dt = $this->db->query($sql);

        /*
        $sql = 'SELECT
                    map_group_module.id_group,
                    map_group_module.id_module,
                    cms_module.module_name
                FROM
                    map_group_module
                INNER JOIN cms_module ON map_group_module.id_module = cms_module.id
                WHERE map_group_module.id_group = ? ';
        $dt = $this->db->query($sql,array($id));
        */
        $output = '<div class="content-title3 w1000">';
        $output .= '    <div class="opco-col-1">'.$this->lang->line('OpCo').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_view').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_add').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_update').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('add_all_month').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('update_all_month').'</div>';
        $output .='</div>';
        $output .= '<div class="priv-opco">';
        foreach ($dt->result() as $row) {
            # code...

            $output .= '<div class="table-body padding-top w1000">';

            $output .= '    <div class="opco-col-1">'.$row->opconame.'</div>';
            $sql = 'SELECT * FROM map_group_opco
                    WHERE group_id = ? AND opco_id=?';
            $pv = $this->db->query($sql,array($id,$row->opcoid));
            //print_r($pv);
            //die;
            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID ){
                $disabled = ''; 
            }else{
                $disabled = 'disabled';
            }
            if($pv->num_rows()>0){
                $ret = $pv->row();
                $output .= '<div class="opco-col-3">';
                if($ret->view_opco_config){
                    $view_checked = 'checked';
                }else{
                    $view_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_view" type="checkbox" '.$view_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->add_opco_config){
                    $add_checked = 'checked';
                }else{
                    $add_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_add" type="checkbox" '.$add_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->update_opco_config){
                    $update_checked = 'checked';
                }else{
                    $update_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update" type="checkbox" '.$update_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->add_all_month){
                    $current_month_checked = 'checked';
                }else{
                    $current_month_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_add_all_month" type="checkbox" '.$current_month_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                 $output .= '<div class="opco-col-3">';
                if($ret->update_all_month){
                    $current_month_checked = 'checked';
                }else{
                    $current_month_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update_all_month" type="checkbox" '.$current_month_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                if($ret->view_incident){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view_incident" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view_incident" type="hidden" value=0>';
                }

                if($ret->create_incident){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_create_incident" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_create_incident" type="hidden" value=0>';
                }

                if($ret->update_incident){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_incident" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_incident" type="hidden" value=0>';
                }

                if($ret->bulk_incident){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_bulk_incident" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_bulk_incident" type="hidden" value=0>';
                }
                


            }else{
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_view" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_add" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_add_all_month" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update_all_month" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view_incident" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_create_incident" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_incident" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_bulk_incident" type="hidden" value=0>';

            }
            

            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ 

                //$output .= '    <div class="mod-col-3 right"><button class="std-btn bkgr-red" onclick="delete_module('.$row->id_module.','.$id.')" name="privilege-2">'.$this->lang->line('delete').'</button ></div>';
            }
            $output .= '</div>';



        }
        /*
        $output .= '<div class="rows">';
        if($this->session->userdata('get_privilege')[1]['map_group']){
            $output .= '    <div class=""><button class="std-btn bkgr-green" onclick="add_map_module()" name="privilege-2">'.$this->lang->line('add').'</button></div>';
        }
        $output .= '</div>'; 
        */
        $output .= '</div>';

        $sql_opco = 'SELECT * FROM cms_opco_list';
        $dtm = $this->db->query($sql_opco);
        
        $opco_list['opco_list'] = $dtm;
        $opco_list['id_group'] = $id;
        $output .= $this->load->view('template/mapping_opco',$opco_list,TRUE);
        return $output;
    }

    public function mapping_incident($id){
        $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame ASC';
        $dt = $this->db->query($sql);

        $output = '<div class="content-title3 w1000">';
        $output .= '    <div class="opco-col-1">'.$this->lang->line('OpCo').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_view').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_add').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('opco_update').'</div>';
        $output .= '    <div class="opco-col-3">'.$this->lang->line('bulk_incident').'</div>';
        $output .='</div>';
        $output .= '<div class="priv-opco">';
        foreach ($dt->result() as $row) {
            # code...

            $output .= '<div class="table-body padding-top w1000">';

            $output .= '    <div class="opco-col-1">'.$row->opconame.'</div>';
            $sql = 'SELECT * FROM map_group_opco
                    WHERE group_id = ? AND opco_id=?';
            $pv = $this->db->query($sql,array($id,$row->opcoid));
            //print_r($pv);
            //die;
            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
                $disabled = ''; 
            }else{
                $disabled = 'disabled';
            }
            if($pv->num_rows()>0){
                $ret = $pv->row();
                
                if($ret->view_opco_config){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view" type="hidden" value=0>';
                }

                if($ret->add_opco_config){
                   $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add" type="hidden" value=0>';
                }
                
                if($ret->update_opco_config){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update" type="hidden" value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update" type="hidden" value=0>';
                }

                if($ret->add_all_month){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add_all_month" type="hidden"  value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add_all_month" type="hidden"  value=0>';
                }

                if($ret->update_all_month){
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_all_month" type="hidden"  value=1>';
                }else{
                    $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_all_month" type="hidden"  value=0>';
                }

                $output .= '<div class="opco-col-3">';
                if($ret->view_incident){
                    $view_incident_checked = 'checked';
                }else{
                    $view_incident_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_view_incident" type="checkbox" '.$view_incident_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->create_incident){
                    $create_incident_checked = 'checked';
                }else{
                    $create_incident_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_create_incident" type="checkbox" '.$create_incident_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->update_incident){
                    $update_incident_checked = 'checked';
                }else{
                    $update_incident_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update_incident" type="checkbox" '.$update_incident_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                $output .= '<div class="opco-col-3">';
                if($ret->bulk_incident){
                    $bulk_incident_checked = 'checked';
                }else{
                    $bulk_incident_checked = '';
                }
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_bulk_incident" type="checkbox" '.$bulk_incident_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';
                


            }else{

                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_view" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_add_all_month" type="hidden" value=0>';
                $output .= '<input id="opco_'.$row->opcoid.'_group_'.$id.'_update_all_month" type="hidden" value=0>';

                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_view_incident" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_create_incident" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_update_incident" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';
                $output .= '<div class="opco-col-3">';
                $output .= '     <input id="opco_'.$row->opcoid.'_group_'.$id.'_bulk_incident" type="checkbox" '.$disabled.' onchange="">';
                $output .= '</div>';

            }
            

            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ 

                //$output .= '    <div class="mod-col-3 right"><button class="std-btn bkgr-red" onclick="delete_module('.$row->id_module.','.$id.')" name="privilege-2">'.$this->lang->line('delete').'</button ></div>';
            }
            $output .= '</div>';



        }
        /*
        $output .= '<div class="rows">';
        if($this->session->userdata('get_privilege')[1]['map_group']){
            $output .= '    <div class=""><button class="std-btn bkgr-green" onclick="add_map_module()" name="privilege-2">'.$this->lang->line('add').'</button></div>';
        }
        $output .= '</div>'; 
        */
        $output .= '</div>';

        $sql_opco = 'SELECT * FROM cms_opco_list';
        $dtm = $this->db->query($sql_opco);
        
        $opco_list['opco_list'] = $dtm;
        $opco_list['id_group'] = $id;
        $output .= $this->load->view('template/mapping_incident',$opco_list,TRUE);
        return $output;
    }    

    public function mapping_report($id){
        $sql = 'SELECT * FROM cms_report ORDER BY reportid ASC';
        $dt = $this->db->query($sql);

        /*
        $sql = 'SELECT
                    map_group_module.id_group,
                    map_group_module.id_module,
                    cms_module.module_name
                FROM
                    map_group_module
                INNER JOIN cms_module ON map_group_module.id_module = cms_module.id
                WHERE map_group_module.id_group = ? ';
        $dt = $this->db->query($sql,array($id));
        */
        $output = '<div class="content-title3 w1000">';
        $output .= '    <div class="report-col-1">'.$this->lang->line('Report').'</div>';
        $output .= '    <div class="report-col-3">'.$this->lang->line('opco_view').'</div>';

        $output .='</div>';
        $output .= '<div class="priv-opco">';
        foreach ($dt->result() as $row) {
            # code...

            $output .= '<div class="table-body padding-top w1000">';

            $output .= '    <div class="report-col-1">'.$row->report_name.'</div>';
            $sql = 'SELECT * FROM map_group_report
                    WHERE group_id = ? AND report_id=?';
            $pv = $this->db->query($sql,array($id,$row->reportid));
            //print_r($pv);
            //die;
            if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
                $disabled = ''; 
            }else{
                $disabled = 'disabled';
            }
            if($pv->num_rows()>0){
                $ret = $pv->row();
                $output .= '<div class="report-col-3">';
                if($ret->p_view){
                    $view_checked = 'checked';
                }else{
                    $view_checked = '';
                }
                $output .= '     <input id="report_'.$row->reportid.'_group_'.$id.'_view" type="checkbox" '.$view_checked.' '.$disabled.' onchange="">';
                $output .= '</div>';

                



            }else{
                $output .= '<div class="report-col-3">';
                $output .= '     <input id="report_'.$row->reportid.'_group_'.$id.'_view" type="checkbox" '.$disabled.' onchange="change_report('.$row->reportid.','.$id.')">';
                $output .= '</div>';
            }
            

            if($this->session->userdata('get_privilege')[1]['map_group']){ 

                //$output .= '    <div class="mod-col-3 right"><button class="std-btn bkgr-red" onclick="delete_module('.$row->id_module.','.$id.')" name="privilege-2">'.$this->lang->line('delete').'</button ></div>';
            }
            $output .= '</div>'; 
        }
        /*
        $output .= '<div class="rows">';
        if($this->session->userdata('get_privilege')[1]['map_group']){
            $output .= '    <div class=""><button class="std-btn bkgr-green" onclick="add_map_module()" name="privilege-2">'.$this->lang->line('add').'</button></div>';
        }
        $output .= '</div>'; 
        */
        $output .= '</div>';

        $sql_opco = 'SELECT * FROM cms_report';
        $dtm = $this->db->query($sql_opco);
        
        $report_list['report_list'] = $dtm;
        
        $report_list['id_group'] = $id;
        $output .= $this->load->view('template/mapping_report',$report_list,TRUE);
        return $output;
    }

    public function group_used($id){
        $sql = 'SELECT * FROM cms_user WHERE ';
    }

    public function update_group($id,$group_name,$group_type,$authen_type,$group_status,$id_user){

        // old value
        $sql = 'SELECT * FROM cms_group WHERE id=?';
        $old = $this->db->query($sql,array($id));
        $old_data = $old->row();
        $sql = 'UPDATE cms_group SET group_name = ?, group_type = ?, authen_type= ?, group_status =?, date_update=NOW(), updated_by = ? 
                WHERE id= ?';
        //echo $group_name.' '.$group_type.' '.$authen_type.' '.$group_status.' '.$id_user.' '.$id;
        $this->db->db_debug = FALSE;
        $st = $this->db->query($sql,array($group_name, $group_type, $authen_type, $group_status, $id_user,$id));
        
        //echo "ok";
        //echo $st;
       
        if($st){
            $desc = 'Update group id# '.$id.'. Name : "'.$old_data->group_name.'"->"'.$group_name.'". status : "'.$old_data->group_status.'"=>"'.$group_status.'"';
            $activity = 'Update Group';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;
        

    }

    public function delete_module_map($id_group,$id_module,$id_user){
        $sql= 'DELETE FROM map_group_module WHERE id_group =? AND id_module = ?';
        $st = $this->db->query($sql,array($id_group,$id_module));

        if($st){
            $desc = 'Delete access module id# '.$id_module.' for group id#'.$id_group;
            $activity = 'Update Group';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;        
    }

    public function add_module_map($id_group,$id_module,$id_user){
        $this->db->db_debug = FALSE;
        $sql = 'INSERT INTO map_group_module(id_group,id_module) VALUES (?,?)';
        $st = $this->db->query($sql,array($id_group,$id_module));
        if($st){
            $desc = 'Add access module id# '.$id_module.' for group id#'.$id_group;
            $activity = 'Update Group';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;        

    }

    public function get_group_type(){
        $sql = 'SELECT * FROM group_type';
        $dt= $this->db->query($sql);
        return $dt;  
    }

    public function add_group($group_name,$group_type,$group_authen,$group_status,$id_user){
        $this->db->db_debug = FALSE;
        $sql = 'INSERT INTO cms_group(group_name,group_type,authen_type,group_status,date_created,created_by) VALUES (?,?,?,?,NOW(),?)';
        $st = $this->db->query($sql,array($group_name,$group_type,$group_authen,$group_status,$id_user));
        if($st){
            $desc = 'Add new Group name '.$group_name;
            $activity = 'Add Group';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;
    }


    public function get_group_authen(){
        $sql = 'SELECT * FROM group_authen';
        $dt= $this->db->query($sql);
        return $dt;  
    }    

    public function get_ui_detail($id){
        $sql = 'SELECT
                    universe.id,
                    universe.map_name,
                    universe_detail.id AS detail_id,
                    universe_detail.`name`
                FROM
                    universe
                INNER JOIN universe_detail ON universe.id = universe_detail.universe_id
                WHERE universe.id = ?';
        return $this->db->query($sql,array($id));
    }

    public function delete_currency($id,$id_user){
        $sql= 'DELETE FROM map_vipgroup_currency WHERE id =? ';
        $st = $this->db->query($sql,array($id));

        if($st){
            $desc = 'Delete VimpelCom Group Currency id# '.$id;
            $activity = 'Delete VimpelCom Group Currency';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;            
    }

    public function delete_detection($id,$id_user){
        $sql= 'DELETE FROM cms_detection_tool WHERE dtid =? ';
        $status = $this->db->query($sql,array($id));



        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already deleted');

            $desc = 'Delete Detection Tool id# '.$id;
            $activity = 'Delete Detection Tool';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }


        return $out;            
    }

    public function delete_opco($id,$id_user){
        $sql= 'DELETE FROM cms_opco_list WHERE opcoid =? ';
        $status = $this->db->query($sql,array($id));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already deleted');

            $desc = 'Delete OpCo id# '.$id;
            $activity = 'Delete OpCo';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }

    public function delete_revenue($id,$id_user){
        $sql= 'DELETE FROM cms_revenue_stream WHERE id =? ';
        $status = $this->db->query($sql,array($id));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Revenue Stream already deleted');

            $desc = 'Delete Revenue Stream id# '.$id;
            $activity = 'Delete Revenue Stream';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }



    public function add_currency($currencyname,$currencysymbol,$id_user){
        $sql = 'SELECT * FROM map_vipgroup_currency WHERE currencyname=? AND currencysymbol=?';
        $dt = $this->db->query($sql,array($currencyname,$currencysymbol));
        if($dt->num_rows()>0){
            return false;
        }else{
            $sql= 'INSERT INTO map_vipgroup_currency(currencyname,currencysymbol) VALUES (?,?)';
            $st = $this->db->query($sql,array($currencyname,$currencysymbol));

            if($st){
                $desc = 'Add VimpelCom Group Currency name '.$currencyname;
                $activity = 'Add new VimpelCom Group Currency';
                $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
                $this->db->query($sql_log,array($id_user,$activity,$desc));
            }
            return $st;            
        }
    }


    public function add_detection($dtname,$dtcost,$id_user){
        $sql= 'INSERT INTO cms_detection_tool(dtname,dtcost) VALUES (?,?)';
        $status = $this->db->query($sql,array($dtname,$dtcost));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');

            $desc = 'Add Detection Tool name '.$dtname;
            $activity = 'Add new Detection Tool';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;
    }

    public function add_opco($opconame,$currency_id,$id_user){
        $sql= 'INSERT INTO cms_opco_list(opconame,currency_id) VALUES (?,?)';
        $status = $this->db->query($sql,array($opconame,$currency_id));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');

            $desc = 'Add OpCo name '.$opconame;
            $activity = 'Add new OpCo';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }



    public function update_currency($id,$currencyname,$currencysymbol,$id_user){
        $sql= 'UPDATE map_vipgroup_currency SET currencyname=?,currencysymbol=? WHERE id=?';
        $st = $this->db->query($sql,array($currencyname,$currencysymbol,$id));

        if($st){
            $desc = 'Update VimpelCom Group Currency id# '.$id;
            $activity = 'Update VimpelCom Group Currency';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

        return $st;            
    }  


    public function update_detection($dtid,$dtname,$dtcost,$id_user){

        // old 
        $sql = 'SELECT * FROM cms_detection_tool WHERE dtid=?';
        $old = $this->db->query($sql,array($dtid));
        $old_data = $old->row();

        $sql= 'UPDATE cms_detection_tool SET dtname=?,dtcost=? WHERE dtid=?';
        $status = $this->db->query($sql,array($dtname,$dtcost,$dtid));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');


            $desc = 'Update Detection Tool "'.$old_data->dtname.'"->"'.$dtname.'", cost "'.$old_data->dtcost.'"->'.$dtcost;
            $activity = 'Update Detection Tool';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }  

    public function update_revenue($id,$revenue_stream_name,$id_user){
        // old
        $sql = 'SELECT * FROM cms_revenue_stream WHERE id=?';
        $old = $this->db->query($sql,array($id));
        $old_data = $old->row();

        $sql = 'UPDATE cms_revenue_stream SET revenue_stream_name=? WHERE id=?';
        $status = $this->db->query($sql,array($revenue_stream_name,$id));

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Revenue Stream already saved');

            $desc = 'Update Revenue Stream "'.$old_data->revenue_stream_name.'"->"'.$revenue_stream_name.'"';
            $activity = 'Update Revenue Stream';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }

    public function update_opco($opcoid,$opconame,$currency_id, $id_user){
        // old 
        $sql = 'SELECT * FROM cms_opco_list WHERE opcoid=?';
        $old = $this->db->query($sql,array($opcoid));
        $old_data = $old->row();

        $sql = 'UPDATE cms_opco_list SET opconame=?, currency_id=? WHERE opcoid=?';
        $status = $this->db->query($sql,array($opconame,$currency_id, $opcoid));
        //echo $this->db->last_query();

        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');

            $desc = 'Update OpCo "'.$old_data->opconame.'"->"'.$opconame.'", currency id #'.$old_data->currency_id.'->#'.$currency_id;
            $activity = 'Update OpCo';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }  



    public function get_group_name($id){
        $sql = 'SELECT * FROM cms_group WHERE id=?';
        $dt = $this->db->query($sql,array($id));
        if($dt->num_rows() > 0){
            foreach ($dt->result() as $row) {
                # code...
                return $row->group_name;
            }
        }else{
            return '';
        }
    }

    public function content_privilege($id){

        $sqlm = 'SELECT * FROM cms_module';
        $dm = $this->db->query($sqlm);
        $output = array();
        foreach ($dm->result() as $module) {
            # code...
            $sql = 'SELECT DISTINCT
                        cms_privilege.id,
                        cms_privilege.privilege_name,
                        cms_privilege.privilege_status,
                        cms_privilege.module_id,
                        cms_module.module_name
                    FROM
                        cms_privilege
                    INNER JOIN cms_module ON cms_module.id = cms_privilege.module_id
                    WHERE cms_privilege.module_id = ?';

            $dt = $this->db->query($sql,array($module->id)); 
            $privilege = array();
            foreach ($dt->result() as $privilege_module) {
                $sql_map = 'SELECT * FROM map_group_privilege WHERE group_id=? AND privilege_id=?';
                $dtmap = $this->db->query($sql_map,array($id,$privilege_module->id));
                if($dtmap->num_rows()>0) $grand = true; else $grand = false;
                $privilege_detail = array(  'privilege_id'=>$privilege_module->id,
                                            'privilege_name'=>$privilege_module->privilege_name,
                                            'privilege_access'=>$grand);
                //print_r($privilege_detail);
                //echo '<br>';
                array_push($privilege, $privilege_detail);
            }
            //print_r($privilege);
            //echo '<br> new line <br>';
            $priv = array(  'module_id'=>$module->id,
                            'module_name'=>$module->module_name,
                            'privilege'=>$privilege);
            array_push($output, $priv);
        }
        
        //print_r($output);
        //die;
        $data['data'] = $output;
        $data['group_id'] = $id;
        return $this->load->view('template/privilege_content',$data,TRUE);
        
    }

    public function set_map_privilege($privilege_id,$group_id,$id_user){
        $sql = 'INSERT INTO map_group_privilege(group_id,privilege_id) VALUES (?,?)';
        //echo $sql;
        $st = $this->db->query($sql,array($group_id,$privilege_id));
        if($st){
            $desc = 'Update Privilege Group id# '.$group_id;
            $activity = 'Update Privilege';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }

    }

    public function del_map_privilege($privilege_id,$group_id,$id_user){
        $sql = 'DELETE FROM map_group_privilege WHERE group_id=?  AND privilege_id= ?';
        //echo $sql;
        
        $st = $this->db->query($sql,array($group_id,$privilege_id));
        if($st){
            $desc = 'Update Privilege Group id# '.$group_id;
            $activity = 'Update Privilege';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }
    }

    public function get_privilege($group_id){
        $sqlp = 'SELECT * FROM cms_privilege';
        $dp = $this->db->query($sqlp);
        $output = array();
        foreach ($dp->result() as $privilege) {
             # code...
            $sql = 'SELECT
                        map_group_privilege.group_id,
                        map_group_privilege.privilege_id,
                        cms_privilege.privilege_name,
                        cms_privilege.privilege_status
                    FROM
                        map_group_privilege
                    INNER JOIN cms_privilege ON cms_privilege.id = map_group_privilege.privilege_id
                    WHERE 
                        map_group_privilege.group_id=? AND map_group_privilege.privilege_id=?';
            $dt = $this->db->query($sql,array($group_id,$privilege->id));
            if($dt->num_rows()>0){
                $grand = TRUE;
            }else {
                $grand = FALSE;
            }
            array_push($output, array('privilege_id'=>$privilege->id,
                                      'privilege_name'=>$privilege->privilege_name,
                                      'map_group'=>$grand));

         }


        return $output;
    }

    public function get_map_privilege($group_id,$privilege_id){
        $sql = 'SELECT * FROM map_group_privilege WHERE group_id=? AND privilege_id=?';
        $dt = $this->db->query($sql,array($group_id,$privilege_id));
        if($dt->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    public function verify_ldap_user($user){

        $username   = LDAP_USER;
        $password   = LDAP_PASSWORD;
        $server     = LDAP_SERVER;
        $domain     = LDAP_DOMAIN;
        $port       = LDAP_PORT;      
        //$ldap_connection = ldap_connect($server, $port);  
        //$ldap_connection = ldap_connect(LDAP_SERVER, LDAP_PORT);

        $data_ldap = array();
        $ldap_connection = true;
        if (! $ldap_connection)
        {
            $data_ldap = array(
                'code'=>'3',
                'username'=>'',
                'firstname'=>'',
                'lastname'=>'',
                'email'=>'');
            return $data_ldap;
            //echo '<p>LDAP SERVER CONNECTION FAILED</p>';
        }else{
            // Help talking to AD
            //ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            //ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            
            //ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            //ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            
            //$ldap_bind = @ldap_bind($ldap_connection, $username.$domain, $password);

            //$ldap_bind = @ldap_bind($ldap_connection, LDAP_USER.LDAP_DOMAIN, LDAP_PASSWORD);
            $ldap_bind=true;
            if (! $ldap_bind)
            {
                $data_ldap = array(
                    'code'=>'4',
                    'username'=>'',
                    'firstname'=>'',
                    'lastname'=>'',
                    'email'=>'');
                //echo '<p>LDAP BINDING FAILED</p>';
                return $data_ldap;
            }else{

                //$filter_person = '(&(sAMAccountName='.$user.')(objectCategory=user))';
                //$sr_person = ldap_search($ldap_connection,'CN=Users,DC='.LDAP_DOMAIN_DC1.',DC='.LDAP_DOMAIN_DC2,$filter_person);
                //$sr = ldap_get_entries($ldap_connection, $sr_person);
                $sr['count']=0;
                
                if($sr['count']>0){
                    // LDAP user Found
                    // Get all information
                    /*
                    if(in_array('givenname',$sr[0])){
                        $firstname = $sr[0]['givenname'][0];
                    }else{
                        $firstname = '';
                    }

                    if(in_array('sn',$sr[0])){
                        $lastname = $sr[0]['sn'][0];
                    }else{
                        $lastname = '';
                    }

                    if(in_array('userprincipalname',$sr[0])){
                        $email = $sr[0]['userprincipalname'][0];
                    }else{
                        $email = '';
                    }

                    $data_ldap = array(
                        'code'=>'0',
                        'username'=>$user,
                        'firstname'=>$firstname,
                        'lastname'=>$lastname,
                        'email'=>$email);
                    return $data_ldap;
                    */

                }else{

                    // LDAP user not found
                    $data_ldap = array(
                        'code'=>'1',
                        'username'=>$user,
                        'firstname'=>'',
                        'lastname'=>'',
                        'email'=>'');
                    return $data_ldap;

                }


            }

        }
    }


    public function default_config2(){
        $opco_config[0]= array('id' => '', 'variable' => 'general_numbers_of_subscribers_prepaid','desc' => 'Numbers of Subscribers', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[1]= array('id' => '', 'variable' => 'general_numbers_of_subscribers_postpaid','desc' => 'Numbers of Subscribers', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[2]= array('id' => '', 'variable' => 'general_gross_adds_prepaid','desc' => 'Gross-Adds', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[3]= array('id' => '', 'variable' => 'general_gross_adds_postpaid','desc' => 'Gross-Adds', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[4]= array('id' => '', 'variable' => 'general_no_of_mo_voice_minutes_prepaid','desc' => 'No. of MO Voice Minutes', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        $opco_config[5]= array('id' => '', 'variable' => 'general_no_of_mo_voice_minutes_postpaid','desc' => 'No. of MO Voice Minutes', 'value_desc' => 'Postpaid', 'value' => '', 'group' => 'general');
        $opco_config[6]= array('id' => '', 'variable' => 'general_inbound_roaming_revenue','desc' => 'Inbound Roaming Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[7]= array('id' => '', 'variable' => 'general_international_interconnect_revenue','desc' => 'International Interconnect Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[8]= array('id' => '', 'variable' => 'general_total_mt_international_minutes','desc' => 'Total MT International minutes', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[9]= array('id' => '', 'variable' => 'general_international_interconnect_cost','desc' => 'International Interconnect Cost', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[10]= array('id' => '', 'variable' => 'general_total_mo_international_minutes','desc' => 'Total MO International minutes', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[11]= array('id' => '', 'variable' => 'general_total_bu_revenue','desc' => 'Total BU Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[12]= array('id' => '', 'variable' => 'general_in_system','desc' => 'IN system', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[13]= array('id' => '', 'variable' => 'general_gl_accounting_system','desc' => 'GL Accounting System', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[14]= array('id' => '', 'variable' => 'general_third_parties_provision','desc' => 'Third parties', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[15]= array('id' => '', 'variable' => 'general_third_parties_bad_debts','desc' => 'Third parties', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[16]= array('id' => '', 'variable' => 'general_postpaid_defaulters_provision','desc' => 'Postpaid defaulters', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[17]= array('id' => '', 'variable' => 'general_postpaid_defaulters_bad_debts','desc' => 'Postpaid defaulters', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[18]= array('id' => '', 'variable' => 'general_prepaid_customers_provision','desc' => 'Prepaid Customers', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[19]= array('id' => '', 'variable' => 'general_prepaid_customers_bad_debts','desc' => 'Prepaid Customers', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[20]= array('id' => '', 'variable' => 'general_local_interconnect_partners_provision','desc' => 'Local Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[21]= array('id' => '', 'variable' => 'general_local_interconnect_partners_bad_debts','desc' => 'Local Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[22]= array('id' => '', 'variable' => 'general_international_interconnect_partners_provision','desc' => 'International Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[23]= array('id' => '', 'variable' => 'general_international_interconnect_partners_bad_debts','desc' => 'International Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[24]= array('id' => '', 'variable' => 'general_roaming_partners_provision','desc' => 'Roaming Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[25]= array('id' => '', 'variable' => 'general_roaming_partners_bad_debts','desc' => 'Roaming Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        $opco_config[26]= array('id' => '', 'variable' => 'general_mfs_provision','desc' => 'MFS', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[27]= array('id' => '', 'variable' => 'general_mfs_bad_debts','desc' => 'MFS', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');


        //group bypass
        $opco_config[28]= array('id' => '', 'variable' => 'bypass_avg_disconnecting_duration','desc' => 'Avg. Disconnecting Duration (Minutes)', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[29]= array('id' => '', 'variable' => 'bypass_on_net_terminated_minutes','desc' => 'On-net Terminated minutes', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[30]= array('id' => '', 'variable' => 'bypass_off_net_terminated_minutes','desc' => 'Off-net Terminated minutes', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[31]= array('id' => '', 'variable' => 'bypass_on_net_suspended_sims','desc' => 'On-net Suspended SIMs', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[32]= array('id' => '', 'variable' => 'bypass_off_net_suspended_sims','desc' => 'Off-net Suspended SIMs', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[33]= array('id' => '', 'variable' => 'bypass_leakage_per_sim','desc' => 'Leakage Per Sim', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[34]= array('id' => '', 'variable' => 'bypass_leakage','desc' => 'Leakage', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[35]= array('id' => '', 'variable' => 'bypass_recovery','desc' => 'Recovery', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[36]= array('id' => '', 'variable' => 'bypass_prevented','desc' => 'Prevented', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[37]= array('id' => '', 'variable' => 'bypass_detection_cost','desc' => 'Detection Cost', 'value_desc' => '', 'value' => '', 'group' => 'bypass');

        //group MFS
        $opco_config[38]= array('id' => '', 'variable' => 'mfs_payments_no_of_fraud_incidents','desc' => 'Payments', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[39]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_customers','desc' => 'Payments', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[40]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_bu','desc' => 'Payments', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        
        $opco_config[41]= array('id' => '', 'variable' => 'mfs_transfers_no_of_fraud_incidents','desc' => 'Transfers', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[42]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_customers','desc' => 'Transfers', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[43]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_bu','desc' => 'Transfers', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[44]= array('id' => '', 'variable' => 'mfs_accounts_no_of_fraud_incidents','desc' => 'Accounts', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[45]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_customers','desc' => 'Accounts', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[46]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_bu','desc' => 'Accounts', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[47]= array('id' => '', 'variable' => 'mfs_loans_no_of_fraud_incidents','desc' => 'Loans', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[48]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_customers','desc' => 'Loans', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[49]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_bu','desc' => 'Loans', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[50]= array('id' => '', 'variable' => 'mfs_insurance_no_of_fraud_incidents','desc' => 'Insurance', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[51]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_customers','desc' => 'Insurance', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[52]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_bu','desc' => 'Insurance', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        $opco_config[53]= array('id' => '', 'variable' => 'mfs_merchant_services_no_of_fraud_incidents','desc' => 'Merchant Services', 'value_desc' => 'No. of Fraud Incidents', 'value' => '', 'group' => 'mfs');
        $opco_config[54]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_customers','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[55]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_bu','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');

        //group currency
        $opco_config[56]= array('id' => '', 'variable' => 'currency_currency_symbol','desc' => 'Currency Symbol', 'value_desc' => '', 'value' => '', 'group' => 'currency');
        $opco_config[57]= array('id' => '', 'variable' => 'currency_rate','desc' => 'Rate', 'value_desc' => '', 'value' => '', 'group' => 'currency');

        //EXTEND
        $opco_config[58]= array('id' => '', 'variable' => 'general_over_charging','desc' => 'Over Charging', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[59]= array('id' => '', 'variable' => 'general_deferred_balance','desc' => 'Deferred Balance', 'value_desc' => '', 'value' => '', 'group' => 'general');

        //group Control Coverage
        $opco_config[60]= array('id' => '', 'variable' => 'control_coverage_fm','desc' => 'FM', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[61]= array('id' => '', 'variable' => 'control_coverage_ra','desc' => 'RA', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[62]= array('id' => '', 'variable' => 'control_coverage_mfs','desc' => 'MFS', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[63]= array('id' => '', 'variable' => 'control_coverage_bypass','desc' => 'ByPass', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');

        // USD Mirror
        $opco_config[64]= array('id' => '', 'variable' => 'general_inbound_roaming_revenue_lc','desc' => 'Inbound Roaming Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        
        $opco_config[65]= array('id' => '', 'variable' => 'general_international_interconnect_revenue_lc','desc' => 'International Interconnect Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[66]= array('id' => '', 'variable' => 'general_international_interconnect_cost_lc','desc' => 'International Interconnect Cost', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[67]= array('id' => '', 'variable' => 'general_total_bu_revenue_lc','desc' => 'Total BU Revenue', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[68]= array('id' => '', 'variable' => 'general_in_system_lc','desc' => 'IN system', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[69]= array('id' => '', 'variable' => 'general_gl_accounting_system_lc','desc' => 'GL Accounting System', 'value_desc' => '', 'value' => '', 'group' => 'general');
                
        $opco_config[70]= array('id' => '', 'variable' => 'general_over_charging_lc','desc' => 'Over Charging', 'value_desc' => '', 'value' => '', 'group' => 'general');
        $opco_config[71]= array('id' => '', 'variable' => 'general_deferred_balance_lc','desc' => 'Deferred Balance', 'value_desc' => '', 'value' => '', 'group' => 'general');
        
        $opco_config[72]= array('id' => '', 'variable' => 'general_third_parties_provision_lc','desc' => 'Third parties', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[73]= array('id' => '', 'variable' => 'general_third_parties_bad_debts_lc','desc' => 'Third parties', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');

        $opco_config[74]= array('id' => '', 'variable' => 'general_postpaid_defaulters_provision_lc','desc' => 'Postpaid defaulters', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[75]= array('id' => '', 'variable' => 'general_postpaid_defaulters_bad_debts_lc','desc' => 'Postpaid defaulters', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[76]= array('id' => '', 'variable' => 'general_prepaid_customers_provision_lc','desc' => 'Prepaid Customers', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[77]= array('id' => '', 'variable' => 'general_prepaid_customers_bad_debts_lc','desc' => 'Prepaid Customers', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[78]= array('id' => '', 'variable' => 'general_local_interconnect_partners_provision_lc','desc' => 'Local Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[79]= array('id' => '', 'variable' => 'general_local_interconnect_partners_bad_debts_lc','desc' => 'Local Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[80]= array('id' => '', 'variable' => 'general_international_interconnect_partners_provision_lc','desc' => 'International Interconnect Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[81]= array('id' => '', 'variable' => 'general_international_interconnect_partners_bad_debts_lc','desc' => 'International Interconnect Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[82]= array('id' => '', 'variable' => 'general_roaming_partners_provision_lc','desc' => 'Roaming Partners', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[83]= array('id' => '', 'variable' => 'general_roaming_partners_bad_debts_lc','desc' => 'Roaming Partners', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');
        
        $opco_config[84]= array('id' => '', 'variable' => 'general_mfs_provision_lc','desc' => 'MFS', 'value_desc' => 'Provision', 'value' => '', 'group' => 'general');
        $opco_config[85]= array('id' => '', 'variable' => 'general_mfs_bad_debts_lc','desc' => 'MFS', 'value_desc' => 'Bad debts', 'value' => '', 'group' => 'general');



        
        $opco_config[86]= array('id' => '', 'variable' => 'bypass_leakage_per_sim_lc','desc' => 'Leakage Per Sim', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[87]= array('id' => '', 'variable' => 'bypass_leakage_lc','desc' => 'Leakage', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[88]= array('id' => '', 'variable' => 'bypass_recovery_lc','desc' => 'Recovery', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[89]= array('id' => '', 'variable' => 'bypass_prevented_lc','desc' => 'Prevented', 'value_desc' => '', 'value' => '', 'group' => 'bypass');
        $opco_config[90]= array('id' => '', 'variable' => 'bypass_detection_cost_lc','desc' => 'Detection Cost', 'value_desc' => '', 'value' => '', 'group' => 'bypass');


        
        $opco_config[91]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_customers_lc','desc' => 'Payments', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        
        $opco_config[92]= array('id' => '', 'variable' => 'mfs_payments_financial_losses_to_bu_lc','desc' => 'Payments', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[93]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_customers_lc','desc' => 'Transfers', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[94]= array('id' => '', 'variable' => 'mfs_transfers_financial_losses_to_bu_lc','desc' => 'Transfers', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[95]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_customers_lc','desc' => 'Accounts', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[96]= array('id' => '', 'variable' => 'mfs_accounts_financial_losses_to_bu_lc','desc_lc' => 'Accounts', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[97]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_customers_lc','desc' => 'Loans', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[98]= array('id' => '', 'variable' => 'mfs_loans_financial_losses_to_bu_lc','desc' => 'Loans', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[99]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_customers_lc','desc' => 'Insurance', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[100]= array('id' => '', 'variable' => 'mfs_insurance_financial_losses_to_bu_lc','desc' => 'Insurance', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');
        $opco_config[101]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_customers_lc','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to Customers', 'value' => '', 'group' => 'mfs');
        $opco_config[102]= array('id' => '', 'variable' => 'mfs_merchant_services_financial_losses_to_bu_lc','desc' => 'Merchant Services', 'value_desc' => 'Financial Losses to BU', 'value' => '', 'group' => 'mfs');     


        return $opco_config;

    }


    public function default_config(){
        $opco_config[0]=array('id'=>'','variable'=>'general_number_of_subscribers_prepaid','desc'=>'Number of Subscribers','value_desc'=>'Prepaid','value'=>'','group'=>'general');
        $opco_config[1]=array('id'=>'','variable'=>'general_number_of_subscribers_postpad','desc'=>'Number of Subscribers','value_desc'=>'Pospaid','value'=>'','group'=>'general');
        $opco_config[2]=array('id'=>'','variable'=>'general_gross_adds_prepaid','desc'=>'Gross-Adds','value_desc'=>'Prepaid','value'=>'','group'=>'general');
        $opco_config[3]=array('id'=>'','variable'=>'general_gross_adds_postpaid','desc'=>'Gross-Adds','value_desc'=>'Pospaid','value'=>'','group'=>'general');
        $opco_config[4]=array('id'=>'','variable'=>'general_churn_prepaid','desc'=>'Churn','value_desc'=>'Prepaid','value'=>'','group'=>'general');
        $opco_config[5]=array('id'=>'','variable'=>'general_churn_postpaid','desc'=>'Churn','value_desc'=>'Pospaid','value'=>'','group'=>'general');
        $opco_config[6]=array('id'=>'','variable'=>'general_no_of_mo_voice_minutes_prepaid','desc'=>'No. of MO Voice minutes','value_desc'=>'Prepaid','value'=>'','group'=>'general');
        $opco_config[7]=array('id'=>'','variable'=>'general_no_of_mo_voice_minutes_postpaid','desc'=>'No. of MO Voice minutes','value_desc'=>'Pospaid','value'=>'','group'=>'general');
        $opco_config[8]=array('id'=>'','variable'=>'general_total_mt_international_minutes','desc'=>'Total MT International minutes','value_desc'=>'','value'=>'','group'=>'general');
        $opco_config[9]=array('id'=>'','variable'=>'general_total_mo_international_minutes','desc'=>'Total MO International minutes','value_desc'=>'','value'=>'','group'=>'general');
        $opco_config[10]=array('id'=>'','variable'=>'general_deferred_revenue_balance_in_system','desc'=>'Deferred revenue balance (IN System)','value_desc'=>'','value'=>'','group'=>'general');
        $opco_config[11]=array('id'=>'','variable'=>'general_deferred_revenue_balance_gl_system','desc'=>'Deferred revenue balance (GL System)','value_desc'=>'','value'=>'','group'=>'general');
        $opco_config[12]=array('id'=>'','variable'=>'bad_debts_third_parties','desc'=>'Third parties','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[13]=array('id'=>'','variable'=>'bad_debts_third_parties_lc','desc'=>'Third parties','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[14]=array('id'=>'','variable'=>'bad_debts_postpaid_defaulters','desc'=>'Postpaid defaulters','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[15]=array('id'=>'','variable'=>'bad_debts_postpaid_defaulters_lc','desc'=>'Postpaid defaulters','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[16]=array('id'=>'','variable'=>'bad_debts_prepaid_customers','desc'=>'Prepaid Customers','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[17]=array('id'=>'','variable'=>'bad_debts_prepaid_customers_lc','desc'=>'Prepaid Customers','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[18]=array('id'=>'','variable'=>'bad_debts_local_interconnect_partners','desc'=>'Local Interconnect Partners','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[19]=array('id'=>'','variable'=>'bad_debts_local_interconnect_partners_lc','desc'=>'Local Interconnect Partners','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[20]=array('id'=>'','variable'=>'bad_debts_international_interconnect_partners','desc'=>'International Interconnect Partners','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[21]=array('id'=>'','variable'=>'bad_debts_international_interconnect_partners_lc','desc'=>'International Interconnect Partners','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[22]=array('id'=>'','variable'=>'bad_debts_roaming_partners','desc'=>'Roaming Partners','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[23]=array('id'=>'','variable'=>'bad_debts_roaming_partners_lc','desc'=>'Roaming Partners lc','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[24]=array('id'=>'','variable'=>'bad_debts_mfs','desc'=>'MFS','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[25]=array('id'=>'','variable'=>'bad_debts_mfs_lc','desc'=>'MFS lc','value_desc'=>'','value'=>'','group'=>'bad_debts');
        $opco_config[26]=array('id'=>'','variable'=>'total_operating_revenue_sales_of_equipment_and_accessories','desc'=>'Sales of equipment & accessories','value_desc'=>'','value'=>'','group'=>'total_operating_revenue');
        $opco_config[27]=array('id'=>'','variable'=>'total_operating_revenue_sales_of_equipment_and_accessories_lc','desc'=>'Sales of equipment & accessories','value_desc'=>'','value'=>'','group'=>'total_operating_revenue');
        $opco_config[28]=array('id'=>'','variable'=>'total_operating_revenue_other_revenue_and_operating_income','desc'=>'Other revenue & operating income','value_desc'=>'','value'=>'','group'=>'total_operating_revenue');
        $opco_config[29]=array('id'=>'','variable'=>'total_operating_revenue_other_revenue_and_operating_income_lc','desc'=>'Other revenue & operating income','value_desc'=>'','value'=>'','group'=>'total_operating_revenue');
        $opco_config[30]=array('id'=>'','variable'=>'service_revenue_standard_voice_revenue_mobile','desc'=>'Standard Voice Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[31]=array('id'=>'','variable'=>'service_revenue_standard_voice_revenue_mobile_lc','desc'=>'Standard Voice Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[32]=array('id'=>'','variable'=>'service_revenue_national_interconnection_revenue_mobile','desc'=>'National Interconnection Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[33]=array('id'=>'','variable'=>'service_revenue_national_interconnection_revenue_mobile_lc','desc'=>'National Interconnection Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[34]=array('id'=>'','variable'=>'service_revenue_international_interconnection_revenue_mobile','desc'=>'International Interconnection Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[35]=array('id'=>'','variable'=>'service_revenue_international_interconnection_revenue_mobile_lc','desc'=>'International Interconnection Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[36]=array('id'=>'','variable'=>'service_revenue_data_revenue_mobile','desc'=>'Data Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[37]=array('id'=>'','variable'=>'service_revenue_data_revenue_mobile_lc','desc'=>'Data Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[38]=array('id'=>'','variable'=>'service_revenue_mfs_revenue_mobile','desc'=>'MFS Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[39]=array('id'=>'','variable'=>'service_revenue_mfs_revenue_mobile_lc','desc'=>'MFS Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[40]=array('id'=>'','variable'=>'service_revenue_guest_roaming_revenue_mobile','desc'=>'Guest Roaming Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[41]=array('id'=>'','variable'=>'service_revenue_guest_roaming_revenue_mobile_lc','desc'=>'Guest Roaming Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[42]=array('id'=>'','variable'=>'service_revenue_messaging_revenue_mobile_','desc'=>'Messaging Revenue (Mobile) ','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[43]=array('id'=>'','variable'=>'service_revenue_messaging_revenue_mobile_lc','desc'=>'Messaging Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[44]=array('id'=>'','variable'=>'service_revenue_value_added_services_revenue_mobile','desc'=>'Value Added Services revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[45]=array('id'=>'','variable'=>'service_revenue_value_added_services_revenue_mobile_lc','desc'=>'Value Added Services revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[46]=array('id'=>'','variable'=>'service_revenue_connection_fees_mobile','desc'=>'Connection fees (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[47]=array('id'=>'','variable'=>'service_revenue_connection_fees_mobile_lc','desc'=>'Connection fees (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[48]=array('id'=>'','variable'=>'service_revenue_other_service_revenue_mobile','desc'=>'Other Service Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[49]=array('id'=>'','variable'=>'service_revenue_other_service_revenue_mobile_lc','desc'=>'Other Service Revenue (Mobile)','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[50]=array('id'=>'','variable'=>'service_revenue_fixed_line_revenue','desc'=>'Fixed Line Revenue','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[51]=array('id'=>'','variable'=>'service_revenue_fixed_line_revenue_lc','desc'=>'Fixed Line Revenue','value_desc'=>'','value'=>'','group'=>'service_revenue');
        $opco_config[52]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_roaming_wholesale_cost_mobile','desc'=>'Roaming Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');
        $opco_config[53]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_roaming_wholesale_cost_mobile_lc','desc'=>'Roaming Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');
        $opco_config[54]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_local_interconnection_wholesale_cost_mobile','desc'=>'Local Interconnection Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');
        $opco_config[55]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_local_interconnection_wholesale_cost_mobile_lc','desc'=>'Local Interconnection Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');
        //$opco_config[56]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_local_interconnection_wholesale_cost_mobile','desc'=>'Local Interconnection Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');
        //$opco_config[57]=array('id'=>'','variable'=>'roaming_and_interconnect_cost_local_interconnection_wholesale_cost_mobile_lc','desc'=>'Local Interconnection Wholesale Cost (Mobile)','value_desc'=>'','value'=>'','group'=>'roaming_and_interconnect_cost');

                //group Control Coverage
        $opco_config[56]= array('id' => '', 'variable' => 'control_coverage_fm','desc' => 'FM', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[57]= array('id' => '', 'variable' => 'control_coverage_ra','desc' => 'RA', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[58]= array('id' => '', 'variable' => 'control_coverage_mfs','desc' => 'MFS', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');
        $opco_config[59]= array('id' => '', 'variable' => 'control_coverage_bypass','desc' => 'ByPass', 'value_desc' => '', 'value' => '', 'group' => 'control_coverage');

        $opco_config[60]= array('id' => '', 'variable' => 'currency_currency_symbol','desc' => 'Currency Symbol', 'value_desc' => '', 'value' => '', 'group' => 'currency');
        $opco_config[61]= array('id' => '', 'variable' => 'currency_rate','desc' => 'Rate', 'value_desc' => '', 'value' => '', 'group' => 'currency');

        return $opco_config;
    }

    public function opco_config($opco_id,$month,$year,$mode){
        if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
            $sql = 'SELECT * FROM cms_opco_list';
            $opco_list = $this->db->query($sql);
        }else{
            $sql=   'SELECT
                        cms_opco_list.opcoid,
                        cms_opco_list.opconame,
                        map_group_opco.add_opco_config
                        FROM
                        map_group_opco
                    INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                    WHERE map_group_opco.group_id=? AND map_group_opco.add_opco_config=1';
            $opco_list = $this->db->query($sql,array($this->session->userdata('group_id')));
        }
        
        // gate rate information from cms_currency_exhange
        $sql = 'SELECT
                    cms_opco_list.opcoid,
                    cms_opco_list.opconame,
                    cms_opco_list.currency_id,
                    cms_currency_exchange.`year`,
                    cms_currency_exchange.`month`,
                    cms_currency_exchange.rate,
                    map_vipgroup_currency.currencysymbol
                FROM
                    cms_opco_list
                LEFT OUTER JOIN cms_currency_exchange ON cms_opco_list.currency_id = cms_currency_exchange.currency_id
                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                WHERE cms_opco_list.opcoid=? AND cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? ';

        $dt_rate = $this->db->query($sql,array($opco_id,$year,$month));
        //echo $this->db->last_query();
        //die;
        if($dt_rate->num_rows()>0){
            $row = $dt_rate->row();
            $config['rate'] = $row->rate;
        }else{
            $config['rate'] = 0;
        }
        //echo $this->db->last_query();
        //echo $config['rate'];
        //die;
        //-----

        // get Default 

        $sql = 'SELECT
                cms_opco_list.opcoid,
                cms_opco_list.opconame,
                cms_opco_list.currency_id,
                map_vipgroup_currency.currencysymbol
                FROM
                cms_opco_list
                INNER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                 WHERE opcoid=?';
        $dt_opco = $this->db->query($sql,array($opco_id));

        $d = $dt_opco->row();
        $config['symbol'] = $d->currencysymbol;

        
        $select_opco = '<select class="std-select" name="id-opco" id="id-opco" disabled>';
        $select_opco2 = '<select class="std-select width250" name="new-id-opco" id="new-id-opco">';
        foreach ($opco_list->result() as $row) {
            if($row->opcoid == $opco_id) $opco_selected = 'selected'; else $opco_selected ='';
            $select_opco .= '<option value="'.$row->opcoid.'" '.$opco_selected.'>'.$row->opconame.'</option>';
            $select_opco2 .= '<option value="'.$row->opcoid.'" '.$opco_selected.'>'.$row->opconame.'</option>';
        }

        $select_opco .= '</select>';
        $select_opco2 .= '</select>';
        $config['select_opco'] = $select_opco;
        $config['select_opco2'] = $select_opco2;

        $array_month = array();
        $array_month[0] = array('month_name'=>$this->lang->line('January'),'month' => '01');
        $array_month[1] = array('month_name'=>$this->lang->line('February'),'month' => '02');
        $array_month[2] = array('month_name'=>$this->lang->line('March'),'month' => '03');
        $array_month[3] = array('month_name'=>$this->lang->line('April'),'month' => '04');
        $array_month[4] = array('month_name'=>$this->lang->line('May'),'month' => '05');
        $array_month[5] = array('month_name'=>$this->lang->line('June'),'month' => '06');
        $array_month[6] = array('month_name'=>$this->lang->line('July'),'month' => '07');
        $array_month[7] = array('month_name'=>$this->lang->line('August'),'month' => '08');
        $array_month[8] = array('month_name'=>$this->lang->line('September'),'month' => '09');
        $array_month[9] = array('month_name'=>$this->lang->line('October'),'month' => '10');
        $array_month[10] = array('month_name'=>$this->lang->line('November'),'month' => '11');
        $array_month[11] = array('month_name'=>$this->lang->line('December'),'month' => '12');

        $select_month ='<select class="std-select" name="month" id="month" disabled>';
        $select_month2 ='<select class="std-select" name="month" id="new-month">';
        foreach ($array_month as $row) {
            if($row['month']==$month) {
                $month_selected = 'selected'; 
            }else {
                $month_selected='';
            }
            $select_month .= '<option value="'.$row['month'].'" '.$month_selected.'>'.$row['month_name'].'</option>';
            $select_month2 .= '<option value="'.$row['month'].'" '.$month_selected.'>'.$row['month_name'].'</option>'; 
        }
        $select_month .= '</select>';
        $select_month2 .= '</select>';


        $config['select_month']= $select_month;
        $config['select_month2']= $select_month2;

        $array_year     = array('2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026');
        $select_year    = '<select class="std-select" name="year" id="year" disabled>';
        $select_year2   = '<select class="std-select" name="year" id="new-year">';
        
        foreach ($array_year as $row) {
            # code...
            if($row==$year) $year_selected='selected'; else $year_selected='';
            $select_year .= '<option value='.$row.' '.$year_selected.'>'.$row.'</option>';
            $select_year2 .= '<option value='.$row.' '.$year_selected.'>'.$row.'</option>';
        }
        $select_year .= '</select>';
        $select_year2 .= '</select>';

        $config['select_year'] = $select_year;
        $config['select_year2'] = $select_year2;


        $select_date = $year.'-'.$month.'-01 00:00:00';


        $sql = 'SELECT * FROM cms_opco_config
                WHERE opcoid=? AND config_date=?';
        $dt_opco_config = $this->db->query($sql,array($opco_id,$select_date));

        
        if($dt_opco_config->num_rows()>0){
            $config['message'] = 0;
            $opco_config = $this->get_opco_config($opco_id,$month,$year);

        }else{
            $config['message'] = 1;
            $opco_config = $this->default_config();
        }

        $sql = 'SELECT * FROM map_vipgroup_currency';
        $dt = $this->db->query($sql);
        $config['currency'] = $dt;

        $config['config'] = $opco_config;
        $config['mode']=$mode;
        
        
        $config['current_month'] = date("m");
        if(strlen($config['current_month'])==1) $config['current_month'] = '0'.$config['current_month'];
        $config['current_year'] = date("Y");
        $config['opco_id'] = $opco_id;
        $sql = 'SELECT * FROM cms_opco_list WHERE opcoid=?';
        $dt = $this->db->query($sql,array($opco_id));
        if($dt->num_rows()>0){
            $ret = $dt->row();
            $config['opco_name'] = $ret->opconame;
        }else{
            $config['opco_name'] = '';
        }
        $config['month'] = $month;
        $config['year'] = $year;


        $output = $this->load->view('template/opco_config_detail',$config,TRUE);
        return $output;
            
    }

    public function check_opco_config($opcoid,$month,$year){
        $date_data = $year.'-'.$month.'-01 00:00:00';
        
        $sql = 'SELECT * FROM cms_opco_config WHERE config_date=? AND opcoid=?';
        $data = $this->db->query($sql,array($date_data,$opcoid));
        
        if($data->num_rows()>0){
            return '1';
        }else{
            return '0';
        }
    }

    public function add_opco_config($json,$id_user){
        $json_obj = json_decode($json);
        $out = ''; 
        $date_check = $json_obj[0]->config_date;
        $opcoid_check = $json_obj[0]->opcoid;
        $sql = 'SELECT * FROM cms_opco_config WHERE config_date=? AND opcoid=?';
        $data = $this->db->query($sql,array($date_check,$opcoid_check));

        if($data->num_rows()>0){
            return '1';
        }else{
            //prosess 
            foreach ($json_obj as $row) {
                # code...
                $sql = 'INSERT INTO cms_opco_config(config_date,opcoid,config_key,config_value) VALUES (?,?,?,?)';
                $this->db->query($sql,array($row->config_date,$row->opcoid,$row->variable,$row->value));

            }

            $desc = 'Add OpCo Configuration id# '.$opcoid_check;
            $activity = 'Add OpCo Configuration';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }

    }

    public function update_opco_config($json,$id_user){
        $this->db->db_debug = true;
        $json_obj = json_decode($json);
        $out = ''; 
        $date_check = $json_obj[0]->config_date;
        $opcoid_check = $json_obj[0]->opcoid;
        //prosess 
        foreach ($json_obj as $row) {
            # code...
            //$sql2 = 'UPDATE cms_opco_config SET config_value="'.$row->value.'" WHERE opcoid="'.$row->opcoid.'" AND config_date="'.$row->config_date.'" AND config_key="'.$row->variable.'"';
            //echo $sql2.'<br>';

            // search
            $sql = 'SELECT * FROM cms_opco_config WHERE opcoid=? AND config_date=? AND config_key=?';
            $s = $this->db->query($sql,array($row->opcoid,$row->config_date,$row->variable)); 
            if($s->num_rows()>0){
                $sql = 'UPDATE cms_opco_config SET config_value=? WHERE opcoid=? AND config_date=? AND config_key=?';
                $this->db->query($sql,array($row->value,$row->opcoid,$row->config_date,$row->variable));
            }else{
                $sql = 'INSERT INTO cms_opco_config(config_date,opcoid,config_key,config_value) VALUES (?,?,?,?)';
                $this->db->query($sql,array($row->config_date,$row->opcoid,$row->variable,$row->value));
            }

        }

        $desc = 'Update OpCo Configuration id# '.$opcoid_check;
        $activity = 'Update OpCo Configuration';
        $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
        $this->db->query($sql_log,array($id_user,$activity,$desc));
        return '0'; 
        

    }


    public function get_opco_config($opco_id,$month,$year){
        /*
        $opco_config[0]= array('id' => '1', 'variable' => 'general_numbers_of_subscribers_prepaid','desc' => 'Numbers of Subscribers Postpaid', 'value_desc' => 'Prepaid', 'value' => '', 'group' => 'general');
        */

        $date_config = $year.'-'.$month.'-01 00:00:00';
        $sql = 'SELECT * FROM cms_opco_config WHERE opcoid=? AND config_date=?';
        $data = $this->db->query($sql,array($opco_id,$date_config));
        $opco_config = $this->default_config();
        
        foreach ($data->result() as $row) {
            $key = array_search($row->config_key, array_column($opco_config, 'variable'));
            if($key >= 0) {
                //echo $row->config_value;
                $opco_config[$key]['value']=$row->config_value;
                $opco_config[$key]['id']=$row->opcoid;
            }
        }
        return $opco_config;
    }

    public function rut(){
        $sql = 'SELECT * FROM cms_risk_rut ORDER BY rutid ASC';
        $dt = $this->db->query($sql);
        return $dt;
    }

    public function add_rut($rutname,$bypass,$id_user){
        $sql ='INSERT INTO cms_risk_rut(rutname,bypass) VALUES (?,?)';
        $data = $this->db->query($sql,array($rutname,$bypass));
        if($data){
            $desc = 'Add Risk Universe Type name# '.$rutname;
            $activity = 'Add Risk Universe Type';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }

    public function delete_rut($rutid,$id_user){
        $sql ='DELETE FROM cms_risk_rut WHERE rutid=?';
        $data = $this->db->query($sql,array($rutid));
        if($data){
            $desc = 'Delete Risk Universe Type id# '.$rutid;
            $activity = 'Delete Risk Universe Type';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }

    public function update_rut($rutid,$rutname,$bypass,$id_user){

        // old rut
        $sql = 'SELECT * FROM cms_risk_rut WHERE rutid=?';
        $old = $this->db->query($sql,array($rutid));
        $old_data = $old->row();

        $sql ='UPDATE cms_risk_rut SET rutname=?, bypass=? WHERE rutid=?';
        $data = $this->db->query($sql,array($rutname,$bypass,$rutid));
        if($data){
            $desc = 'Update Risk Universe Type "'.$old_data->rutname.'"->"'.$rutname.'"';
            $activity = 'Update Risk Universe Type';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }

    public function get_rut(){
        $sql = 'SELECT * FROM cms_risk_rut ORDER BY rutid ASC';
        return $this->db->query($sql);
    }

    public function get_st($rutid){
        $sql = 'SELECT
                    cms_risk_mapping.rutid,
                    cms_risk_mapping.stid,
                    cms_risk_st.stname
                FROM
                    cms_risk_mapping
                INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
                WHERE cms_risk_mapping.rutid = ?
                ORDER BY cms_risk_mapping.stid';
        $data = $this->db->query($sql,array($rutid));

        return $data;
    }

    public function insert_st($rutid,$stname,$id_user){
        // Check Duplicate
        $sql = 'SELECT
                cms_risk_mapping.mid,
                cms_risk_mapping.rutid,
                cms_risk_mapping.stid,
                cms_risk_st.stname
                FROM
                cms_risk_mapping
                INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
                WHERE TRIM(UPPER(cms_risk_st.stname))=TRIM(UPPER(?)) AND cms_risk_mapping.rutid=?';

        $check = $this->db->query($sql,array($stname,$rutid));


        if($check->num_rows() > 0){
                return '1';
                exit;
        }

        $sql = 'INSERT INTO cms_risk_st(stname) VALUES (?)';
        $data = $this->db->query($sql,array($stname));
        if($data){
            $last_insert_id = $this->db->insert_id();
            $sqlw = 'INSERT INTO cms_risk_mapping(rutid,stid) VALUES (?,?)';
            $this->db->query($sqlw,array($rutid,$last_insert_id));

            $sqloc = 'SELECT * FROM cms_so_mapping WHERE stid=? ORDER BY soid,stid ASC LIMIT 1,1';

            $get_so_mapping = $this->db->query($sqloc,array($last_insert_id));
            if($get_so_mapping->num_rows==0){
                $sql_get_first_oc = 'SELECT * FROM cms_risk_oc ORDER BY ocid ASC';
                $dt_get_first_oc = $this->db->query($sql_get_first_oc);
                if($dt_get_first_oc->num_rows()==0){
                    $sql_insert_oc='INSERT INTO cms_risk_oc(ocname) VALUES("All Operator Type")';
                    $dt_insert_oc = $this->db->query($sql_insert_oc);
                    $ocid = $this->db->insert_id();
                }else{
                    $row=$dt_get_first_oc->row();
                    $ocid=$row->ocid;
                }

                $sqlw = 'INSERT INTO cms_so_mapping(stid,ocid) VALUES (?,?)';
                $this->db->query($sqlw,array($last_insert_id,$ocid));
            }



            $desc = 'Add Section Title name# '.$stname;
            $activity = 'Add Section Title';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0';

        }else{
            return '1';
        }
    }

    public function delete_st($stid,$id_user){
        $sql ='DELETE FROM cms_risk_st WHERE stid=?';
        $data = $this->db->query($sql,array($stid));
        if($data){
            $desc = 'Delete Section Title id# '.$stid;
            $activity = 'Delete Section Title';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }    

    public function update_st($rutid,$stid,$stname,$id_user){
        //Check duplicate
        $sql = 'SELECT
                cms_risk_mapping.mid,
                cms_risk_mapping.rutid,
                cms_risk_mapping.stid,
                cms_risk_st.stname
                FROM
                cms_risk_mapping
                INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
                WHERE UPPER(cms_risk_st.stname)=UPPER(?) AND cms_risk_mapping.rutid=? AND cms_risk_st.stid <> ?';
        $check = $this->db->query($sql,array($stname,$rutid,$stid));

        if($check->num_rows() > 0){
            return '1';
            exit;
        }

        // old
        $sql = 'SELECT * FROM cms_risk_st WHERE stid=?';
        $old = $this->db->query($sql,array($stid));
        $old_data =$old->row();

        $sql ='UPDATE cms_risk_st SET stname=? WHERE stid=?';
        $data = $this->db->query($sql,array($stname,$stid));
        if($data){
            $desc = 'Update Section Title "'.$old_data->stname.'"->"'.$stname.'" ';
            $activity = 'Update Section Title';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }
    }    

    public function get_oc($stid){
        $sql = 'SELECT
                    cms_so_mapping.soid,
                    cms_so_mapping.stid,
                    cms_so_mapping.ocid,
                    cms_risk_oc.ocname
                FROM
                    cms_so_mapping
                INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
                WHERE cms_so_mapping.stid=?';
        $data = $this->db->query($sql,array($stid));

        return $data;
    }    


    public function insert_oc($stid,$ocname,$id_user){
        // Check Duplicate
        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_oc.ocname
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
                WHERE UPPER(cms_risk_oc.ocname)=UPPER(?) AND cms_so_mapping.stid=?';
        
        $check = $this->db->query($sql, array($ocname,$stid));

        if($check->num_rows() > 0){
            return "1";
            exit;
        }


        $sql = 'INSERT INTO cms_risk_oc(ocname) VALUES (?)';
        $data = $this->db->query($sql,array($ocname));
        if($data){
            $last_insert_id = $this->db->insert_id();
            $sqlw = 'INSERT INTO cms_so_mapping(stid,ocid) VALUES (?,?)';
            $this->db->query($sqlw,array($stid,$last_insert_id));
            $desc = 'Add Operator Category name# '.$ocname;
            $activity = 'Add Operator Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0';

        }else{
            return '1';
        }
    }    

    public function delete_oc($ocid,$id_user){
        $sql ='DELETE FROM cms_risk_oc WHERE ocid=?';
        $data = $this->db->query($sql,array($ocid));
        if($data){
            $desc = 'Delete Operator Category id# '.$ocid;
            $activity = 'Delete Operator Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }    

    public function update_oc($stid,$ocid,$ocname,$id_user){
        //Check Duplicate
        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_oc.ocname
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
                WHERE UPPER(cms_risk_oc.ocname)=UPPER(?) AND cms_so_mapping.stid=? AND cms_so_mapping.ocid<>?';
        $check = $this->db->query($sql,array($ocname,$stid,$ocid));
        if($check->num_rows() > 0){
            return '1';
            exit;
        }

        $sql ='UPDATE cms_risk_oc SET ocname=? WHERE ocid=?';
        $data = $this->db->query($sql,array($ocname,$ocid));
        if($data){
            $desc = 'Update Operator Category id# '.$ocid;
            $activity = 'Update Operator Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }
    } 

    public function get_rc($stid,$coid){

        $sql = 'SELECT
                    cms_risk_rc.ref_number,
                    cms_risk_rc.rcid,
                    cms_risk_rc.soid,
                    cms_risk_rc.rcname,
                    cms_risk_rc.rcdec,
                    cms_so_mapping.stid,
                    cms_so_mapping.ocid
                FROM
                    cms_risk_rc
                INNER JOIN cms_so_mapping ON cms_risk_rc.soid = cms_so_mapping.soid
                WHERE cms_so_mapping.stid=? AND cms_so_mapping.ocid=?
                ORDER BY cms_risk_rc.ref_number ASC';
        $data = $this->db->query($sql,array($stid,$coid));

        return $data;
    }    

    public function insert_rc($ref_number,$stid,$ocid,$rcname,$rcdec,$id_user){
        //Check Duplicate
        
        if($ocid==''){
            // get default ocid
            $sqloc = 'SELECT * FROM cms_risk_oc ORDER BY ocid ASC LIMIT 1,1';
            $dt_oc = $this->db->query($sqloc);
            if($dt_oc->num_rows()>0){
                $row = $dt_oc->row();
                $ocid= $row->ocid;
            }else{
                $sql_insert_oc = 'INSERT INTO cms_risk_oc(ocname) VALUES ("All Operator Types")';
                $dt_insert_oc = $this->db->query($sql_insert_oc);
                $ocid= $this->db->insert_id();
            }

            // check mapping
            $sql='SELECT * FROM cms_so_mapping WHERE stid=? AND ocid=?';
            $dt = $this->db->query($sql,array($stid,$ocid));
            if($dt->num_rows()==0){
                $sql_insert = 'INSERT INTO cms_so_mapping (stid,ocid) VALUES (?,?)';
                $go_insert = $this->db->query($sql_insert,array($stid,$ocid));
            }
        }
        
        if (preg_match("/[A-Z]{1}\.[0-9]{2}/", $ref_number)==1 && trim(strlen($ref_number))==4) {
            
        } else {
            return '-2';
            exit;
        }

        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_rc.rcname
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_rc ON cms_so_mapping.soid = cms_risk_rc.soid
                WHERE UPPER(cms_risk_rc.rcname)=UPPER(?) AND cms_so_mapping.stid=?';
        $check = $this->db->query($sql,array($rcname,$stid));

        //echo $this->db->last_query();
        
        if($check->num_rows() > 0){
            return '1';
            exit;
        }

        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_rc.rcname
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_rc ON cms_so_mapping.soid = cms_risk_rc.soid
                WHERE UPPER(cms_risk_rc.ref_number)=UPPER(?) AND cms_so_mapping.stid=?';
        $check = $this->db->query($sql,array($ref_number,$stid));

        //echo $this->db->last_query();
        
        if($check->num_rows() > 0){
            return '-2';
            exit;
        }


        // get soid
        $sql = 'SELECT DISTINCT * FROM cms_so_mapping WHERE stid=? AND ocid=?';
        $r = $this->db->query($sql,array($stid,$ocid));
        $soid = '';
        foreach ($r->result() as $row) {
            # code...
            $soid = $row->soid;
        }
        //echo "stid=".$stid." -- ocid=".$ocid." -- soid = ".$soid;
        
        $sql = 'INSERT INTO cms_risk_rc(soid,rcname,rcdec,ref_number) VALUES (?,?,?,?)';
        $data = $this->db->query($sql,array($soid,$rcname,$rcdec,$ref_number));
        if($data){
            $last_insert_id = $this->db->insert_id();
            $desc = 'Add Risk Category name# '.$rcname;
            $activity = 'Add Risk Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0';

        }else{
            return '1';
        }
        
    }  

    public function delete_rc($rcid,$id_user){
        $sql ='DELETE FROM cms_risk_rc WHERE rcid=?';
        $data = $this->db->query($sql,array($rcid));
        if($data){
            $desc = 'Delete Risk Category id# '.$rcid;
            $activity = 'Delete Risk Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }

    }  

    public function get_first_rut_id(){
        $sql = 'SELECT * FROM cms_risk_rut  ORDER BY rutid ASC LIMIT 0,1';
        $data = $this->db->query($sql);
        $output = -1;
        foreach ($data->result() as $row) {
            # code...
            $output = $row->rutid;
        }
        return $output;

    }


    public function update_rc($stid,$ref_number, $ocid,$rcid,$rcname,$rcdec,$id_user){
        //Check Duplcate
        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_rc.rcname,
                cms_risk_rc.rcid
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_rc ON cms_so_mapping.soid = cms_risk_rc.soid
                WHERE UPPER(cms_risk_rc.rcname)=UPPER(?) AND cms_so_mapping.ocid=? AND cms_risk_rc.rcid <>? AND cms_so_mapping.stid=?';
        
        $check = $this->db->query($sql,array($rcname,$ocid,$rcid,$stid));
        if($check->num_rows() > 0){
            return '1';
            exit;
        }

        $sql = 'SELECT
                cms_so_mapping.soid,
                cms_so_mapping.stid,
                cms_so_mapping.ocid,
                cms_risk_rc.rcname,
                cms_risk_rc.rcid
                FROM
                cms_so_mapping
                INNER JOIN cms_risk_rc ON cms_so_mapping.soid = cms_risk_rc.soid
                WHERE UPPER(cms_risk_rc.ref_number)=UPPER(?) AND cms_so_mapping.ocid=? AND cms_risk_rc.rcid <>? AND cms_so_mapping.stid=?';
        
        $check = $this->db->query($sql,array($ref_number,$ocid,$rcid,$stid));
        if($check->num_rows() > 0){
            return '-2';
            exit;
        }

        if (preg_match("/[A-Z]{1}\.[0-9]{2}/", $ref_number)==1 && trim(strlen($ref_number))==4) {
            
        } else {
            return '-2';
            exit;
        }

        // old
        $sql = 'SELECT * FROM cms_risk_rc WHERE rcid=?';
        $old = $this->db->query($sql,array($rcid));
        $old_data = $old->row();
        $sql ='UPDATE cms_risk_rc SET rcname=?, rcdec=?, ref_number=?  WHERE rcid=?';
        $data = $this->db->query($sql,array($rcname,$rcdec, $ref_number, $rcid));
        if($data){
            $desc = 'Update Risk Category "'.$old_data->rcname.'"->"'.$rcname.'": desc "'.$old_data->rcdec.'"->"'.$rcdec.'"';
            $activity = 'Update Risk Category';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
            return '0'; 
        }else{
            return '1';
        }
    } 

    public function get_first_st_id($rutid){
        $sql = 'SELECT
                    cms_risk_mapping.rutid,
                    cms_risk_mapping.stid,
                    cms_risk_st.stname
                FROM
                    cms_risk_mapping
                INNER JOIN cms_risk_st ON cms_risk_mapping.stid = cms_risk_st.stid
                WHERE cms_risk_mapping.rutid = ?
                ORDER BY cms_risk_mapping.stid ASC LIMIT 0,1';
        $output = -1;
        $data = $this->db->query($sql,array($rutid));
        foreach ($data->result() as $row) {
            # code...
            $output = $row->stid;
        }

        return $output;

    }

    public function get_first_oc_id($stid){
        $sql = 'SELECT
                    cms_so_mapping.soid,
                    cms_so_mapping.stid,
                    cms_so_mapping.ocid,
                    cms_risk_oc.ocname
                FROM
                    cms_so_mapping
                INNER JOIN cms_risk_oc ON cms_so_mapping.ocid = cms_risk_oc.ocid
                WHERE cms_so_mapping.stid=?
                ORDER BY cms_so_mapping.soid ASC LIMIT 0,1';
        $output = -1;                
        $data = $this->db->query($sql,array($stid));
        foreach ($data->result() as $row) {
            # code...
            $output = $row->ocid;
        }
        return $output;
    }      

    public function add_control_coverage($ccname,$start_date,$end_date,$id_user){
        $sql = 'INSERT INTO cms_control_coverage(ccname,start_date,end_date) VALUES (?,?,?)';
        $status = $this->db->query($sql, array($ccname,$start_date,$end_date));
        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');

            $desc = 'Add Control Coverage name# '.$ccname;
            $activity = 'Add Control Coverage';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;
    }

    public function delete_control_coverage($ccid,$id_user){
        $sql = 'DELETE FROM cms_control_coverage WHERE ccid = ?';
        $status = $this->db->query($sql,array($ccid));
        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already deleted');

            $desc = 'Delete Control Coverage id# '.$ccid;
            $activity = 'Delete Control Coverage';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;
    }

    public function modify_control_coverage($ccid,$ccname,$start_date,$end_date,$id_user){
        $sql = 'UPDATE cms_control_coverage SET ccname=?,start_date=?,end_date=? WHERE ccid=?';
        $status = $this->db->query($sql, array($ccname,$start_date,$end_date,$ccid));
        if($status){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Control coverage already saved');

            $desc = 'Modify Control Coverage id# '.$ccid;
            $activity = 'Modify Control Coverage';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;
    }

    public function report_list(){
        $sql = 'SELECT * FROM cms_report';
        return $this->db->query($sql);
    }

    public function save_report($name,$path,$json,$id_user){
        $sql = 'INSERT INTO cms_report(report_name,report_path) VALUES (?,?)';
        $status = $this->db->query($sql,array($name,$path));
        if($status){
            $insert_id = $this->db->insert_id();
            $p = json_decode($json);

            foreach ($p as $param) {
                # code...
                $sql = 'INSERT INTO cms_report_parameter(report_id,name,multiselect) VALUES (?,?,?)';
                $this->db->query($sql,array($insert_id,$param->name,$param->multiselect));
            }

            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Report already saved');

            $desc = 'Add Report name# '.$name;
            $activity = 'Add Report';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }

    public function delete_report($id,$id_user){
        $sql = 'DELETE FROM cms_report WHERE reportid=?';
        $status = $this->db->query($sql,array($id));
        if($status){

            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Report deleted');

            $desc = 'Delete report id# '.$id;
            $activity = 'Delete Report';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }

    public function get_report($id){
        $sql = 'SELECT
                cms_report.reportid,
                cms_report.report_name,
                cms_report.report_path,
                cms_report_parameter.params_id,
                cms_report_parameter.`name`,
                cms_report_parameter.multiselect
                FROM
                cms_report
                LEFT OUTER JOIN cms_report_parameter ON cms_report.reportid = cms_report_parameter.report_id
                WHERE cms_report.reportid=?';
        $status = $this->db->query($sql,array($id));
        if($status){

            $out['status'] = 0;
            $out['msg'] = '';

            $out['report'] = array();
            $parameters = array();

            foreach ($status->result() as $report) {
                # code...
                $report_id = $report->reportid;
                $report_name = $report->report_name;
                $report_path = $report->report_path;
                $a = array('name'=>$report->name,'multiselect'=>$report->multiselect);
                array_push($parameters,$a);
            }

            $html = '';
            $html .= '<div class="group-table">';
            $html .= '  <div class="table-head">';
            $html .= '      <div class="table-head-item ct1">'.$this->lang->line('Parameter').'</div>';
            $html .= '      <div class="table-head-item ct2">'.$this->lang->line('Type').'</div>';
            $html .= '  </div>';
            $i = 0;
            foreach ($parameters as $p) {
                # code...
                $i++;
                $html .= '  <div class="table-body">';
                $html .= '      <div class="table-body-item ct1">';
                $html .= '         <input class="std-input" id="e_param_name_'.$i.'" value="'.$p['name'].'" disabled>';
                $html .= '     </div>';
                $html .= '      <div class="table-body-item ct2">';
                if($p['multiselect']==1){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
                $html .= '          <input type="checkbox" id="e_p_'.$i.'" name="p_'.$i.'" disabled '.$checked.'> '.$this->lang->line('Multi Select');
                $html .= '      </div>';
                $html .= '  </div>';
                


            }
            $html .= '  </div>';
            $html .= '</div>';





            $out['report']=array('report_id'=>$report_id,'report_name'=>$report_name,'report_path'=>$report_path,'parameters'=>$html,'num_params'=>$i);

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
            $out['report'] = array();
        }
        return $out;

    }

    public function update_report($id,$name,$path,$json,$id_user){
        $sql = 'UPDATE cms_report SET report_name=?, report_path=? WHERE reportid=?';
        $status = $this->db->query($sql,array($name,$path,$id));
        if($status){
            $sql = 'DELETE FROM cms_report_parameter WHERE report_id=?';
            $this->db->query($sql,array($id));
            
            $p = json_decode($json);

            foreach ($p as $param) {
                # code...
                $sql = 'INSERT INTO cms_report_parameter(report_id,name,multiselect) VALUES (?,?,?)';
                $this->db->query($sql,array($id,$param->name,$param->multiselect));
            }

            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Report already saved');

            $desc = 'Modify Report name# '.$name;
            $activity = 'Modify Report';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;

    }

    public function get_opco_config_table($opco){
        // select opco
        if($opco == -1){

            if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
                $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame  ASC limit 1 ';
                $query = $this->db->query($sql);
                if($query->num_rows()>0){
                     $row = $query->row(1);
                     $opco = $row->opcoid;
                }
            }else{
                $sql = 'SELECT
                            cms_opco_list.opconame,
                            cms_opco_list.opcoid,
                            map_group_opco.group_id
                        FROM
                            map_group_opco
                        INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                        WHERE map_group_opco.group_id=? AND map_group_opco.view_opco_config=1 ORDER BY cms_opco_list.opconame  ASC limit 1';
                $query = $this->db->query($sql,array($this->session->userdata('group_id')));
                if($query->num_rows()>0){
                     $row = $query->row(1);
                     $opco = $row->opcoid;
                }
            }

        }
        if($this->session->userdata('group_id')==ADMIN_GROUP_ID){
            $sql = 'SELECT * FROM cms_opco_list ORDER BY opconame';
            $data['select_opco'] = $this->db->query($sql);
        }else{
            $sql = 'SELECT
                            cms_opco_list.opconame,
                            cms_opco_list.opcoid,
                            map_group_opco.group_id
                        FROM
                            map_group_opco
                        INNER JOIN cms_opco_list ON map_group_opco.opco_id = cms_opco_list.opcoid
                        WHERE map_group_opco.group_id=?  AND view_opco_config ORDER BY cms_opco_list.opconame';
            $data['select_opco'] = $this->db->query($sql,array($this->session->userdata('group_id')));
        }
        $data['opco_id'] = $opco;

        $sql = "SELECT
                cms_opco_config.config_date,
                    DATE_FORMAT(cms_opco_config.config_date,'%m') AS `Month`,
                    YEAR(cms_opco_config.config_date) AS `Year`,
                    cms_opco_config.opcoid
                FROM
                    cms_opco_config
                WHERE opcoid=?
                GROUP BY
                    cms_opco_config.config_date
                ORDER BY
                    cms_opco_config.config_date DESC";
        $data['data'] = $this->db->query($sql,array($opco));
        //echo $this->db->last_query();
        //die;
        $data['current_month'] = date('m');
        $data['current_year'] = date('Y');

        return $this->load->view('template/opco_config_table_content',$data,TRUE);

    }

    public function change_map_opco($group_id,$opco_id,$view,$add,$update,$add_all_month,$update_all_month,$view_incident,$create_incident,$update_incident,$bulk_incident,$id_user){

        if($view==0 && $add==0 && $update==0 && $add_all_month==0 && $update_all_month==0 && $view_incident==0 && $create_incident==0 && $update_incident==0 && $bulk_incident==0){
            $sql = 'DELETE FROM map_group_opco WHERE group_id=? AND opco_id=?';
            $st = $this->db->query($sql,array($group_id,$opco_id));
            //echo $this->db->last_query();
        }else{
            $sql = 'SELECT * FROM map_group_opco WHERE group_id=? AND opco_id=?';
            $dt = $this->db->query($sql,array($group_id,$opco_id));
            if($dt->num_rows()>0){
                // update
                //echo "view : ".$view."--add:".$add."--update:".$update;
                $sql = 'UPDATE map_group_opco SET 
                            view_opco_config=?, 
                            add_opco_config=?, 
                            update_opco_config=?, 
                            add_all_month=?, 
                            update_all_month=?, 
                            view_incident=?, 
                            create_incident=?, 
                            update_incident=?, 
                            bulk_incident=? 
                        WHERE  group_id=? AND opco_id=?';
                $st = $this->db->query($sql,array(
                                                $view,
                                                $add,
                                                $update, 
                                                $add_all_month, 
                                                $update_all_month,
                                                $view_incident,
                                                $create_incident,
                                                $update_incident,
                                                $bulk_incident, 
                                                $group_id,
                                                $opco_id));
                echo $this->db->last_query();
                //echo 'update';
            }else{
                // insert
                echo $update;
                $sql = 'INSERT INTO map_group_opco(
                                        group_id,
                                        opco_id,
                                        view_opco_config,
                                        add_opco_config,
                                        update_opco_config,
                                        add_all_month,
                                        update_all_month,
                                        view_incident,
                                        create_incident,
                                        update_incident,
                                        bulk_incident) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
                $st = $this->db->query($sql,array(
                                                $group_id,
                                                $opco_id,
                                                $view,
                                                $add,
                                                $update,
                                                $add_all_month,
                                                $update_all_month,
                                                $view_incident,
                                                $create_incident,
                                                $update_incident,
                                                $bulk_incident));
                echo $this->db->last_query();
            }
            // audit trail
            $desc = 'Update privilage Opco for group #'.$group_id;
            $activity = 'Update privilege group opco';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }


    }

    public function change_map_report($group_id,$report_id,$view,$id_user){
        if($view==0){
            $sql = 'DELETE FROM map_group_report WHERE group_id=? AND report_id=?';
            $st = $this->db->query($sql,array($group_id,$report_id));
            //echo $this->db->last_query();
        }else{
            $sql = 'SELECT * FROM map_group_report WHERE group_id=? AND report_id=?';
            $dt = $this->db->query($sql,array($group_id,$report_id));
            if($dt->num_rows()>0){
                // update
                //echo "view : ".$view."--add:".$add."--update:".$update;
                $sql = 'UPDATE map_group_report SET p_view=? WHERE  group_id=? AND report_id=?';
                $st = $this->db->query($sql,array($view, $group_id,$report_id));
                //echo $this->db->last_query();
                //echo 'update';
            }else{
                // insert
                //echo $update;
                $sql = 'INSERT INTO map_group_report(group_id,report_id,p_view) VALUES (?,?,?)';
                $st = $this->db->query($sql,array($group_id,$report_id,$view));
                //echo $this->db->last_query();
            }
            // audit trail
            $desc = 'Update privilage report for group #'.$group_id;
            $activity = 'Update privilege group report';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));
        }
    }

    public function get_opco_config_privelege($group_id){

    }

    public function add_revenue_stream($revenue_stream_name,$id_user){
        $sql = 'INSERT INTO cms_revenue_stream(revenue_stream_name) VALUES (?)';
        $dt = $this->db->query($sql,array($revenue_stream_name));
        if($dt){
            $out['status'] = 0;
            $out['msg'] = $this->lang->line('Revenue Stream already saved');

            $desc = 'Add revenue stream '.$revenue_stream_name;
            $activity = 'Add new Revenue Stream';
            $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
            $this->db->query($sql_log,array($id_user,$activity,$desc));

        }else{
            $err =$this->db->error();
            $out['status'] =$err['code'];
            $out['msg'] = $err['message'];
        }
        return $out;
    }

    public function table_rate(){
        $sql = 'SELECT DISTINCT
                    cms_currency_exchange.`year`,
                    cms_currency_exchange.`month`
                FROM
                    cms_currency_exchange
                ORDER BY
                    cms_currency_exchange.`year` DESC,
                    cms_currency_exchange.`month` DESC';

        $dt = $this->db->query($sql);
        $data['yearmonth'] =  $dt;
        $sql='SELECT * FROM map_vipgroup_currency ORDER BY id';
        $data['all_currency'] = $this->db->query($sql);

        $sql = 'SELECT DISTINCT
                cms_currency_exchange.`year`
                FROM
                cms_currency_exchange
                ORDER BY `year` DESC';
        $dt_year = $this->db->query($sql);
        $data['all_year'] = $dt_year;

        $data['all_month'] = array( array('m'=>'01','month'=>$this->lang->line('January')),
                                    array('m'=>'02','month'=>$this->lang->line('February')),
                                    array('m'=>'03','month'=>$this->lang->line('March')),
                                    array('m'=>'04','month'=>$this->lang->line('April')),
                                    array('m'=>'05','month'=>$this->lang->line('May')),
                                    array('m'=>'06','month'=>$this->lang->line('June')),
                                    array('m'=>'07','month'=>$this->lang->line('July')),
                                    array('m'=>'08','month'=>$this->lang->line('August')),
                                    array('m'=>'09','month'=>$this->lang->line('September')),
                                    array('m'=>'10','month'=>$this->lang->line('October')),
                                    array('m'=>'11','month'=>$this->lang->line('November')),
                                    array('m'=>'12','month'=>$this->lang->line('December')));
                                
                                

        $data['current_year'] = date('Y');
        $data['current_month'] = date('m');
        //echo  $data['current_year'].'-'. $data['current_month'];

        return $this->load->view('template/table_exchange',$data,TRUE);
    }

    public function get_rate_form($year,$month){
            $sql = 'SELECT * FROM cms_currency_exchange WHERE `year`=? AND `month`=?';
            $dt = $this->db->query($sql,array($year,$month));
        return $dt;
    }



    public function run_procedure($year, $month){
        $this->load->library('mydb');
        $arr  = $this->mydb->GetMultiResults("CALL update_currency(".$year.",".$month.")");
        
        //$sql = 'call update_currency(?,?);';
                    
        //$qry_res = $this->db->query($sql,array($year,$month));
        //echo $this->db->last_query();
        //echo '<br>';
        //print_r($arr);
        //die;
        /*
        $res        = $qry_res->result();

        $qry_res->next_result(); // Dump the extra resultset.
        $qry_res->free_result(); // Does what it says.
        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
        
        */
       return $arr;
    }


    public function update_rate($json,$id_user){
        $obj = json_decode($json);
        $this->db->db_debug = FALSE;
        foreach ($obj as $row) {
            # code...
            $its_ok = true;
            $s_year = $row->year;
            $s_month = $row->month;
            $sql = 'SELECT * FROM cms_currency_exchange WHERE `year`=? AND `month`=? AND `currency_id`=?';
            $dt = $this->db->query($sql,array($row->year,$row->month,$row->currency_id));
            if(!$dt){
                //$errNo   = $this->db->error();
                
                //print_r($errNo);
                //die;
            }
            
            if($dt->num_rows()>0){
                $sqld = 'UPDATE cms_currency_exchange SET rate=? WHERE `year`=? AND `month`=? AND `currency_id`=?';
                $status = $this->db->query($sqld,array($row->rate,$row->year,$row->month,$row->currency_id));
                //echo $this->db->last_query();
                if(!$status){
                    $its_ok = false;

                }else{
                    //$st = $this->run_procedure($row->currency_id,$row->year,$row->month);
                    //print_r($st);
                    //$sql = 'call update_exchange_rate(?,?,?)';
                    //$this->db->query($sql,array($row->currency_id,$row->year,$row->month));

                    $desc = 'Update exchange rate currency_id='.$row->currency_id.' to '.$row->rate.' in period ' .$row->year.'-'.$row->month;
                    $activity = 'Update exchange rate';
                    $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
                    $this->db->query($sql_log,array($id_user,$activity,$desc));
                }

            }else{
                $sqld = 'INSERT INTO cms_currency_exchange(`year`,`month`,`currency_id`,`rate`) VALUES (?,?,?,?)';
                $status = $this->db->query($sqld,array($row->year,$row->month,$row->currency_id,$row->rate));
                //echo $this->db->last_query();
                if(!$status){
                    $its_ok = false;
                }else{
                    
                    //$sql = 'call update_exchange_rate(?,?,?)';
                    //$this->db->query($sql,array($row->currency_id,$row->year,$row->month));
                    
                    $desc = 'Insert exchange rate currency_id='.$row->currency_id.' to '.$row->rate.' in period ' .$row->year.'-'.$row->month;
                    $activity = 'Insert exchange rate';
                    $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
                    $this->db->query($sql_log,array($id_user,$activity,$desc));
                }
            }


        }

        if($its_ok){
            $out = array(   'status'=>0,
                            'msg'=>$this->lang->line('Configuration has been saved'));
            $st = $this->run_procedure($s_year,$s_month);
            //print_r($st);
            //die;
        }else{
            $out = array(   'status'=>100,
                            'msg'=>$this->lang->line('Configuration has been saved'));
            $st = $this->run_procedure($s_year,$s_month);
            //print_r($st);
        }
        return $out;
    }

    public function add_rate($json,$id_user){
        

        $obj = json_decode($json);

        foreach ($obj as $test) {
            # code...
            $s_year = $test->year;
            $s_month = $test->month;
        }
        $its_ok = true;
        $sql = 'SELECT * FROM cms_currency_exchange WHERE `month` =? AND `year`=?';
        $dt = $this->db->query($sql,array($s_month,$s_year));
        if($dt->num_rows()>0){
            $out = array('status'=>-200,'msg'=>$this->lang->line('already_exist'));
        }else{
            foreach ($obj as $row) {
                $sqld = 'INSERT INTO cms_currency_exchange(`year`,`month`,`currency_id`,`rate`) VALUES (?,?,?,?)';
                $status = $this->db->query($sqld,array($row->year,$row->month,$row->currency_id,$row->rate));
                
                if(!$status){
                    $its_ok = false;
                }else{
                    $desc = 'Insert exchange rate currency_id='.$row->currency_id.' to '.$row->rate.' in period ' .$row->year.'-'.$row->month;
                    $activity = 'Insert exchange rate';
                    $sql_log = 'INSERT cms_user_log (id_user,id_module,activity,description,log_time) VALUES (?,1,?,?,NOW())';
                    $this->db->query($sql_log,array($id_user,$activity,$desc));
                }
            }

            if($its_ok){
                $out = array(   'status'=>0,
                                'msg'=>$this->lang->line('Configuration has been saved'));
                $this->run_procedure($s_year,$s_month);
            }else{
                $out = array(   'status'=>100,
                                'msg'=>$this->lang->line('Configuration has been saved'));
                $this->run_procedure($s_year,$s_month);
            }

        }

        return $out;
    }

    public function get_rate($month,$year,$opcoid){
        $sql = 'SELECT
                cms_opco_list.opcoid,
                cms_opco_list.opconame,
                cms_opco_list.currency_id,
                cms_currency_exchange.rate,
                cms_currency_exchange.`year`,
                cms_currency_exchange.`month`,
                map_vipgroup_currency.currencysymbol
                FROM
                cms_opco_list
                LEFT OUTER JOIN cms_currency_exchange ON cms_currency_exchange.currency_id = cms_opco_list.currency_id
                LEFT OUTER JOIN map_vipgroup_currency ON cms_opco_list.currency_id = map_vipgroup_currency.id
                WHERE cms_currency_exchange.`year`=? AND cms_currency_exchange.`month`=? AND cms_opco_list.opcoid=?';
        $dt = $this->db->query($sql,array($year,$month,$opcoid));
        //echo $this->db->last_query();
        
        if($dt->num_rows()>0){
            $row= $dt->row();
            $out = array($row->rate,$row->currencysymbol);
        }else {
            $out = array(0,'');
        }
        return $out;
    }
}
