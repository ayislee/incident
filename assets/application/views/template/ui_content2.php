<div class="ui_content">

	<?php echo $ui_content_menu?>
	<div class="ui_content_menu_map">
		<div class="table-title">
			<div class="rows">
				<div class="ui-item-id bold">
					<?php echo $this->lang->line('id')?>
				</div>
				<div class="ui-item-name bold">
					<?php echo $this->lang->line('name')?>
				</div>
			</div>
		</div>
		<div class="ui_detail">
			<div class="rows">
				<div class="ui_title">
					<?php echo ""?>
				</div>
				
				<?php
					foreach ($ui_detail->result() as $row) { ?>
					 	<div class="rows">
					 		<div class="ui-item-id">
					 			#<?php echo $row->detail_id?>
					 		</div>

					 		<div class="ui-item-name">
					 			<?php echo $row->name?>
					 		</div>

					 		<button onclick="modify_ui_detail(<?php echo $row->detail_id?>,'<?php echo $row->name?>')"><?php echo $this->lang->line('modify')?></button>
					 		<button onclick="delete_ui_detail(<?php echo $row->detail_id?>)"><?php echo $this->lang->line('delete')?></button>
					 	</div>
				<?php
					 } 
				?>
			</div>
		</div>
		<div class="ui_content_menu_map_block_btn">
			<button onclick="$('#new-ui-detail-name').val('');pop('disable-background','add-ui-detail')" class="btn-right"><?php echo $this->lang->line('add')?></button>
		</div>
	</div>
</div>

<div class="del-confirm" id="win-del-confirm">
	<div class="confirm-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message" id="ui-detail-delete-confirm"></div>
	<div class="confirm-btn">
		<button onClick="confirm(id_ui_detail_delete)"><?php echo $this->lang->line('yes'); ?></button>
		<button onClick="hide('disable-background','win-del-confirm')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>
<div class="ontop" id="disable-background"></div>

<div class="add-modify-ui-detail" id="add-ui-detail">
	<div class="modify-title">
		<?php echo $this->lang->line('add_ui_title')?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item">
			<?php echo $this->lang->line('name')?>
			</div>:
			<div class="detail-item">
				<input id="new-ui-detail-name" name="new-ui-detail-name" type="text">
			</div>
		</div>
		<div class="rows">
			<button onclick="insert_ui_detail()"><?php echo $this->lang->line('save')?></button>
			<button onclick="hide('disable-background','add-ui-detail')"><?php echo $this->lang->line('cancel')?></button> 
		</div>
	</div>
</div>

<div class="add-modify-ui-detail" id="modify-ui-detail">
	<div class="modify-title">
		<?php echo $this->lang->line('modify_ui_title')?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item">
			<?php echo $this->lang->line('name')?>
			</div>:
			<div class="detail-item">
				<input id="modify-ui-detail-name" name="modify-ui-detail-name" type="text">
			</div>
		</div>
		<div class="rows">
			<button onclick="update_ui_detail()"><?php echo $this->lang->line('save')?></button>
			<button onclick="hide('disable-background','modify-ui-detail')"><?php echo $this->lang->line('cancel')?></button> 
		</div>
	</div>
</div>

<div class="del-confirm" id="msg-error">
	<div class="confirm-title">
		<?php echo $this->lang->line("Notify"); ?>
	</div>
	<div class="confirm-message" id="notify-error"></div>
	<div class="confirm-btn">
		<button onClick="hide('disable-background','msg-error');"><?php echo $this->lang->line('ok'); ?></button>
	</div>

</div>

<script type="text/javascript">
    var id_ui_detail_delete;
    var id_ui_detail_modify;
	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}

	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}



	function delete_ui_detail(id){
		id_ui_detail_delete = id;
		document.getElementById("ui-detail-delete-confirm").innerHTML = "<?php echo $this->lang->line('Are you sure to delete')?>";
		pop("disable-background","win-del-confirm");
	}



	function confirm(id){
		// alert(id);
		
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/delete_ui_detail",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id_detail: id,
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	location.reload();
            }
		});

	}

	function insert_ui_detail(){
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/add_ui_detail",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	name: $("#new-ui-detail-name").val(),
            	active : "<?php echo $active ?>",
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//location.reload();
            	//alert(data);
            	switch(data){
            		case '0':
            			location.reload();
            			break;
            		case '1':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('fail_add_ui')?>";
            			pop("disable-background","msg-error");
            			break;
            		case '2':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('name_blank')?>";
            			hide("disable-background","add-ui-detail");
            			pop("disable-background","msg-error");
            			break;
            		case '3':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			hide("disable-background","add-ui-detail");
            			pop("disable-background","msg-error");
            			break;	
            	}
            }
		});

	}

	function modify_ui_detail(id,name){
		id_ui_detail_modify = id;
		$("#modify-ui-detail-name").val(name);
		pop("disable-background","modify-ui-detail");
	}

	function update_ui_detail(){
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/update_ui_detail",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id: id_ui_detail_modify,
            	name: $("#modify-ui-detail-name").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//location.reload();
            	//alert(data);
            	switch(data){
            		case '0':
            			location.reload();
            			break;
            		case '1':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('fail_update_ui')?>";
            			pop("disable-background","msg-error");
            			break;
            		case '2':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('name_blank')?>";
            			hide("disable-background","modify-ui-detail");
            			pop("disable-background","msg-error");
            			break;
            		case '3':
            			document.getElementById("notify-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			hide("disable-background","modify-ui-detail");
            			pop("disable-background","msg-error");
            			break;	
            	}
            }
		});
	}

</script>>