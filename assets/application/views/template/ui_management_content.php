<style type="text/css">
    .width200 {
        width: 200px;
    }

    .c001 {
        width: 250px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        vertical-align: middle;
    }

    .c001a {
        width: 400px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        vertical-align: middle;
    }

  .ui_menu {
    width: 700px;
    border-radius: 5px;
    background-color: white;
  }

  .ui-menu-item-title {
    width: 620px;
  }
</style>
<div class="ui_menu">
	<div class="ui-menu-item">
		<div class="ui-menu-item-title"><?php echo $this->lang->line('VimpelCom Group Currency')?></div>
		<button class="btn-down " id="btn-down-group-currency" data-toggle="collapse" data-target="#detail-group-currency"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_group_currency()"></i></i></button>
	</div>
	<div id="detail-group-currency" class="ui-detail collapse">
		<div class="ui-detail-content">
        <div class="table-head">
            <div class="table-head-item c001 margin-5"><?php echo $this->lang->line('Currency')?></div>
            <div class="table-head-item width150 margin-5"><?php echo $this->lang->line('Currency Symbol')?></div>
        </div>
		<?php
			foreach ($group_currency->result() as $currency) { ?>
			<div class="ui-detail-row">
				<div class="ui-detail-item c001"><?php echo $currency->currencyname?></div>
				<div class="ui-detail-item width150"><?php echo $currency->currencysymbol?></div>
                <?php if($currency->id != DEFAULT_CURRENCY) {?>
				<button class="std-btn bkgr-red ui-detail-item-btn" onclick="delete_group_currency_id_<?php echo $currency->id?>()"><?php echo $this->lang->line('delete')?></button>
                <?php  } ?>
				
			</div>


			 <script type="text/javascript">
                    
                function modify_group_currency_id_<?php echo $currency->id?>(){
               		document.getElementById("title-modify-detail").innerHTML="<?php echo $this->lang->line('Modify for').' '.$currency->currencyname?>";
                	document.getElementById("modify-ui-detail-name").placeholder = "<?php echo $this->lang->line('Type Currency name ...')?>";
		            document.getElementById("modify-ui-detail-value").placeholder = "<?php echo $this->lang->line('Type Currency symbol ...')?>";
		            document.getElementById('modify-ui-detail-value').type = 'text';
		            document.getElementById("modify-ui-detail-value").style.display = 'block';
                	$("#modify-ui-detail-name").val("<?php echo $currency->currencyname?>");
                	$("#modify-ui-detail-value").val("<?php echo $currency->currencysymbol?>");
                	
                	document.getElementById("btn-save-modify-ui-detail").onclick = function() {modify_currency("<?php echo $currency->id?>")};
                	pop("disable-background","modify-ui-detail");
                }
                
               function delete_group_currency_id_<?php echo $currency->id?>(){
               		document.getElementById("ui-detail-delete-confirm").innerHTML="<?php echo $this->lang->line('Are you sure to delete')?>";
                	document.getElementById("btn-delete-ui-detail").onclick = function() {delete_currency("<?php echo $currency->id ?>") };
                	pop("disable-background","win-del-confirm");
                }
           </script>
		<?php
			 }
		?>
		</div>
		<div class="ui-control">
         	<button class="std-btn bkgr-blue" onclick="add_group_currency()"><?php echo $this->lang->line('add')?></button> 
        </div>	
	</div>

	<script>
        function func_group_currency(){ 
            $(".collapse").collapse("hide");
        }
        
        function add_group_currency(){
            /*
            document.getElementById("title-add-detail").innerHTML="<?php echo $this->lang->line('add_new').' '.$this->lang->line('VimpelCom Group Currency')?>";
            document.getElementById("add-ui-detail-message").innerHTML="<?php echo $this->lang->line('Please add').' '.$this->lang->line('VimpelCom Group Currency').' '.$this->lang->line('name that you desire')?>";
            document.getElementById("new-ui-detail-name").placeholder = "<?php echo $this->lang->line('Type Currency name ...')?>";
            document.getElementById("new-ui-detail-value").placeholder = "<?php echo $this->lang->line('Type Currency symbol ...')?>";
            document.getElementById('new-ui-detail-value').type = 'text';
            document.getElementById("new-ui-detail-value").style.display = 'block';
            $("#new-ui-detail-name").val("");
            $("#new-ui-detail-value").val("");
            
            document.getElementById("btn-save-add-ui-detail").onclick = function() {add_group()}; */

            pop("disable-background","add-currency");
            document.getElementById('symbol').innerHTML = $("#select_currency").val();
            $("#currencysymbol").val($("#select_currency").val());
            $("#currencyname").val($( "#select_currency option:selected" ).text());
            document.getElementById('symbol').innerHTML = $("#select_currency").val();

        }

   	</script>


   	<!-- This accordion detection tool -->
   	<div class="ui-menu-item">
		<div class="ui-menu-item-title"><?php echo $this->lang->line('Detection Tool')?></div>
		<button class="btn-down " id="btn-down-group-currency" data-toggle="collapse" data-target="#detail-detection-tool"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_detection()"></i></i></button>
	</div>
	<div id="detail-detection-tool" class="ui-detail collapse">
		<div class="ui-detail-content">
            <div class="table-head">
                <div class="table-head-item c001 margin-5"><?php echo $this->lang->line('Tools Name')?></div>
                <div class="table-head-item width150 margin-5"><?php echo $this->lang->line('Cost')?></div>
            </div>
		<?php
			foreach ($detection_tool->result() as $detection) { ?>
			<div class="ui-detail-row">
				<div class="ui-detail-item c001"><?php echo $detection->dtname?></div>
				<div class="ui-detail-item width150"><?php echo number_format($detection->dtcost,2)?></div>
				<button class="std-btn bkgr-red ui-detail-item-btn" onclick="delete_detection_id_<?php echo $detection->dtid?>()"><?php echo $this->lang->line('delete')?></button>
				<button class="std-btn bkgr-green ui-detail-item-btn" onclick="modify_detection_id_<?php echo $detection->dtid?>()"><?php echo $this->lang->line('modify')?></button>
			</div>


			 <script type="text/javascript">
                    
                function modify_detection_id_<?php echo $detection->dtid?>(){
               		document.getElementById("title-modify-ui-detail").innerHTML="<?php echo $this->lang->line('Modify for').' '.$this->lang->line('Detection Tool')?>";
                	document.getElementById("modify-ui-detail-name").placeholder = "<?php echo $this->lang->line('Type Detection tool name ...')?>";
		            document.getElementById("modify-ui-detail-value").placeholder = "<?php echo $this->lang->line('Type Detection tool cost ...')?>";
		            document.getElementById('modify-ui-detail-value').type = 'number';
		            document.getElementById("modify-ui-detail-value").style.display = 'block';
                	$("#modify-ui-detail-name").val("<?php echo $detection->dtname?>");
                	$("#modify-ui-detail-value").val("<?php echo $detection->dtcost?>");
                	
                	document.getElementById("btn-save-modify-ui-detail").onclick = function() {modify_detection("<?php echo $detection->dtid?>")};
                	pop("disable-background","modify-ui-detail");
                }
                
               function delete_detection_id_<?php echo $detection->dtid?>(){
               		document.getElementById("ui-detail-delete-confirm").innerHTML="<?php echo $this->lang->line('Are you sure to delete')?>";
                	document.getElementById("btn-delete-ui-detail").onclick = function() {delete_detection("<?php echo $detection->dtid ?>")};
                	pop("disable-background","win-del-confirm");
                }
           </script>
		<?php
			 }
		?>
		</div>
		<div class="ui-control">
         	<button class="std-btn bkgr-blue" onclick="add_detection()"><?php echo $this->lang->line('add')?></button> 
        </div>	
	</div>
	<script>
        function func_detection(){ 
            $(".collapse").collapse("hide");
        }

        function add_detection(){
            document.getElementById("title-add-detail").innerHTML="<?php echo $this->lang->line('add_new').' '.$this->lang->line('Detection Tool')?>";
            document.getElementById("add-ui-detail-message").innerHTML="<?php echo $this->lang->line('Please add').' '.$this->lang->line('Detection Tool').' '.$this->lang->line('name that you desire')?>";
            document.getElementById("new-ui-detail-name").placeholder = "<?php echo $this->lang->line('Type Detection tool name ...')?>";
            document.getElementById("new-ui-detail-value").placeholder = "<?php echo $this->lang->line('Type Detection tool cost ...')?>";
            document.getElementById('new-ui-detail-value').type = 'number';
            document.getElementById("new-ui-detail-value").style.display = 'block';
            $("#new-ui-detail-name").val("");
            $("#new-ui-detail-value").val("");
            document.getElementById("btn-save-add-ui-detail").onclick = function() {add_detect()};
            pop("disable-background","add-ui-detail");
        }

   	</script>


   	<!-- This accordion OpCo -->
   	<div class="ui-menu-item">
		<div class="ui-menu-item-title"><?php echo $this->lang->line('OpCo')?></div>
		<button class="btn-down " id="btn-down-group-currency" data-toggle="collapse" data-target="#detail-opco"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_opco()"></i></i></button>
	</div>
	<div id="detail-opco" class="ui-detail collapse">
		<div class="ui-detail-content">
		<?php
			foreach ($opco_list->result() as $opco) { ?>
			<div class="ui-detail-row">
				<div class="ui-detail-item c001"><?php echo $opco->opconame?></div>
                <div class="ui-detail-item width150"><?php echo $opco->currencysymbol?></div>
				<button class="std-btn bkgr-red ui-detail-item-btn" onclick="delete_opco_id_<?php echo $opco->opcoid?>()"><?php echo $this->lang->line('delete')?></button>
				<button class="std-btn bkgr-green ui-detail-item-btn" onclick="modify_opco_id_<?php echo $opco->opcoid?>()"><?php echo $this->lang->line('modify')?></button>
			</div>


			 <script type="text/javascript">
                    
                function modify_opco_id_<?php echo $opco->opcoid?>(){
                    $.ajax({
                          type: "POST",  
                          url: "<?php echo base_url()?>index.php/admin/get_list_currency",  
                          contentType: 'application/x-www-form-urlencoded',
                          data: { 
                                curr_currency: <?php echo $opco->currency_id?>,
                                sess: "<?php echo session_id()?>"
                          },
                          dataType: "text",
                          beforeSend: function(){

                          },
                          complete: function(){
                                
                          },
                          success: function(data){
                                //alert(data);
                                document.getElementById('modify-opco-detail-value').innerHTML=data;
                                
                          }
                    });

               		document.getElementById("title-modify-opco-detail").innerHTML="<?php echo $this->lang->line('Modify for').' '.$this->lang->line('OpCo')?>";
                	document.getElementById("modify-opco-detail-name").placeholder = "<?php echo $this->lang->line('Type OpCo name ...')?>";
                	

                	$("#modify-opco-detail-name").val("<?php echo $opco->opconame?>");
                	
                	document.getElementById("btn-save-modify-opco-detail").onclick = function() {modify_opco("<?php echo $opco->opcoid?>")};
                	pop("disable-background","modify-opco-detail");
                }
                
               function delete_opco_id_<?php echo $opco->opcoid?>(){
               		document.getElementById("ui-detail-delete-confirm").innerHTML="<?php echo $this->lang->line('Are you sure to delete')?>";
                	document.getElementById("btn-delete-ui-detail").onclick = function() {delete_opco("<?php echo $opco->opcoid ?>")};
                	pop("disable-background","win-del-confirm");
                }
           </script>
		<?php
			 }
		?>
		</div>
		<div class="ui-control">
         	<button class="std-btn bkgr-blue" onclick="add_opco()"><?php echo $this->lang->line('add')?></button> 
        </div>	
	</div>
	<script>
        function func_opco(){ 
            $(".collapse").collapse("hide");
        }

        function add_opco(){
            document.getElementById("title-add-detail").innerHTML="<?php echo $this->lang->line('add_new').' '.$this->lang->line('OpCo')?>";
            document.getElementById("add-opco-detail-message").innerHTML="<?php echo $this->lang->line('Please add').' '.$this->lang->line('OpCo').' '.$this->lang->line('name that you desire')?>";
            document.getElementById("new-opco-detail-name").placeholder = "<?php echo $this->lang->line('Type OpCo name ...')?>";
            
            $("#new-ui-detail-name").val("");
            $.ajax({
                          type: "POST",  
                          url: "<?php echo base_url()?>index.php/admin/get_list_currency",  
                          contentType: 'application/x-www-form-urlencoded',
                          data: { 
                                curr_currency: <?php echo $opco->currency_id?>,
                                sess: "<?php echo session_id()?>"
                          },
                          dataType: "text",
                          beforeSend: function(){

                          },
                          complete: function(){
                                
                          },
                          success: function(data){
                                //alert(data);
                                document.getElementById('new-opco-detail-value').innerHTML=data;
                                
                          }
                    });
            document.getElementById("btn-save-add-opco-detail").onclick = function() {add_opco_list()};
            pop("disable-background","add-opco-detail");
        }

   	</script>   	

    <!-- This accordion Revenue Stream -->
    <div class="ui-menu-item">
        <div class="ui-menu-item-title"><?php echo $this->lang->line('Revenue Stream')?></div>
        <button class="btn-down " id="btn-down-group-revenue-stream" data-toggle="collapse" data-target="#detail-revenue-stream"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_revenue_stream()"></i></i></button>
    </div>
    <div id="detail-revenue-stream" class="ui-detail collapse">
        <div class="ui-detail-content">
        <?php
            foreach ($revenue_stream->result() as $revenue) { ?>
            <div class="ui-detail-row">
                <div class="ui-detail-item c001a"><?php echo $revenue->revenue_stream_name?></div>
                <button class="std-btn bkgr-red ui-detail-item-btn" onclick="delete_revenue_id_<?php echo $revenue->id?>()"><?php echo $this->lang->line('delete')?></button>
                <button class="std-btn bkgr-green ui-detail-item-btn" onclick="modify_revenue_id_<?php echo $revenue->id?>()"><?php echo $this->lang->line('modify')?></button>
            </div>
            <script type="text/javascript">
                function delete_revenue_id_<?php echo $revenue->id?>(){
                        document.getElementById("ui-detail-delete-confirm").innerHTML="<?php echo $this->lang->line('Are you sure to delete')?>";
                        document.getElementById("btn-delete-ui-detail").onclick = function() {delete_revenue("<?php echo $revenue->id ?>")};
                        pop("disable-background","win-del-confirm");
                }

                function modify_revenue_id_<?php echo $revenue->id?>(){
                    document.getElementById("title-modify-revenue-stream").innerHTML="<?php echo $this->lang->line('Modify for').' '.$this->lang->line('Revenue Stream')?>";
                    $("#modify-revenue-stream-name").val("<?php echo $revenue->revenue_stream_name?>");
                    
                    document.getElementById("btn-save-modify-revenue-stream").onclick = function() {modify_revenue("<?php echo $revenue->id?>")};
                    pop("disable-background","modify-revenue-stream");
                }
            </script>

        <?php } ?>
        </div>
        <div class="ui-control">
            <button class="std-btn bkgr-blue" onclick="add_revenue_stream()"><?php echo $this->lang->line('add')?></button> 
        </div>
    </div>
    <script type="text/javascript">
        function func_revenue_stream(){ 
            $(".collapse").collapse("hide");
        }

        function add_revenue_stream(){
            document.getElementById("title-add-revenue-stream").innerHTML="<?php echo $this->lang->line('add_new').' '.$this->lang->line('Revenue Stream')?>";
            document.getElementById("add-revenue-stream-message").innerHTML="<?php echo $this->lang->line('Please add').' '.$this->lang->line('Revenue Stream').' '.$this->lang->line('name that you desire')?>";
            document.getElementById("new-revenue-stream-name").placeholder = "<?php echo $this->lang->line('Type revenue stream name ...')?>";
            $("#new-revenue-stream-name").val("");
            document.getElementById("btn-save-add-revenue-stream").onclick = function() {add_revenue()};
            pop("disable-background","add-revenue-stream");
        }
    </script>

    <div class="ui-menu-item">
        <div class="ui-menu-item-title">FAQ</div>
        <button class="btn-down " id="btn-down-group-revenue-faq" data-toggle="collapse" data-target="#detail-revenue-faq"><i class="fa fa-chevron-circle-down" aria-hidden="true" onclick="func_revenue_faq()"></i></i></button>
    </div>
    <div id="detail-revenue-faq" class="ui-detail collapse">
      <h4>Admin</h4>
        <div class="ui-detail-content">
            <div style="width: 100%;padding: 10px;border: 3px solid gray;border-radius:15px;margin: 0;">
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">English</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=english_admin_faq" class="std-btn ui-detail-item-btn" id="faq_adm_english" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_adm_english')"></input></form>
              </div>
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">French</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=french_admin_faq" class="std-btn ui-detail-item-btn" id="faq_adm_french" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_adm_french')"></input></form>
              </div>
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">Russian</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=russian_admin_faq" class="std-btn ui-detail-item-btn" id="faq_adm_russian" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_adm_russian')"></input></form>
              </div>
            </div>
            </div>
      <h4>Incident</h4>
        <div class="ui-detail-content">
            <div style="width: 100%;padding: 10px;border: 3px solid gray;border-radius:15px;margin: 0;">
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">English</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=english_faq" class="std-btn ui-detail-item-btn" id="faq_incident_english" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_incident_english')"></input></form>
              </div>
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">French</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=french_faq" class="std-btn ui-detail-item-btn" id="faq_incident_french" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_incident_french')"></input></form>
              </div>
              <div class="ui-detail-row">
                  <div class="ui-detail-item c001a">Russian</div>
                  <form method="post" action="<?php echo base_url();?>index.php/admin/faq_upload/?name=russian_faq" class="std-btn ui-detail-item-btn" id="faq_incident_russian" accept-charset="utf-8" enctype="multipart/form-data"><input style="border:0;width:92px;" name="fileupload"  class="std-input" type="file" onchange="form_faq('faq_incident_russian')"></input></form>
              </div>
            </div>
        </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  // function form_faq(idform){
  //    //document.getElementById(idform).submit();
     
  // }     
   


  function form_faq(idform) {
        var data = new FormData($('#'+idform)[0]);

      
          $.ajax({
              type: "POST",  
              url: "<?php echo base_url();?>index.php/admin/faq_upload/?name=english_admin_faq",  
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              data: data,
              dataType: "text",
              beforeSend: function(){
                 // alert('befre');
              },
              complete: function(){
                    //alert('complete');
              },
              success: function(data){
                    //location.reload();
                    document.getElementById('upload-message').innerHTML=data;
                    pop('disable-background','win-upload');
                    
              }
          });          
      
    }           
</script>