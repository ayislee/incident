<style type="text/css">
    .area-save {
        margin-left: 25px;
        width: 1000px;

    }
    .f-r{
        float: right;
    }
    .report-col-1{
        width: 820px;
    }
</style>
<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
<div class="area-save w1000">
    <div class="f-r">
        <button class="std-btn bkgr-green" onclick="report_save()"><?php echo $this->lang->line('save')?></button>
        <button class="std-btn bkgr-red" onclick="report_cancel()"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>
<?php } ?>
<div id="query"></div>

<div class="del-access-module" id="save-module">
    <div class="modify-title">
        <?php echo $this->lang->line('Notify'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Configuration has been saved'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onClick="location.reload()"><?php echo $this->lang->line('ok'); ?></button>
    </div>
</div>
<script type="text/javascript">
    function change_report(report_id,group_id){
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_view").prop('checked'));
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_add").prop('checked'));
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_update").prop('checked'));
        if($("#report_"+report_id+"_group_"+group_id+"_view").prop('checked')){
            val_view = 1;
        }else{
            val_view = 0;
        }
        
        var obj_report = {
            group_id: group_id,
            report_id: report_id,
            view: val_view
        }

        return obj_report;

        /*
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/change_map_report",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: group_id,
                report_id: report_id,
                view: val_view,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //location.reload();
                document.getElementById('query').innerHTML=data;
                //alert(data);
            }
        });
        */
        
    }

    function report_save(){
        //alert("haha");
        var json = '<?php echo json_encode($report_list->result_array())?>';
        var obj = JSON.parse(json);
        //alert(obj.length);
        //alert("jsdkj");
        var all_report = [];
        obj.forEach(function(item, index){
            var list_module = item.opcoid;
            all_report.push(change_report(item.reportid,<?php echo $id_group ?>));
            //alert(item.opconame); 
        });
        var json_str = JSON.stringify(all_report);
        console.log(json_str);

        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/change_map_report",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                json: json_str
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //location.reload();
                document.getElementById('query').innerHTML=data;
                //alert(data);
                pop('disable-background','save-module');
            }
        });

        
    }

    function report_cancel(){
        location.reload();
    }
</script>