<style type="text/css">
	.t-content {
		margin-top: 10px;
		margin-left: 24px;
		margin-right: 24px;
	}

	.table-head {
		
		padding-right: 10px;
	}

	.c-1 {
		display: inline-block;
		width: 250px;
		padding-left: 10px;
	}

	.c-2 {
		display: inline-block;
		width: 100px;
		padding-left: 10px;
	}

	.c-3 {
		display: inline-block;
		float: right;
		padding-right: 20px;

	}

	.table-rate {
		max-height: 500px;
		overflow: auto;
	}

	.title-rate {
		background-color: #FFC711;
		text-transform: uppercase;
		text-align: center;
		font-weight: bold;
		color: black;
		display: block;
		height: 30px;
		vertical-align: middle;
		padding-top: 5px;
	}

	.view-modify-rate {
		width: 500px;
		padding: 5px;
	}

	.view-modify-rate {
		display: none;
		background-color: white;
		width :400px;
		margin-left: 24px;
		padding-top: 5px;
		position: fixed;
		top:50px;
		left: 50%;
		margin-left: -200px;

	}

	.cur-name {
		display: inline-block;
		padding-left: 20px;
		width: 150px;

	}
	.cur-rate {
		display: inline-block;	
	}

	.std-input{
		text-align: right;
		padding-right: 10px;
	}
	.rate-content{
		padding: 10px;
	}

	.btn-cmd {
		padding-top: 10px;
		padding-bottom: 10px;
		text-align: center;
	}
</style>
<div class="t-content">
	<div class="table-head">
		<div class="table-head-item c-1"><?php echo $this->lang->line('Month')?></div>
		<div class="table-head-item c-2"><?php echo $this->lang->line('Year')?></div>
		
	</div>
	<div class="table-rate">
	
		<?php foreach ($yearmonth->result() as $yearmonth) {
				switch ($yearmonth->month) {
					case 1:
						# code...
						$mth = $this->lang->line('January');
						break;
					case 2:
						# code...
						$mth = $this->lang->line('February');
						break;
					case 3:
						# code...
						$mth = $this->lang->line('March');
						break;
					case 4:
						# code...
						$mth = $this->lang->line('April');
						break;
					case 5:
						# code...
						$mth = $this->lang->line('May');
						break;
					case 6:
						# code...
						$mth = $this->lang->line('June');
						break;
					case 7:
						# code...
						$mth = $this->lang->line('July');
						break;
					case 8:
						# code...
						$mth = $this->lang->line('August');
						break;
					case 9:
						# code...
						$mth = $this->lang->line('September');
						break;
					case 10:
						# code...
						$mth = $this->lang->line('October');
						break;
					case 11:
						# code...
						$mth = $this->lang->line('November');
						break;
					case 12:
						# code...
						$mth = $this->lang->line('December');
						break;
				}

		?>
		<div class="table-body">
			<div class="table-body-item c-1"><?php echo $mth ?></div>
			<div class="table-body-item c-2"><?php echo $yearmonth->year ?></div>

			<div class="table-body-item c-3"><button class="std-btn bkgr-blue" onclick="view_rate(<?php echo $yearmonth->year?>,<?php echo $yearmonth->month?>,'<?php echo $mth?>')"><?php echo $this->lang->line('view')?></button></div>

		</div>
		<?php }?>
	</div>
	<div class="btn-area">
		<?php if($this->session->userdata['get_privilege'][10]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){?>
		<button class="std-btn bkgr-green" onclick="add_month()"><?php echo $this->lang->line('add')?></button>
		<?php } ?>
	</div>




</div>
<div id="json"></div>

<div class="ontop" id="disable-background"></div>
<div class="ontop2" id="disable-background2"></div>

<div class="view-modify-rate" id="modify-rate">
	<div class="title-rate" id="title-modif" ><?php echo $this->lang->line('Exchange Rate')?></div>
	<div class="rate-content" id="rate-content">
		<?php foreach ($all_currency->result() as $all) {?>
		<?php
			if($all->currencysymbol=='USD') {
				$disabled = 'style="display:none"';
			} else{
				$disabled = 'style="display:block"';
			} 
		?>
		<div class="rows" <?php echo $disabled?>>
			<div class="cur-name"><?php echo $all->currencysymbol?></div>
			<div class="cur-rate"><input class="std-input" name="currency_list" type="text" id="s_<?php echo $all->id?>" disabled></div>

		</div>
		<script type="text/javascript">
			$("#s_<?php echo $all->id?>").number(true,6);
		</script>
		<?php }?>		
	</div>
	<div class="btn-cmd">
		<button class="std-btn bkgr-blue" onclick="" id="save_modif"><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" onclick="cancel_modif()"><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="view-modify-rate" id="add-rate">
	<div class="title-rate"><?php echo $this->lang->line('Add Exchange Rate')?></div>
	<div class="rate-content" id="add-rate-content">
		<div class="rows">
			<div class="cur-name">
			<?php echo $this->lang->line('Year')?>
			</div>
			<div class="cur-rate">
				<select class="std-select" style="width:150px" id="new-year">
				<?php
					$current_year_found = false;
					if($all_year->num_rows()>0){
						foreach ($all_year->result() as $row) {
							# code...
							if($row->year == $current_year){
								$selected = 'selected';
								$curr_year_found = true;
							}else{
								$selected='';
							}
							echo '<option value="'.$row->year.'" '.$selected.'>'.$row->year.'</option>';
						}
					} 
					if(!$curr_year_found){
						echo '<option value="'.$current_year.'" selected>'.$current_year.'</option>';
					}
				?>
				</select>
			</div>
		</div>
		<div class="rows">
			<div class="cur-name">
			<?php echo $this->lang->line('Month')?>
			</div>
			<div class="cur-rate">
				<select class="std-select" style="width:150px" id="new-month">
				<?php
					
					foreach ($all_month as $row) {
						if($row['m'] == $current_month){
							$selected='selected';
						}else{
							$selected='';
						}							;
						echo '<option name ="'.$current_month.'" value="'.$row['m'].'" '.$selected.'>'.$row['month'].'</option>';
						
					}
				?>		
				</select>
			
			</div>
		</div>

		
		<?php foreach ($all_currency->result() as $all) {?>
		<?php
			if($all->currencysymbol=='USD') {
				$value = 'value="1"';
				$disabled = 'disabled';
			} else{
				$value = '';
				$disabled = '';
			}
		?>
		<div class="rows">
			<div class="cur-name"><?php echo $all->currencysymbol?></div>
			<div class="cur-rate"><input class="std-input" text="text" id="new_<?php echo $all->id?>" <?php echo $value ?> <?php echo $disabled?>></div>

		</div>
		<script type="text/javascript">
			$("#new_<?php echo $all->id?>").number(true,6);
		</script>
		<?php }?>		
	</div>
	<div class="btn-cmd">
		<button class="std-btn bkgr-blue" onclick="save_add()"><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','add-rate')"><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="view-modify-rate" id="not-allowed">
	<div class="title-rate"><?php echo $this->lang->line('Exchange Rate')?></div>
	<div class="rate-content" id="msg-content" style="text-align:center">
		<?php echo $this->lang->line('not_allowed')?>		
	</div>
	<div class="btn-cmd">
		<button class="std-btn bkgr-blue" onclick="hide('disable-background','not-allowed')" id="ok-msg"><?php echo $this->lang->line('ok')?></button>
		
	</div>
</div>

<script type="text/javascript">

	<?php if($this->session->userdata['get_privilege'][10]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){?>
		$('#save_modif').prop( "disabled", false );
		$("[name='currency_list']").prop( "disabled", false );
	<?php }else{ ?>
		$('#save_modif').prop( "disabled", true );
		$("[name='currency_list']").prop( "disabled", true );
	<?php } ?>
	function save_add(){
		var selected_month = $("#new-year").val()+'-'+$("#new-month").val();
		var curr_month = "<?php echo $current_year?>-<?php echo $current_month?>";
		if(curr_month<selected_month){
			hide('disable-background','add-rate');
			pop('disable-background','not-allowed');
		}else{
			//alert('go head save');
			// check field
			var check_null = false;
			<?php
				foreach ($all_currency->result() as $row) {?>
					if( $("#new_<?php echo $row->id?>").val()<=0  ) {
						check_null= true;
					}


				 <?php } 
			?>
			if(check_null){
				document.getElementById('msg-content').innerHTML="<?php echo $this->lang->line('invalid_rate')?>";
				hide('disable-background','add-rate');
				//document.getElementById("ok-msg").onclick = "hide('disable-background','not-allowed');pop('disable-background','add-rate')";
				pop('disable-background','not-allowed');
			}else{
				var json = '<?php echo json_encode($all_currency->result_array());?>';
				var new_obj;
				var arr_obj=[];
				obj = jQuery.parseJSON(json);
				var y = Number($("#new-year").val());
				var m = Number($("#new-month").val());
				obj.forEach(function(item,index){
					//alert($("#s_"+item['id']).val());
					new_obj = {
								year:y,
								 month:m,
								 currency_id:item['id'],
								 rate:$("#new_"+item['id']).val()};
					arr_obj.push(new_obj);

				});

				//document.getElementById('json').innerHTML=JSON.stringify(arr_obj);
				$.ajax({
		              type: "POST",  
		              url: "<?php echo base_url()?>index.php/admin/add_rate",  
		              contentType: 'application/x-www-form-urlencoded',
		              data: { 
		                    json: JSON.stringify(arr_obj),
		                    sess: "<?php echo session_id()?>"
		              },
		              dataType: "text",
		              beforeSend: function(){

		              },
		              complete: function(){
		                    
		              },
		              success: function(data){
		              		//alert(data);
		                	var obj = JSON.parse(data);
		                	switch(obj.status){
		                		case 0 :
		                			location.reload();
		                			break;
		                		default:
		                			document.getElementById('msg-content').innerHTML=obj.msg;
		                			hide('disable-background','add-rate');
		                			pop('disable-background','not-allowed');
		                			break;
		                	}
		                    
		              }
		        });



			}

		}

		
	}

	function add_month(){
		pop('disable-background','add-rate');
	}

	function go_save_modif(y,m){
		var json = '<?php echo json_encode($all_currency->result_array());?>';
		//alert(json);
		var new_obj;
		var arr_obj=[];
		obj = jQuery.parseJSON(json);
		obj.forEach(function(item,index){
			//alert($("#s_"+item['id']).val());
			new_obj = {
						year:y,
						 month:m,
						 currency_id:item['id'],
						 rate:$("#s_"+item['id']).val()};
			arr_obj.push(new_obj);

		});
		//alert(JSON.stringify(arr_obj));
		//document.getElementById('json').innerHTML=JSON.stringify(arr_obj);
		//console.log(JSON.stringify(arr_obj));
		$.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/admin/update_rate",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                    json: JSON.stringify(arr_obj),
                    sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                    
              },
              success: function(data){
              		//alert(data);
                	var obj = JSON.parse(data);
                	switch(obj.status){
                		case 0 :
                			location.reload();
                			break;
                		default:
                			document.getElementById('msg-content').innerHTML=obj.msg;
                			pop('disable-background','not-allowed');
                			break;
                	}
                    
              }
        });
	}

	function cancel_modif(){
		hide('disable-background','modify-rate');
	}

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}

	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}	

	function view_rate(y,m,txt_month){

		document.getElementById('title-modif').innerHTML = "<?php echo $this->lang->line('Exchange Rate')?> : "+txt_month+' '+y;
		
		$.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/admin/get_rate_form",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                    month: m,
                    year:y,
                    sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                    
              },
              success: function(data){
                //alert(data);
                 var obj = jQuery.parseJSON(data);
                //document.getElementById('rate-content').innerHTML=data;
                if(obj.length>0){
                	obj.forEach(function(item,index){
                		//alert(item['currency_id']);
                		if ($("#s_"+item['currency_id']).length){
                			var rate_value = item['rate'];
                		}else{
                			var rate_value = 0;
                		}
                		$("#s_"+item['currency_id']).val(rate_value);
                	});
                }

                document.getElementById("save_modif").onclick = function() {go_save_modif(y,m)};  
                    
              }
        });


		pop('disable-background','modify-rate');


	}
</script>