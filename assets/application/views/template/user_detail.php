<div class="user_detail">
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('id')?>
		</div>
		:
		<div class="detail-item">
			#<?php echo $id ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('username')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $user_name ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('email')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $email ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('first_name')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $first_name ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('last_name')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $last_name ?>
		</div>
	</div>	
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('group')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $group_name ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('last_login')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $last_login ?>
		</div>
	</div>
	<div class="rows">
		<div class="label-item">
			<?php echo $this->lang->line('status')?>
		</div>
		:
		<div class="detail-item">
			<?php echo $status ?>
		</div>
	</div>

</div>
<div class="area-btn">
	<button class="std-btn bkgr-green" onclick="modify()"><?php echo $this->lang->line('modify') ?></button>
</div>


<div class="ontop" id="disable-background">

</div>

<div class="pop-user-modify" id="win-user-modify">
	<div class="modify-title">
		<?php echo $this->lang->line('modify')?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('id')?>
			</div>
			:
			<div class="detail-item">
				#<?php echo $id ?>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('username')?>
			</div>
			:
			<div class="detail-item">
				<?php echo $user_name ?>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('email')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="email" name="email" value="<?php echo $email ?>" class="input-items">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('first_name')?>
			</div>
			:
			<div class="detail-item">
				<input  class="std-input" id="first_name" name="first_name" value="<?php echo $first_name ?>" class="input-items">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('last_name')?>
			</div>
			:
			<div class="detail-item">
				<input  class="std-input" id="last_name" name="last_name" value="<?php echo $last_name ?>" class="input-items">
			</div>
		</div>	
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group')?>
			</div>
			:
			<div class="detail-item">
				<?php echo $select_group ?>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('last_login')?>
			</div>
			:
			<div class="detail-item">
				<?php echo $last_login ?>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('status')?>
			</div>
			<double>:</double>
			<div class="detail-item">
				<?php echo $status_radio ?>
			</div>
		</div>

		<div class="inner-area-btn">
			<button class="std-btn bkgr-blue" onclick="pass()"><?php echo $this->lang->line('Change Password') ?></button>
			<button class="std-btn bkgr-green" onclick="save()"><?php echo $this->lang->line('save') ?></button>
			<button class="std-btn bkgr-red" onclick="hide('disable-background','win-user-modify')"><?php echo $this->lang->line('cancel') ?></button>
		</div>

	</div>

</div>


<div class="ontop" id="disable-background2">

</div>

<div class="ontop" id="disable-background3">

</div>

<div class="pop-ldap-user" id="ldap-user">
	<div class="modify-title">
		<?php echo $this->lang->line("Can't change password"); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('ldap_user'); ?>
	</div>
	<div class="confirm-btn">
		<button onClick="hide('disable-background2','ldap-user');"><?php echo $this->lang->line('ok'); ?></button>
	</div>

</div>

<div class="pop-ch-pass" id="ch-pass">
	<div class="modify-title">
		<?php echo $this->lang->line("Change password"); ?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('New password')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="new-password" name="new-password" class="input-items" type="password">
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('Retype password')?>
			</div>
			:
			<div class="detail-item">
				<input class="std-input" id="retype-password" name="retype-password" class="input-items" type="password">
			</div>
		</div>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="change_pass();"><?php echo $this->lang->line('ok'); ?></button>
		<button class="std-btn bkgr-red" onClick="hide('disable-background2','ch-pass');"><?php echo $this->lang->line('cancel'); ?></button>
	</div>

</div>

<div class="pop-msg-pass" id="msg-pass">
	<div class="modify-title">
		<?php echo $this->lang->line("Notify"); ?>
	</div>
	<div class="confirm-message" id="notify-pass">
		
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background2','msg-pass');"><?php echo $this->lang->line('ok'); ?></button>
	</div>

</div>


<script type="text/javascript">
	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}
	function cancel(){
		hide('disable-background','win-del-confirm');
	}
	//To detect escape button
	/*
	document.onkeydown = function(evt) {
		evt = evt || window.event;
		if (evt.keyCode == 27) {
			hide('disable-background','win-user-modify');
		}
	};
	*/
	function modify(){
		$("#email").val("<?php echo $email ?>");
		$("#first_name").val("<?php echo $first_name ?>");
		$("#last_name").val("<?php echo $last_name ?>");
		$("#grp").val("<?php echo $user_group ?>");
		$("#status").val("<?php echo $user_status ?>");
		pop('disable-background','win-user-modify');
	}

	function save(){
		if(($("#email").val().trim() == '') || validateEmail()){
			$.post("<?php echo base_url()?>index.php/admin/save_user",
		    {
		        id: "<?php echo $id ?>",
		        email: $("#email").val().trim(),
		        f: $("#first_name").val(),
		        l: $("#last_name").val(),
		        g: $("#grp").val(),
		        stat: $("#status").val(),
		        s: "<?php echo session_id()?>"
		    },
		    function(data, status){
		    	location.reload();
		        //alert("Data: " + data + "\nStatus: " + status);

		    });
		}

	}

	function validateEmail() {
	    var x = $("#email").val();
	    var atpos = x.indexOf("@");
	    var dotpos = x.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
	        alert("<?php echo $this->lang->line('Not a valid e-mail address')?>");
	        return false;
	    }else{
	    	return true;
	    }
	}

	function pass(){
		// check ldp user
		var ldap_user;
		$.post("<?php echo base_url()?>index.php/admin/check_ldap",
	    {
	        id: "<?php echo $id ?>",
	        s: "<?php echo session_id()?>"
	    },
	    function(data, status){
	    	//location.reload();
	        //alert("Data: " + data + "\nStatus: " + status);
	        
	        if(data==0){
	        	// change password
	        	$("#new-password").val('');
	        	$("#retype-password").val('');

	        	pop('disable-background2','ch-pass');
	        }else{
	        	// cant change password
	        	pop('disable-background2','ldap-user');


	        }
	    });
	}

	function change_pass(){
		if(($("#new-password").val().trim() != '') && ($("#retype-password").val().trim() != '')){
			$.post("<?php echo base_url()?>index.php/admin/change_pass",
		    {
		        id: "<?php echo $id ?>",
		        p1: $("#new-password").val(),
		        p2: $("#retype-password").val(),
		        s: "<?php echo session_id()?>"
		    },
		    function(data, status){
		    	//location.reload();
		        //alert("Data: " + data + "\nStatus: " + status);
		        hide('disable-background2','ch-pass');
		        document.getElementById("notify-pass").innerHTML = data;
		        pop('disable-background2','msg-pass');

		        
		    });
		}else{
			 hide('disable-background2','ch-pass');
		     document.getElementById("notify-pass").innerHTML = "<?php echo $this->lang->line('Password you entered is not the same')?>";
		     pop('disable-background2','msg-pass');
		}
	}

</script>