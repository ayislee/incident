<style type="text/css">
.rut_content {
  margin-left: 25px;
  margin-top: 5px;
  display: block;
  background-color: #ffc711;
  padding-top: 1px;
  padding-bottom: 1px;
  padding-left: 20px;
  border-radius: 5px;
  width: 850px;
}

.rut-btn-block {
  margin-left: 25px;
  margin-top: 10px;
  display: block;
  background-color: white;
  padding-top: 1px;
  padding-bottom: 1px;
  padding-left: 20px;
  border-radius: 5px;
  width: 850px;
  text-align: right;
}

.table-risk {
  margin-left: 25px;
  width: 850px;
  display: block;
}	
</style>
<div class="rut_content">
	<div class="label-item width10">
		<?php echo $this->lang->line('Risk Universe Type')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="rutid" id="rutid">
			<?php foreach ($rut->result() as $row) { 
				if($selected_rut==$row->rutid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->rutid?>" <?php echo $selected?>><?php echo $row->rutname?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Section Title')?>
	</div>
	<div class="detail-item width250">
		<input class="std-input width250" name="stname" id="stname">
	</div>
</div>

<div class="rut-btn-block">
      <button class="std-btn bkgr-green" onclick="insert_st()"><?php echo $this->lang->line('add')?>
</div>

<div class="rut-title">
      <?php echo $this->lang->line('Section Title List')?>
</div>

<div class="table-risk">
      <div class="table-head">
            <!-- <div class="table-head-item rut-col-1"><?php echo $this->lang->line('id')?></div> -->
            <div class="table-head-item rut-col-2"><?php echo $this->lang->line('Section Title List')?></div>
      </div>
      <?php
            foreach ($st->result() as $row) {
      ?>
      <div class="table-body">
            <!-- <div class="table-body-item rut-col-1"><?php echo $row->stid?></div> -->
            <div class="table-body-item rut-col-2"><?php echo $row->stname?></div>
            <div class="table-body-item right"><button class="std-btn bkgr-red" onclick="delete_id(<?php echo $row->stid?>)"><?php echo $this->lang->line('delete')?></button></div>
            <div class="table-body-item right"><button class="std-btn bkgr-blue" onclick="modify_id(<?php echo $row->stid?>,'<?php echo $row->stname?>')"><?php echo $this->lang->line('modify')?></button></div>
      </div>
      <?php
             } 
      ?>

</div>


<div class="ontop" id="disable-background"></div>

<div class="add-access-module" id="win-msg">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="detail-group-error"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-green" onClick="hide('disable-background','win-msg')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>

<div class="del-access-module" id="del-st">
    <div class="modify-title">
        <?php echo $this->lang->line('delete'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Are you sure to delete'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onClick="remove_st()"><?php echo $this->lang->line('delete'); ?></button>
        <button class="std-btn bkgr-grey" onClick="hide('disable-background','del-st')"><?php echo $this->lang->line('cancel'); ?></button>
    </div>
</div>

<div class="add-modify-ui-detail" id="modify-st">
      <div class="modify-title" id="title-modify-detail"></div>
      <div class="rows">
            <input type="hidden" name="modify-stid" id="modify-stid">
            <input class="input-ui-detail" id="modify-stname" name="modify-stname" type="text">
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-st" onclick="update_st()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-st')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>

<script type="text/javascript">
	var delete_st_id;
	$( "#rutid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/st/"+$("#rutid").val();
	});

	function pop(div,div2) {
	    document.getElementById(div).style.display = 'block';
	    document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
	    document.getElementById(div).style.display = 'none';
	    document.getElementById(div2).style.display = 'none';
	}

	function insert_st(){
		//alert('hahh');
		$.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/insert_st",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 
				rutid: $("#rutid").val(),
				stname: $("#stname").val(),
				sess: "<?php echo session_id()?>"
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
				
			},
			success: function(data){
			    //alert(data);
				switch(data){
					case '0':
						location.reload();
						break;
					case '1':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to save data')?>";
						pop('disable-background','win-msg');
						break;
					case '2':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Risk Universe Type Name is blank')?>";
						pop('disable-background','win-msg');
						break;
					case '3':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
						pop('disable-background','win-msg');
						break;
				}
			}
		});

	}

	function delete_id(id){
		delete_st_id = id;
           pop('disable-background','del-st');
	}

	function remove_st(){
	    //alert(delete_rut_id);
	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/admin/delete_st",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	                stid: delete_st_id,
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	                //alert(data);
	                switch(data){
	                      case '0':
	                            location.reload();
	                            break;
	                      case '1':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to delete data')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                      case '2':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                }
	          }
	    });
	}

	function modify_id(id,stname){
	    $("#modify-stname").val(stname);
	    $("#modify-stid").val(id);
	    pop('disable-background','modify-st');
	}

	function update_st(){
	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/admin/update_st",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	          		rutid: $("#rutid").val(),
	                stid: $("#modify-stid").val(),
	                stname: $("#modify-stname").val(),
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	                //alert(data);
	                switch(data){
	                      case '0':
	                            location.reload();
	                            break;
	                      case '1':
	                            hide('disable-background','modify-st');
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to update data')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                      case '2':
	                            hide('disable-background','modify-st');
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Section Title  Name is blank')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                      case '3':
	                            hide('disable-background','modify-st');
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                }
	          }
	    });
	}

</script>