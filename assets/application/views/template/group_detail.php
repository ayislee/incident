<style type="text/css">
	.group-col-1 {
		display: inline-block;
		width: 300px;
	}

	.w1000 {
		width: 1000px;
	}
	.privilege_selected {
		background-color: #4A4C4E;
		color: white;
	}

	.padding-top {
		padding-top: 5px;
	}

	.a-link {
		background-color: none !important;
		border-style: none;
	}

	.float-left{
		display: inline-block;
		float: left;
		padding-top: 10px;
	}

	.head-table-text {
		color: white;
		font-weight: bold;
	}

</style>

<div class="group_detail w1000">
	<div class="group-col-1">
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('id') ?></div>:
	    	<div class="detail-item">#<?php echo $id ?></div>
	    </div>
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('group_name') ?></div>:
	    	<div class="detail-item width150"><?php echo $group_name ?></div>
	    </div>
    </div>
    <!--
    <div class="rows">
    	<div class="label-item width150"> <?php echo $this->lang->line('group_type') ?></div>:
    	<div class="detail-item"><?php echo $group_type_name ?></div>
    </div>
    <div class="rows">
    	<div class="label-item width150"> <?php echo $this->lang->line('authentification_type') ?></div>:
    	<div class="detail-item"><?php echo $authen_type_name ?></div>
    </div>
    -->
    <div class="group-col-1">
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('date_created') ?></div>:
	    	<div class="detail-item"><?php echo $date_created ?></div>
	    </div>
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('created_by') ?></div>:
	    	<div class="detail-item"><?php echo $created_by ?></div>
	    </div>
	</div>

	<div class="group-col-1">
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('date_update') ?></div>:
	    	<div class="detail-item"><?php echo $date_update ?></div>
	    </div>
	    <div class="rows">
	    	<div class="label-item width150"> <?php echo $this->lang->line('update_by') ?></div>:
	    	<div class="detail-item"><?php echo $updated_by ?></div>
	    </div>
	</div>
    <div class="rows">
    	<div class="label-item width150"> <?php echo $this->lang->line('status') ?></div>:
    	<div class="detail-item"><?php echo $group_status_name ?></div>
    </div>
</div>

<div class="rows btn-group w1000">
	<div class="float-left">
		|<div class="detail-item"><a href="#" class="a-link " onclick="load_module()" name="group-opco"><?php echo $this->lang->line('Module Privelege') ?></a></div>
		<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
		|<div class="detail-item"><a href="#" class="a-link " onclick="load_privilege()" name="privilege-2"><?php echo $this->lang->line('Menu Privelege') ?></a></div>
		<?php } ?>
		|<div class="detail-item"><a href="#" class="a-link " onclick="load_opco()" name="group-opco"><?php echo $this->lang->line('OpCo Configuration Privelege') ?></a></div>
		|<div class="detail-item"><a href="#" class="a-link " onclick="load_incident()" name="group-incident"><?php echo $this->lang->line('Incident Management Privelege') ?></a></div>
		|<div class="detail-item"><a href="#" class="a-link " onclick="load_report()" name="group-report"><?php echo $this->lang->line('Report Privelege') ?></a></div>|
	</div>
	<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
    <div class="detail-item"><button class="std-btn bkgr-blue"onclick="modify()" name="privilege-2"><?php echo $this->lang->line('modify') ?></button></div>
    
	<?php } ?>
</div> 

<div class="ontop" id="disable-background"></div>


<div class="del-confirm" id="win-del-confirm">
	<div class="confirm-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('delete_group_confirm_message'); ?>
	</div>
	<div class="confirm-btn">
		<button onClick="confirm_delete_group()"><?php echo $this->lang->line('yes'); ?></button>
		<button onClick="cancel_delete_group()"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>


<div class="pop-user-modify" id="edit-group">
	<div class="modify-title">
		<?php echo $this->lang->line('modify')?>
	</div>
	<div class="tips">
		<?php echo $this->lang->line('You can modify this Group Detail below')?>
	</div>
	<div class="modify-content">
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('id')?>
			</div>
			:
			<div class="detail-item">
				#<?php echo $id ?>
			</div>
		</div>
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group_name')?>
			</div>:
			<div class="detail-item">
				<input class="std-input" id="group_name" name="group_name" value="<?php echo $group_name ?>" class="input-items">
			</div>
		</div>
		<!--
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('group_type')?>
			</div>:
			<div class="detail-item">
				<?php echo $select_group_type?>
			</div>
		</div>	
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('authentification_type')?>
			</div>:
			<div class="detail-item">
				<?php echo $select_authen_type?>
			</div>
		</div>		
		-->
		<div class="rows">
			<div class="label-item width10">
				<?php echo $this->lang->line('status')?>
			</div>:
			<div class="detail-item">
				<?php echo $select_status?>
			</div>
		</div>

		<div class="rows center10">
			<button class="std-btn bkgr-green" onClick="save_modify()"><?php echo $this->lang->line('save'); ?></button>
			<button class="std-btn bkgr-red" onClick="hide('disable-background','edit-group')"><?php echo $this->lang->line('cancel'); ?></button>
		</div>	
	</div>
	
</div>

<div class="add-access-module" id="result-group-error">
	<div class="modify-title">
		<?php echo $this->lang->line('Error Message'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','result-group-error');pop('disable-background','edit-group');"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<script type="text/javascript">

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}


	function modify(){
		$("#group_name").val("<?php echo $group_name?>");
		$("#group_type").val("<?php echo $group_type?>");
		$("#authen_type").val("<?php echo $authen_type?>");
		$("#group_status").val("<?php echo $group_status?>");

		pop('disable-background','edit-group');
	}

	function deleted(){
		pop('disable-background','win-del-confirm');
	}

	function save_modify(){
		//alert('save_modify');
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/update_group",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	id: "<?php echo $id?>",
            	group_name: $("#group_name").val(),
            	//group_type: $("#group_type").val(),
            	//authen_type: $("#authen_type").val(),
            	group_type: null,
            	authen_type: null,
            	group_status: $("#group_status").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	switch(data){
            		case '0':
            			location.reload();
            			break;
            		case '1':
            			hide('disable-background','edit-group');
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Group name already use')?>";
            			pop('disable-background','result-group-error');
            			break;
            		case '2':
            			hide('disable-background','edit-group');
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
            			pop('disable-background','result-group-error');
            			break;
            		case '3':
            			hide('disable-background','edit-group');
            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('group_name_blank')?>";
            			pop('disable-background','result-group-error');
            			break;
            	}
            }
		});		
	}

	function cancel_delete_group(){
		hide('disable-background','win-del-confirm');
	}

	function load_privilege(){
		document.location.href="<?php echo base_url()?>index.php/admin/privilege/<?php echo $id ?>";
	}

	function load_module(){
		document.location.href="<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $id ?>/1";	
	}

	function load_opco(){
		document.location.href="<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $id ?>/3";	
	}

	function load_incident(){
		document.location.href="<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $id ?>/4";	
	}
	function load_report(){
		document.location.href="<?php echo base_url()?>index.php/admin/group_management_detail/<?php echo $id ?>/5";	
	}
</script>
