<div class="rut_content">
	<div class="label-item width10">
		<?php echo $this->lang->line('Risk Universe Type')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="rutid" id="rutid">
			<?php foreach ($rut->result() as $row) { 
				if($selected_rut==$row->rutid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->rutid?>" <?php echo $selected?>><?php echo $row->rutname?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Section Title')?>
	</div>
	<div class="detail-item width250">
		<select class="std-select width250" name="stid" id="stid">
			<?php foreach ($st->result() as $row) { 
				if($selected_st==$row->stid) $selected='selected'; else $selected='';?>
			<option value="<?php echo $row->stid?>" <?php echo $selected?>><?php echo $row->stname?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="rut_content margin-top-3">
	<div class="label-item width10">
		<?php echo $this->lang->line('Operator Category')?>
	</div>
	<div class="detail-item width250">
		<input class="std-input width250" name="ocname" id="ocname">
	</div>
</div>

<div class="rut-btn-block">
      <button class="std-btn bkgr-green" onclick="insert_oc()"><?php echo $this->lang->line('add')?>
</div>

<div class="rut-title">
      <?php echo $this->lang->line('Operator Category List')?>
</div>

<div class="table-risk">
      <div class="table-head">
            <div class="table-head-item rut-col-1"><?php echo $this->lang->line('id')?></div>
            <div class="table-head-item rut-col-2"><?php echo $this->lang->line('Operator Category')?></div>
      </div>
      <?php
            foreach ($oc->result() as $row) {
      ?>
      <div class="table-body">
            <div class="table-body-item rut-col-1"><?php echo $row->ocid?></div>
            <div class="table-body-item rut-col-2"><?php echo $row->ocname?></div>
            <div class="table-body-item right"><button class="std-btn bkgr-red" onclick="delete_id(<?php echo $row->ocid?>)"><?php echo $this->lang->line('delete')?></button></div>
            <div class="table-body-item right"><button class="std-btn bkgr-blue" onclick="modify_id(<?php echo $row->ocid?>,'<?php echo $row->ocname?>')"><?php echo $this->lang->line('modify')?></button></div>
      </div>
      <?php
             } 
      ?>

</div>


<div class="ontop" id="disable-background"></div>

<div class="add-access-module" id="win-msg">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="detail-group-error"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-green" onClick="hide('disable-background','win-msg')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>

<div class="del-access-module" id="del-oc">
    <div class="modify-title">
        <?php echo $this->lang->line('delete'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Are you sure to delete'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onClick="remove_oc()"><?php echo $this->lang->line('delete'); ?></button>
        <button class="std-btn bkgr-grey" onClick="hide('disable-background','del-oc')"><?php echo $this->lang->line('cancel'); ?></button>
    </div>
</div>

<div class="add-modify-ui-detail" id="modify-oc">
      <div class="modify-title" id="title-modify-detail"></div>
      <div class="rows">
            <input type="hidden" name="modify-ocid" id="modify-ocid">
            <input class="input-ui-detail" id="modify-ocname" name="modify-ocname" type="text">
      </div>
      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-st" onclick="update_oc()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-oc')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>

<script type="text/javascript">
	$( "#rutid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/oc/"+$("#rutid").val();
	});	

	$( "#stid" ).change(function() {
  		window.location.href="<?php echo base_url()?>index.php/admin/oc/"+$("#rutid").val()+"/"+$("#stid").val();
	});

	var delete_oc_id;

	function pop(div,div2) {
	    document.getElementById(div).style.display = 'block';
	    document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
	    document.getElementById(div).style.display = 'none';
	    document.getElementById(div2).style.display = 'none';
	}

	function insert_oc(){
		//alert('hahh');
		$.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/insert_oc",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 
				stid: $("#stid").val(),
				ocname: $("#ocname").val(),
				sess: "<?php echo session_id()?>"
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
				
			},
			success: function(data){
			    //alert(data);
				switch(data){
					case '0':
						location.reload();
						break;
					case '1':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to save data')?>";
						pop('disable-background','win-msg');
						break;
					case '2':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Please fill in all fields')?>";
						pop('disable-background','win-msg');
						break;
					case '3':
			                
						document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
						pop('disable-background','win-msg');
						break;
				}
			}
		});

	}

	function delete_id(id){
		delete_oc_id = id;
        pop('disable-background','del-oc');
	}

	function remove_oc(){
	    //alert(delete_rut_id);
	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/admin/delete_oc",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	                ocid: delete_oc_id,
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	                //alert(data);
	                switch(data){
	                      case '0':
	                            location.reload();
	                            break;
	                      case '1':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to delete data')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                      case '2':
	                            document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	                            pop('disable-background','win-msg');
	                            break;
	                }
	          }
	    });
	}

	function modify_id(id,ocname){
	    $("#modify-ocname").val(ocname);
	    $("#modify-ocid").val(id);
	    pop('disable-background','modify-oc');
	}

	function update_oc(){
	    $.ajax({
			type: "POST",  
			url: "<?php echo base_url()?>index.php/admin/update_oc",  
			contentType: 'application/x-www-form-urlencoded',
			data: { 
				stid: $("#stid").val(),
			    ocid: $("#modify-ocid").val(),
			    ocname: $("#modify-ocname").val(),
			    sess: "<?php echo session_id()?>"
			},
			dataType: "text",
			beforeSend: function(){

			},
			complete: function(){
			    
			},
			success: function(data){
			    //alert(data);
			    switch(data){
			          case '0':
			                location.reload();
			                break;
			          case '1':
			                hide('disable-background','modify-oc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to update data')?>";
			                pop('disable-background','win-msg');
			                break;
			          case '2':
			                hide('disable-background','modify-oc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Operator Category Name is blank')?>";
			                pop('disable-background','win-msg');
			                break;
			          case '3':
			                hide('disable-background','modify-oc');
			                document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
			                pop('disable-background','win-msg');
			                break;
			    }
			}
		});
	}	
</script>