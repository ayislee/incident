<style type="text/css">
.user-search input {
	
	width: 125px;
	background-color: #cbcbcb;
	border: none;
	border-radius: 10px;
	outline: none;
	padding-left: 10px;
	padding-right: 30px;
	height: 20px;
}
</style>
<div class="user-search">
	<?php echo $this->lang->line('username')?> 
	<input name="user-search" id="user-search" value="<?php echo $user_search ?>">
	<button class="button-search" id="search" name="search"><i class="fa fa-search" aria-hidden="true"></i></button>
</div>

<script type="text/javascript">
	$('#search').click(function(){
		window.location.href = '<?php echo base_url()?>index.php/admin/user_management?s='+$('#user-search').val(); //Will take you to Google.
	});

	$("#user-search").keypress(function (e) {
	    if (e.which == 13) {
	        window.location.href = '<?php echo base_url()?>index.php/admin/user_management?s='+$('#user-search').val();
	    }
	});
</script>
