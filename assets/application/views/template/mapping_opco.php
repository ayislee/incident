<style type="text/css">
    .area-save {
        margin-left: 25px;
        width: 1000px;

    }
    .f-r{
        float: right;
    }
</style>
<?php if($this->session->userdata('get_privilege')[1]['map_group'] || $this->session->userdata('group_id')==ADMIN_GROUP_ID){ ?>
<div class="area-save w1000">
    <div class="f-r">
        <button class="std-btn bkgr-green" onclick="opco_save()"><?php echo $this->lang->line('save')?></button>
        <button class="std-btn bkgr-red" onclick="opco_cancel()"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>
<?php } ?>
<div id="query"></div>
<div id="dt-module"></div>
<div class="del-access-module" id="save-module">
    <div class="modify-title">
        <?php echo $this->lang->line('Notify'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Configuration has been saved'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onClick="location.reload()"><?php echo $this->lang->line('ok'); ?></button>
    </div>
</div>
<script type="text/javascript">
    function change_opco(opco_id,group_id){
        //alert("change_opco");
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_view").prop('checked'));
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_add").prop('checked'));
        //alert($("#opco_"+opco_id+"_group_"+group_id+"_update").prop('checked'));

        if($("#opco_"+opco_id+"_group_"+group_id+"_view").prop('checked')){
            val_view = 1;
        }else{
            val_view = 0;
        }
        if($("#opco_"+opco_id+"_group_"+group_id+"_add").prop('checked')){
            val_add = 1;
        }else{
            val_add = 0;
        }
        
        if($("#opco_"+opco_id+"_group_"+group_id+"_update").prop('checked')){
            val_update = 1;
        }else{
            val_update = 0;
        }

        if($("#opco_"+opco_id+"_group_"+group_id+"_add_all_month").prop('checked')){
            val_add_all_month = 1;
        }else{
            val_add_all_month = 0;
        }

        if($("#opco_"+opco_id+"_group_"+group_id+"_update_all_month").prop('checked')){
            val_update_all_month = 1;
        }else{
            val_update_all_month = 0;
        }

        val_view_incident = $("#opco_"+opco_id+"_group_"+group_id+"_view_incident").val();
        val_create_incident = $("#opco_"+opco_id+"_group_"+group_id+"_create_incident").val();
        val_update_incident = $("#opco_"+opco_id+"_group_"+group_id+"_update_incident").val();
        val_bulk_incident = $("#opco_"+opco_id+"_group_"+group_id+"_bulk_incident").val();
        // create array
        var obj_opco = {
            group_id: group_id,
            opco_id: opco_id,
            view: val_view,
            add: val_add,
            update:val_update,
            add_all_month:val_add_all_month,
            update_all_month:val_update_all_month,
            view_incident:val_view_incident,
            create_incident: val_create_incident,
            update_incident:val_update_incident,
            bulk_incident:val_bulk_incident
        };

        
        return obj_opco;

        /*
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/change_map_opco",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: group_id,
                opco_id: opco_id,
                view: val_view,
                add: val_add,
                update:val_update,
                add_all_month:val_add_all_month,
                update_all_month:val_update_all_month,

                view_incident:val_view_incident,
                create_incident: val_create_incident,
                update_incident:val_update_incident,
                bulk_incident:val_bulk_incident,

                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //location.reload();
                //document.getElementById('query').innerHTML=data;
                //alert(data);
            }
        });
        */
        
    }

    function opco_save(){
        //alert("haha");
        var json = '<?php echo json_encode($opco_list->result_array())?>';
        var obj = JSON.parse(json);
        //alert(obj.length);
        //alert("jsdkj");
        var all_opco = [];
        obj.forEach(function(item, index){
            var list_module = item.opcoid;
            all_opco.push(change_opco(item.opcoid,<?php echo $id_group ?>));
            //alert(item.opconame); 
        });
        var json_str = JSON.stringify(all_opco);
        console.log(json_str);
        
        
        $.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/change_map_opco",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                json: json_str
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
                //location.reload();
                //document.getElementById('query').innerHTML=data;
                //alert(data);
                pop('disable-background','save-module');
            }
        });
        
        
    }

    function opco_cancel(){
        location.reload();
    }

</script>