<style type="text/css">
      .inline {
            display: inline-block;
            padding-left: 30px;
            vertical-align: middle;
      }

      .rut-col-2 {
            width: 300px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;

      }

      .rut-col-3 {
            width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;

      }

      .center-text {
            text-align: center;
      }

      .rut_content {
          margin-left: 25px;
          margin-top: 5px;
          display: block;
          background-color: #ffc711;
          padding-top: 1px;
          padding-bottom: 1px;
          padding-left: 20px;
          border-radius: 5px;
          width: 850px;
      }

      .rut-btn-block {
          margin-left: 25px;
          margin-top: 10px;
          display: block;
          background-color: white;
          padding-top: 1px;
          padding-bottom: 1px;
          padding-left: 20px;
          border-radius: 5px;
          width: 850px;
          text-align: right;
      }

      .table-risk {
          margin-left: 25px;
          width: 850px;
          display: block;
      }

</style>
<div class="rut_content">
	<div class="label-item width10">
		<?php echo $this->lang->line('Risk Universe Type')?>
	</div>
	<div class="detail-item width250">
		<input class="std-input width250" name="rutname" id="rutname">
	</div>
      <div class="checkbox rut-col-3 inline">
        <label><input type="checkbox" id="bypass"><?php echo $this->lang->line('bypass')?></label>
      </div>
</div>

<div class="rut-btn-block">
      <button class="std-btn bkgr-green" onclick="save_rut()"><?php echo $this->lang->line('add')?>
</div>

<div class="rut-title">
      <?php echo $this->lang->line('Risk Universe Type List')?>
</div>

<div class="table-risk">
      <div class="table-head">
            <!-- <div class="table-head-item rut-col-1"><?php echo $this->lang->line('id')?></div> -->
            <div class="table-head-item rut-col-2"><?php echo $this->lang->line('Risk Universe Type')?></div>
            <div class="table-head-item rut-col-3"><?php echo $this->lang->line('bypass')?></div>
      </div>
      <?php
            foreach ($rut->result() as $row) {
      ?>
      <div class="table-body">
            <!-- <div class="table-body-item rut-col-1"><?php echo $row->rutid?></div>-->
            <div class="table-body-item rut-col-2"><?php echo $row->rutname?></div>
            <?php if ($row->bypass == 1) $txt_baypass=$this->lang->line('Yes'); else $txt_baypass=$this->lang->line('No');?>
            <div class="table-body-item rut-col-3"><?php echo $txt_baypass?></div>
            <div class="table-body-item right"><button class="std-btn bkgr-red" onclick="delete_id(<?php echo $row->rutid?>)"><?php echo $this->lang->line('delete')?></button></div>
            <div class="table-body-item right"><button class="std-btn bkgr-blue" onclick="modify_id(<?php echo $row->rutid?>,'<?php echo $row->rutname?>',<?php echo $row->bypass?>)"><?php echo $this->lang->line('modify')?></button></div>
      </div>
      <?php
             } 
      ?>

</div>
<div class="ontop" id="disable-background"></div>

<div class="add-access-module" id="win-msg">
      <div class="modify-title">
            <?php echo $this->lang->line('Notify'); ?>
      </div>
      <div class="confirm-message" id="detail-group-error"></div>
      <div class="confirm-btn">
            <button class="std-btn bkgr-green" onClick="hide('disable-background','win-msg')"><?php echo $this->lang->line('ok'); ?></button>
      </div>
</div>

<div class="del-access-module" id="del-rut">
    <div class="modify-title">
        <?php echo $this->lang->line('delete'); ?>
    </div>
    <div class="confirm-message" id="detail-group-error"><?php echo $this->lang->line('Are you sure to delete'); ?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-red" onClick="remove_rut()"><?php echo $this->lang->line('delete'); ?></button>
        <button class="std-btn bkgr-grey" onClick="hide('disable-background','del-rut')"><?php echo $this->lang->line('cancel'); ?></button>
    </div>
</div>

<div class="add-modify-ui-detail" id="modify-rut">
      <div class="modify-title" id="title-modify-detail"></div>
      <div class="rows">
            <input type="hidden" name="modify-rutid" id="modify-rutid">
            <input class="input-ui-detail" id="modify-rutname" name="modify-rutname" type="text">

      </div>
      
      <div class="rows">
            <div class="checkbox center-text">
                  <label><input type="checkbox" id="modify-bypass"><?php echo $this->lang->line('bypass')?></label>
            </div>
      </div>

      <div class="rows center11">
            <button class="std-btn bkgr-green" id="btn-save-modify-rut" onclick="update_rut()"><?php echo $this->lang->line('save')?></button>
            <button class="std-btn bkgr-red" onclick="hide('disable-background','modify-rut')"><?php echo $this->lang->line('cancel')?></button> 
      </div>

</div>

<script type="text/javascript">
      var delete_rut_id;
      function pop(div,div2) {
            document.getElementById(div).style.display = 'block';
            document.getElementById(div2).style.display = 'block';

      }
      function hide(div,div2) {
            document.getElementById(div).style.display = 'none';
            document.getElementById(div2).style.display = 'none';
      }

	function save_rut(){
            var bypass = ( $("#bypass").is(':checked') ) ? 1 : 0;
		$.ajax({
			type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/save_rut",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                  	rutname: $("#rutname").val(),
                        bypass: bypass,
                  	sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                  	
                  },
                  success: function(data){
                        //alert(data);
                  	switch(data){
                  		case '0':
                  			location.reload();
                  			break;
                  		case '1':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to save data')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  		case '2':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Risk Universe Type Name is blank')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  		case '3':
                                    
                  			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                  			pop('disable-background','win-msg');
                  			break;
                  	}
                  }
		});
	}

      function delete_id(id){
            delete_rut_id = id;
            pop('disable-background','del-rut');
      }

      function remove_rut(){
            //alert(delete_rut_id);
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/delete_rut",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        rutid: delete_rut_id,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        switch(data){
                              case '0':
                                    location.reload();
                                    break;
                              case '1':
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to delete data')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '2':
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                                    pop('disable-background','win-msg');
                                    break;
                        }
                  }
            });
      }

      function modify_id(id,rutname,bypass){
            $("#modify-rutname").val(rutname);
            $("#modify-rutid").val(id);
            if(bypass==1) $('#modify-bypass').prop('checked', true); else $('#modify-bypass').prop('checked', false);

            pop('disable-background','modify-rut');
      }

      function update_rut(){
            var bypass = ( $("#modify-bypass").is(':checked') ) ? 1 : 0;
            $.ajax({
                  type: "POST",  
                  url: "<?php echo base_url()?>index.php/admin/update_rut",  
                  contentType: 'application/x-www-form-urlencoded',
                  data: { 
                        rutid: $("#modify-rutid").val(),
                        rutname: $("#modify-rutname").val(),
                        bypass: bypass,
                        sess: "<?php echo session_id()?>"
                  },
                  dataType: "text",
                  beforeSend: function(){

                  },
                  complete: function(){
                        
                  },
                  success: function(data){
                        //alert(data);
                        switch(data){
                              case '0':
                                    location.reload();
                                    break;
                              case '1':
                                    hide('disable-background','modify-rut');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Fail to update data')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '2':
                                    hide('disable-background','modify-rut');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Risk Universe Type Name is blank')?>";
                                    pop('disable-background','win-msg');
                                    break;
                              case '3':
                                    hide('disable-background','modify-rut');
                                    document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
                                    pop('disable-background','win-msg');
                                    break;
                        }
                  }
            });
      }

</script>