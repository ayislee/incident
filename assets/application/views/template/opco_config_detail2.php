<style type="text/css">
	.w-cc{
		width: 180px;
	}	
</style>

<div class="opco-content" >
	<div class="label-item-var"><?php echo $this->lang->line('OpCo')?></div>
	<?php echo $select_opco?>
	<div class="label-item align-right" ><?php echo $this->lang->line('Month')?></div>
	<?php echo $select_month?>
	<div class="label-item align-right"><?php echo $this->lang->line('Year')?></div>
	<?php echo $select_year?>
	<button class="std-btn bkgr-green text-right" onclick="reload_page()" id="btn-get-config"><?php echo $this->lang->line('Get Opco Configuration')?></button>
</div>

<div class="opco-content">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#general" class="tab-constant"><?php echo $this->lang->line('General')?></a></li>
	  <li><a data-toggle="tab" href="#cc" class="tab-constant w-cc"><?php echo $this->lang->line('Control Coverage')?></a></li>
	  <li><a data-toggle="tab" href="#bypass" class="tab-constant"><?php echo $this->lang->line('ByPass')?></a></li>
	  <li><a data-toggle="tab" href="#mfs" class="tab-constant"><?php echo $this->lang->line('MFS')?></a></li>
	  <li><a data-toggle="tab" href="#currency" class="tab-constant"><?php echo $this->lang->line('Currency')?></a></li>
	</ul>

	<div class="tab-content">
	  <div class="deviden"></div>
	  <div id="general" class="tab-pane fade in active">

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[0]['id']?>" id="id-<?php echo $config[0]['id']?>" value="<?php echo $config[0]['id']?>">
	  			<input type="hidden" name="<?php echo $config[0]['variable']?>" id="<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[1]['id']?>" id="id-<?php echo $config[1]['id']?>" value="<?php echo $config[1]['id']?>">
	  			<input type="hidden" name="<?php echo $config[1]['variable']?>" id="<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['variable']?>">
	  			<?php echo $config[0]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[0]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[0]['variable']?>" id="value-<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[1]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[1]['variable']?>" id="value-<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[2]['id']?>" id="id-<?php echo $config[2]['id']?>" value="<?php echo $config[2]['id']?>">
	  			<input type="hidden" name="<?php echo $config[2]['variable']?>" id="<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[3]['id']?>" id="id-<?php echo $config[3]['id']?>" value="<?php echo $config[3]['id']?>">
	  			<input type="hidden" name="<?php echo $config[3]['variable']?>" id="<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['variable']?>">
	  			<?php echo $config[2]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[2]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[2]['variable']?>" id="value-<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[3]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[3]['variable']?>" id="value-<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[4]['id']?>" id="id-<?php echo $config[4]['id']?>" value="<?php echo $config[4]['id']?>">
	  			<input type="hidden" name="<?php echo $config[4]['variable']?>" id="<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[5]['id']?>" id="id-<?php echo $config[5]['id']?>" value="<?php echo $config[5]['id']?>">
	  			<input type="hidden" name="<?php echo $config[5]['variable']?>" id="<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['variable']?>">
	  			<?php echo $config[4]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[4]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[4]['variable']?>" id="value-<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[5]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[5]['variable']?>" id="value-<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[6]['id']?>" id="id-<?php echo $config[6]['id']?>" value="<?php echo $config[6]['id']?>">
	  			<input type="hidden" name="<?php echo $config[6]['variable']?>" id="<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['variable']?>">
	  			<?php echo $config[6]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[6]['variable']?>" id="value-<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[7]['id']?>" id="id-<?php echo $config[7]['id']?>" value="<?php echo $config[7]['id']?>">
	  			<input type="hidden" name="<?php echo $config[7]['variable']?>" id="<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['variable']?>">
	  			<?php echo $config[7]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[7]['variable']?>" id="value-<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[8]['id']?>" id="id-<?php echo $config[8]['id']?>" value="<?php echo $config[8]['id']?>">
	  			<input type="hidden" name="<?php echo $config[8]['variable']?>" id="<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['variable']?>">
	  			<?php echo $config[8]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[8]['variable']?>" id="value-<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[9]['id']?>" id="id-<?php echo $config[9]['id']?>" value="<?php echo $config[9]['id']?>">
	  			<input type="hidden" name="<?php echo $config[9]['variable']?>" id="<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['variable']?>">
	  			<?php echo $config[9]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[9]['variable']?>" id="value-<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[10]['id']?>" id="id-<?php echo $config[10]['id']?>" value="<?php echo $config[10]['id']?>">
	  			<input type="hidden" name="<?php echo $config[10]['variable']?>" id="<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['variable']?>">
	  			<?php echo $config[10]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[10]['variable']?>" id="value-<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[11]['id']?>" id="id-<?php echo $config[11]['id']?>" value="<?php echo $config[11]['id']?>">
	  			<input type="hidden" name="<?php echo $config[11]['variable']?>" id="<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['variable']?>">
	  			<?php echo $config[11]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[11]['variable']?>" id="value-<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[12]['id']?>" id="id-<?php echo $config[12]['id']?>" value="<?php echo $config[12]['id']?>">
	  			<input type="hidden" name="<?php echo $config[12]['variable']?>" id="<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['variable']?>">
	  			<?php echo $config[12]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[12]['variable']?>" id="value-<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[13]['id']?>" id="id-<?php echo $config[13]['id']?>" value="<?php echo $config[13]['id']?>">
	  			<input type="hidden" name="<?php echo $config[13]['variable']?>" id="<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['variable']?>">
	  			<?php echo $config[13]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[13]['variable']?>" id="value-<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['value']?>" disabled>
	  	</div>

	  	<!-- Extend -->
	    <div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[58]['id']?>" id="id-<?php echo $config[58]['id']?>" value="<?php echo $config[58]['id']?>">
	  			<input type="hidden" name="<?php echo $config[58]['variable']?>" id="<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['variable']?>">
	  			<?php echo $config[58]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[58]['variable']?>" id="value-<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[59]['id']?>" id="id-<?php echo $config[59]['id']?>" value="<?php echo $config[59]['id']?>">
	  			<input type="hidden" name="<?php echo $config[59]['variable']?>" id="<?php echo $config[59]['variable']?>" value="<?php echo $config[59]['variable']?>">
	  			<?php echo $config[59]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[59]['variable']?>" id="value-<?php echo $config[59]['variable']?>" value="<?php echo $config[59]['value']?>" disabled>
	  	</div>
	  	<!-- End Extend -->

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[14]['id']?>" id="id-<?php echo $config[14]['id']?>" value="<?php echo $config[14]['id']?>">
	  			<input type="hidden" name="<?php echo $config[14]['variable']?>" id="<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[15]['id']?>" id="id-<?php echo $config[15]['id']?>" value="<?php echo $config[15]['id']?>">
	  			<input type="hidden" name="<?php echo $config[15]['variable']?>" id="<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['variable']?>">
	  			<?php echo $config[14]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[14]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[14]['variable']?>" id="value-<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[15]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[15]['variable']?>" id="value-<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[16]['id']?>" id="id-<?php echo $config[16]['id']?>" value="<?php echo $config[16]['id']?>">
	  			<input type="hidden" name="<?php echo $config[16]['variable']?>" id="<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[17]['id']?>" id="id-<?php echo $config[17]['id']?>" value="<?php echo $config[17]['id']?>">
	  			<input type="hidden" name="<?php echo $config[17]['variable']?>" id="<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['variable']?>">
	  			<?php echo $config[16]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[16]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[16]['variable']?>" id="value-<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[17]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[16]['variable']?>" id="value-<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[18]['id']?>" id="id-<?php echo $config[18]['id']?>" value="<?php echo $config[18]['id']?>">
	  			<input type="hidden" name="<?php echo $config[18]['variable']?>" id="<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[18]['id']?>" id="id-<?php echo $config[19]['id']?>" value="<?php echo $config[19]['id']?>">
	  			<input type="hidden" name="<?php echo $config[19]['variable']?>" id="<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['variable']?>">
	  			<?php echo $config[18]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[18]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[18]['variable']?>" id="value-<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[19]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[19]['variable']?>" id="value-<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[20]['id']?>" id="id-<?php echo $config[20]['id']?>" value="<?php echo $config[20]['id']?>">
	  			<input type="hidden" name="<?php echo $config[20]['variable']?>" id="<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[21]['id']?>" id="id-<?php echo $config[21]['id']?>" value="<?php echo $config[21]['id']?>">
	  			<input type="hidden" name="<?php echo $config[21]['variable']?>" id="<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['variable']?>">
	  			<?php echo $config[20]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[20]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[20]['variable']?>" id="value-<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[21]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[21]['variable']?>" id="value-<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[22]['id']?>" id="id-<?php echo $config[22]['id']?>" value="<?php echo $config[22]['id']?>">
	  			<input type="hidden" name="<?php echo $config[22]['variable']?>" id="<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[23]['id']?>" id="id-<?php echo $config[23]['id']?>" value="<?php echo $config[23]['id']?>">
	  			<input type="hidden" name="<?php echo $config[23]['variable']?>" id="<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['variable']?>">
	  			<?php echo $config[22]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[22]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[22]['variable']?>" id="value-<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[23]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[23]['variable']?>" id="value-<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[24]['id']?>" id="id-<?php echo $config[24]['id']?>" value="<?php echo $config[24]['id']?>">
	  			<input type="hidden" name="<?php echo $config[24]['variable']?>" id="<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[25]['id']?>" id="id-<?php echo $config[25]['id']?>" value="<?php echo $config[25]['id']?>">
	  			<input type="hidden" name="<?php echo $config[25]['variable']?>" id="<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['variable']?>">
	  			<?php echo $config[24]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[24]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[24]['variable']?>" id="value-<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[25]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[25]['variable']?>" id="value-<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[26]['id']?>" id="id-<?php echo $config[26]['id']?>" value="<?php echo $config[26]['id']?>">
	  			<input type="hidden" name="<?php echo $config[26]['variable']?>" id="<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[27]['id']?>" id="id-<?php echo $config[27]['id']?>" value="<?php echo $config[27]['id']?>">
	  			<input type="hidden" name="<?php echo $config[27]['variable']?>" id="<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['variable']?>">
	  			<?php echo $config[26]['desc']?>
	  		</div>
	  		<div class="value-item"><?php echo $config[26]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[26]['variable']?>" id="value-<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[27]['value_desc']?></div>
	  		<input class="input-90" type="number" name="value-<?php echo $config[27]['variable']?>" id="value-<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['value']?>" disabled>
	  	</div>


	  </div>

	  <div id="bypass" class="tab-pane fade">

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[28]['id']?>" id="id-<?php echo $config[28]['id']?>" value="<?php echo $config[28]['id']?>">
	  			<input type="hidden" name="<?php echo $config[28]['variable']?>" id="<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['variable']?>">
	  			<?php echo $config[28]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[28]['variable']?>" id="value-<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[29]['id']?>" id="id-<?php echo $config[29]['id']?>" value="<?php echo $config[29]['id']?>">
	  			<input type="hidden" name="<?php echo $config[29]['variable']?>" id="<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['variable']?>">
	  			<?php echo $config[29]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[29]['variable']?>" id="value-<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[30]['id']?>" id="id-<?php echo $config[30]['id']?>" value="<?php echo $config[30]['id']?>">
	  			<input type="hidden" name="<?php echo $config[30]['variable']?>" id="<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['variable']?>">
	  			<?php echo $config[30]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[30]['variable']?>" id="value-<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[31]['id']?>" id="id-<?php echo $config[31]['id']?>" value="<?php echo $config[31]['id']?>">
	  			<input type="hidden" name="<?php echo $config[31]['variable']?>" id="<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['variable']?>">
	  			<?php echo $config[31]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[31]['variable']?>" id="value-<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[32]['id']?>" id="id-<?php echo $config[32]['id']?>" value="<?php echo $config[32]['id']?>">
	  			<input type="hidden" name="<?php echo $config[32]['variable']?>" id="<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['variable']?>">
	  			<?php echo $config[32]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[32]['variable']?>" id="value-<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[33]['id']?>" id="id-<?php echo $config[33]['id']?>" value="<?php echo $config[33]['id']?>">
	  			<input type="hidden" name="<?php echo $config[33]['variable']?>" id="<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['variable']?>">
	  			<?php echo $config[33]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[33]['variable']?>" id="value-<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[34]['id']?>" id="id-<?php echo $config[34]['id']?>" value="<?php echo $config[34]['id']?>">
	  			<input type="hidden" name="<?php echo $config[34]['variable']?>" id="<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['variable']?>">
	  			<?php echo $config[34]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[34]['variable']?>" id="value-<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[35]['id']?>" id="id-<?php echo $config[35]['id']?>" value="<?php echo $config[35]['id']?>">
	  			<input type="hidden" name="<?php echo $config[35]['variable']?>" id="<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['variable']?>">
	  			<?php echo $config[35]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[35]['variable']?>" id="value-<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[36]['id']?>" id="id-<?php echo $config[36]['id']?>" value="<?php echo $config[36]['id']?>">
	  			<input type="hidden" name="<?php echo $config[36]['variable']?>" id="<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['variable']?>">
	  			<?php echo $config[36]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[36]['variable']?>" id="value-<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[37]['id']?>" id="id-<?php echo $config[37]['id']?>" value="<?php echo $config[37]['id']?>">
	  			<input type="hidden" name="<?php echo $config[37]['variable']?>" id="<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['variable']?>">
	  			<?php echo $config[37]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[37]['variable']?>" id="value-<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['value']?>" disabled>
	  	</div>

	  	<div class="deviden2"></div>
	  </div>

	  <div id="mfs" class="tab-pane fade">
	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[38]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[38]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[38]['variable']?>" id="value-<?php echo $config[38]['variable']?>" value="<?php echo $config[38]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[39]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[39]['variable']?>" id="value-<?php echo $config[39]['variable']?>" value="<?php echo $config[39]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[40]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[40]['variable']?>" id="value-<?php echo $config[40]['variable']?>" value="<?php echo $config[40]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[41]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[41]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[42]['variable']?>" id="value-<?php echo $config[41]['variable']?>" value="<?php echo $config[41]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[42]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[42]['variable']?>" id="value-<?php echo $config[42]['variable']?>" value="<?php echo $config[42]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[43]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[43]['variable']?>" id="value-<?php echo $config[43]['variable']?>" value="<?php echo $config[43]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>


	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[44]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[44]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[44]['variable']?>" id="value-<?php echo $config[44]['variable']?>" value="<?php echo $config[44]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[45]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[45]['variable']?>" id="value-<?php echo $config[45]['variable']?>" value="<?php echo $config[45]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[46]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[46]['variable']?>" id="value-<?php echo $config[46]['variable']?>" value="<?php echo $config[46]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[47]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[47]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[47]['variable']?>" id="value-<?php echo $config[47]['variable']?>" value="<?php echo $config[47]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[48]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[48]['variable']?>" id="value-<?php echo $config[48]['variable']?>" value="<?php echo $config[48]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[49]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[49]['variable']?>" id="value-<?php echo $config[49]['variable']?>" value="<?php echo $config[49]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>

	  	<div class="row-vh">
	  		<div class="c1-h c-odd">
	  			<?php echo $config[50]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[50]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[50]['variable']?>" id="value-<?php echo $config[50]['variable']?>" value="<?php echo $config[50]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[51]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[51]['variable']?>" id="value-<?php echo $config[51]['variable']?>" value="<?php echo $config[51]['value']?>" disabled>
	  			</div>
	  			<div class="rows-3">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[52]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[52]['variable']?>" id="value-<?php echo $config[52]['variable']?>" value="<?php echo $config[52]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>


	  	<div class="row-vh">
	  		<div class="c1-h c-even">
	  			<?php echo $config[53]['desc']?>
	  		</div>	
	  		<div class="c2-h">
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[53]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[53]['variable']?>" id="value-<?php echo $config[53]['variable']?>" value="<?php echo $config[53]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[54]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[54]['variable']?>" id="value-<?php echo $config[54]['variable']?>" value="<?php echo $config[54]['value']?>" disabled>
	  			</div>
	  			<div class="rows-4">
	  				<div class="label-item width250 font-normal">
	  					<?php echo $config[55]['value_desc']?>
	  				</div>
	  				<input class="input-90 width161" type="number" name="value-<?php echo $config[55]['variable']?>" id="value-<?php echo $config[55]['variable']?>" value="<?php echo $config[55]['value']?>" disabled>
	  			</div>
	  		</div>
	  	</div>
	  	<!-- <div class="deviden3"></div> -->

	  </div>

	  <div id="currency" class="tab-pane fade">
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[56]['id']?>" id="id-<?php echo $config[56]['id']?>" value="<?php echo $config[56]['id']?>">
	  			<input type="hidden" name="<?php echo $config[56]['variable']?>" id="<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['variable']?>">
	  			<?php echo $config[56]['desc']?>
	  		</div>
	  		<select class="input-90 width161" name="value-<?php echo $config[56]['variable']?>" id="value-<?php echo $config[56]['variable']?>" disabled>
	  		<?php foreach ($currency->result() as $row) {
	  			if(intval($config[56]['value']) == $row->id) $curr_selected = 'selected'; else $curr_selected='';
	  			echo '<option value="'.$row->id.'" '.$curr_selected.'>'.$row->currencysymbol.'</option>';
	  		}?>
	  		</select>
	  		<!--
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[56]['variable']?>" id="value-<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['value']?>" disabled>
	  		-->
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[57]['id']?>" id="id-<?php echo $config[57]['id']?>" value="<?php echo $config[57]['id']?>">
	  			<input type="hidden" name="<?php echo $config[57]['variable']?>" id="<?php echo $config[57]['variable']?>" value="<?php echo $config[57]['variable']?>">
	  			<?php echo $config[57]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[57]['variable']?>" id="value-<?php echo $config[57]['variable']?>" value="<?php echo $config[57]['value']?>" disabled>
	  	</div>

	  </div>

	  <div id="cc" class="tab-pane fade">
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[60]['id']?>" id="id-<?php echo $config[60]['id']?>" value="<?php echo $config[60]['id']?>">
	  			<input type="hidden" name="<?php echo $config[60]['variable']?>" id="<?php echo $config[60]['variable']?>" value="<?php echo $config[60]['variable']?>">
	  			<?php echo $config[60]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[60]['variable']?>" id="value-<?php echo $config[60]['variable']?>" value="<?php echo $config[60]['value']?>" disabled>
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[61]['id']?>" id="id-<?php echo $config[61]['id']?>" value="<?php echo $config[61]['id']?>">
	  			<input type="hidden" name="<?php echo $config[61]['variable']?>" id="<?php echo $config[61]['variable']?>" value="<?php echo $config[61]['variable']?>">
	  			<?php echo $config[61]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[61]['variable']?>" id="value-<?php echo $config[61]['variable']?>" value="<?php echo $config[61]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[62]['id']?>" id="id-<?php echo $config[62]['id']?>" value="<?php echo $config[62]['id']?>">
	  			<input type="hidden" name="<?php echo $config[62]['variable']?>" id="<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['variable']?>">
	  			<?php echo $config[62]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[62]['variable']?>" id="value-<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['value']?>" disabled>
	  	</div>
	  	
	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[63]['id']?>" id="id-<?php echo $config[63]['id']?>" value="<?php echo $config[63]['id']?>">
	  			<input type="hidden" name="<?php echo $config[63]['variable']?>" id="<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['variable']?>">
	  			<?php echo $config[63]['desc']?>
	  		</div>
	  		<input class="input-90 width161" type="number" name="value-<?php echo $config[63]['variable']?>" id="value-<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['value']?>" disabled>
	  	</div>

	  </div>

	</div>
	<div class="cmd-command">
		<button class="std-btn bkgr-green" id="btn-add-opco" onclick="add_opco_config()"><?php echo $this->lang->line('add')?></button>
		<button class="std-btn bkgr-blue" id="btn-modify-opco" onclick="modify_opco()"><?php echo $this->lang->line('modify')?></button>
		<button class="std-btn bkgr-green" id="btn-save-opco" onclick="save_value()" disabled><?php echo $this->lang->line('save')?></button>
		<button class="std-btn bkgr-red" id="btn-cancel-opco" onclick="pop('disable-background','cancel-process')" disabled><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div class="add-access-module" id="status-config">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('OpCo no configuration, configuration values set to default')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','status-config')"><?php echo $this->lang->line('ok')?></button>
	</div>

</div>

<div class="add-modify-ui-detail" id="add-opco-config">
	<div class="modify-title">
		<?php echo $this->lang->line('Add OpCo Configuration')?>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('OpCo')?></div>
		<div class="label-item"><?php echo $select_opco2 ?></div>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('Period')?></div>
		<div class="label-item"><?php echo $select_month2 ?></div>
		<div class="label-item"><?php echo $select_year2 ?></div>
	</div>
	<div class="rows center10">
		<button class="std-btn bkgr-green" onclick="new_open()"><?php echo $this->lang->line('add')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','add-opco-config')"><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="add-access-module" id="config-found">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration in selected already exists, the process is canceled')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background2','config-found')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="save-success">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration has been saved')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','save-success')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="result-group-error">
	<div class="modify-title">
		<?php echo $this->lang->line('Error Message'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','result-group-error');pop('disable-background','edit-group');"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div class="del-access-module" id="cancel-process">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('Are you sure to cancel the operation?'); ?>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-greed" onClick="location.reload()"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-red" onClick="hide('disable-background','cancel-process')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div id="testout"></div>
<script type="text/javascript">
	var mode = 'view';
	var new_value = [];
	
	$(document).ready(function(){
		
		$("#opco-list").collapse("show");
		data_config = <?php echo $message?>;
		if(data_config==1){
			document.getElementById('btn-modify-opco').disabled=true;
			//pop('disable-background','status-config');


		}else{
			document.getElementById('btn-modify-opco').disabled=false;
		}
	});

	function reload_page(){
		window.location.href = '<?php echo base_url()?>index.php/admin/opco_config/'+$("#id-opco").val()+'/'+$("#month").val()+'/'+$("#year").val();
	}

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}



	function add_opco_config(){
		$("#new-id-opco").val($("#id-opco").val());
		$("#new-month").val($("#month").val());
		$("#new-year").val($("#year").val());
		pop('disable-background','add-opco-config');
	}

	function new_open(){
		
		// Check existing data
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/admin/check_opco_config",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	opcoid: $("#new-id-opco").val(),
            	month: $("#new-month").val(),
            	year: $("#new-year").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	switch(data){
            		case '0':
            			// disable select opco,mounth,year
            			hide('disable-background','add-opco-config');
            			document.getElementById('btn-get-config').disabled=true;
            			document.getElementById('id-opco').disabled=true
            			document.getElementById('month').disabled=true
            			document.getElementById('year').disabled=true;

            			document.getElementById('btn-add-opco').disabled=true;
            			document.getElementById('btn-modify-opco').disabled=true;
            			document.getElementById('btn-save-opco').disabled=false;
            			document.getElementById('btn-cancel-opco').disabled=false;

            			<?php 
            				foreach ($config as $new_config) {
            			?>
            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;
            			$("#value-<?php echo $new_config['variable']?>").val("");
            			<?php
            				}
            			?>
            			mode = 'add';
            			break;
            		case '1':
            			hide('disable-background','add-opco-config');
            			pop('disable-background2','config-found');
            			break;
            		case '2':
            			
            			break;
            	}
            }
		});
	}


	function save_value(){
		//alert(mode);
		if(mode=='add'){
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			item = { 
				opcoid:$("#new-id-opco").val(),
				config_date: $("#new-year").val()+'-'+$("#new-month").val()+"-01 00:00:00",
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);

			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/add_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
	            	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";
	            },
	            success: function(data){
	            	
	            	switch(data){
	            		case '0':
		            		document.getElementById('btn-get-config').disabled=false;
	            			document.getElementById('id-opco').disabled=false
	            			document.getElementById('month').disabled=false
	            			document.getElementById('year').disabled=false;

	            			document.getElementById('btn-add-opco').disabled=false;
	            			document.getElementById('btn-modify-opco').disabled=false;
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');
	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

			
		}else{
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			item = { 
				opcoid:$("#id-opco").val(),
				config_date: $("#year").val()+'-'+$("#month").val()+"-01 00:00:00",
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);
			//document.getElementById("testout").innerHTML=json_newdata;
			//alert(json_newdata);
			
			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/admin/update_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
			    	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";       	
	            },
	            success: function(data){
	            	//alert(data);
	            	switch(data){
	            		case '0':

		            		document.getElementById('btn-get-config').disabled=false;
	            			document.getElementById('id-opco').disabled=false
	            			document.getElementById('month').disabled=false
	            			document.getElementById('year').disabled=false;

	            			document.getElementById('btn-add-opco').disabled=false;
	            			document.getElementById('btn-modify-opco').disabled=false;
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');

	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

		
		}
	}

	function modify_opco(){
		document.getElementById('btn-get-config').disabled=true;
		document.getElementById('id-opco').disabled=true
		document.getElementById('month').disabled=true
		document.getElementById('year').disabled=true;

		document.getElementById('btn-add-opco').disabled=true;
		document.getElementById('btn-modify-opco').disabled=true;
		document.getElementById('btn-save-opco').disabled=false;
		document.getElementById('btn-cancel-opco').disabled=false;

		<?php 
			foreach ($config as $new_config) {
		?>
		document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;

		<?php
			}
		?>
		mode = 'edit';		
	}



</script>