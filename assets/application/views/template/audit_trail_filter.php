<div class="audit_trail_filter">
	<div class="rows">
		<div class= "filter_label">
			<?php echo $this->lang->line('user')?>
		</div>
		<?php echo $user ?>
	</div>
	<div class="rows">
		<div class= "filter_label">
			<?php echo $this->lang->line('module')?>
		</div>
		<?php echo $module ?>
	</div>
	<div class="rows">
		<div class= "filter_label">
			<?php echo $this->lang->line('start_date')?>
		</div>

		<input type="text" id="startdate" class="filter_item" value="<?php echo $start?>">
	</div>
	<div class="rows">
		<div class= "filter_label">
			<?php echo $this->lang->line('end_date')?>
		</div>
		<input type="text" id="enddate" class="filter_item" value="<?php echo $end?>">
		<button class="btn_filter" id="btn-filter"><?php echo $this->lang->line('filter')?></button>
	</div>
</div>

<script>
  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd',showOn: "button",buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
  });
  
  $(function() {
    $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd',showOn: "button",buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
  });

  $("#btn-filter").click(function(){
  	var url = "<?php echo base_url()?>index.php/admin/audit_trail?user="+$("#username").val()+"&module="+$("#module").val()+"&start="+$("#startdate").val()+"&end="+$("#enddate").val();
  	//alert(url);
  	window.location.href=url;

  });

  function pokus(){
  	$( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  }

</script>