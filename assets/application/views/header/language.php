<?php

?>

<select onchange="select_language()" name="select_lang" id="select_lang">
    <option value="english" <?php if($language == 'english') echo 'selected="selected"'; ?>>English</option>
    <option value="french" <?php if($language == 'french') echo 'selected="selected"'; ?>>French</option>
    <!-- <option value="german" <?php if($language == 'german') echo 'selected="selected"'; ?>>German</option> -->
    <option value="russian" <?php if($language == 'russian') echo 'selected="selected"'; ?>>Russian</option>
</select>
<script type="text/javascript">
	function select_language(){
		var language = document.getElementById("select_lang"); 
		//alert('<?php echo base_url()?>');
		this.document.location.href = "<?php echo base_url()?>index.php/LanguageSwitcher/index/"+language.value;
	}
</script>
