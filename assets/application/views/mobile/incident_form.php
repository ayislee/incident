<div class="incident-content">
    <ul class="nav nav-tabs">
    	  <li class="active"><a data-toggle="tab" href="#general" class="width161 center21"><?php echo $this->lang->line('General')?></a></li>
    	  <li><a data-toggle="tab" href="#risk" class="width161 center21"><?php echo $this->lang->line('risk_tab')?></a></li>
    	  <li><a data-toggle="tab" href="#attachment" class="width161 center21"><?php echo $this->lang->line('Attachment')?></a></li>
    	  <li><a data-toggle="tab" href="#history" class="width161 center21"><?php echo $this->lang->line('History')?></a></li>
	  </ul>

  	<div class="tab-content risk-setting">
  	  	<div id="general" class="tab-pane risk-pane fade in active">
    	  		<div class="column100">
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('Incident ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="incident_num" name="incident_num" value="<?php echo $detail['incident_num']?>" class="std-input  dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('External ID')?></div>
        	  				<div class="rc2">
        	  					  <input id="external_id" name="external_id" value="<?php echo $detail['external_id']?>" class="std-input  dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1"><?php echo $this->lang->line('OpCo')?></div>
        	  				<div class="rc2">
                        <input type="hidden" id="opcoid" name="opcoid" value="<?php echo $detail['opcoid']?>">
        	  					  <input id="opconame" name="opconame" value="<?php echo $detail['opconame']?>" class="std-input  dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
      	  				  <div class="rc1"><?php echo $this->lang->line('date_created')?></div>
        	  				<div class="rc2">
        	  					  <input id="create_date" name="create_date" value="<?php echo $detail['create_date']?>" class="std-input  dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
      	  			    <div class="rc1"><?php echo $this->lang->line('created_by')?></div>
      	  				  <div class="rc2">
      	  					    <input id="create_by" name="create_by" value="<?php echo $detail['create_by']?>" class="std-input width250" type="hidden">
      	  					    <input id="user_create" name="user_create" value="<?php echo $detail['user_create']?>" class="std-input  dis width250 padding5" disabled>
      	  				  </div>
      	  			</div>
        	  		
      	  		
        	  		
      	  			<div class="rows">
        	  				<div class="rc1">
        	  					  <?php echo $this->lang->line('date_update')?>
        	  				</div>
        	  				<div class="rc2">
      	  					    <input id="update_date" name="update_date" value="<?php echo $detail['update_date']?>" class="std-input dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1">
        	  					  <?php echo $this->lang->line('update_by')?>
        	  				</div>
        	  				<div class="rc2">
          	  					<input id="updated_by" name="updated_by" value="<?php echo $detail['updated_by']?>" class="std-input width250" type="hidden">
          	  					<input id="user_update" name="user_update" value="<?php echo $detail['user_update']?>" class="std-input  dis width250 padding5" disabled>
        	  				</div>
      	  			</div>
      	  			<div class="rows">
        	  				<div class="rc1">
        	  					  <?php echo $this->lang->line('status')?>
        	  				</div>
        	  				<div class="rc2">
          	  					<select class="std-select  dis width250" id="incident_status" name="incident_status" onchange="status_onchange()">
            	  						<?php switch ($detail['incident_status']) {
            	  							case '0':
            	  								$select_status = '';
            	  								$select_status .= '<option value="0" selected>'.$this->lang->line('Open').'</option>';
            	  								$select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
            	  								break;
            	  							
            	  							case '1':
            	  								$select_status = '';
            	  								$select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
            	  								$select_status .= '<option value="1" selected>'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
            	  								break;
                              case '2':
                                $select_status = '';
                                $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2" selected>'.$this->lang->line('Under Investigation').'</option>';
                                break;
            	  						}
            	  						echo $select_status?>
          	  					</select>
        	  				</div>
      	  			</div>

                <script type="text/javascript">
                  function status_onchange(){
                      $("#incident_status_detail").val($("#incident_status").val());
                  }
                </script>
            </div>
    	  		

    	  		<div class="column100">
    	  			  <?php echo $this->lang->line('Description')?>
    	  		</div>

    	  		<div class="column100">

    	  			  <textarea id="description" name="description" class="std-textarea" rows="4"><?php echo $detail['description']?></textarea>
    	  		</div>
    	  		<div class="column100">
    	  			  <?php echo $this->lang->line('Note')?>
    	  		</div>
    	  		<div class="column100" id="note-area">
    	  			  <textarea id="note" name="note" class="std-textarea" rows="4"><?php echo $detail['note']?></textarea>
    	  		</div>

  	  	</div>

  	<div id="risk" class="tab-pane fade risk-pane">
    		<div class="column100">
      			<div class="rows  padding-bottom5">
        				<div class="rc1">
        					<?php echo $this->lang->line('Detection Tool')?>
        				</div>
        				<div class="rc2">
        					<select id="detection_tool" name="detection_tool" class="std-select  dis width177">
        					<?php 
        					$option_dt = '';
        					foreach ($select_detection_tool->result() as $row) {
        						# code...
        						if($detail['detection_tool']==$row->dtid) $dt_selected = 'selected'; else $dt_selected ='';
        						$option_dt .= '<option value="'.$row->dtid.'" '.$dt_selected.'>'.$row->dtname.'</option>';
        					}
        					echo $option_dt;
        					?>
        					</select>
        					
        				</div>
        		</div>
      			<div class="rows padding-bottom5">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Risk Universe Type')?>
        				</div>
        				<div class="rc2">
          					<select id="rutid" name="rutid" class="std-select  dis width177" onchange="onchange_rut()">
          					<?php 
          					$option_rut = '';
          					foreach ($select_rut->result() as $row) {
          						# code...
          						if($detail['rutid']==$row->rutid) $rut_selected = 'selected'; else $rut_selected ='';
          						$option_rut .= '<option value="'.$row->rutid.'" '.$rut_selected.'>'.$row->rutname.'</option>';
          					}
          					echo $option_rut;
          					?>
          					</select>
        				</div>
      			</div>
      			<script type="text/javascript">
              function onchange_rut(){
                  if(list_rc.length>0){
                      pop('disable-background','rut-confirm');
                  }else{
                    change_rut();
                  }
              }
      				function change_rut(){
                //alert(JSON.stringify(list_rc));
                //alert(list_rc);
                list_rc = [];
                document.getElementById("rc-data").innerHTML = '';
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_st",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                rutid: $("#rutid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                //alert(data);
    			                document.getElementById('stid').innerHTML = data;
    			                change_st();
    			                change_oc();
    			            }
    			        });
      				}
      			</script>

      			<div class="rows  padding-bottom5">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Section Title')?>
        				</div>
        				<div class="rc2">
        				    <select id="stid" name="stid" class="std-select  dis width177" onchange="change_st()">
          					<?php  
          					$option_st = '';
          					foreach ($select_st->result() as $row) {
          						# code...
          						if($detail['stid']==$row->stid) $st_selected = 'selected'; else $st_selected ='';
          						$option_st .= '<option value="'.$row->stid.'" '.$st_selected.'>'.$row->stname.'</option>';
          					}
          					echo $option_st;
          					?>
        					   </select>
        				</div>
      			</div>
      			<script type="text/javascript">
      				function change_st(){
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_oc",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                stid: $("#stid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                //alert(data);
    			                document.getElementById('ocid').innerHTML = data;
    			                change_oc();
    			            }
    			        });
      				}

      			</script>

      			<div class="rows padding-bottom5">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Operator Category')?>
        				</div>
        				<div class="rc2">
          					<select id="ocid" name="ocid" class="std-select  dis width177" onchange="change_oc()">
              					<?php 
              					$option_oc = '';
              					foreach ($select_oc->result() as $row) {
              						# code...
              						if($detail['ocid']==$row->ocid) $oc_selected = 'selected'; else $oc_selected ='';
              						$option_oc .= '<option value="'.$row->ocid.'" '.$oc_selected.'>'.$row->ocname.'</option>';
              					}
              					echo $option_oc;
              					?>
        					 </select>
        				</div>
      			</div>

      			<script type="text/javascript">
      				function change_oc(){
      					$.ajax({
    			            type: "POST",  
    			            url: "<?php echo base_url()?>index.php/incident/get_rc",  
    			            contentType: 'application/x-www-form-urlencoded',
    			            data: { 
    			                ocid: $("#ocid").val(),
    			                sess: "<?php echo session_id()?>"
    			            },
    			            dataType: "text",
    			            beforeSend: function(){

    			            },
    			            complete: function(){
    			                
    			            },
    			            success: function(data){
    			                
    			                document.getElementById('rcid').innerHTML = data;
    			            }
    			        });
      				}
      			</script>

      			<div class="rows padding-bottom5">
        				<div class="rc1">
        				    <?php echo $this->lang->line('Risk')?>
        				</div>
        				<div class="rc2">
          					<select id="rcid" name="rcid" class="std-select  dis width177">
              					<?php 
              					$option_rc = '';
              					foreach ($select_rc->result() as $row) {
              						# code...
              						$option_rc .= '<option value="'.$row->rcid.'" >'.$row->rcname.'</option>';
              					}
              					echo $option_rc;
              					?>
          					</select>
          					<button id="btn-add-rc" onclick="add_rc()">+</button>
        				</div>
      			</div>

      			<div class="rows">
    	  			  <div class="rc-data-area" id="rc-data"></div>
      			</div>


  			<div class="rows padding-bottom5">
    				<div class="rc1">
    					<?php echo $this->lang->line('Impact Severity')?>
    				</div>
    				<div class="rc2">
    					<select id="impact_severity" name="impact_severity" value="<?php echo $detail['impact_severity']?>" class="std-select width177 dis ">
    						<?php for ($i=1; $i<= 10 ; $i++) {
    							if($i== $detail['impact_severity']) $is = 'selected'; else $is = '';?>
    						<option value="<?php echo $i?>" <?php echo $is?>><?php echo $i?></option>
    						<?php }?>

    					</select>
    				</div>
    			</div>
    			<div class="rows">
    				<div class="rc1"><?php echo $this->lang->line('Detection Date')?></div>
    				<div class="rc2">
    					<input type="text" id="detection_date" name="detection_date" class="std-input  dis width177 padding5" value="<?php echo $detail['detection_date']?>" >
    				</div>
    			</div>
    			<div class="rows">
    				<div class="rc1"><?php echo $this->lang->line('Case Start Date')?></div>
    				<div class="rc2">
    					<input type="text" id="case_start_date" name="case_start_date" class="std-input  dis width177 padding5" value="<?php echo $detail['case_start_date']?>">
    				</div>
    			</div>
    			<div class="rows">
    				<div class="rc1"><?php echo $this->lang->line('Case End Date')?></div>
    				<div class="rc2">
    					<input type="text" id="case_end_date" name="case_end_date" class="std-input  dis width177  padding5" value="<?php echo $detail['case_end_date']?>">
    				</div>
    			</div>
    			<div class="rows">
    				<div class="rc1"><?php echo $this->lang->line('Leakage One Day')?></div>
    				<div class="rc2">

    					<input type="number" id="leakage_one_day" name="leakage_one_day" class="std-input  dis width177 padding5" value="<?php echo $detail['leakage_one_day']?>">
    				</div>
    			</div>
    			<div class="rows">
    				<div class="rc1"><?php echo $this->lang->line('Recovery')?></div>
    				<div class="rc2">
    					<input type="number" id="recovery" name="recovery" class="std-input  dis width177 padding5" value="<?php echo $detail['recovery']?>">
    				</div>
    			</div>
    			<div class="rows padding-bottom5">
    				<div class="rc1">
    					<?php echo $this->lang->line('Leakage Frequency')?>
    				</div>
    				<div class="rc2">
    					<select id="leakage_freq" name="leakage_freq" value="<?php echo $detail['leakage_freq']?>" class="std-select dis width177">
                <?php foreach ($leakage_freq->result() as $row) {
                  if(intval($detail['leakage_freq'])==$row->id) $is = 'selected'; else $is = '';
                  echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                }?>
    					</select>
    				</div>
    			</div>


    			<div class="rows padding-bottom5">
    				<div class="rc1">
    					<?php echo $this->lang->line('Leakage Timing')?>
    				</div>
    				<div class="rc2">
    					<select id="leakage_timing" name="leakage_timing" value="<?php echo $detail['leakage_timing']?>" class="std-select  dis width177">
    						<?php foreach ($leakage_timing->result() as $row) {
                  if(intval($detail['leakage_timing'])==$row->id) $is = 'selected'; else $is = '';
                  echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                }?>
    					</select>
    				</div>
    			</div>

            <div class="rows padding-bottom5">
                <div class="rc1">
                    <?php echo $this->lang->line('Leakage ID Type')?>
                </div>
                <div class="rc2">
                  	<select id="leakage_id_type" name="leakage_id_type" value="<?php echo $detail['leakage_id_type']?>" class="std-select dis  width177">
                    		<?php foreach ($leakage_id_type->result() as $row) {
                          if(intval($detail['leakage_id_type'])==$row->id) $is = 'selected'; else $is = '';
                          echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                        }?>
                  	</select>
                </div>
            </div>

            <div class="rows padding-bottom5">
            		<div class="rc1">
            		    <?php echo $this->lang->line('Recovery Type')?>
            		</div>
            		<div class="rc2">
            		    <select id="recovery_type" name="recovery_type" value="<?php echo $detail['recovery_type']?>" class="std-select width177 dis ">
                				<?php foreach ($recovery_type->result() as $row) {
                          if(intval($detail['recovery_type'])==$row->id) $is = 'selected'; else $is = '';
                          echo '<option value ="'.$row->id.'" '.$is.'>'.$row->name.'</option>';
                        }?>
            		    </select>
            		</div>
            </div>
            
            <div class="rows padding-bottom5">
            		<div class="rc1">
            			<?php echo $this->lang->line('Incident Status')?>
            		</div>
                <div class="rc2">
                    <select id="incident_status_detail" name="incident_status_detail" value="<?php echo $detail['incident_status_detail']?>" class="std-select width177 dis " onchange="status_detail_onchange()">
                      	<?php switch ($detail['incident_status_detail']) {
                              case '0':
                                $select_status = '';
                                $select_status .= '<option value="0" selected>'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                break;
                              
                              case '1':
                                $select_status = '';
                                $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1" selected>'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2">'.$this->lang->line('Under Investigation').'</option>';
                                break;
                              case '2':
                                $select_status = '';
                                $select_status .= '<option value="0">'.$this->lang->line('Open').'</option>';
                                $select_status .= '<option value="1">'.$this->lang->line('Close').'</option>';
                                $select_status .= '<option value="2" selected>'.$this->lang->line('Under Investigation').'</option>';
                                break;
                            }
                            echo $select_status?>
                    </select>
                </div>
            </div>

            <script type="text/javascript">
                function status_detail_onchange(){
                    $("#incident_status").val($("#incident_status_detail").val());
                }
            </script>
            <?php
                if($detail['c_final_leakage']==null) $c_final_leakage = ''; else $c_final_leakage = '$'.number_format($detail['c_final_leakage'],2);
                if($detail['c_recovered']==null) $c_recovered = ''; else $c_recovered = '$'.number_format($detail['c_recovered'],2);
                if($detail['c_prevented']==null) $c_prevented = ''; else $c_prevented = '$'.number_format($detail['c_prevented'],2);
                if($detail['c_potential']==null) $c_potential = ''; else $c_potential = '$'.number_format($detail['c_potential'],2);
            ?>
    			  <div class="row padding-bottom5">
        				<div class="after-close">
          					<div class="rows2">
            	  					<div class="fc-1"><?php echo $this->lang->line('Final Leakage')?></div>
            	  					<div class="fc-2"><?php echo $c_final_leakage?></div>
          					</div>
          					<div class="rows2">
            	  					<div class="fc-1"><?php echo $this->lang->line('Recovered Value')?></div>
            	  					<div class="fc-2"><?php echo $c_recovered?></div>
          					</div>
          					<div class="rows2">
            	  					<div class="fc-1"><?php echo $this->lang->line('Prevented Value')?></div>
            	  					<div class="fc-2"><?php echo $c_prevented?></div>
          					</div>
          					<div class="rows2">
            	  					<div class="fc-1"><?php echo $this->lang->line('Potential Leakage')?></div>
            	  					<div class="fc-2"><?php echo $c_potential?></div>
          					</div>
        				</div>
    			  </div>


  		</div>
  	</div>



            <div id="attachment" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <div id="fa">
                    <?php
                    $no = 1;
                    if(sizeof($attachment)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Files').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($attachment as $row) { ?>
                        <div class="table-body">
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('No')?></div>
                                <div class="table-body-item c2"><?php echo $no ?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('Date Time')?></div>
                                <div class="table-body-item c2"><?php echo $row['correct_date']?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('User')?></div>
                                <div class="table-body-item c2"><a href="#" onclick="show_user('<?php echo $row['corerct_by']?>','<?php echo $row['first_name']?>','<?php echo $row['last_name']?>','<?php echo $row['email']?>')"><?php echo $row['user_name']?></a></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('Filename')?></div>
                                <div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/download/<?php echo $row['file_id']?>/<?php echo session_id()?>" target="_blank"><?php echo $row['name']?></a></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('File Description')?></div>
                                <div class="table-body-item c2"><?php echo $row['file_description']?></div>
                            </div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    </div>
                    <script type="text/javascript">
                        function show_user(id,first_name,last_name,email){
                            document.getElementById('contact-full-name').innerHTML='Full name: '+first_name+' '+last_name;
                            document.getElementById('contact-email').innerHTML='Email: '+email;
                            pop('disable-background','show-contact');
                        }
                    </script> 
                    <div class="navigator-2">
                        <div class="command">
                            <button class="std-btn bkgr-blue" onclick="pop('disable-background','upload-form')" id="btn-upload"><?php echo $this->lang->line('UPLOAD')?></button>
                        </div>
                    </div>
                </div>
                
            </div>

          	<div id="history" class="tab-pane fade risk-pane">
                <div class="attactment-content">
                    <?php
                    $no = 1;
                    if(sizeof($history)==0){
                        echo '<div class="table-body center21">';
                        echo '<div class="table-body-item">'.$this->lang->line('No Histories').'</div>';
                        echo '</div>';
                    }else{
                        foreach ($history as $row) { ?>
                        <div class="table-body bkgr-yellow-thin">
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('No')?></div>
                                <div class="table-body-item c2"><?php echo $no?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('Date Time')?></div>
                                <div class="table-body-item c2"><?php echo $row['correct_date']?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('Action')?></div>
                                <div class="table-body-item c2"><?php echo $row['action']?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('User')?></div>
                                <div class="table-body-item c2"><?php echo $row['user_name']?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><?php echo $this->lang->line('Comments')?></div>
                                <div class="table-body-item c2"><?php echo $row['note']?></div>
                            </div>
                            <div class="rows">
                                <div class="table-body-item c1"><a class= "btn btn-info std-btn alink" role="button" href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row['incident_id']?>/<?php echo $row['id']?>"><?php echo $this->lang->line('view')?></a></div>
                            </div>
                        </div>
                        
                    <?php $no++; }
                    }?>
                    
                </div>
                <div class="navigator-2">
                    
                </div>
          	</div>
        </div>	
        <div class="footer">
            <div class="command">

                <button class="std-btn bkgr-green" onclick="btn_save_click()" id="btn-save"><?php echo $this->lang->line('save')?></button>
                
                <button class="std-btn bkgr-blue" onclick="btn_update_click()" id="btn-modify"><?php echo $this->lang->line('Update')?></button>
                
                <button class="std-btn bkgr-red" onclick="btn_cancel_click()" id="btn-cancel"><?php echo $this->lang->line('cancel')?></button>
            
            </div>
        </div>
</div>


<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div id="confirm-save" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('Confirm')?></div>
        <div class="confirm-message"><?php echo $this->lang->line('Are you sure to update this incident ?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-green" onclick="save_incident()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-green" onclick="hide('disable-background','confirm-save')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>

<div id="error-message" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('Error Message')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('Please fill in all fields')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','error-message')"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="cancel-message" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('Are you sure to cancel the operation?')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="prev_page()"><?php echo $this->lang->line('ok')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','cancel-message')"><?php echo $this->lang->line('cancel')?></button>
    </div>
</div>


<div id="upload-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload attacment')?>
    </div>
    <form name="form-upload" id="form-upload" >
        <div class="content-message">
            <input type="file" name="file-attachment" id="file-attachment" class="margin-left30" />
            <div class="label-text"><?php echo $this->lang->line('File Description')?></div>
            <textarea id="file-description" name="file-description"  class="std-textarea" rows="4"></textarea>
        </div>
        <div class="confirm-btn">
            <button type="submit" class="std-btn bkgr-green"><?php echo $this->lang->line('Upload')?></button>
            <button type="button" class="std-btn bkgr-red" onclick="cancel_upload()"><?php echo $this->lang->line('cancel')?></button>
        </div>
    </form>

</div>

<div id="success-message" class="add-access-module">
    <div class="modify-title" id="title-msg"><?php echo $this->lang->line('Create Incident Succes')?></div>
    <div class="confirm-message" id="content-msg"><?php echo $this->lang->line('Incident already saved')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" id="btn-success-message" onclick="success_save()"><?php echo $this->lang->line('ok')?></button>

    </div>
</div>


<div id="rut-confirm" class="add-access-module">
    <div class="modify-title"><?php echo $this->lang->line('confirm')?></div>
    <div class="confirm-message"><?php echo $this->lang->line('rut reset')?></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="change_rut();hide('disable-background','rut-confirm');"><?php echo $this->lang->line('yes')?></button>
        <button class="std-btn bkgr-red" onclick="hide('disable-background','rut-confirm')"><?php echo $this->lang->line('No')?></button>
    </div>
</div>

<div id="show-contact" class="show-contact">
    <div class="modify-title"><?php echo $this->lang->line('Contact')?></div>
    <div class="contact-message" id="contact-full-name"></div>
    <div class="contact-message" id="contact-email"></div>
    <div class="confirm-btn">
        <button class="std-btn bkgr-blue" onclick="hide('disable-background','show-contact');"><?php echo $this->lang->line('ok')?></button>
        
    </div>
</div>


<div id="json"></div>

<script type="text/javascript">
	var list_rc = [];
	var obj_rc;
  var mode = "<?php echo $mode?>";
  var attachment = [];
  var incident_status = <?php echo $detail['incident_status']?>;
  var group = <?php echo $this->session->userdata('group_id')?>;



	$(document).ready(function(){
  		<?php foreach ($categories as $cats) {
  			# code...
  			$jvs = 'obj_rc = { incident_detail_id: "'.$cats['incident_detail_id'].'", rcid: "'.$cats['rcid'].'",rcname: "'.$cats['rcname'].'"};';
  			$jvs .= 'list_rc.push(obj_rc);';
  			echo $jvs;
  		}?>
  		//json_rc = JSON.stringify(list_rc);
  		//alert(list_rc[1].incident_detail_id);
  		
  		for(i=0;i<list_rc.length;i++){
    			//document.getElementById("rc-data").appendChild("haha");
    			insert_div = '<div class="rc-items">';
    			insert_div += list_rc[i].rcname;
    			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
    			insert_div += '</div>';
    			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
  		}

      switch(mode){
          case "NEW":
              document.getElementById('external_id').disabled=false;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              document.getElementById('btn-save').style.display="inline-block";              
              document.getElementById('btn-modify').style.display="none";
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";


              break;
          case "UPDATE":
              document.getElementById('external_id').disabled=true;
              document.getElementById('incident_status').disabled=false;
              document.getElementById('incident_status_detail').disabled=false;              
              document.getElementById('description').disabled=false;
              document.getElementById('note-area').style.display="block";

              document.getElementById('btn-save').style.display="inline-block";              
              document.getElementById('btn-modify').style.display="none";
              document.getElementById('btn-cancel').style.display="inline-block";
              document.getElementById('btn-upload').style.display="inline-block";          
              break;
          case "VIEW":

              document.getElementById('external_id').disabled=true;
              
              document.getElementsByName("incident_status").disabled=true;
              document.getElementById('incident_status_detail').disabled=true;
              $('[name=incident_status]').attr('disabled','disabled');
              document.getElementById('description').disabled=true;
              document.getElementById('note-area').style.display="none";

              document.getElementById('btn-save').style.display="none";
              <?php if($this->session->userdata('get_privilege')[6]['map_group']) { ?>
              if(incident_status==0 || incident_status==2 || group==<?php echo ADMIN_GROUP_ID?>){              
                  document.getElementById('btn-modify').style.display="inline-block";
              }else{
                  document.getElementById('btn-modify').style.display="none";
              }
              <?php } else {?>
              document.getElementById('btn-modify').style.display="none";
              <?php } ?>   
              document.getElementById('btn-cancel').style.display="none";
              document.getElementById('btn-upload').style.display="none";

              document.getElementById('detection_tool').disabled=true;
              document.getElementById('rutid').disabled=true;
              document.getElementById('stid').disabled=true;
              document.getElementById('ocid').disabled=true;
              document.getElementById('rcid').disabled=true;
              document.getElementById('btn-add-rc').disabled=true;

              document.getElementsByClassName("btn-rc").disabled=true;
              $(".btn-rc").hide().attr('disabled','disabled');
              document.getElementById('btn-add-rc').style.display="none";
              

              document.getElementById('impact_severity').disabled=true;
              document.getElementById('detection_date').disabled=true;
              document.getElementById('case_start_date').disabled=true;
              document.getElementById('case_end_date').disabled=true;
              document.getElementById('leakage_one_day').disabled=true;
              document.getElementById('recovery').disabled=true;
              document.getElementById('leakage_freq').disabled=true;
              document.getElementById('leakage_timing').disabled=true;
              document.getElementById('leakage_id_type').disabled=true;
              document.getElementById('recovery_type').disabled=true;


             



              break;
      }
		
	});


  function pop(div,div2) {
    document.getElementById(div).style.display = 'block';
    document.getElementById(div2).style.display = 'block';

  }
  function hide(div,div2) {
    document.getElementById(div).style.display = 'none';
    document.getElementById(div2).style.display = 'none';
  }
  
  function search(nameKey, myArray){
      found = false;
      for (var i=0; i < myArray.length; i++) {
          if (myArray[i].rcid == nameKey) {
              found = true;
          }
      }
      return found;
  }

	function add_rc(){
		// find same rcid
        a = search($("#rcid").val(),list_rc);

        if(!a){
            insert_rc = { incident_detail_id: "<?php $id?>",rcid: $("#rcid").val(), rcname: document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text};
            list_rc.push(insert_rc);
            last_num = list_rc.length -1 ;
            insert_div = '<div class="rc-items">';
            insert_div += document.getElementById('rcid').options[document.getElementById('rcid').selectedIndex].text;
            insert_div += '<button class="btn-rc" onclick="del_rc('+last_num+')">x</button>';
            insert_div += '</div>';
            document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;

        }else{
          alert("<?php echo $this->lang->line('Risk Category already selected')?>");
        }


	}

	function del_rc(num){
		list_rc.splice(num,1);
		// redraw;
		document.getElementById("rc-data").innerHTML = '';
		for(i=0;i<list_rc.length;i++){
			//document.getElementById("rc-data").appendChild("haha");
			insert_div = '<div class="rc-items">';
			insert_div += list_rc[i].rcname;
			insert_div += '<button class="btn-rc" onclick="del_rc('+i+')">x</button>';
			insert_div += '</div>';
			document.getElementById("rc-data").innerHTML = document.getElementById("rc-data").innerHTML + insert_div;
		}
	}

<?php if($mode != 'VIEW') {?>
	$(function() {
	    $( "#detection_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_start_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

	$(function() {
	    $( "#case_end_date" ).datepicker({ 
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          maxDate: "+0m +0w",
          buttonText: "<i class='fa fa-calendar' aria-hidden='true'></i>" });
	  });

<?php } ?>
  function btn_save_click(){
      var detection_date = $("#detection_date").val();
      var case_start_date = $("#case_start_date").val();
      //alert(detection_date);
      //alert(case_start_date);
      if(detection_date > case_start_date){
        //alert('lebih besar');
      }else{
        //alert('lebih kecil');
      }

      if($("#external_id").val().trim()=='' || 
         $("#description").val().trim()=='' ||
         $("#note").val().trim()=='' ||
         $("#leakage_one_day").val().trim()=='' ||
         $("#recovery").val().trim()=='' ) {
          pop('disable-background','error-message');
      }else{
        // code here 

        //var car = {type:"Fiat", model:"500", color:"white"};        

        <?php if($mode=='NEW'){?>
        var incident = { 
          incident_id: "", 
          external_id: $("#external_id").val(),
          opcoid: $("#opcoid").val(),
          opconame: $('#opconame').val(),
          date_created: $("#date_created").val(),
          create_by: $("create_by").val(),
          date_update: $("#date_update").val(),
          updated_by: $("#updated_by").val(),
          incident_status: $("#incident_status").val(),
          incident_status_detail: $("#incident_status_detail").val(),

          incident_detail_id: "",
          description: $("#description").val(),
          note: $("#note").val(),
          detection_tool : $("#detection_tool").val(),
          rutid: $("#rutid").val(),
          stid: $("#stid").val(),
          ocid: $("#ocid").val(),
          impact_severity: $("#impact_severity").val(),
          detection_date: $("#detection_date").val(),
          case_start_date: $("#case_start_date").val(),
          case_end_date: $("#case_end_date").val(),
          leakage_one_day: $("#leakage_one_day").val(),
          recovery: $("#recovery").val(),
          leakage_freq: $("#leakage_freq").val(),
          leakage_timing: $("#leakage_timing").val(),
          leakage_id_type: $("#leakage_id_type").val(),
          recovery_type: $("#recovery_type").val(),
          list_rc: list_rc,
          attachment: attachment
        };

        json_incident = JSON.stringify(incident);
        //alert(json_incident);
        //document.getElementById('json').innerHTML = json_incident;

        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  var obj = jQuery.parseJSON(data);
                  //alert(data);
                  switch(obj.status){
                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";
                          pop('disable-background','success-message');
                          document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          break;
                      default:
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });  
      <?php }else {?>
        var incident = { 
          incident_id: "<?php echo $detail['id']?>", 
          external_id: $("#external_id").val(),
          opcoid: $("#opcoid").val(),
          opconame: $('#opconame').val(),
          date_created: $("#date_created").val(),
          create_by: $("create_by").val(),
          date_update: $("#date_update").val(),
          updated_by: $("#updated_by").val(),
          incident_status: $("#incident_status").val(),
          incident_status_detail: $("#incident_status_detail").val(),

          incident_detail_id: "",
          description: $("#description").val(),
          note: $("#note").val(),
          detection_tool : $("#detection_tool").val(),
          rutid: $("#rutid").val(),
          stid: $("#stid").val(),
          ocid: $("#ocid").val(),
          impact_severity: $("#impact_severity").val(),
          detection_date: $("#detection_date").val(),
          case_start_date: $("#case_start_date").val(),
          case_end_date: $("#case_end_date").val(),
          leakage_one_day: $("#leakage_one_day").val(),
          recovery: $("#recovery").val(),
          leakage_freq: $("#leakage_freq").val(),
          leakage_timing: $("#leakage_timing").val(),
          leakage_id_type: $("#leakage_id_type").val(),
          recovery_type: $("#recovery_type").val(),
          list_rc: list_rc,
          attachment: attachment
        };

        json_incident = JSON.stringify(incident);
        //alert(json_incident);
        //document.getElementById('json').innerHTML = json_incident;
        
        
        $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/insert_incident",  
              contentType: 'application/x-www-form-urlencoded',
              data: { 
                json: json_incident,
                mode: "<?php echo $mode ?>",
                sess: "<?php echo session_id()?>"
              },
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                
              },
              success: function(data){
                  var obj = jQuery.parseJSON(data);
                  //alert(data);

                  switch(obj.status){
                      case 1 :
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Create Incident Succes')?>";
                          document.getElementById('content-msg').innerHTML = "<?php echo $this->lang->line('Incident already saved')?>";
                          pop('disable-background','success-message');
                          document.getElementById("btn-success-message").onclick = function() { success_save(0); }
                          break;
                      default:
                          document.getElementById('title-msg').innerHTML = "<?php echo $this->lang->line('Error Message')?>";
                          document.getElementById('content-msg').innerHTML = obj.msg;
                          document.getElementById("btn-success-message").onclick = function() { success_save(1); }
                          pop('disable-background','success-message');
                          break;
                  }


              }
        });

      <?php } ?>      
        
      }
  }

  function btn_cancel_click(){
      pop('disable-background','cancel-message');

  }

  function prev_page(){
      location.href="<?php echo $cancel_operation_link?>";
  }

  function do_upload() {
      //alert('jsv');
      var data = new FormData($('#form-upload')[0]);

      if($("#file-description").val()==''){
          alert('Please type file description');
      }else {
          $.ajax({
              type: "POST",  
              url: "<?php echo base_url()?>index.php/incident/upload_attachment",  
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              data: data,
              dataType: "text",
              beforeSend: function(){

              },
              complete: function(){
                    
              },
              success: function(data){
                    //location.reload();
                    alert(data);
                    
              }
          });          
      }
      
  }
  function cancel_upload(){
      hide('disable-background','upload-form');
      return false;
  }

  $(function() {
      $('#form-upload').submit(function(e) {
          hide('disable-background','upload-form');
          document.getElementById('disable-background2').style.display = 'block';

          e.preventDefault();
          $.ajaxFileUpload({
              url             :"<?php echo base_url()?>index.php/incident/upload_attachment", 
              secureuri       :false,
              fileElementId   :'file-attachment',
              dataType: 'JSON',
              beforeSend: function(){
                  
              },
              complete: function(){

              },
              success : function (data){
                  document.getElementById('disable-background2').style.display = 'none';
                  //alert(data);
                  var obj = jQuery.parseJSON(data);
                  if(obj.status=='success'){
                      fa = {
                          incident_detail_id: "",
                          file_id: "",
                          file_description: $("#file-description").val(),
                          type: obj.filetype,
                          name: obj.filename,
                          fullpath: obj.fullpath
                          }

                      attachment.push(fa);
                      num_file = attachment.length;
                      tbl_attachment = '';
                      for(i=0;i<attachment.length;i++){
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth()+1; //January is 0!
                          var yyyy = today.getFullYear();
                          var hh = today.getHours();
                          var mm = today.getMinutes();
                          var ss = today.getSeconds();

                          if(dd<10){
                              dd='0'+dd
                          } 
                          if(mm<10){
                              mm='0'+mm
                          } 
                          var today = yyyy+'-'+mm+'-'+dd+' '+hh+':'+mm+':'+ss;
                          var user = "<?php echo $this->session->userdata('username')?>";
                          //var desc = 

                          tbl_attachment += '<div class="table-body">';
                          tbl_attachment += '   <div class="rows">';
                          tbl_attachment += '       <div class="table-body-item c1"><?php echo $this->lang->line("No")?></div>';
                          tbl_attachment += '       <div class="table-body-item col-no">'+(i+1)+'</div>';
                          tbl_attachment += '   </div>';
                          tbl_attachment += '   <div class="rows">';
                          tbl_attachment += '       <div class="table-body-item c1"><?php echo $this->lang->line("Date Time")?></div>';
                          tbl_attachment += '       <div class="table-body-item col-username">'+today+'</div>';
                          tbl_attachment += '   </div>';
                          tbl_attachment += '   <div class="rows">';
                          tbl_attachment += '       <div class="table-body-item c1"><?php echo $this->lang->line("User")?></div>';
                          tbl_attachment += '       <div class="table-body-item col-username">'+user+'</div>';
                          tbl_attachment += '   </div>';
                          tbl_attachment += '   <div class="rows">';
                          tbl_attachment += '       <div class="table-body-item c1"><?php echo $this->lang->line("Filename")?></div>';
                          tbl_attachment += '       <div class="table-body-item col-filename">'+(attachment[i].name)+'</div>';
                          tbl_attachment += '   </div>';
                          tbl_attachment += '   <div class="rows">';
                          tbl_attachment += '       <div class="table-body-item c1"><?php echo $this->lang->line("File Description")?></div>';  
                          tbl_attachment += '       <div class="table-body-item col-desc">'+attachment[i].file_description+'</div>';
                          tbl_attachment += '   </div>';
                          tbl_attachment += '</div>'; 

                      }
                      //alert(tbl_attachment);
                      document.getElementById('fa').innerHTML = tbl_attachment;
                  }else{
                      alert(obj.msg);
                  }
              }
          });
          return false;  
      });
  });

  function success_save(c){
    if(c==0){
        hide('disable-background','success-message');
        location.href ="<?php echo $cancel_operation_link?>";
    }else{
        hide('disable-background','success-message');
    }
  }

  function btn_update_click(){
    location.href = "<?php echo base_url()?>index.php/incident/incident_update/<?php echo $detail['id']?>/<?php echo $detail['incident_detail_id']?>";
  }


</script>
