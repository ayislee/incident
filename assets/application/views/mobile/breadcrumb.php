<div class="breadcrumb-area">
	<ol class="breadcrumb">
	<?php 
		  foreach ($breadcrumb as $breadcrumb_item) 
		  { 
	?>
		  <li <?php echo $breadcrumb_item['active']?>><a href="<?php echo $breadcrumb_item['link']?>" ><?php echo $breadcrumb_item['name']?></a></li>
	<?php } ?>
	</ol>
</div>