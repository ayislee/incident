<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Incdent Management')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/component.css" />
		<script src="<?php echo base_url()?>assets/js/modernizr.custom.js"></script>
		<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery-ui-1.11.4.custom/jquery-ui.min.css">
  		<script src="<?php echo base_url()?>assets/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/mobile.css" />
		<script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
	</head>
	<body class="cbp-spmenu-push">
		<?php echo $sidemenu?>
		<div class="container">
			<?php echo $header?>
			<div class="content">
			<?php echo $title ?>
			<?php echo $breadcrumb ?>
			<?php echo $content ?>
			</div>
		</div>

		<div class="ontop" id="disable-background"></div>

		<!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
		<script src="<?php echo base_url()?>assets/js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				body = document.body;

		var keys = {37: 1, 38: 1, 39: 1, 40: 1};

		function preventDefault(e) {
		  e = e || window.event;
		  if (e.preventDefault)
		      e.preventDefault();
		  e.returnValue = false;  
		}

		function preventDefaultForScrollKeys(e) {
		    if (keys[e.keyCode]) {
		        preventDefault(e);
		        return false;
		    }
		}

		function disableScroll() {
		  if (window.addEventListener) // older FF
		      window.addEventListener('DOMMouseScroll', preventDefault, false);
		  window.onwheel = preventDefault; // modern standard
		  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
		  window.ontouchmove  = preventDefault; // mobile
		  document.onkeydown  = preventDefaultForScrollKeys;
		}

		function enableScroll() {
		    if (window.removeEventListener)
		        window.removeEventListener('DOMMouseScroll', preventDefault, false);
		    window.onmousewheel = document.onmousewheel = null; 
		    window.onwheel = null; 
		    window.ontouchmove = null;  
		    document.onkeydown = null;  
		}

		  function pop1(div) {
		    document.getElementById(div).style.display = 'block';
		    

		  }
		  function hide1(div) {
		    document.getElementById(div).style.display = 'none';

		  }

			showLeft.onclick = function() {
				
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				if ( $( "#cbp-spmenu-s1" ).is( ".cbp-spmenu-open" ) ) { 
					pop1('disable-background');
				}else{
					hide1('disable-background');
				}
			};

			closemenu.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				if ( $( "#cbp-spmenu-s1" ).is( ".cbp-spmenu-open" ) ) { 
					pop1('disable-background');
				}else{
					hide1('disable-background');
				}
				
			};

			
			
		</script>
	</body>
</html>
