<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />

		<title>Login</title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/mobile.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	</head>
	<style type="text/css">

	</style>
	<body>
		
		<div class="login-content">
			<div align="center">
				<img class="login-logo" src="<?php echo base_url()?>assets/images/login-logo.png">
			</div>
			<div class="module-title">
				<?php 
				if($this->session->userdata('loaded_module')=='2'){
					echo $this->lang->line('Incident Module');
				}else{
					echo $this->lang->line('Administrator Module');
				}
				?>
				<form method="post" action="<?php echo $action ?>">
					<br>
					<?php echo $language_switcher?>
					<br>
					<br>
					<div class="login-area">
						<div class="login_title"><?php echo $this->lang->line('please_login')?></div>
						<input class="form-control login-input" name="username" id="username" type="text" placeholder="<?php echo $this->lang->line('username')?>" autocomplete="off">
						<input class="form-control login-input" name="password" id="password" type="password" placeholder="<?php echo $this->lang->line('password')?>">
						<button class="btn login-btn"><?php echo $this->lang->line('login')?></button>
					</div>
				</form>
			</div>
			<div class="login-message">
				<br>
					<?php echo $message ?>
				<br>
			</div>

		</div>
	</body>
</html>