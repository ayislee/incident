<div class="opco-filter-table">

	<?php
	$count = 0; 
	foreach($by_opco->result() AS $row) {
		$count++;
	?>
	<div class="table-body">
		<div class="rows">
			<div class="table-body-item fo-c1"><?php echo $count?></div>	
		</div>
		<div class="rows">
			<div class="table-body-item c1"><?php echo $this->lang->line('OpCo')?></div>
			<div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/incident_table/<?php echo urlencode('%')?>/<?php echo $row->opcoid?>"><?php echo $row->opconame?></a></div>
		</div>
		<div class="rows">
		<div class="table-body-item c1"><?php echo $this->lang->line('Open Incident') ?></div>
			<div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/incident_table/0/<?php echo $row->opcoid?>"><?php echo $this->lang->line('Open Incident').'('.$row->open.')';?></a></div>
		</div>
		<div class="rows">
		<div class="table-body-item c1"><?php echo $this->lang->line('Under Investigation2')?></div>
			<div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/incident_table/2/<?php echo $row->opcoid?>"><?php echo $this->lang->line('Under Investigation2').'('.$row->investigation.')';?></a></div>
		</div>
		<div class="rows">
		<div class="table-body-item c1"><?php echo $this->lang->line('Close Incident')?></div>
			<div class="table-body-item c2"><a href="<?php echo base_url()?>index.php/incident/incident_table/1/<?php echo $row->opcoid?>"><?php echo $this->lang->line('Close Incident').'('.$row->close.')';?></a></div>
		</div>
		
	</div>
	<?php } ?>

</div>