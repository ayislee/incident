<style type="text/css">
	.right-align{
		
		vertical-align: middle;
		padding-right: 10px;
	}

	.navigator {
		position: relative;
		bottom: 0;
	}

	.amount-block {
		display: inline-block;;
	}
</style>

<div class="user-search2">
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('Search')?>		
		</div>
		<div class="std-input width10" >
			<input name="id-search" id="id-search">
		</div>
	</div>
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('amount')?>		
		</div>
		<div class="user-search-item">
			<?php echo $select_amount?>
		</div>
		<div class="amount-block" id="amount-value">
			<div class="std-input width10" >
				<input name="min-value" id="min-value" placeholder="<?php echo $this->lang->line('min_value')?>" type="number">
			</div>
			<div class="std-input width10" >
				<input name="max-value" id="max-value" placeholder="<?php echo $this->lang->line('max_value')?>" type="number">
			</div>
		</div>
	</div>
	<div class="rows">
		<div class="user-search-title">
			<?php echo $this->lang->line('status')?>		
		</div>
		<div class="user-search-item">
			<?php echo $select_status?>
		</div>
		
		<div class="user-search-item">
			<button class="std-btn bkgr-green" style="height: 28px" id="btn-filter"><?php echo $this->lang->line('Filter')?></button>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#id-search").keypress(function (e) {
	    if (e.which == 13) {
	        location.href = "<?php echo base_url()?>index.php/incident/incident_table/2/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val()+"?amount="+$("#filter-amount").val()+"&min="+$("#min-value").val()+"&max="+$("#max-value").val());
	    }
	});

	$("#min-value").keypress(function (e) {
	    if (e.which == 13) {
	        location.href = "<?php echo base_url()?>index.php/incident/incident_table/2/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val()+"?amount="+$("#filter-amount").val()+"&min="+$("#min-value").val()+"&max="+$("#max-value").val());
	    }
	});

	$("#max-value").keypress(function (e) {
	    if (e.which == 13) {
	        location.href = "<?php echo base_url()?>index.php/incident/incident_table/2/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val()+"?amount="+$("#filter-amount").val()+"&min="+$("#min-value").val()+"&max="+$("#max-value").val());
	    }
	});


</script>

<style type="text/css">
	.sortir {
		vertical-align: top;
		display: inline-block;
	}

	.table-head {
		min-width: 1400px;
	}
	.table-body {
		min-width: 1400px;
	}
	.btn-sort-asc {
		vertical-align: middle;
		background: none;
		padding-top: 0;
		padding-bottom: 0px;
		display: inline-block;
		border-width: 0px;
		outline: none;
		margin-left: -10px;
		margin-right: 0;
		margin-top: 5px;

		

	}

	.btn-sort-desc {
		vertical-align: top;
		background: none;
		padding-bottom: 0px;
		display: inline-block;
		border-width: 0px;
		outline: none;
		margin-left: 0;
		margin-right: 0;
		margin-top: -2px;


	}
	.mid {
		display: inline-block;
		vertical-align: middle;

	}

	.user-table{
		min-width: 1056px;		
	}

	.right-align {
		text-align: right;
		padding-right: 30px;
	}

	.navigator {
		min-width: 1056px;	
	}

	.asc {
		
	}
	.desc {
		
	}

	.inc-c1 {

	}

	.inc-c2 {

	}

	.inc-c3 {

	}

	.inc-c4 {

	}

	.inc-c5 {

	}

	.inc-c6 {

	}

	.inc-c7 {

	}

</style>
<div class="user-table" id="user-table">
	<div class="table-head">
		<div class="table-head-item inc-c1"><?php echo $this->lang->line('No')?></div>
		<div class="table-head-item inc-c2"><?php echo $this->lang->line('Incident ID')?></div>
		<div class="table-head-item inc-c3"><?php echo $this->lang->line('External ID')?></div>
		<div class="table-head-item inc-c4">
			<div class="mid">
				<?php echo $this->lang->line('date_created')?>
			</div>
			
			<div class="sortir">
			
				<button class="btn-sort-desc" onclick="setUrl(2)">
					<i class="fa fa-sort-desc desc" aria-hidden="true"></i>
				</button>
				<button class="btn-sort-asc" onclick="setUrl(1)">
					<i class="fa fa-sort-asc asc" aria-hidden="true"></i>
				</button>
			
			</div>
		</div>

		<div class="table-head-item inc-c4">
			<div class="mid">
				<?php echo $this->lang->line('close_date')?>
			</div>
			<div class="sortir">
			
				<button class="btn-sort-desc" onclick="setUrl(2)">
					<i class="fa fa-sort-desc desc" aria-hidden="true"></i>
				</button>
				<button class="btn-sort-asc" onclick="setUrl(1)">
					<i class="fa fa-sort-asc asc" aria-hidden="true"></i>
				</button>
			
			</div>
		</div>

		<div class="table-head-item inc-c6"><?php echo $this->lang->line('status')?></div>
		<div class="table-head-item inc-c7">
			<div class="mid">
				<?php echo $this->lang->line('actual_loss')?>
			</div>
			<div class="sortir">
			
				<button class="btn-sort-desc" onclick="setUrl(6)">
					<i class="fa fa-sort-desc desc" aria-hidden="true"></i>
				</button>
				<button class="btn-sort-asc" onclick="setUrl(5)">
					<i class="fa fa-sort-asc asc" aria-hidden="true"></i>
				</button>
			
			</div>
		</div>
		<div class="table-head-item inc-c7">
			<div class="mid">
				<?php echo $this->lang->line('recovered')?>
			</div>
			<div class="sortir">
			
				<button class="btn-sort-desc" onclick="setUrl(8)">
					<i class="fa fa-sort-desc desc" aria-hidden="true"></i>
				</button>
				<button class="btn-sort-asc" onclick="setUrl(7)">
					<i class="fa fa-sort-asc asc" aria-hidden="true"></i>
				</button>
			
			</div>
		</div>
		<div class="table-head-item inc-c7">
			<div class="mid">
				<?php echo $this->lang->line('Potential Leakage')?>
			</div>
			
			<div class="sortir">
			
				<button class="btn-sort-desc" onclick="setUrl(4)">
					<i class="fa fa-sort-desc desc" aria-hidden="true"></i>
				</button>
				<button class="btn-sort-asc" onclick="setUrl(3)">
					<i class="fa fa-sort-asc asc" aria-hidden="true"></i>
				</button>
			
			</div>
		</div>

	</div>
	<?php

	if($data->num_rows()>0){	
		$number = (($page-1)*ROW_PER_PAGE) + 1; 
		foreach ($data->result() as $row) {?>
	<div class="table-body">
		<div class="table-body-item inc-c1"><?php echo $number?></div>
		<div class="table-body-item inc-c2"><a href="<?php echo base_url()?>index.php/incident/incident_view/<?php echo $row->id?>"><?php echo $row->incident_num?></a></div>	
		<div class="table-body-item inc-c3"><?php echo $row->external_id?></div>			
		<div class="table-body-item inc-c4"><?php echo $row->create_date?></div>
		<div class="table-body-item inc-c5"><?php echo $row->close_date?></div>
		<?php
			switch ($row->incident_status) {
			 	case '0':
			 		$dsp = $this->lang->line('Open');
			 		break;
			 	
			 	case '1':
			 		$dsp = $this->lang->line('Close');
			 		break;

			 	case '2':
			 		$dsp = $this->lang->line('Under Investigation');
			 		break;

			 } 
		?>
		<div class="table-body-item inc-c6"><?php echo $dsp?></div>
		<div class="table-body-item inc-c7 kiri">$ <?php echo number_format($row->c_final_leakage,0)?></div>
		<div class="table-body-item inc-c7 kiri">$ <?php echo number_format($row->c_recovered,0)?></div>
		<div class="table-body-item inc-c7 kiri">$ <?php echo number_format($row->c_potential,0)?></div>
		
	</div>
	<?php
		$number++; 
		}
	}else{?>
	<div class="table-body center21">
		<?php echo $this->lang->line('No Records')?>
	</div>
	<?php }?>
</div>

<div class="navigator">
	<div class="pages">
		<div >
			<?php echo $pagination ?>
		</div>
	</div>
	<div class="command">
		<?php if($create_incident){ ?>
		<button class="std-btn bkgr-green" id="btn-add"><?php echo $this->lang->line('add')?></button>
		<?php }?>
		<?php if($bulk_incident){ ?>
		<button class="std-btn bkgr-blue" id="btn-upload" style="display:none"><?php echo $this->lang->line('BULK UPLOAD')?></button>
		<button class="std-btn bkgr-blue" id="btn-upload" style="display:none" onclick="pop('disable-background','help-form')"><?php echo $this->lang->line('?')?></button>
		<?php }?>

	</div>
</div>

<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div id="upload-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload bulk file')?>
    </div>
    <form name="form-upload" id="form-upload" >
        <div class="content-message">
            <input type="file" name="file-attachment" id="file-attachment" class="margin-left30" />
        </div>
        <div class="confirm-btn">
            <button type="submit" class="std-btn bkgr-green"><?php echo $this->lang->line('Upload')?></button>
            <button type="button" class="std-btn bkgr-red" onclick="hide('disable-background','upload-form')"><?php echo $this->lang->line('cancel')?></button>
        </div>
    </form>

</div>

<div id="help-form" class="add-access-module3">
    <div class="modify-title">
        <?php echo $this->lang->line('Example Upload bulk CSV file')?>
    </div>
    
    <div class="content-message">
    	<div class="center21" style="font-weight:bold;"><?php echo $this->lang->line('Examples')?></div>
        <?php $example = '<ExternalID>,<OpCoID>,<Detection Tool>,<Risk Universe Type>,<Section Title>,<Operator Category>,<RiskCat>,<Impact Severity>,<Risk Start Date>,<Detection Date>,<Case Start Date>, <Case End Date>,<Leakage One Day>,<Recovery>,<Leakage Freq>,<Leakage Timing>,<Leakage ID Type>,<Recovery Type>, <Description>,<Investigation Note>

829882075,Algeria,FMS,RA (Revenue Assurance),Order Management and Provisioning,Mobile,"Failure to meet contract volume commitment,Ported-in number not billed,Incorrect capture of equipment sales",5,1012016,1012016,31012016,10000,100000,Recursive,Real-Time,Proactive,Recovered,"Incident reported by FMS, pending detailed investigation.",Confirmed and create incident
';?>
        <textarea class="std-textarea" rows="10" disabled><?php echo $example?></textarea>
    </div>
    <div class="confirm-btn">
        <button type="button" class="std-btn bkgr-blue" onclick="hide('disable-background','help-form')"><?php echo $this->lang->line('ok')?></button>
    </div>
   

</div>


<div id="uploading" class="add-access-module2">
    <div class="content-message2" id="upload-message">
    	<?php echo $this->lang->line('Uploading file ... please wait')?>
    </div>
    <div class="confirm-btn" style="display:none" id="btn-area">
    	<button class="std-btn bkgr-green" onclick="hide('disable-background2','uploading');"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>

<div id="success-form" class="add-access-module2">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload Result')?>
    </div>
    
    <div class="content-message">
    	<div class="center21" style="font-weight:bold;" id="success-result"></div>
    </div>

    <div class="confirm-btn">
        <button type="button" class="std-btn bkgr-blue" onclick="location.reload()"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>

<div id="error-form" class="add-access-module3">
    <div class="modify-title">
        <?php echo $this->lang->line('Upload Result')?>
    </div>

    <div class="content-message">
        <textarea class="std-textarea" rows="10" disabled id="error-result"></textarea>
    </div>

    <div class="confirm-btn">
        <button type="button" class="std-btn bkgr-blue" onclick="hide('disable-background','error-form')"><?php echo $this->lang->line('ok')?></button>
    </div>
</div>


<script type="text/javascript">
	$("#btn-filter").click(function(){
		location.href = "<?php echo base_url()?>index.php/incident/incident_table/2/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val()+"?amount="+$("#filter-amount").val()+"&min="+$("#min-value").val()+"&max="+$("#max-value").val());
	});

	$("#btn-add").click(function(){
		location.href = "<?php echo base_url()?>index.php/incident/new_incident/<?php echo $opcoid?>";
	});

	$("#btn-upload").click(function(){
		pop('disable-background','upload-form');
	});

  function pop(div,div2) {
    document.getElementById(div).style.display = 'block';
    document.getElementById(div2).style.display = 'block';

  }
  function hide(div,div2) {
    document.getElementById(div).style.display = 'none';
    document.getElementById(div2).style.display = 'none';
  }

  $(function() {
      $('#form-upload').submit(function(e) {
          hide('disable-background','upload-form');
          document.getElementById('disable-background2').style.display = 'block';
          document.body.style.cursor='wait';
          document.getElementById('uploading').style.display = 'block';


          e.preventDefault();
          $.ajaxFileUpload({
              url             :"<?php echo base_url()?>index.php/incident/upload_bulk", 
              secureuri       :false,
              fileElementId   :'file-attachment',
              dataType: 'JSON',
              beforeSend: function(){
                  
              },
              complete: function(){

              },
              success : function (data){
              		

				var obj = jQuery.parseJSON(data);
				//alert(obj.fullpath);
				
				if(obj.status=="success"){
						document.getElementById('upload-message').innerHTML = "<?php echo $this->lang->line('process_bulk')?>";
						$.ajax({
				        type: "POST",  
				        url: "<?php echo base_url()?>index.php/incident/analyst_csv",  
				        contentType: 'application/x-www-form-urlencoded',
				        data: { 
				            filename: obj.fullpath,
				            sess: "<?php echo session_id()?>"
				        },
				        dataType: "text",
				        beforeSend: function(){

				        },
				        complete: function(){
				            
				        },
				        success: function(data){
				            //location.reload();
				            hide('disable-background2','uploading');
				            document.body.style.cursor='default';
				            var status = jQuery.parseJSON(data);
				            //alert(status.msg);
				            if(status.status){
				            	document.getElementById('success-result').innerHTML = status.msg; 
				            	pop('disable-background','success-form');
				            }else{
				            	document.getElementById('error-result').innerHTML = status.msg; 
				            	pop('disable-background','error-form');
				            }
				            //location.reload();
				            //stateChange(-1);

				        }
				    });


				}else{
						document.body.style.cursor='default';
						document.getElementById('upload-message').innerHTML = obj.msg;
						document.getElementById('btn-area').style.display = 'block';
				}
                 
              }
        });
        return false;  
      });
  });

	function stateChange(newState) {
	    setTimeout(function(){
	        if(newState == -1){alert('VIDEO HAS STOPPED');}
	    }, 3000);
	}

	function sort_create_asc(){
		alert("hello");
	}

	function setUrl(mode) {
	    var url = window.location.href;
	    location.href = "<?php echo base_url()?>index.php/incident/incident_table/"+mode+"/"+encodeURI($("#filter-status").val())+"/<?php echo $opcoid?>/"+encodeURI($("#id-search").val())+"?amount="+$("#filter-amount").val()+"&min="+$("#min-value").val()+"&max="+$("#max-value").val();
	  }

	 $(document).ready(function(){
	 	sortir = <?php echo $sorting?>;
	 });



	var resizeEventsTrigger = (function () {
	    function triggerResizeStart($el) {
	        $el.trigger('resizestart');
	        isStart = !isStart;
	    }

	    function triggerResizeEnd($el) {
	        clearTimeout(timeoutId);
	        timeoutId = setTimeout(function () {
	            $el.trigger('resizeend');
	            isStart = !isStart;
	        }, delay);
	    }

	    var isStart = true;
	    var delay = 200;
	    var timeoutId;

	    return function ($el) {
	        isStart ? triggerResizeStart($el) : triggerResizeEnd($el);
    	};

	})();

	$("body").on('resizestart', function () {
	    console.log('resize start');
	});
	$("body").on('resizeend', function () {
		refwindow();
	});

	window.onresize = function () {
	    resizeEventsTrigger( $("body") );
	};

	function refwindow(){
		console.log(window.innerHeight);
	    console.log('resize end');
	    totalusertable = window.innerHeight;
	    console.log(totalusertable);
	    headerheight = 56;
	    titleheight = 31;
	    breadcrumheight = 33;
	    searchheight = 94;
	    navigatorheight = 41;
	    usertable = totalusertable - headerheight - headerheight - titleheight - breadcrumheight - searchheight - navigatorheight;
	    document.getElementById("user-table").style.maxHeight = usertable+"px";
	    console.log(usertable);
	}

	$(window).resize(function()
	{   
	    refwindow();
	});
	
	$( document ).ready(function() {
		$("#id-search").val('<?php echo $txt_search ?>');
		$("#filter-amount").val('<?php echo $amount ?>');
		$("#min-value").val(<?php echo $min ?>);
		$("#max-value").val(<?php echo $max ?>);
		

	    refwindow();

	});

</script>