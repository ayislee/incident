<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title>Template Desktop</title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	</head>
	<style type="text/css">
		.main-logo {
			display: block;
			text-align: center;
		}
		.msg-denied {
			display: block;
			font-weight: bold;
			font-size: 14px;

		}
	</style>


	<body>
		<?php echo $sidemenu ?>
		<div class="main-content">
			<?php echo $header ?>
			<?php echo $title ?>
			<div class="main-logo">
				<img class="login-logo" src="<?php echo base_url()?>assets/images/access-denied.png">
				<div class="msg-denied"><?php echo $this->lang->line('access_denied_msg')?></div>
			</div>
		</div>
		
		
	</body>
</html>