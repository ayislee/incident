<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Report')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-3.3.2.min.css">

		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap-3.3.2.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery-ui.css" />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>


		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.multiselect.css" />
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.multiselect.js"></script>
		
		<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery-ui-1.11.4.custom/jquery-ui.min.css">
  		<script src="<?php echo base_url()?>assets/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
  		<script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
  		<script src="<?php echo base_url()?>assets/js/jquery.number.js"></script>
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/incident-desktop.css">  	

	</head>
	<style type="text/css">
		.body {
			min-height:720px;
		}
		.sidemenu {
			min-height:720px;
		}

	</style>
	<body>
		<?php echo $sidemenu ?>
		<div class="main-content2">
			<?php echo $header ?>
			<?php echo $title ?>
			<?php //echo $breadcrumb ?>
			<?php echo $select_report?>
			<?php echo $content ?>
			<?php //echo $user_table ?>
		</div>
		
		
		<div class="content"></div>
	</body>
</html>