<div class="breadcrumb">
	<div class="params-label">
		<?php echo $this->lang->line('Report Name')?>
	</div>
	<select class="std-select" id="select-report">
	<?php foreach ($select_report->result() as $reports): ?>
		<?php if($id==$reports->reportid) $selected='selected'; else $selected='';?>
		<option value="<?php echo $reports->reportid?>" <?php echo $selected?>><?php echo $reports->report_name?></option>
	<?php endforeach?>
	</select>
</div>

<script type="text/javascript">
	$("#select-report").change(function(){
		location.href="<?php echo base_url()?>index.php/incident/report/"+$(this).val();
	});
</script>