<style type="text/css">
.btn-group {
    display: inline-block;
    margin-right: 5px;
    padding-left: 0px;
    padding-top: 0px;
    margin-top: 0px;
    margin-bottom: 5px;
    margin-left: 0px;
    width: auto;
    text-align: right;
}

.btn {
	min-width: 200px;
	width: auto;
}
</style>
<div class="parameters-area">
<?php

	if($parameters['status'] != 0){
		echo '<div class="">'.$parameters['msg'].'</div>';
	}else{
		$no_params=1;
		$count_params = count($parameters['params']);
		foreach ($parameters['params'] as $params) {
			# code...
			echo '<div class="rows">';
			echo '	<div class="params-label">'.$params['Name'].'</div>';
			$multiselect = '';
			foreach ($report['parameters'] as $std_param) {
				# code...
				if(strtoupper($params['Name'])==strtoupper($std_param['name'])) {
					if($std_param['multiselect']==0) $multiselect = ''; else $multiselect='multiple';

				}
			}
			
			echo '	<select id="params_'.$no_params.'" class="" '.$multiselect.'>';
			foreach ($params['ValidValues'] as $value) {
				# code...
				echo '	<option value="'.$value->Value.'">'.$value->Label.'</option>';
			}
			echo '	</select>';
			if($count_params==$no_params){
				echo '<button id="preview" class="btn-preview" >'.'VIEW'.'</button>';
			}
			echo '</div>';
			// js here
			if($multiselect=='multiple'){
				echo '<script type="text/javascript">';
				echo '    $(document).ready(function() {';
				echo '    		$("#params_'.$no_params.'").multiselect({';
				echo '				includeSelectAllOption: true,';
				echo '				selectAllText: "Select All!"';
				echo '			});';
				echo '    });';
				echo '</script>';

			}else{
				echo '<script type="text/javascript">';
				echo '    $(document).ready(function() {';
				echo '    		$("#params_'.$no_params.'").multiselect({';
				echo '			});';
				echo '    });';
				echo '</script>';
			}
			$no_params++;
		}

	}


?>
</div>

<div class="print-report" id="print-report"></div>

<script type="text/javascript">


function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}	

$("#preview").click(function(){
    var selectedValues = [];    
    $("#params_2 :selected").each(function(){
        selectedValues.push($(this).val()); 
    });
    //alert(selectedValues);

<?php
	echo ' var prm = [];';
	echo 'status = '.$parameters['status'].';';
	if($parameters['status'] == 0){
		$no_params=1;
		$count_params = count($parameters['params']);
		foreach ($parameters['params'] as $params) {
			$multi = 0;
			echo 'p = [];';
			foreach ($report['parameters'] as $std_param){
				echo 'obj = [];';		
				echo 'var selectedValues = [];';    
    			echo '$("#params_'.$no_params.' :selected").each(function(){';
        		echo '	selectedValues.push($(this).val());'; 
    			echo '});';				
				echo 'obj = {'.$params['Name'].':[selectedValues]};';

				if($std_param['multiselect']!=0) echo 'p.push(obj);';
				
				$multi++;
			}
			//echo 'j = JSON.stringify(p);';
			//echo 'alert(j);';
			echo 'prm.push(p);';
			$no_params++;
		}
		echo 'j = JSON.stringify(prm);';
		//echo 'alert(j);';
		echo 'document.getElementById("print-report").innerHTML = j;';
		?>

	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/incident/get_report",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	          		report: "<?php echo $report['report']?>",
	                parameters: j,
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	                document.getElementById("print-report").innerHTML = data;
	                
	          }
	    });

	    <?php	


	}else{

	}	
?>    
    return false;
});
</script>
