<style type="text/css">
	.w-cc{
		width: 180px;
	}	

	.view-header {
		display: inline-block;
	}

	.opco-symbol {
		display: inline-block;
		width: 30px;
		text-align: right;
	}

	.special {
		display: inline-block;
		text-align: right;
	}

	.form-input{
		background-color: #FFC711;
		padding: 10px;
		margin-bottom: 3px;
		
		margin-top: 3px;
	}

	.sub-head {
		font-weight: bold;
		font-size: 14px;

	}

	.font-normal{
		width: 300px;
	}

	.tab-content {

    	width: 730px;
    }

    .cmd-command {
	    display: block;
	    width: 730px;
	    text-align: right;
	}
</style>
<div class="opco-content" >
	<div class="label-item-var" ><?php echo $this->lang->line('OpCo')?></div>
	<div class="view-header" id="opco-name-view"></div>
	<input id="var-opco-id" type="hidden" value="<?php echo $opco_id?>">
	
	<div class="label-item align-right"><?php echo $this->lang->line('Month')?></div>
	<div class="view-header" id="month-view"></div>
	<input id="var-month" type="hidden" value="<?php echo $month?>">

	<div class="label-item align-right"><?php echo $this->lang->line('Year')?></div>
	<div class="view-header" id="year-view"></div>
	<input id="var-year" type="hidden" value="<?php echo $year?>">
	<!--<button class="std-btn bkgr-green text-right" onclick="reload_page()" id="btn-get-config"><?php echo $this->lang->line('Get Opco Configuration')?></button> -->
</div>


<div class="opco-content">
	

	<div class="tab-content">
	  
		<div class="form-input">
			<div class="sub-head"><?php echo $this->lang->line('General')?></div>
		</div>

		<div class="rows-2">
			<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[0]['id']?>" id="id-<?php echo $config[0]['id']?>" value="<?php echo $config[0]['id']?>">
	  			<input type="hidden" name="<?php echo $config[0]['variable']?>" id="<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[1]['id']?>" id="id-<?php echo $config[1]['id']?>" value="<?php echo $config[1]['id']?>">
	  			<input type="hidden" name="<?php echo $config[1]['variable']?>" id="<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['variable']?>">
	  			<?php echo $config[0]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<div class="value-item"><?php echo $config[0]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[0]['variable']?>" value="<?php echo $config[0]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[1]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[1]['variable']?>" value="<?php echo $config[1]['value']?>" disabled>			
		</div>

		<div class="rows-2">
			<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[2]['id']?>" id="id-<?php echo $config[2]['id']?>" value="<?php echo $config[2]['id']?>">
	  			<input type="hidden" name="<?php echo $config[2]['variable']?>" id="<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[3]['id']?>" id="id-<?php echo $config[3]['id']?>" value="<?php echo $config[3]['id']?>">
	  			<input type="hidden" name="<?php echo $config[3]['variable']?>" id="<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['variable']?>">
	  			<?php echo $config[2]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<div class="value-item"><?php echo $config[2]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[2]['variable']?>" value="<?php echo $config[2]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[3]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[3]['variable']?>" value="<?php echo $config[3]['value']?>" disabled>			

		</div>

		<div class="rows-2">
			<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[4]['id']?>" id="id-<?php echo $config[4]['id']?>" value="<?php echo $config[4]['id']?>">
	  			<input type="hidden" name="<?php echo $config[4]['variable']?>" id="<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[5]['id']?>" id="id-<?php echo $config[5]['id']?>" value="<?php echo $config[5]['id']?>">
	  			<input type="hidden" name="<?php echo $config[5]['variable']?>" id="<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['variable']?>">
	  			<?php echo $config[4]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<div class="value-item"><?php echo $config[4]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[4]['variable']?>" value="<?php echo $config[4]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[5]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[5]['variable']?>" value="<?php echo $config[5]['value']?>" disabled>			

		</div>

		<div class="rows-2">
			<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[6]['id']?>" id="id-<?php echo $config[6]['id']?>" value="<?php echo $config[6]['id']?>">
	  			<input type="hidden" name="<?php echo $config[6]['variable']?>" id="<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['variable']?>">
	  			<input type="hidden" name="id-<?php echo $config[7]['id']?>" id="id-<?php echo $config[7]['id']?>" value="<?php echo $config[7]['id']?>">
	  			<input type="hidden" name="<?php echo $config[7]['variable']?>" id="<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['variable']?>">
	  			<?php echo $config[6]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<div class="value-item"><?php echo $config[6]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[6]['variable']?>" value="<?php echo $config[6]['value']?>" disabled>
	  		<div class="width98"><?php echo $config[7]['value_desc']?></div>
	  		<input class="input-90" type="text" name="number" id="value-<?php echo $config[7]['variable']?>" value="<?php echo $config[7]['value']?>" disabled>			

		</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[8]['id']?>" id="id-<?php echo $config[8]['id']?>" value="<?php echo $config[8]['id']?>">
	  			<input type="hidden" name="<?php echo $config[8]['variable']?>" id="<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['variable']?>">
	  			<?php echo $config[8]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[8]['variable']?>" value="<?php echo $config[8]['value']?>" disabled>
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[9]['id']?>" id="id-<?php echo $config[9]['id']?>" value="<?php echo $config[9]['id']?>">
	  			<input type="hidden" name="<?php echo $config[9]['variable']?>" id="<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['variable']?>">
	  			<?php echo $config[9]['desc']?>
	  		</div>
	  		<div class="opco-symbol"></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[9]['variable']?>" value="<?php echo $config[9]['value']?>" disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[10]['id']?>" id="id-<?php echo $config[10]['id']?>" value="<?php echo $config[10]['id']?>">
	  			<input type="hidden" name="<?php echo $config[10]['variable']?>" id="<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['variable']?>">
	  			<?php echo $config[10]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[62]['id']?>" id="id-<?php echo $config[62]['id']?>" value="<?php echo $config[62]['id']?>">
	  			<input type="hidden" name="<?php echo $config[62]['variable']?>" id="<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[62]['variable']?>" value="<?php echo $config[62]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[10]['variable']?>" value="<?php echo $config[10]['value']?>" readonly disabled>
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[11]['id']?>" id="id-<?php echo $config[11]['id']?>" value="<?php echo $config[11]['id']?>">
	  			<input type="hidden" name="<?php echo $config[11]['variable']?>" id="<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['variable']?>">
	  			<?php echo $config[11]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[63]['id']?>" id="id-<?php echo $config[63]['id']?>" value="<?php echo $config[63]['id']?>">
	  			<input type="hidden" name="<?php echo $config[63]['variable']?>" id="<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[63]['variable']?>" value="<?php echo $config[63]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[11]['variable']?>" value="<?php echo $config[11]['value']?>" readonly disabled>
	  	</div>

		<div class="form-input">
			<div class="sub-head"><?php echo $this->lang->line('Bad Debts')?></div>
		</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[12]['id']?>" id="id-<?php echo $config[12]['id']?>" value="<?php echo $config[12]['id']?>">
	  			<input type="hidden" name="<?php echo $config[12]['variable']?>" id="<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['variable']?>">
	  			<?php echo $config[12]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[13]['id']?>" id="id-<?php echo $config[13]['id']?>" value="<?php echo $config[13]['id']?>">
	  			<input type="hidden" name="<?php echo $config[13]['variable']?>" id="<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[13]['variable']?>" value="<?php echo $config[13]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[12]['variable']?>" value="<?php echo $config[12]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[14]['id']?>" id="id-<?php echo $config[14]['id']?>" value="<?php echo $config[14]['id']?>">
	  			<input type="hidden" name="<?php echo $config[14]['variable']?>" id="<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['variable']?>">
	  			<?php echo $config[14]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[15]['id']?>" id="id-<?php echo $config[15]['id']?>" value="<?php echo $config[15]['id']?>">
	  			<input type="hidden" name="<?php echo $config[15]['variable']?>" id="<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[15]['variable']?>" value="<?php echo $config[15]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[14]['variable']?>" value="<?php echo $config[14]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[16]['id']?>" id="id-<?php echo $config[16]['id']?>" value="<?php echo $config[16]['id']?>">
	  			<input type="hidden" name="<?php echo $config[16]['variable']?>" id="<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['variable']?>">
	  			<?php echo $config[16]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[17]['id']?>" id="id-<?php echo $config[17]['id']?>" value="<?php echo $config[17]['id']?>">
	  			<input type="hidden" name="<?php echo $config[17]['variable']?>" id="<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[17]['variable']?>" value="<?php echo $config[17]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[16]['variable']?>" value="<?php echo $config[16]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[18]['id']?>" id="id-<?php echo $config[18]['id']?>" value="<?php echo $config[18]['id']?>">
	  			<input type="hidden" name="<?php echo $config[18]['variable']?>" id="<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['variable']?>">
	  			<?php echo $config[18]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[19]['id']?>" id="id-<?php echo $config[19]['id']?>" value="<?php echo $config[19]['id']?>">
	  			<input type="hidden" name="<?php echo $config[19]['variable']?>" id="<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[19]['variable']?>" value="<?php echo $config[19]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[18]['variable']?>" value="<?php echo $config[18]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[20]['id']?>" id="id-<?php echo $config[20]['id']?>" value="<?php echo $config[20]['id']?>">
	  			<input type="hidden" name="<?php echo $config[20]['variable']?>" id="<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['variable']?>">
	  			<?php echo $config[20]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[21]['id']?>" id="id-<?php echo $config[21]['id']?>" value="<?php echo $config[21]['id']?>">
	  			<input type="hidden" name="<?php echo $config[21]['variable']?>" id="<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[21]['variable']?>" value="<?php echo $config[21]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[20]['variable']?>" value="<?php echo $config[20]['value']?>" readonly disabled>
	  		
	  	</div>	  		  	

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[22]['id']?>" id="id-<?php echo $config[22]['id']?>" value="<?php echo $config[22]['id']?>">
	  			<input type="hidden" name="<?php echo $config[22]['variable']?>" id="<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['variable']?>">
	  			<?php echo $config[22]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[23]['id']?>" id="id-<?php echo $config[23]['id']?>" value="<?php echo $config[23]['id']?>">
	  			<input type="hidden" name="<?php echo $config[23]['variable']?>" id="<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[23]['variable']?>" value="<?php echo $config[23]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[22]['variable']?>" value="<?php echo $config[22]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[24]['id']?>" id="id-<?php echo $config[24]['id']?>" value="<?php echo $config[24]['id']?>">
	  			<input type="hidden" name="<?php echo $config[24]['variable']?>" id="<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['variable']?>">
	  			<?php echo $config[24]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[25]['id']?>" id="id-<?php echo $config[25]['id']?>" value="<?php echo $config[25]['id']?>">
	  			<input type="hidden" name="<?php echo $config[25]['variable']?>" id="<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[25]['variable']?>" value="<?php echo $config[25]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[24]['variable']?>" value="<?php echo $config[24]['value']?>" readonly disabled>
	  		
	  	</div>

		<div class="form-input">
			<div class="sub-head"><?php echo $this->lang->line('Total Operating Revenue')?></div>
		</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[26]['id']?>" id="id-<?php echo $config[26]['id']?>" value="<?php echo $config[26]['id']?>">
	  			<input type="hidden" name="<?php echo $config[26]['variable']?>" id="<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['variable']?>">
	  			<?php echo $config[26]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[27]['id']?>" id="id-<?php echo $config[27]['id']?>" value="<?php echo $config[27]['id']?>">
	  			<input type="hidden" name="<?php echo $config[27]['variable']?>" id="<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[27]['variable']?>" value="<?php echo $config[27]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[26]['variable']?>" value="<?php echo $config[26]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[28]['id']?>" id="id-<?php echo $config[28]['id']?>" value="<?php echo $config[28]['id']?>">
	  			<input type="hidden" name="<?php echo $config[28]['variable']?>" id="<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['variable']?>">
	  			<?php echo $config[28]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[29]['id']?>" id="id-<?php echo $config[29]['id']?>" value="<?php echo $config[29]['id']?>">
	  			<input type="hidden" name="<?php echo $config[29]['variable']?>" id="<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[29]['variable']?>" value="<?php echo $config[29]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[28]['variable']?>" value="<?php echo $config[28]['value']?>" readonly disabled>
	  		
	  	</div>

		<div class="form-input">
			<div class="sub-head"><?php echo $this->lang->line('Service Revenue')?></div>
		</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[30]['id']?>" id="id-<?php echo $config[30]['id']?>" value="<?php echo $config[30]['id']?>">
	  			<input type="hidden" name="<?php echo $config[30]['variable']?>" id="<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['variable']?>">
	  			<?php echo $config[30]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[31]['id']?>" id="id-<?php echo $config[31]['id']?>" value="<?php echo $config[31]['id']?>">
	  			<input type="hidden" name="<?php echo $config[31]['variable']?>" id="<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[31]['variable']?>" value="<?php echo $config[31]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[30]['variable']?>" value="<?php echo $config[30]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[32]['id']?>" id="id-<?php echo $config[32]['id']?>" value="<?php echo $config[32]['id']?>">
	  			<input type="hidden" name="<?php echo $config[32]['variable']?>" id="<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['variable']?>">
	  			<?php echo $config[32]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[33]['id']?>" id="id-<?php echo $config[33]['id']?>" value="<?php echo $config[33]['id']?>">
	  			<input type="hidden" name="<?php echo $config[33]['variable']?>" id="<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[33]['variable']?>" value="<?php echo $config[33]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[32]['variable']?>" value="<?php echo $config[32]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[34]['id']?>" id="id-<?php echo $config[34]['id']?>" value="<?php echo $config[34]['id']?>">
	  			<input type="hidden" name="<?php echo $config[34]['variable']?>" id="<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['variable']?>">
	  			<?php echo $config[34]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[35]['id']?>" id="id-<?php echo $config[35]['id']?>" value="<?php echo $config[35]['id']?>">
	  			<input type="hidden" name="<?php echo $config[35]['variable']?>" id="<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[35]['variable']?>" value="<?php echo $config[35]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[34]['variable']?>" value="<?php echo $config[34]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[36]['id']?>" id="id-<?php echo $config[36]['id']?>" value="<?php echo $config[36]['id']?>">
	  			<input type="hidden" name="<?php echo $config[36]['variable']?>" id="<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['variable']?>">
	  			<?php echo $config[36]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[37]['id']?>" id="id-<?php echo $config[37]['id']?>" value="<?php echo $config[37]['id']?>">
	  			<input type="hidden" name="<?php echo $config[37]['variable']?>" id="<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[37]['variable']?>" value="<?php echo $config[37]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[36]['variable']?>" value="<?php echo $config[36]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[38]['id']?>" id="id-<?php echo $config[38]['id']?>" value="<?php echo $config[38]['id']?>">
	  			<input type="hidden" name="<?php echo $config[38]['variable']?>" id="<?php echo $config[38]['variable']?>" value="<?php echo $config[38]['variable']?>">
	  			<?php echo $config[38]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[39]['id']?>" id="id-<?php echo $config[39]['id']?>" value="<?php echo $config[39]['id']?>">
	  			<input type="hidden" name="<?php echo $config[39]['variable']?>" id="<?php echo $config[39]['variable']?>" value="<?php echo $config[39]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[39]['variable']?>" value="<?php echo $config[39]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[38]['variable']?>" value="<?php echo $config[38]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[40]['id']?>" id="id-<?php echo $config[40]['id']?>" value="<?php echo $config[40]['id']?>">
	  			<input type="hidden" name="<?php echo $config[40]['variable']?>" id="<?php echo $config[40]['variable']?>" value="<?php echo $config[40]['variable']?>">
	  			<?php echo $config[40]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[41]['id']?>" id="id-<?php echo $config[41]['id']?>" value="<?php echo $config[41]['id']?>">
	  			<input type="hidden" name="<?php echo $config[41]['variable']?>" id="<?php echo $config[41]['variable']?>" value="<?php echo $config[41]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[41]['variable']?>" value="<?php echo $config[41]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[40]['variable']?>" value="<?php echo $config[40]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[42]['id']?>" id="id-<?php echo $config[42]['id']?>" value="<?php echo $config[42]['id']?>">
	  			<input type="hidden" name="<?php echo $config[42]['variable']?>" id="<?php echo $config[42]['variable']?>" value="<?php echo $config[42]['variable']?>">
	  			<?php echo $config[42]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[43]['id']?>" id="id-<?php echo $config[43]['id']?>" value="<?php echo $config[43]['id']?>">
	  			<input type="hidden" name="<?php echo $config[43]['variable']?>" id="<?php echo $config[43]['variable']?>" value="<?php echo $config[43]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[43]['variable']?>" value="<?php echo $config[43]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[42]['variable']?>" value="<?php echo $config[42]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[44]['id']?>" id="id-<?php echo $config[44]['id']?>" value="<?php echo $config[44]['id']?>">
	  			<input type="hidden" name="<?php echo $config[44]['variable']?>" id="<?php echo $config[44]['variable']?>" value="<?php echo $config[44]['variable']?>">
	  			<?php echo $config[44]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[45]['id']?>" id="id-<?php echo $config[45]['id']?>" value="<?php echo $config[45]['id']?>">
	  			<input type="hidden" name="<?php echo $config[45]['variable']?>" id="<?php echo $config[45]['variable']?>" value="<?php echo $config[45]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[45]['variable']?>" value="<?php echo $config[45]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[44]['variable']?>" value="<?php echo $config[44]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[46]['id']?>" id="id-<?php echo $config[46]['id']?>" value="<?php echo $config[46]['id']?>">
	  			<input type="hidden" name="<?php echo $config[46]['variable']?>" id="<?php echo $config[46]['variable']?>" value="<?php echo $config[46]['variable']?>">
	  			<?php echo $config[46]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[47]['id']?>" id="id-<?php echo $config[47]['id']?>" value="<?php echo $config[47]['id']?>">
	  			<input type="hidden" name="<?php echo $config[47]['variable']?>" id="<?php echo $config[47]['variable']?>" value="<?php echo $config[47]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[47]['variable']?>" value="<?php echo $config[47]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[46]['variable']?>" value="<?php echo $config[46]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[48]['id']?>" id="id-<?php echo $config[48]['id']?>" value="<?php echo $config[48]['id']?>">
	  			<input type="hidden" name="<?php echo $config[48]['variable']?>" id="<?php echo $config[48]['variable']?>" value="<?php echo $config[48]['variable']?>">
	  			<?php echo $config[48]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[49]['id']?>" id="id-<?php echo $config[49]['id']?>" value="<?php echo $config[49]['id']?>">
	  			<input type="hidden" name="<?php echo $config[49]['variable']?>" id="<?php echo $config[49]['variable']?>" value="<?php echo $config[49]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[49]['variable']?>" value="<?php echo $config[49]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[48]['variable']?>" value="<?php echo $config[48]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[50]['id']?>" id="id-<?php echo $config[50]['id']?>" value="<?php echo $config[50]['id']?>">
	  			<input type="hidden" name="<?php echo $config[50]['variable']?>" id="<?php echo $config[50]['variable']?>" value="<?php echo $config[50]['variable']?>">
	  			<?php echo $config[50]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[51]['id']?>" id="id-<?php echo $config[51]['id']?>" value="<?php echo $config[51]['id']?>">
	  			<input type="hidden" name="<?php echo $config[51]['variable']?>" id="<?php echo $config[51]['variable']?>" value="<?php echo $config[51]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[51]['variable']?>" value="<?php echo $config[51]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[50]['variable']?>" value="<?php echo $config[50]['value']?>" readonly disabled>
	  		
	  	</div>

		<div class="form-input">
			<div class="sub-head"><?php echo $this->lang->line('Roaming & Interconnect Cost')?></div>
		</div>	  	

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[52]['id']?>" id="id-<?php echo $config[52]['id']?>" value="<?php echo $config[52]['id']?>">
	  			<input type="hidden" name="<?php echo $config[52]['variable']?>" id="<?php echo $config[52]['variable']?>" value="<?php echo $config[52]['variable']?>">
	  			<?php echo $config[52]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[53]['id']?>" id="id-<?php echo $config[53]['id']?>" value="<?php echo $config[53]['id']?>">
	  			<input type="hidden" name="<?php echo $config[53]['variable']?>" id="<?php echo $config[53]['variable']?>" value="<?php echo $config[53]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[53]['variable']?>" value="<?php echo $config[53]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[52]['variable']?>" value="<?php echo $config[52]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[54]['id']?>" id="id-<?php echo $config[54]['id']?>" value="<?php echo $config[54]['id']?>">
	  			<input type="hidden" name="<?php echo $config[54]['variable']?>" id="<?php echo $config[54]['variable']?>" value="<?php echo $config[54]['variable']?>">
	  			<?php echo $config[54]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[55]['id']?>" id="id-<?php echo $config[55]['id']?>" value="<?php echo $config[55]['id']?>">
	  			<input type="hidden" name="<?php echo $config[55]['variable']?>" id="<?php echo $config[55]['variable']?>" value="<?php echo $config[55]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[55]['variable']?>" value="<?php echo $config[55]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[54]['variable']?>" value="<?php echo $config[54]['value']?>" readonly disabled>
	  		
	  	</div>

	  	<div class="rows-2">
	  		<div class="label-item width250 font-normal">
	  			<input type="hidden" name="id-<?php echo $config[64]['id']?>" id="id-<?php echo $config[64]['id']?>" value="<?php echo $config[64]['id']?>">
	  			<input type="hidden" name="<?php echo $config[64]['variable']?>" id="<?php echo $config[64]['variable']?>" value="<?php echo $config[64]['variable']?>">
	  			<?php echo $config[64]['desc']?>
	  			<input type="hidden" name="id-<?php echo $config[65]['id']?>" id="id-<?php echo $config[65]['id']?>" value="<?php echo $config[65]['id']?>">
	  			<input type="hidden" name="<?php echo $config[65]['variable']?>" id="<?php echo $config[65]['variable']?>" value="<?php echo $config[65]['variable']?>">
	  		</div>
	  		<div class="opco-symbol"><?php echo $symbol?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[65]['variable']?>" value="<?php echo $config[65]['value']?>" disabled>
	  		<div class="opco-symbol"><?php echo DEFAULT_CURRENCY_SYMBOL?></div>
	  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[64]['variable']?>" value="<?php echo $config[64]['value']?>" readonly disabled>
	  		
	  	</div>


	  	<!-- hiding  value-->
	  	<div style="display:none">

			<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[56]['id']?>" id="id-<?php echo $config[56]['id']?>" value="<?php echo $config[56]['id']?>">
		  			<input type="hidden" name="<?php echo $config[56]['variable']?>" id="<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['variable']?>">
		  			<?php echo $config[56]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[56]['variable']?>" value="<?php echo $config[56]['value']?>" disabled>
		  	</div>
		  	
			<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[57]['id']?>" id="id-<?php echo $config[57]['id']?>" value="<?php echo $config[57]['id']?>">
		  			<input type="hidden" name="<?php echo $config[57]['variable']?>" id="<?php echo $config[57]['variable']?>" value="<?php echo $config[57]['variable']?>">
		  			<?php echo $config[57]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[57]['variable']?>" value="<?php echo $config[57]['value']?>" disabled>
		  	</div>

		  	<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[58]['id']?>" id="id-<?php echo $config[58]['id']?>" value="<?php echo $config[58]['id']?>">
		  			<input type="hidden" name="<?php echo $config[58]['variable']?>" id="<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['variable']?>">
		  			<?php echo $config[58]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[58]['variable']?>" value="<?php echo $config[58]['value']?>" disabled>
		  	</div>
		  	
		  	<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[59]['id']?>" id="id-<?php echo $config[59]['id']?>" value="<?php echo $config[59]['id']?>">
		  			<input type="hidden" name="<?php echo $config[59]['variable']?>" id="<?php echo $config[59]['variable']?>" value="<?php echo $config[59]['variable']?>">
		  			<?php echo $config[59]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[59]['variable']?>" value="<?php echo $config[59]['value']?>" disabled>
		  	</div>


		  	<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[60]['id']?>" id="id-<?php echo $config[60]['id']?>" value="<?php echo $config[60]['id']?>">
		  			<input type="hidden" name="<?php echo $config[60]['variable']?>" id="<?php echo $config[60]['variable']?>" value="<?php echo $config[60]['variable']?>">
		  			<?php echo $config[60]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="text" name="number" id="value-<?php echo $config[60]['variable']?>" value="<?php echo $symbol?>" readonly disabled>

		  	</div>
	  	
		  	<div class="rows-2">
		  		<div class="label-item width250 font-normal">
		  			<input type="hidden" name="id-<?php echo $config[61]['id']?>" id="id-<?php echo $config[61]['id']?>" value="<?php echo $config[61]['id']?>">
		  			<input type="hidden" name="<?php echo $config[61]['variable']?>" id="<?php echo $config[61]['variable']?>" value="<?php echo $config[61]['variable']?>">
		  			<?php echo $config[61]['desc']?>
		  		</div>
		  		<input class="input-90 width161" type="hidden" name="number" id="value-<?php echo $config[61]['variable']?>" value="<?php echo $rate?>" readonly disabled>
		  		<div class="special width161" id="fix-rate"><?php echo $rate?></div>
		  	</div>





		  	<!--  Hidden -->
	  	</div>

		<div class="form-input">
			
		</div>	

		<div class="cmd-command">
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.add_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>
			<div style="display:none">
			<button class="std-btn bkgr-green" id="btn-add-opco" onclick="add_opco_config()"><?php echo $this->lang->line('add')?></button>
			</div>
			<?php } ?>
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.update_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>
			<button class="std-btn bkgr-blue" id="btn-modify-opco" onclick="modify_opco()"><?php echo $this->lang->line('modify')?></button>
			<?php } ?>
			<button class="std-btn bkgr-green" id="btn-save-opco" onclick="save_value()" disabled><?php echo $this->lang->line('save')?></button>
			<button class="std-btn bkgr-red" id="btn-cancel-opco" onclick="pop('disable-background','cancel-process')" disabled><?php echo $this->lang->line('cancel')?></button>
		</div>
	</div>
</div>
<div class="ontop" id='disable-background'></div>
<div class="ontop" id='disable-background2'></div>

<div class="add-access-module" id="status-config">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('OpCo no configuration, configuration values set to default')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','status-config')"><?php echo $this->lang->line('ok')?></button>
	</div>

</div>

<div class="add-modify-ui-detail" id="add-opco-config">
	<div class="modify-title">
		<?php echo $this->lang->line('Add OpCo Configuration')?>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('OpCo')?></div>
		<div class="label-item"><?php echo $select_opco2 ?></div>
	</div>
	<div class="rows">
		<div class="label-item width98"><?php echo $this->lang->line('Period')?></div>
		<div class="label-item"><?php echo $select_month2 ?></div>
		<div class="label-item"><?php echo $select_year2 ?></div>
	</div>
	<div class="rows center10">
		<button class="std-btn bkgr-green" onclick="new_open()"><?php echo $this->lang->line('add')?></button>
		<button class="std-btn bkgr-red" onclick="hide('disable-background','add-opco-config')"><?php echo $this->lang->line('cancel')?></button>
	</div>
</div>

<div class="add-access-module" id="config-found">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration in selected already exists, the process is canceled')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background2','config-found')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="save-success">
	<div class="modify-title"><?php echo $this->lang->line('Message')?></div>
	<div class="confirm-message"><?php echo $this->lang->line('Configuration has been saved')?></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onclick="hide('disable-background','save-success')"><?php echo $this->lang->line('ok')?></button>
	</div>
</div>

<div class="add-access-module" id="result-group-error">
	<div class="modify-title">
		<?php echo $this->lang->line('Error Message'); ?>
	</div>
	<div class="confirm-message" id="detail-group-error"></div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-green" onClick="hide('disable-background','result-group-error');"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div class="del-access-module" id="cancel-process">
	<div class="modify-title">
		<?php echo $this->lang->line('confirm'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('Are you sure to cancel the operation?'); ?>
	</div>
	<div class="confirm-btn">
		<button class="std-btn bkgr-greed" onClick="cancel_op()"><?php echo $this->lang->line('yes'); ?></button>
		<button class="std-btn bkgr-red" onClick="hide('disable-background','cancel-process')"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<div class="del-access-module" id="modify-restrict">
	<div class="modify-title">
		<?php echo $this->lang->line('Notify'); ?>
	</div>
	<div class="confirm-message">
		<?php echo $this->lang->line('cant_modify'); ?>
	</div>
	<div class="confirm-btn">
		
		<button class="std-btn bkgr-red" onClick="hide('disable-background','modify-restrict')"><?php echo $this->lang->line('ok'); ?></button>
	</div>
</div>

<div id="testout"></div>
<script type="text/javascript">
	var mode = 'view';
	var selected_opco='<?php echo $opco_id?>';
	var selected_month = '<?php echo $month ?>';
	var selected_year = '<?php echo $year?>';
	var new_value = [];

	var general_rate = <?php echo $rate?>;

	<?php for($i=0;$i<count($config);$i++){ ?>
		//echo $i; 


	$("#value-<?php echo $config[$i]['variable']?>").number(true,0); // 
	
	<?php } ?>	

	<?php for($i=12;$i<=55;$i+=2){ ?>
		//echo $i; 


		$("#value-<?php echo $config[$i+1]['variable']?>").focusout(function(){
			var rate = Number($("#value-<?php echo $config[$i+1]['variable']?>").val());
			$("#value-<?php echo $config[$i]['variable']?>").val(rate*general_rate);
		});
	
	<?php } ?>



	$("#value-<?php echo $config[62]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[62]['variable']?>").val());
		$("#value-<?php echo $config[10]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[63]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[63]['variable']?>").val());
		$("#value-<?php echo $config[11]['variable']?>").val(rate*general_rate);
	});

	$("#value-<?php echo $config[65]['variable']?>").focusout(function(){
		var rate = Number($("#value-<?php echo $config[65]['variable']?>").val());
		$("#value-<?php echo $config[64]['variable']?>").val(rate*general_rate);
	});

	// ** tagonchange

	$(document).ready(function(){
		
		$("#opco-list").collapse("show");
		data_config = <?php echo $message?>;
		if(data_config==1){
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.add_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>

			document.getElementById('btn-modify-opco').disabled=true;
			//pop('disable-background','status-config');
			<?php } ?>


		}else{
			<?php
			$sql = 'SELECT
						map_group_opco.group_id,
						map_group_opco.opco_id,
						map_group_opco.update_opco_config
					FROM
						map_group_opco
					WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
			$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
			if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
			?>

			document.getElementById('btn-modify-opco').disabled=false;

			<?php } ?>
		}

		if("<?php echo $mode?>"=="add"){
			add_opco_config();
		}else{
			document.getElementById('opco-name-view').innerHTML='<?php echo $opco_name ?>';
			document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
			document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		}
	});

	function reload_page(){
		window.location.href = '<?php echo base_url()?>index.php/incident/opco_config/'+$("#id-opco").val()+'/'+$("#month").val()+'/'+$("#year").val();
	}

	function pop(div,div2) {
		document.getElementById(div).style.display = 'block';
		document.getElementById(div2).style.display = 'block';

	}
	function hide(div,div2) {
		document.getElementById(div).style.display = 'none';
		document.getElementById(div2).style.display = 'none';
	}



	function add_opco_config(){
		
		//$("#new-id-opco").val($("#id-opco").val());
		//$("#new-month").val($("#month").val());
		//$("#new-year").val($("#year").val());

		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());

		pop('disable-background','add-opco-config');
	}

	function get_add_all_month(group_id,opco_id){
		$.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/get_add_all_month",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: group_id,
                opco_id: opco_id,
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
            	//alert(data);
                if(data.trim()=='1'){
                	document.getElementById("new-month").disabled = false;
                	document.getElementById("new-year").disabled = false;
                }else{
                	$("#new-month").val('<?php echo $current_month?>');
                	$("#new-year").val('<?php echo $current_year?>');
                	document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
					document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
                	document.getElementById("new-month").disabled = true;
                	document.getElementById("new-year").disabled = true;

                }
            }
        });
	}

	function Get_Rate(year,month,opcoid){
	    $.ajax({
	        type: "POST",  
	        url: "<?php echo base_url()?>index.php/incident/get_rate2",  
	        contentType: 'application/x-www-form-urlencoded',
	        data: { 
	            year: year,
	            month: month,
	            opcoid:opcoid,
	            sess: "<?php echo session_id()?>"
	        },
	        dataType: "text",
	        beforeSend: function(){

	        },
	        complete: function(){
	            
	        },
	        success: function(data){
	            //alert(data);
	            var obj = JSON.parse(data);
	            general_rate = obj[0];
	            document.getElementById('fix-rate').innerHTML=obj[0];
	            
	            //alert(obj[0]);


	            //alert(data);
	            
	        }
	    });
	}

	function new_open(){
		
		// Check existing data
		$.ajax({
			type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/check_opco_config",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
            	opcoid: $("#new-id-opco").val(),
            	month: $("#new-month").val(),
            	year: $("#new-year").val(),
            	sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
            	
            },
            success: function(data){
            	//alert(data);
            	switch(data.trim()){
            		case '0':
            			// disable select opco,mounth,year
            			hide('disable-background','add-opco-config');
            			//document.getElementById('btn-get-config').disabled=true;
            			//document.getElementById('id-opco').disabled=true
            			//document.getElementById('month').disabled=true
            			//document.getElementById('year').disabled=true;

            			$("#id-opco").val($("#new-id-opco").val());
            			$("#month").val($("#new-month").val());
            			$("#year").val($("#new-year").val());

            			Get_Rate($("#new-year").val(),$("#new-month").val(),$("#new-id-opco").val());

            			<?php
						$sql = 'SELECT
									map_group_opco.group_id,
									map_group_opco.opco_id,
									map_group_opco.add_opco_config
								FROM
									map_group_opco
								WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
						$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
						if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
						?>
            			document.getElementById('btn-add-opco').disabled=true;
            			<?php } ?>

            			<?php
						$sql = 'SELECT
									map_group_opco.group_id,
									map_group_opco.opco_id,
									map_group_opco.update_opco_config
								FROM
									map_group_opco
								WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
						$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
						if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
						?>

            			document.getElementById('btn-modify-opco').disabled=true;
            			<?php } ?>
            			document.getElementById('btn-save-opco').disabled=false;
            			document.getElementById('btn-cancel-opco').disabled=false;

            			<?php 
            				$i=0;
            				foreach ($config as $new_config) {
            					
            					$comment='';
            			?>
		            			<?php echo $comment ?>document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;
		            			<?php echo $comment ?>$("#value-<?php echo $new_config['variable']?>").val("");
            			<?php
            					
            				$i++;
            				}
            			?>
            			mode = 'add';
            			break;
            		case '1':
            			hide('disable-background','add-opco-config');
            			pop('disable-background2','config-found');
            			break;
            		case '2':
            			
            			break;
            	}
            }
		});
	}


	function save_value(){
		//alert(mode);
		if(mode=='add'){
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			var item = { 
				opcoid:$("#new-id-opco").val(),
				config_date: $("#new-year").val()+'-'+$("#new-month").val()+"-01 00:00:00",
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			console.log(JSON.stringify(item));
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);
			console.log(json_newdata);
			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/incident/add_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
	            	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";
	            },
	            success: function(data){
	            	
	            	switch(data.trim()){
	            		case '0':
		            		//document.getElementById('btn-get-config').disabled=false;
	            			//document.getElementById('id-opco').disabled=false
	            			//document.getElementById('month').disabled=false
	            			//document.getElementById('year').disabled=false;
	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.add_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>
	            			document.getElementById('btn-add-opco').disabled=false;
	            			<?php }?>
	            			
							<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.update_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>

	            			document.getElementById('btn-modify-opco').disabled=false;
	            			<?php } ?>
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');
	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

			
		}else{
			new_value = [];
			<?php
			foreach ($config as $new_config) {
			?>
			var key = $("#var-year").val()+"-"+$("#var-month").val()+"-01 00:00:00";
			//alert(key);
			var item = { 
				//opcoid:$("#id-opco").val(),
				opcoid: $("#var-opco-id").val(),
				config_date: key,
				variable:"<?php echo $new_config['variable']?>",
				value:$("#value-<?php echo $new_config['variable']?>").val()
			};
			new_value.push(item);
			<?php	
			 } 
			?>
			
			json_newdata = JSON.stringify(new_value);
			//document.getElementById("testout").innerHTML=json_newdata;
			//alert(json_newdata);
			console.log(json_newdata);
			
			$.ajax({
				type: "POST",  
	            url: "<?php echo base_url()?>index.php/incident/update_opco_config",  
	            contentType: 'application/x-www-form-urlencoded',
	            data: { 
	            	json: json_newdata,
	            	sess: "<?php echo session_id()?>"
	            },
	            dataType: "text",
	            beforeSend: function(){
	            	document.getElementById('disable-background2').style.display = 'block';	
	            	document.getElementById("disable-background2").style.cursor = "wait";
	            },
	            complete: function(){
			    	document.getElementById('disable-background2').style.display = 'none';
			    	document.getElementById("disable-background2").style.cursor = "default";       	
	            },
	            success: function(data){
	            	//alert(data);
	            	//console.log(data);
	            	switch(data.trim()){
	            		case '0':

		            		//document.getElementById('btn-get-config').disabled=false;
	            			//document.getElementById('id-opco').disabled=false
	            			//document.getElementById('month').disabled=false
	            			//document.getElementById('year').disabled=false;
	            			//alert("hello");
	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.add_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>
	            			document.getElementById('btn-add-opco').disabled=false;
	            			<?php }?>

	            			<?php
							$sql = 'SELECT
										map_group_opco.group_id,
										map_group_opco.opco_id,
										map_group_opco.update_opco_config
									FROM
										map_group_opco
									WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
							$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
							if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
							?>

	            			document.getElementById('btn-modify-opco').disabled=false;
	            			<?php } ?>
	            			document.getElementById('btn-save-opco').disabled=true;
	            			document.getElementById('btn-cancel-opco').disabled=true;
	            			<?php 
            					foreach ($config as $new_config) {
	            			?>
	            			document.getElementById("value-<?php echo $new_config['variable']?>").disabled=true;
	            			
	            			<?php
	            				}
	            			?>
	            			mode = 'view';
	            			pop('disable-background','save-success');

	            			break;
	            		case '1':
	            			pop('disable-background2','config-found');
	            			break;
	            		case '2':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('Invalid command')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            		case '5':
	            			document.getElementById("detail-group-error").innerHTML = "<?php echo $this->lang->line('negative_value')?>";
	            			pop('disable-background','result-group-error');
	            			break;
	            	}
	            }
			});

		
		}
	}

	function go_modify(){

		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.add_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.add_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>
		document.getElementById('btn-add-opco').disabled=true;
		<?php }?>

		<?php
		$sql = 'SELECT
					map_group_opco.group_id,
					map_group_opco.opco_id,
					map_group_opco.update_opco_config
				FROM
					map_group_opco
				WHERE map_group_opco.group_id=? AND map_group_opco.opco_id=? AND map_group_opco.update_opco_config=1';
		$dtx = $this->db->query($sql,array($this->session->userdata('group_id'),$opco_id));
		if($dtx->num_rows()>0 || $this->session->userdata('group_id')==ADMIN_GROUP_ID){
		?>

		document.getElementById('btn-modify-opco').disabled=true;
		<?php } ?>
		document.getElementById('btn-save-opco').disabled=false;
		document.getElementById('btn-cancel-opco').disabled=false;

		<?php 
			foreach ($config as $new_config) {
		?>
		document.getElementById("value-<?php echo $new_config['variable']?>").disabled=false;

		<?php
			}
		?>
		mode = 'edit';	

	}

	function modify_opco(){
		//alert('modify');
		//document.getElementById('btn-get-config').disabled=true;
		//document.getElementById('id-opco').disabled=true
		//document.getElementById('month').disabled=true
		//document.getElementById('year').disabled=true;

		// check modify Current Month
		//alert($("#var-year").val()+' '+$("#var-month").val())
		$.ajax({
            type: "POST",  
            url: "<?php echo base_url()?>index.php/incident/update_all_month",  
            contentType: 'application/x-www-form-urlencoded',
            data: { 
                group_id: <?php echo $this->session->userdata("group_id")?>,
                opco_id: $("#var-opco-id").val(),
                month: $("#var-month").val(),
                year: $("#var-year").val(),
                sess: "<?php echo session_id()?>"
            },
            dataType: "text",
            beforeSend: function(){

            },
            complete: function(){
                
            },
            success: function(data){
            	//alert(data);
                if(data.trim()==1){
                	go_modify();
                } else{
                	pop('disable-background','modify-restrict');
                }
            }
        });

			
	}

	function cancel_op(){
		window.location.href="<?php echo base_url()?>index.php/incident/opco_config/"+$("#var-opco-id").val()+"/"+$("#var-month").val()+"/"+$("#var-year").val();
	}

	$("#new-id-opco").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-opco-id").val($("#new-id-opco").val());
		//alert($("#var-opco-id").val());

		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})

	$("#new-month").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-month").val($("#new-month").val());
		//alert($("#var-month").val());
		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})

	$("#new-year").change(function(){
		get_add_all_month(<?php echo $this->session->userdata('group_id')?>,$("#new-id-opco").val());
		$("#var-year").val($("#new-year").val());
		//alert($("#var-year").val());
		document.getElementById('opco-name-view').innerHTML=$("#new-id-opco option:selected").text();
		document.getElementById('month-view').innerHTML=$("#new-month option:selected").text();
		document.getElementById('year-view').innerHTML=$("#new-year option:selected").text();
		
	})



</script>