<style type="text/css">

.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: 1px solid #cccccc;
    background: none;
    font-weight: normal; 
    color: #333; 
}

.ui-widget-content {
    border: 1px solid #dddddd;
    background: white;
    color: #333333;
}


b {
    font-weight: 700;
    float: right;
    margin-top: 7px;
}

.btn {
	min-width: 200px;
	width: auto;

}

.rowcol {
	display: inline-block;
	margin-bottom: 5px;
}

.params-label {
	display: inline-block;
	padding-left: 5px;
	width: 150px;

}

.params {
	padding-right: 5px;
	width: 100px;
}

.params-label {
	padding-right: 5px;
	width: 100px;

}

.btn-preview {
    height: 25px;
    border-radius: 5px;
    border-style: none;
    outline: none;
    vertical-align: top;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 5px;
    background-color: #4a55dc;
    color: white;
}

.ui-widget-header {
    border: 1px solid #4A4C4E;
    background-color: #4A4C4E;
    background: #4A4C4E;
    color: #ffffff;
    font-weight: bold;
}

.ui-state-default .ui-icon {

}

.breadcrumb{
	height: 32px;
	padding-top: 2px;
}

.print-report {
	overflow: auto;
	width: calc(100% - 40px);
	position: relative;
	height: 70%;
	/* zoom: 75%; */
	/* -moz-transform: scale(1); */
	margin-top: 10px;
	margin-left: 20px;
	margin-right: 20px;
	margin-bottom: 0px;
	padding: 20px;
	border-style: solid;
	border-radius: 5px;
	border-width: 1px;
	border-color: #F5F5F5;
	/* resize: both; */
}

.std-select {
	display: inline-block;
}

.msg-export {
	z-index: 3000;
	width: 450px;
	display: none;
	position: fixed;
	top: 30%;
	left: 50%;
	margin-top: -76px;
	margin-left: -163px;
	display: none;
	background-color: white;
	padding-top: 7px;
	padding-bottom: 10px;
}

.w150 {
	width: 150px;
}

.w50 {
	width: 50px !important;
}



</style>
<script type="text/javascript">
	var noreload = 1;	
</script>

<div class="parameters-area" id="parameters-area">

<?php
	
	if($parameters['status'] != 0){
		echo '<div class="param-label">'.$parameters['msg'].'</div>';
	}else{
		$no_params=1;
		$count_params = count($parameters['params']);
		foreach ($parameters['params'] as $params) {
			# code...
			echo '<div class="rowcol">';
			echo '	<div class="params-label">'.$params['Name'].'</div>';
			$multiselect = '';
			if($params['MultiValue']==1) $multiselect = 'multiple="multiple"'; 
			else $multiselect='';
			//foreach ($report['parameters'] as $std_param) {
				# code...
			//	if(strtoupper($params['Name'])==strtoupper($std_param['name'])) {
			//		if($std_param['multiselect']==0) $multiselect = ''; else $multiselect='multiple="multiple"';
			//	}
			//}

			//disini
			echo "<!--" .$params['Type'].  "-->";
			if($params['Type']=='DateTime'){
				echo '<div class="rc2">';
				echo '<input type="text" id="params_'.$no_params.'"name="'.$params['Name'].'" class="std-input  dis w108" value="'.$params['DefaultValues'][0]->Value.'" readonly="true" >';
				echo '</div>';
						echo '<script type="text/javascript">';
                            echo '$("#params_'.$no_params.'").change(function(){';
                                echo 'document.cookie="params_'.$no_params.'="+$("#params_'.$no_params.'").val();';
                            echo '});';
                            echo '$(function() {';
							    echo '$( "#params_'.$no_params.'" ).datepicker({ ';
						          echo 'dateFormat: "mm/dd/yy 00:00:00",';
						          echo 'showOn: "button",';
						          // echo 'maxDate: "+0m +0w",';
						          echo 'buttonText: "<i class=\'fa fa-calendar\' aria-hidden=\'true\'></i>" });';
							      echo '';
							  echo '});';
                        echo '</script>';
			}else{		
				echo '	<select id="params_'.$no_params.'" name="'.$params['Name'].'" class="params" '.$multiselect.' >';
						foreach ($params['ValidValues'] as $value) {
						# code...
						$selected = false;
						if($params['MultiValue']==1){


							foreach($params['DefaultValues'] as $DefaultValues){
								
								if(!$report_url){
									if(count($DefaultValues->Value)>1){
										foreach($DefaultValues->Value as $DV){
											if($DV==$value->Value) $selected=true; 
										}
									}else{
										if($DefaultValues->Value==$value->Value) $selected=true;
									}
									
								}else{
									// here from url
									if(count($params_url['parameters'])>0){
										foreach ($params_url['parameters'] as $pu) {
											# code...
											if($pu['name']==$params['Name']){
												// bla bla bla
												foreach ($pu['value'] as $mv) {
													# code...
													if($value->Value==$mv) $selected=true;
												}
											}
										}
									}else{
										foreach($DefaultValues As $DV){
											if($DV==$value->Value) $selected=true; 
										}
									}
									
								}


							}
							if($selected) $txt_selected ="selected"; else $txt_selected='';

						}else{
							if(!$report_url){
								if($params['DefaultValues'][0]->Value==$value->Value) $txt_selected ="selected"; else $txt_selected='';
							}else {
								// here from url
								//print_r($params_url['parameters']);
								if(count($params_url['parameters'])>0){
									foreach ($params_url['parameters'] as $pu) {
										# code...

										if($pu['name']==$params['Name']){
											//print_r($pu);
											//echo $params['DefaultValues'][0]->Value.'<br>';

											if($value->Value==$pu['value'][0]) $txt_selected ="selected"; else $txt_selected='';
										}
									}
								}else{
									if($params['DefaultValues'][0]->Value==$value->Value) $txt_selected ="selected"; else $txt_selected='';
								}
							}
							

						}
						echo '	<option value="'.$value->Value.'" '.$txt_selected.'>'.$value->Label.'</option>';
					}
					echo '	</select>';

					
					if($multiselect=='multiple="multiple"'){
						echo '<script type="text/javascript">';
						echo '	var noreload_'.$no_params.'= 1 ;';
						echo '	$(function(){';
						echo '	  	$("#params_'.$no_params.'").multiselect({';
						echo '			checkAllText: "'.$this->lang->line('Check all').'",';
						echo '			uncheckAllText: "'.$this->lang->line('Uncheck All').'",';
						echo '			noneSelectedText: "'.$this->lang->line('Select an Option').'",';
						echo '			selectedText: "# '.$this->lang->line('Selected').'",';
						//echo '			close: linkaction(noreload_'.$no_params.'),';
						echo '			close: function(){linkaction(noreload_'.$no_params.'); noreload_'.$no_params.'=0; },';
						echo '			minWidth: 150});'; 
						echo '	});';
						echo '	noreload_'.$no_params.'= 0 ;';
						echo '</script>';
					}else{
						echo '<script type="text/javascript">';
						echo '	var noreload_'.$no_params.'= 1 ;';
						echo '	$(function(){';
						echo '	  	$("#params_'.$no_params.'").multiselect({';
						echo '			multiple: false,';
						echo '			header: "'.$this->lang->line('Select an Option').'",';
						echo '			noneSelectedText: "'.$this->lang->line('Select an Option').'",';
		   				echo '			selectedList: 1,';
		   				//echo '			close: linkaction(noreload_'.$no_params.'),';
		   				echo '			close: function(){linkaction(noreload_'.$no_params.');noreload_'.$no_params.'=0;},';
		   				echo '			minWidth: 150 });'; 
						echo '	});';
						echo '	noreload_'.$no_params.'= 0 ;';
						echo '</script>';
					}

					// echo '<script type="text/javascript">';
					// 	echo 'var a'.$no_params.'=true;';
					// 	echo 'function linkaction'.$no_params.'(){';
					// 	echo 'if(a'.$no_params.') linkaction();';
					// 	echo '}';
					// 	echo 'a'.$no_params.'=false;';
					// 	echo '</script>';
			}
			//echo '	<select id="params_'.$no_params.'" name="'.$params['Name'].'" class="params" '.$multiselect.'>';
			

			if($count_params==$no_params){ ?>

				<button id="preview" class="btn-preview" onclick="preview($('#page-select').val())"><?php echo $this->lang->line('View')?></button>
			<?php
			}
			echo '</div>';
			// js here
			/*
			if($multiselect=='multiple'){
				echo '<script type="text/javascript">';
				echo '    $(document).ready(function() {';
				//echo '    		$("#params_'.$no_params.'").multiselect({';
				//echo '				includeSelectAllOption: true,';
				//echo '				selectAllText: "Select All!",';
				//echo '				maxHeight: 200';
				//echo '			});';
				echo '			$("#params_'.$no_params.'").multiselect();';
				echo '    });';
				echo '</script>';

			}else{
				echo '<script type="text/javascript">';
				echo '    $(document).ready(function() {';
				echo '    		$("#params_'.$no_params.'").multiselect({';
					echo '				maxHeight: 200';
				echo '			});';
				echo '    });';
				echo '</script>';
			}
			*/
			
			//belum beres
			// echo '<script type="text/javascript">';
			// if($params['MultiValue']==1){
			// 	echo '$("#params_'.$no_params.'").change(function(){';
			// 	    echo 'linkaction();';
			// 	echo '});';

			// 	//echo '$("#selectedItemLists").live(\'focusout\',function(){';
			// 		//echo 'console.log("ao");';
			// 	//echo '});';
			// }else{
			// 	echo '$("#params_'.$no_params.'").change(function(){';
			// 	    echo 'linkaction();';
			// 	echo '});';
			// }
			// echo '</script>';

			$no_params++;
		}

	}


?>
</div>

<div class="breadcrumb" style="margin-bottom: 0px;" id='export'>
	<div class="params-label">
		<?php echo $this->lang->line('Export To')?>
	</div>
	<select class="std-select" id="export-to">
		<option value="PDF">PDF</option>
		<option value="EXCEL">Excel</option>
	</select>
	<div class="params-label">
		<button id="btn-export" class="btn-preview" onclick="export_dialog()"><?php echo $this->lang->line('Export')?></button>
	</div>
	<div class="params-label" style="padding-left: 20px;">
		<?php echo $this->lang->line('Page')?>
	</div>

	<div class="params-label" style="padding-left: 20px;" id="pagination">
		
	</div>

</div>

<div class="add-report" id="modify-export">
	<div class="modify-title">
		<?php echo $this->lang->line('Download Report'); ?>
	</div>
	<div class="report-form" >
		<div class="rows">
			<div class="c-label"><?php echo $this->lang->line('filename')?></div>
			<div class="c-input" ><input class="std-input" style="width:300px;" id="m-name"></div>
		</div>
	</div>
	<!--
	<div class="report-form" id="pdf-setting">
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Page Width')?></div>
			<div class="c-input" ><input class="std-input w50" type="number" id="m-pw"> <?php echo $this->lang->line('inches')?></div>
		</div>
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Page Height')?></div>
			<div class="c-input" ><input class="std-input w50"  type="number" id="m-ph"> <?php echo $this->lang->line('inches')?></div> 
		</div>
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Margin Top')?></div>
			<div class="c-input w150" ><input class="std-input w50"  type="number" id="m-mt"> <?php echo $this->lang->line('inches')?></div>
		</div>
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Margin Left')?></div>
			<div class="c-input w150" ><input class="std-input w50"  type="number" id="m-ml"> <?php echo $this->lang->line('inches')?></div>
		</div>
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Margin Right')?></div>
			<div class="c-input w150" ><input class="std-input w50"  type="number" id="m-mr"> <?php echo $this->lang->line('inches')?></div>
		</div>
		<div class="rows">
			<div class="c-label w150"><?php echo $this->lang->line('Margin Bottom')?></div>
			<div class="c-input" ><input class="std-input w50"  type="number" id="m-mb"> <?php echo $this->lang->line('inches')?></div>
		</div>
	</div>
	-->
		
	<div class="confirm-btn">
		<button class="std-btn bkgr-green w100" onClick="export_report($('#m-name').val())"><?php echo $this->lang->line('Export'); ?></button>
		<button class="std-btn bkgr-red w100" onClick="hide('disable-background','modify-export');"><?php echo $this->lang->line('cancel'); ?></button>
	</div>
</div>

<form id="formjson" action="<?php echo base_url()?>index.php/incident/report/<?php echo $id?>/?reportpath=<?php echo $report['report']?>&rs:Command=Render&cmd:Norender=1" method="post" style=>
	<input id="thejson" type="hidden" name="json2" value="">
</form>


<div class="print-report" id="print-report">
	<?php
		if($Norender==1){
			echo $this->session->userdata('html_result');
		} 
	?>
</div>


<div id="params"></div>
<div class="ontop2" id="disable-background2"></div>

<div class="ontop" id="disable-background"></div>

<script type="text/javascript">

function getAsUriParameters(data) {
   var url = '';
   //console.log(JSON.parse(data));
   //for (var prop in JSON.parse(data)) {
   	value = JSON.parse(data);
   	y = "";
   	for (var i = 0; i < value.length; i++) {
        //console.log(value[i].Name);
        //console.log(value[i].Value);
        y= y+value[i].Name+"="+value[i].Value+"&";
    }
    //console.log(y);
   return y;
}


function linkaction(links){
	// var selectedValues = [{Name:'reportpath',Value:'<?php echo $report["report"]?>'},{Name:"rs:Command",Value:"Render"},{Name:"cmd:Norender",Value:"1"}];    
	//document.getElementById('disable-background2').style.display='block';
	var selectedValues = [];
    <?php
    	for($i=1;$i<=count($parameters['params']);$i++){
    		echo "var params='';";
    		echo "var attr =$('#params_".$i."').attr('multiple');";
    		echo "if (typeof attr !== typeof undefined && attr !== false) {";
    		echo "	p = $('#params_".$i."').val();";
    		echo "	if(p!=null){";
    		echo "		p.forEach(function(item){";
    		echo "			params={Name: encodeURI($('#params_".$i."').attr('name')),Value:encodeURI(item.replace('&','#####'))};";
    		echo "			selectedValues.push(params);";
    		echo "		});";
			echo "	}";
    		echo "}else {";
    		echo "	params={Name: encodeURI($('#params_".$i."').attr('name')),Value:encodeURI($('#params_".$i."').val())};";
    		echo "	selectedValues.push(params);";
    		echo "}";
    	}
    ?>

	j = JSON.stringify(selectedValues);
	//JSON.parse(j)
	console.log(j);
	url = getAsUriParameters(j);
	//console.log(url);
	//open("<?php echo base_url()?>index.php/incident/report/<?php echo $id?>/?reportpath=<?php echo $report['report']?>&rs:Command=Render&cmd:Norender=1&"+url,"_self");
	document.getElementById('thejson').value = "reportpath=<?php echo $report['report']?>&rs:Command=Render&cmd:Norender=1&"+url;
	//document.getElementById('thejson').value = j;
	console.log(links);
	if(links != 1)	{ 
		document.getElementById("formjson").submit();
	}
	//console.log("<?php echo base_url()?>index.php/incident/report/<?php echo $id?>/?reportpath=<?php echo $report['report']?>&rs:Command=Render&cmd:Norender=0&"+url);
}

function pop(div,div2) {
	document.getElementById(div).style.display = 'block';
	document.getElementById(div2).style.display = 'block';
}

function hide(div,div2) {
	document.getElementById(div).style.display = 'none';
	document.getElementById(div2).style.display = 'none';
}

function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}	

function preview(page=1){
    var selectedValues = [];    
    
    document.getElementById('disable-background2').style.display='block';
    <?php
    	for($i=1;$i<=count($parameters['params']);$i++){
    		echo "var params='';";
    		echo "var attr =$('#params_".$i."').attr('multiple');";
    		echo "if (typeof attr !== typeof undefined && attr !== false) {";
    		echo "	p = $('#params_".$i."').val();";
    		echo "	if(p!=null){";
    		echo "		p.forEach(function(item){";
    		echo "			params={Name: $('#params_".$i."').attr('name'),Value:item};";
    		echo "			selectedValues.push(params);";
    		echo "		});";
			echo "	}";
    		echo "}else {";
    		echo "	params={Name: $('#params_".$i."').attr('name'),Value:$('#params_".$i."').val()};";
    		echo "	selectedValues.push(params);";
    		echo "}";
    	}
    ?>
	j = JSON.stringify(selectedValues);
	//document.getElementById('params').innerHTML = j;
	document.cookie='p='+j;
	//console.log(j);
	// alert(data);
	$.ajax({
		type: "POST",  
		url: "<?php echo base_url()?>index.php/incident/get_report/<?php echo $id?>/",  
		contentType: 'application/x-www-form-urlencoded',
		data: { 
				report: "<?php echo $report['report']?>",
				parameters: j,
				page: page,
		    	sess: "<?php echo session_id()?>"
			},
		dataType: "text",
		beforeSend: function(){

		},
		complete: function(){
		    
		},
		success: function(data){


			//alert(data);
			console.log(data);
			document.getElementById('disable-background2').style.display='none';
			obj = jQuery.parseJSON(data);

			if(obj.status==0){
				document.getElementById("print-report").innerHTML = obj.html_result;	
				document.getElementById("pagination").innerHTML = obj.pageselect;
				$("#page-select").multiselect({
					multiple: false,
					selectedList: 1,
					header:null,
					minWidth: 70,
					
				});
			}else{
				document.getElementById("print-report").innerHTML = obj.msg;
			}

		    
		}
	});

	
		
	    

 
    return false;
}

function export_dialog(){
	var  timestamp = Number(new Date())
	export_name = $("#select-report :selected").text();

	export_name = export_name.replace(/ /g, "_")+'_'+timestamp;
	$("#m-name").val(export_name);
	pop('disable-background','modify-export');

}

function export_report(filename){
    var selectedValues = [];    
  
    document.getElementById('disable-background2').style.display='block';
    <?php
    	for($i=1;$i<=count($parameters['params']);$i++){
    		echo "var params='';";
    		echo "var attr =$('#params_".$i."').attr('multiple');";
    		echo "if (typeof attr !== typeof undefined && attr !== false) {";
    		echo "	p = $('#params_".$i."').val();";
    		echo "	if(p!=null){";
    		echo "		p.forEach(function(item){";
    		echo "			params={Name: $('#params_".$i."').attr('name'),Value:item};";
    		echo "			selectedValues.push(params);";
    		echo "		});";
			echo "	}";
    		echo "}else {";
    		echo "	params={Name: $('#params_".$i."').attr('name'),Value:$('#params_".$i."').val()};";
    		echo "	selectedValues.push(params);";
    		echo "}";
    	}
    ?>
	j = JSON.stringify(selectedValues);
	//document.getElementById('params').innerHTML = j;
		
	    $.ajax({
	          type: "POST",  
	          url: "<?php echo base_url()?>index.php/incident/export_report",  
	          contentType: 'application/x-www-form-urlencoded',
	          data: { 
	          		report: "<?php echo $report['report']?>",
	          		export_to: $("#export-to").val(),
	          		filename: filename,
	                parameters: j,
	                sess: "<?php echo session_id()?>"
	          },
	          dataType: "text",
	          beforeSend: function(){

	          },
	          complete: function(){
	                
	          },
	          success: function(data){
	          	document.getElementById('disable-background2').style.display='none';
	          	obj = jQuery.parseJSON(data);
	          	//alert(data);
	          	if(obj.status==0){
	          		hide('disable-background','modify-export');
	          		window.location.href='<?php echo base_url()?>index.php/incident/download_delete?file='+obj.filepath;
	          		//alert(obj.filepath);	
	          	}else{
	          		document.getElementById("print-report").innerHTML = obj.msg;
	          	}
	            
	                
	          }
	    });


 
    return false;
}

$(document).ready(function(){
	//console.log("<?php echo $Norender?>");
	if(<?php echo $Norender?>==0){
		preview();	

	}else{
		
	}
	
	
});
</script>

<?php
	function safe_b64encode($string) {
    
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    function encode($value){ 
        
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, KEY, $text, MCRYPT_MODE_ECB, $iv);
        return trim(safe_b64encode($crypttext)); 
    }

    function decode($value){
        
        if(!$value){return false;}
        $crypttext = safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, KEY, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
?>