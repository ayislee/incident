<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<title><?php echo $this->lang->line('Report')?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" >
  	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
		  
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-3.3.2.min.css">

		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/desktop.css">
		<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap-3.3.2.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery-ui.css" />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.multiselect.css" />
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.multiselect.js"></script>
		
		
	</head>

	<body>
		<input id="multi" onblur="namafungsi()"/>
		<select multiple="multiple" id="my-select" name="my-select[]" id="multie" onblur="namafungsi()">
	      <option value='elem_1'>elem 1</option>
	      <option value='elem_2'>elem 2</option>
	      <option value='elem_3'>elem 3</option>
	      <option value='elem_4'>elem 4</option>
	      <option value='elem_100'>elem 100</option>
	    </select>
	</body>

	<script type="text/javascript">

		function namafungsi(){
			console.log("cumaaku");
		}

		$(function(){
			$("#my-select").multiselect({
				close: function(event, ui){
        			// event handler here
        			alert("ks");
    				},
				minWidth: 150
			});
		});


	</script>
</html>